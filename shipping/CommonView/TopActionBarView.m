//
//  TopActionBarView.m
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "TopActionBarView.h"

@implementation TopActionBarView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init {
    self = [super init];
    if(self) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] firstObject];
    }
    return self;
}

- (void)animationView:(BOOL)isShow {
    [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        if(isShow == YES) {
            [_delegate showSearchView];
        } else {
            [_delegate hideSearchView];
        }
    } completion:nil];
}

#pragma mark - Action Event
- (IBAction)pressedMenu:(UIButton *)sender {
    [_delegate showMenuViewController];
}

- (IBAction)pressedBack:(UIButton *)sender {
    [_delegate closeViewController];
}

- (IBAction)pressedPromotion:(UIButton *)sender {
    [_delegate pressedPromotion];
}

- (IBAction)pressedSearch:(UIButton *)sender {
    sender.selected = !sender.isSelected;
    [self animationView:sender.isSelected];
}

- (IBAction)pressedAdd:(UIButton *)sender {
    [_delegate pressedAdd];
}

- (IBAction)pressedMap:(UIButton *)sender {
    [_delegate pressedMap];
}

- (IBAction)pressedEditMap:(UIButton *)sender {
    [_delegate pressedEditMap];
}

- (IBAction)pressedHistory:(UIButton *)sender {
    [_delegate pressedHistory];
}

@end
