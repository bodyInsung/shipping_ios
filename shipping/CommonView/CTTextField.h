//
//  CTTextField.h
//  shipping
//
//  Created by ios on 2018. 3. 30..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kDatePickerTypeStart,
    kDatePickerTypeEnd
} DatePickerType;

typedef enum {
    kDatePickerDisableTypeNone,
    kDatePickerDisableTypeMinimum,
    kDatePickerDisableTypeMaximum
} DatePickerDisableType;

@protocol CTTextFieldDelegate
@optional
- (void)pressedDone:(id)sender;
- (void)pressedDone:(id)sender datePickerType:(DatePickerType)type;
@end

@interface CTTextField : UITextField {
    id                  senderTarget;
}

@property (assign) DatePickerType pickerType;
@property (weak) id<CTTextFieldDelegate> accessoryDelegate;
@property (strong, nonatomic) UIPickerView *contentPV;
@property (strong, nonatomic) UIDatePicker *contentDP;

- (void)setPickerView:(id)target;
- (void)setDatePicker:(DatePickerDisableType)type;
- (void)showPickerView:(id)target;
- (void)showDatePicker:(DatePickerDisableType)type;
- (void)setAccessoryView;
@end


