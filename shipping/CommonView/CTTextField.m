//
//  CTTextField.m
//  shipping
//
//  Created by ios on 2018. 3. 30..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "CTTextField.h"

@implementation CTTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

/**
 키보드를 피커뷰로 대체

 @param target 피커뷰
 */
- (void)setPickerView:(id)target {
    if(_contentPV != nil) {
        _contentPV = nil;
    }
    _contentPV = [[UIPickerView alloc] init];
    _contentPV.delegate = target;
    _contentPV.backgroundColor = [UIColor whiteColor];
    senderTarget = _contentPV;
    [self setAccessoryView:YES];
    self.inputView = _contentPV;
}

/**
 키보드를 데이트 피커뷰로 대체

 @param type 데이트 피커뷰의 날짜 비활성 타입
 */
- (void)setDatePicker:(DatePickerDisableType)type {
    if(_contentDP != nil) {
        _contentDP = nil;
    }
    _contentDP = [[UIDatePicker alloc] init];
    _contentDP.backgroundColor = [UIColor whiteColor];
    _contentDP.datePickerMode = UIDatePickerModeDate;
    if(type == kDatePickerDisableTypeMinimum) {
        _contentDP.minimumDate = [NSDate date];
    } else if(type == kDatePickerDisableTypeMaximum) {
        _contentDP.maximumDate = [NSDate date];
    }
    senderTarget = _contentDP;
    [self setAccessoryView:YES];
    self.inputView = _contentDP;
}

/**
 피커뷰 설정 후 포커싱

 @param target 보여지는 타겟
 */
- (void)showPickerView:(id)target {
    [self setPickerView:target];
    [self becomeFirstResponder];
}

/**
 데이트 피커뷰 설정 후 포커싱
 
 @param type 데이트 피커뷰의 날짜 비활성 타입
 */
- (void)showDatePicker:(DatePickerDisableType)type {
    [self setDatePicker:type];
    [self becomeFirstResponder];
}

/**
 악세서리 뷰 설정 (default : 취소버튼 없음)
 */
- (void)setAccessoryView {
    [self setAccessoryView:NO];
}

/**
 악세서리 뷰 설정

 @param isCancel 취소 버튼 유무
 */
- (void)setAccessoryView:(BOOL)isCancel {
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[APP_DELEGATE window] bounds].size.width, 44)];
    self.inputAccessoryView = toolbar;
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                           target:self
                                                                           action:nil];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"완료"
                                                                style:UIBarButtonItemStylePlain
                                                               target:self
                                                               action:@selector(pressedDone:)];
    if(isCancel) {
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"취소"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(pressedCancel:)];
        [toolbar setItems:@[space, cancelBtn, doneBtn]];
    } else {
        [toolbar setItems:@[space, doneBtn]];
    }
}

#pragma mark - Action Event
- (void)pressedCancel:(UIBarButtonItem *)sender {
    [CommonUtil hideKeyboard];
}

- (void)pressedDone:(UIBarButtonItem *)sender {
    [CommonUtil hideKeyboard];
    id delegate = _accessoryDelegate;
    if([delegate respondsToSelector:@selector(pressedDone:datePickerType:)]) {
        [delegate pressedDone:senderTarget datePickerType:_pickerType];
    } else if([delegate respondsToSelector:@selector(pressedDone:)]) {
        [delegate pressedDone:senderTarget];
    }
}

@end
