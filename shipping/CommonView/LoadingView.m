//
//  LoadingView.m
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+ (instancetype)sharedView {
    static LoadingView *sharedView = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedView = [[LoadingView alloc] init];

    });
    return sharedView;
}

- (instancetype)init {
    self = [super init];
    if(self) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] firstObject];
        [self initLayout];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    LoadingView *loadingV = [LoadingView sharedView];
    loadingV.frame = [[APP_DELEGATE window] bounds];
}

- (void)initLayout {
    _imageV.animationImages = [CommonObject LoadingImageArray];
    _imageV.animationRepeatCount = 0;
    _imageV.animationDuration = 1;
}

/**
 로딩뷰 보여짐
 */
+ (void)showLoadingView {
    LoadingView *loadingV = [LoadingView sharedView];
    if(![loadingV.imageV isAnimating]) {
        [loadingV.imageV startAnimating];
        [[APP_DELEGATE window] addSubview:loadingV];
    }
}

/**
 로딩뷰 숨김
 */
+ (void)hideLoadingView {
    LoadingView *loadingV = [LoadingView sharedView];
    if([loadingV.imageV isAnimating]) {
        [loadingV.imageV stopAnimating];
        [loadingV removeFromSuperview];
    }
}

/**
 화면이 그려지는 시간을 주기 위한 딜레이
 */
+ (void)hideLoadingViewToDelay {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self hideLoadingView];
    });
}

@end
