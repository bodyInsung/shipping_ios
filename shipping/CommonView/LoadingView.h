//
//  LoadingView.h
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView
@property (strong, nonatomic) IBOutlet UIImageView *imageV;

+ (instancetype)sharedView;
+ (void)showLoadingView;
+ (void)hideLoadingView;
+ (void)hideLoadingViewToDelay;
@end
