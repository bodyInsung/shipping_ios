//
//  CTLabel.h
//  shipping
//
//  Created by ios on 2018. 3. 30..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CTLabelDelegate
- (void)pressedLink:(NSString *)string;
@end

@interface CTLabel : UILabel

@property (weak) id<CTLabelDelegate> delegate;
@property (strong, nonatomic) UIButton *linkButton;

@end
