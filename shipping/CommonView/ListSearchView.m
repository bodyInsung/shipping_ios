//
//  ListSearchView.m
//  shipping
//
//  Created by ios on 2018. 3. 21..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ListSearchView.h"

@implementation ListSearchView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init {
    self = [super init];
    if(self) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] firstObject];
        [self initLayout];
    }
    return self;
}

- (void)initLayout {
    [_baseDateV applyRoundBorder:4.0 color:UIColorFromRGB(0xB5B5B5) width:0.4];
    [_locationV applyRoundBorder:4.0 color:UIColorFromRGB(0xB5B5B5) width:0.4];
    [_startDateV applyRoundBorder:4.0 color:UIColorFromRGB(0xB5B5B5) width:0.4];
    [_endDateV applyRoundBorder:4.0 color:UIColorFromRGB(0xB5B5B5) width:0.4];
    [_searchBtn applyRoundBorder:4.0];
    
    [_addressTF setAccessoryView];
    [_nameTF setAccessoryView];
    _addressTF.delegate = self;
    _nameTF.delegate = self;
    
    [self setDatePicker];
}

/**
 데이터와 날짜에 따른 초기 값 설정 (스케쥴에서 진입 시 날짜로 검색)
 
 @param datas 데이터
 @param type 검색 타입
 @param date 검색 날짜
 */
- (void)initValueFromData:(NSArray<CodeResultData> *)datas type:(SearchType)type date:(NSDate *)date {
    locationData = datas;
    searchType = type;
    
    if(searchType == kSearchTypeShipping) {
        baseDateArray = [CommonObject SearchShippingBaseDateTypeArray];
    } else if(searchType == kSearchTypeCompletion) {
        baseDateArray = [CommonObject SearchCompletionBaseDateTypeArray];
    } else if(searchType == kSearchTypeReturn) {
        baseDateArray = [CommonObject SearchReturnBaseDateTypeArray];
    }
    
    _dateHeightLC.active = searchType == kSearchTypeShipping;
    
    if(date != nil) {
        startDate = date;
        endDate = date;
    } else {
        NSDate *todayDate = [NSDate date];
        startDate = [CommonUtil preOneMonthDateFromDate:todayDate];
        endDate = todayDate;
    }
    
    [self restoreLocationArray];
    
    [self changeBaseDateTitle:0];
    [self changeLocationTitle:0];
    [_startDateBtn setTitle:[CommonUtil stringFromDate:startDate format:DATE_KOREAN_MONTH_DAY] forState:UIControlStateNormal];
    [_endDateBtn setTitle:[CommonUtil stringFromDate:endDate format:DATE_KOREAN_MONTH_DAY] forState:UIControlStateNormal];
}

/**
 이름 값만 배열 재정의 (index 0에 '전체' 추가)
 */
- (void)restoreLocationArray {
    locationArray = [NSMutableArray array];
    for(CodeResultData *data in locationData) {
        [locationArray addObject:data.detCdNm];
    }
    // 첫번째 인덱스에 "전체" 추가
    [locationArray insertObject:kTitleLocationTotal atIndex:0];
}

/**
 기준일 버튼 타이틀 변경

 @param index 인덱스
 */
- (void)changeBaseDateTitle:(NSInteger)index {
    baseDateTitle = baseDateArray[index];
    [_baseDateBtn setTitle:baseDateTitle forState:UIControlStateNormal];
}

/**
 지역 버튼 타이틀 변경

 @param index 인덱스
 */
- (void)changeLocationTitle:(NSInteger)index {
    locationTitle = locationArray[index];
    [_locationBtn setTitle:locationTitle forState:UIControlStateNormal];
}

/**
 검색에 정의 되어있는 파라미터 전달

 @return 정의 되어있는 파라미터
 */
- (NSMutableDictionary *)sendParameters {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([CommonObject baseDateTypeFromTitle:baseDateTitle]) forKey:@"s_datetype"];
    NSInteger locationIndex = [locationArray indexOfObject:locationTitle];
    locationIndex = locationIndex == NSNotFound ? 0 : locationIndex;
    if(locationIndex > 0) {
        CodeResultData *data = locationData[locationIndex-1];
        [parameters setValue:data.detCd forKey:@"s_area"];
    }
    
    if(searchType != kSearchTypeShipping) { // 배송 리스트는 날짜전송 안함
        [parameters setValue:[CommonUtil stringFromDate:startDate format:DATE_DOT_YEAR_MONTH_DAY] forKey:@"dateStart"];
        [parameters setValue:[CommonUtil stringFromDate:endDate format:DATE_DOT_YEAR_MONTH_DAY] forKey:@"dateEnd"];
    }
    [parameters setValue:_addressTF.text forKey:@"s_addr"];
    [parameters setValue:_nameTF.text forKey:@"s_custname"];
    [parameters setValue:@"M" forKey:@"s_producttype"];
    return parameters;
}

/**
 데이터피커 설정
 */
- (void)setDatePicker {
    if(dateTF != nil) {
        [dateTF removeFromSuperview];
        dateTF = nil;
    }
    
    dateTF = [[CTTextField alloc] init];
    [self addSubview:dateTF];
    dateTF.accessoryDelegate = self;
    [dateTF setDatePicker:kDatePickerDisableTypeMaximum];
}

#pragma mark - Action Event
- (IBAction)pressedBaseDate:(UIButton *)sender {
    [CommonUtil showActionSheet:baseDateArray target:_target selected:^(NSInteger index) {
        [self changeBaseDateTitle:index];
    }];
}

- (IBAction)pressedLocation:(UIButton *)sender {
    [CommonUtil showActionSheet:locationArray target:_target selected:^(NSInteger index) {
        [self changeLocationTitle:index];
    }];
}

- (IBAction)pressedStartDate:(UIButton *)sender {
    dateTF.pickerType = kDatePickerTypeStart;
    [dateTF.contentDP setDate:startDate];
    [dateTF becomeFirstResponder];
}

- (IBAction)pressedEndDate:(UIButton *)sender {
    dateTF.pickerType = kDatePickerTypeEnd;
    [dateTF.contentDP setDate:endDate];
    [dateTF becomeFirstResponder];
}

- (IBAction)pressedSearch:(UIButton *)sender {
    [_delegate pressedSearch];
}

#pragma mark - CTTextFieldDelegate
- (void)pressedDone:(id)sender datePickerType:(DatePickerType)type {
    NSString *title = [CommonUtil stringFromDate:dateTF.contentDP.date format:DATE_KOREAN_MONTH_DAY];
    if(type == kDatePickerTypeStart) {
        startDate = dateTF.contentDP.date;
        [_startDateBtn setTitle:title forState:UIControlStateNormal];
    } else if(type == kDatePickerTypeEnd) {
        endDate = dateTF.contentDP.date;
        [_endDateBtn setTitle:title forState:UIControlStateNormal];
    }

    [dateTF resignFirstResponder];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
@end
