//
//  CTTextView.h
//  shipping
//
//  Created by ios on 2018. 3. 30..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CTTextViewDelegate
- (void)pressedDone:(id)sender;
@end

@interface CTTextView : UITextView

@property (weak) id<CTTextViewDelegate> accessoryDelegate;

- (void)setAccessoryView;
@end
