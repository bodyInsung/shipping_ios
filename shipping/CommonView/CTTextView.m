//
//  CTTextView.m
//  shipping
//
//  Created by ios on 2018. 3. 30..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "CTTextView.h"

@implementation CTTextView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

/**
 악세서리 뷰 설정
 */
- (void)setAccessoryView {
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[APP_DELEGATE window] bounds].size.width, 44)];
    self.inputAccessoryView = toolbar;
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                           target:self
                                                                           action:nil];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"완료"
                                                                style:UIBarButtonItemStylePlain
                                                               target:self
                                                               action:@selector(pressedDone:)];
    [toolbar setItems:@[space, doneBtn]];
}

#pragma mark - Action Event
- (void)pressedCancel:(UIBarButtonItem *)sender {
    [CommonUtil hideKeyboard];
}

- (void)pressedDone:(UIBarButtonItem *)sender {
    [CommonUtil hideKeyboard];
    [_accessoryDelegate pressedDone:self];
}

@end
