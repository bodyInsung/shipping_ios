//
//  ListSearchView.h
//  shipping
//
//  Created by ios on 2018. 3. 21..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kSearchTypeNone,            // 타입없음
    kSearchTypeShipping,        // 배송리스트 타입
    kSearchTypeCompletion,      // 완료리스트 타입
    kSearchTypeReturn,          // 회수리스트 타입
    kSearchTypeSchedule         // 스케쥴 타입
} SearchType;

@protocol ListSearchDelegate
- (void)pressedSearch;
@end

@interface ListSearchView : UIView<UITextFieldDelegate, CTTextFieldDelegate> {
    
    NSArray<CodeResultData>             *locationData;
    NSMutableArray                      *locationArray;
    NSArray                             *baseDateArray;
    NSDate                              *startDate;
    NSDate                              *endDate;
    NSString                            *baseDateTitle;
    NSString                            *locationTitle;
    
    CTTextField                         *dateTF;
    SearchType                          searchType;
}

@property (weak) id<ListSearchDelegate> delegate;
@property (weak) UIViewController *target;
@property (weak, nonatomic) IBOutlet UIView *baseDateV;
@property (weak, nonatomic) IBOutlet UIButton *baseDateBtn;
@property (weak, nonatomic) IBOutlet UIView *locationV;
@property (weak, nonatomic) IBOutlet UIButton *locationBtn;
@property (weak, nonatomic) IBOutlet UIView *startDateV;
@property (weak, nonatomic) IBOutlet UIButton *startDateBtn;
@property (weak, nonatomic) IBOutlet UIView *endDateV;
@property (weak, nonatomic) IBOutlet UIButton *endDateBtn;
@property (weak, nonatomic) IBOutlet CTTextField *addressTF;
@property (weak, nonatomic) IBOutlet CTTextField *nameTF;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *dateHeightLC;

- (void)initValueFromData:(NSArray<CodeResultData> *)datas type:(SearchType)type date:(NSDate *)date;
- (NSMutableDictionary *)sendParameters;
@end
