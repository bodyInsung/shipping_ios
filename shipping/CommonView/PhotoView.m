//
//  PhotoView.m
//  shipping
//
//  Created by ios on 2018. 4. 2..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "PhotoView.h"

@implementation PhotoView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init {
    self = [super init];
    if(self) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] firstObject];
        [self initLayout];
    }
    return self;
}

- (void)initLayout {
    [_backV applyRoundBorder:4.0 color:UIColorFromRGB(0xB5B5B5) width:0.4];
    _plusBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self setPhotoMode:YES];
}

/**
 모드에 따라 히든처리 (mode : 편집모드, 확인모드)

 @param isMode 편집모드 여부
 */
- (void)setPhotoMode:(BOOL)isMode {
    _plusBtn.hidden = !isMode;
    _photoIV.hidden = isMode;
}

#pragma mark - Action Event
- (IBAction)pressedPlus:(UIButton *)sender {
    [_delegate pressedPhotoView:self];
}

@end
