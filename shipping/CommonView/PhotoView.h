//
//  PhotoView.h
//  shipping
//
//  Created by ios on 2018. 4. 2..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PhotoView;
@protocol PhotoViewDelegate
- (void)pressedPhotoView:(PhotoView *)sender;
@end

@interface PhotoView : UIView

@property (weak) id<PhotoViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *backV;
@property (weak, nonatomic) IBOutlet UIImageView *photoIV;
@property (weak, nonatomic) IBOutlet UIButton *plusBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLb;

- (void)setPhotoMode:(BOOL)isMode;
@end

