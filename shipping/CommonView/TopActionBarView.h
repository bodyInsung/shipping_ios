//
//  TopActionBarView.h
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ActionBarDelegate
@optional
- (void)showMenuViewController;
- (void)showSearchView;
- (void)hideSearchView;
- (void)closeViewController;
- (void)pressedPromotion;
- (void)pressedAdd;
- (void)pressedMap;
- (void)pressedEditMap;
- (void)pressedHistory;
@end

@interface TopActionBarView : UIView

@property (weak) id<ActionBarDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet UIImageView *logoIV;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *promotionBtn;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIButton *mapBtn;
@property (weak, nonatomic) IBOutlet UIButton *editMapBtn;
@property (weak, nonatomic) IBOutlet UIButton *historyBtn;
@end

