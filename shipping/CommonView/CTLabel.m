//
//  CTLabel.m
//  shipping
//
//  Created by ios on 2018. 3. 30..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "CTLabel.h"

@implementation CTLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _linkButton.frame = self.bounds;
}

- (void)initLayout {
    self.userInteractionEnabled = YES;
    
    _linkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _linkButton.backgroundColor = [UIColor clearColor];
    [_linkButton addTarget:self action:@selector(pressedLink:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_linkButton];
}

#pragma mark - Action Event
- (void)pressedLink:(UIButton *)sender {
    if(![NSString isEmptyString:self.attributedText.string]) {
        [_delegate pressedLink:self.attributedText.string];
    } else if(![NSString isEmptyString:self.text]){
        [_delegate pressedLink:self.text];
    }
}

@end
