//
//  ResultModel.h
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@interface ResultModel : JSONModel

@property (nonatomic) NSString<Optional> *resultCode;
@property (nonatomic) NSString<Optional> *resultMsg;
@end
