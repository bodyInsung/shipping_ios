//
//  ShippingListModel.h
//  shipping
//
//  Created by ios on 2018. 3. 22..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"
#import "ShippingResultData.h"

@interface ShippingListModel : JSONModel

@property (nonatomic) NSMutableArray<ShippingResultData> *list;
@end
