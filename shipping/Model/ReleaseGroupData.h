//
//  ReleaseGroupData.h
//  shipping
//
//  Created by insung on 17/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@interface ReleaseGroupData : JSONModel

@property (nonatomic) NSString *itemGroupNm;
@property (nonatomic) NSString *itemGroupCd;
@end
