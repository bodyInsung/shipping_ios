//
//  ProductValidationData.m
//  shipping
//
//  Created by insung on 19/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "ProductValidationData.h"

@implementation ProductValidationData

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"procStatNm": @"PROC_STAT_NM",
                                                                  @"issueNo": @"ISSUE_NO",
                                                                  @"giftYn": @"GIFT_YN",
                                                                  @"itemNm": @"ITEM_NM",
                                                                  @"serialCd": @"SERIAL_CD",
                                                                  @"itemCd": @"ITEM_CD",
                                                                  @"movTypeNm": @"MOV_TYPE_NM",
                                                                  @"slCd": @"SL_CD",
                                                                  @"slNm": @"SL_NM",
                                                                  @"lotFlag": @"LOT_FLG",
                                                                  @"badAmount": @"BAD_AMOUNT",
                                                                  @"goodAmount": @"GOOD_AMOUNT",
                                                                  @"resultMsg": @"resultMsg",
                                                                  @"tempLot": @"TEMP_LOT"}];
}
@end
