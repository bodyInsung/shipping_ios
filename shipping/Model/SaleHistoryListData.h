//
//  SaleHistoryListData.h
//  shipping
//
//  Created by insung on 26/02/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "JSONModel.h"
#import "SaleHistoryData.h"

@interface SaleHistoryListData : JSONModel
@property (nonatomic) NSNumber *listCount;
@property (nonatomic) NSArray<SaleHistoryData> *list;
@end
