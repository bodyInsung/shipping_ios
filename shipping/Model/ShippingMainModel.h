//
//  ShippingMainModel.h
//  shipping
//
//  Created by ios on 2018. 3. 21..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"
#import "ProductData.h"
#import "MonthCountData.h"
#import "TotalCountData.h"

@interface ShippingMainModel : JSONModel

@property (nonatomic) NSArray<Optional, ProductData> *compList;
@property (nonatomic) MonthCountData<Optional> *monthCnt;
@property (nonatomic) NSArray<Optional, ProductData> *remainList;
@property (nonatomic) TotalCountData<Optional> *todayCnt;
@end
