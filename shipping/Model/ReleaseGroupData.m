//
//  ReleaseGroupData.m
//  shipping
//
//  Created by insung on 17/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "ReleaseGroupData.h"

@implementation ReleaseGroupData

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"itemGroupNm": @"ITEM_GROUP_NM",
                                                                  @"itemGroupCd": @"ITEM_GROUP_CD"}];
}
@end
