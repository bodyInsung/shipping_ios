//
//  CodeResultData.m
//  shipping
//
//  Created by ios on 2018. 3. 26..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "CodeResultData.h"

@implementation CodeResultData

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"commCd": @"COMM_CD",
                                                                  @"commCdNm": @"COMM_CD_NM",
                                                                  @"desc": @"DESCRIPTION",
                                                                  @"detCd": @"DET_CD",
                                                                  @"detCdNm": @"DET_CD_NM",
                                                                  @"groupCd": @"GROUP_CD",
                                                                  @"insDt": @"INS_DT",
                                                                  @"insId": @"INS_ID",
                                                                  @"ref1": @"REF_1",
                                                                  @"ref2": @"REF_2",
                                                                  @"ref3": @"REF_3",
                                                                  @"sortSeq": @"SORT_SEQ",
                                                                  @"updDt": @"UPD_DT",
                                                                  @"updId": @"UPD_ID",
                                                                  @"useYn": @"USE_YN"}];
}
@end
