//
//  AppVersionData.h
//  shipping
//
//  Created by ios on 2018. 4. 30..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@protocol AppVersionData;
@interface AppVersionData : JSONModel

@property (nonatomic) NSString<Optional> *updateType;
@property (nonatomic) NSString<Optional> *msg;
@property (nonatomic) NSString<Optional> *version;
@end
