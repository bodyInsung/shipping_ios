//
//  ReleaseWarehouseData.h
//  shipping
//
//  Created by insung on 17/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@interface ReleaseWarehouseData : JSONModel

@property (nonatomic) NSString *plantCd;
@property (nonatomic) NSString *slCd;
@property (nonatomic) NSString *slNm;
@end
