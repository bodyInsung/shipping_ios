//
//  CarResultData.h
//  shipping
//
//  Created by ios on 2018. 4. 2..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@interface CarResultData : JSONModel

@property (nonatomic) NSString *adminInfo;
@property (nonatomic) NSString *carGroup;
@property (nonatomic) NSString *carNumber;
@property (nonatomic) NSString *carSeq;
@end
