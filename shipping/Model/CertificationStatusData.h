//
//  CertificationStatusData.h
//  shipping
//
//  Created by insung on 2018. 7. 16..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@protocol CertificationStatusData;
@interface CertificationStatusData : JSONModel
@property (nonatomic) NSString<Optional> *code;
@property (nonatomic) NSString<Optional> *message;
@end
