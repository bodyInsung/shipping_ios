//
//  ProductData.m
//  shipping
//
//  Created by ios on 2018. 3. 21..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ProductData.h"

@implementation ProductData

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"productName": @"PRODUCT_NAME",
                                                                  @"promiseTime": @"PROMISE_TIME"}];
}
@end
