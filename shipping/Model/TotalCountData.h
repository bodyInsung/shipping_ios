//
//  TotalCountData.h
//  shipping
//
//  Created by ios on 2018. 3. 21..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@interface TotalCountData : JSONModel

@property (nonatomic) NSString *completedCnt;
@property (nonatomic) NSString *remainCnt;
@end
