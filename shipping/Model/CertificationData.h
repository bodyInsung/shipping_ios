//
//  CertificationData.h
//  shipping
//
//  Created by insung on 2018. 7. 16..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"
#import "CertificationCodeData.h"
#import "CertificationStatusData.h"

@interface CertificationData : JSONModel

@property (nonatomic) CertificationCodeData<Optional> *data;
@property (nonatomic) CertificationStatusData<Optional> *status;
@end
