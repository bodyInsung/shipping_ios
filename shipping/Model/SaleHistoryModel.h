//
//  SaleHistoryModel.h
//  shipping
//
//  Created by insung on 26/02/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "JSONModel.h"
#import "SaleHistoryListData.h"

@interface SaleHistoryModel : JSONModel
@property (nonatomic) NSString<Optional> *resultCode;
@property (nonatomic) SaleHistoryListData *resultData;
@end
