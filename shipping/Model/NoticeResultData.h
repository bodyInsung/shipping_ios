//
//  NoticeResultData.h
//  shipping
//
//  Created by ios on 2018. 4. 13..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@interface NoticeResultData : JSONModel

@property (nonatomic) NSString *noticeTitle;
@property (nonatomic) NSString *pictureOne;
@property (nonatomic) NSString *rgId;
@property (nonatomic) NSString *noticeContents;
@property (nonatomic) NSString *noticeSeq;
@property (nonatomic) NSString *noticeNew;
@property (nonatomic) NSString *rgNm;
@property (nonatomic) NSString *rgDt;
@property (nonatomic) NSString<Optional> *noticeScopeTarget;
@end
