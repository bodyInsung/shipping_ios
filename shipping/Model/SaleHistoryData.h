//
//  SaleHistoryData.h
//  shipping
//
//  Created by insung on 26/02/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@protocol SaleHistoryData;
@interface SaleHistoryData : JSONModel

@property (nonatomic) NSString<Optional> *inUserName;
@property (nonatomic) NSString<Optional> *inTel;
@property (nonatomic) NSString<Optional> *salesType;
@property (nonatomic) NSString<Optional> *productName;
@property (nonatomic) NSString<Optional> *inHandphone;
@property (nonatomic) NSString<Optional> *managerName;
@property (nonatomic) NSString<Optional> *nType;
@property (nonatomic) NSInteger salesSeq;
@property (nonatomic) NSString<Optional> *regDate;
@property (nonatomic) NSString<Optional> *etcClausal;
@property (nonatomic) NSString<Optional> *productType;
@property (nonatomic) NSString<Optional> *salesConfirm;
@property (nonatomic) NSString<Optional> *receivePs;
@property (nonatomic) NSString<Optional> *productTypeNm;
@property (nonatomic) NSString<Optional> *inUserNo;
@end
