//
//  SalaryResultData.m
//  shipping
//
//  Created by ios on 2018. 5. 8..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "SalaryResultData.h"

@implementation SalaryResultData

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"deliveryman": @"DELIVERYMAN",
                                                                  @"grandTotal": @"GRAND_TOTAL",
                                                                  @"telNo": @"TEL_NO",
                                                                  @"year": @"YEAR",
                                                                  @"departmentNm": @"DEPARTMENT_NM",
                                                                  @"deliverymanNm": @"DELIVERYMAN_NM",
                                                                  @"email": @"EMAIL",
                                                                  @"month": @"MONTH",
                                                                  @"dailyCount": @"DAILY_COUNT",
                                                                  @"inDate": @"IN_DATE",
                                                                  @"shippingType": @"SHIPPING_TYPE",
                                                                  @"total": @"TOTAL",
                                                                  @"type": @"TYPE"}];
}
@end
