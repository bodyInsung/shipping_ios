//
//  CertificationCodeData.h
//  shipping
//
//  Created by insung on 2018. 7. 16..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@protocol CertificationCodeData;
@interface CertificationCodeData : JSONModel
@property (nonatomic) NSString<Optional> *authCode;
@property (nonatomic) NSString<Optional> *msgid;
@end
