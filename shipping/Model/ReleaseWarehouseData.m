//
//  ReleaseWarehouseData.m
//  shipping
//
//  Created by insung on 17/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "ReleaseWarehouseData.h"

@implementation ReleaseWarehouseData

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"plantCd": @"PLANT_CD",
                                                                  @"slCd": @"SL_CD",
                                                                  @"slNm": @"SL_NM"}];
}
@end
