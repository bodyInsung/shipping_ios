//
//  ShippingResultData.h
//  shipping
//
//  Created by ios on 2018. 3. 22..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@protocol ShippingResultData;
@interface ShippingResultData : JSONModel

@property (nonatomic) NSString *area;
@property (nonatomic) NSString <Optional> *backSn;
@property (nonatomic) NSString <Optional> *companyCheck;
@property (nonatomic) NSString *custName;
@property (nonatomic) NSString <Optional> *custUpdate;
@property (nonatomic) NSString <Optional> *deliveryMan;
@property (nonatomic) NSString <Optional> *divDate;
@property (nonatomic) NSString *dueDate;
@property (nonatomic) NSString <Optional> *expireFlag;
@property (nonatomic) NSString <Optional> *freegiftOne;
@property (nonatomic) NSString <Optional> *freegiftTwo;
@property (nonatomic) NSString <Optional> *hasMsg;
@property (nonatomic) NSString <Optional> *hPhoneNo;
@property (nonatomic) NSString *insAddr;
@property (nonatomic) NSString <Optional> *instructionCheck;
@property (nonatomic) NSString <Optional> *insComplete;
@property (nonatomic) BOOL mustFlag;
@property (nonatomic) NSString *productName;
@property (nonatomic) NSString <Optional> *progressNo;
@property (nonatomic) NSString *promise;
@property (nonatomic) NSString <Optional> *promiseFailCd;
@property (nonatomic) NSString <Optional> *promiseFailPs;
@property (nonatomic) NSString <Optional> *promiseTime;
@property (nonatomic) NSString *purchasingOffice;
@property (nonatomic) NSString *qty;
@property (nonatomic) NSString <Optional> *relocationCheck;
@property (nonatomic) NSString <Optional> *serviceMonth;
@property (nonatomic) NSString *shippingPs;
@property (nonatomic) NSString *shippingSeq;
@property (nonatomic) NSString *shippingType;
@property (nonatomic) NSString <Optional> *telNo;
@property (nonatomic) NSString <Optional> *deliveryman1;
@property (nonatomic) NSString <Optional> *deliveryman2;
@property (nonatomic) NSString <Optional> *deliveryman3;
@property (nonatomic) NSString <Optional> *divStatus;
@property (nonatomic) NSString <Optional> *inDate;
@property (nonatomic) NSString <Optional> *phoneNo;
@property (nonatomic) NSString <Optional> *progressNoNm;
@property (nonatomic) NSString <Optional> *receiveDate;
@property (nonatomic) NSString <Optional> *symptom;
@property (nonatomic) NSString <Optional> *eval;
@property (nonatomic) NSString <Optional> *agreeName;
@property (nonatomic) NSString <Optional> *hPhoneNo2;
@property (nonatomic) NSString <Optional> *phoneNo2;
@property (nonatomic) NSString <Optional> *addr;
@property (nonatomic) NSString <Optional> *addr2;
@end
