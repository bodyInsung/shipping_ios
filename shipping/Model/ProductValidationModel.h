//
//  ProductValidationModel.h
//  shipping
//
//  Created by insung on 25/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "JSONModel.h"
#import "ProductValidationData.h"

@interface ProductValidationModel : JSONModel

@property (nonatomic) ProductValidationData<Optional> *resultData;
@property (nonatomic) NSString<Optional> *resultMsg;
@property (nonatomic) NSString<Optional> *resultCode;

@end
