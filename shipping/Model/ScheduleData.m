//
//  ScheduleData.m
//  shipping
//
//  Created by ios on 2018. 4. 27..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ScheduleData.h"

@implementation ScheduleData

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"adCnt": @"AD_CNT",
                                                                  @"p20Cnt": @"P20CNT",
                                                                  @"dueDate": @"DUE_DATE",
                                                                  @"p10Cnt": @"P10CNT",
                                                                  @"d10Cnt": @"D10CNT",
                                                                  @"total": @"TOTAL",
                                                                  @"d20Cnt": @"D20CNT",
                                                                  @"d30Cnt": @"D30CNT"}];
}
@end
