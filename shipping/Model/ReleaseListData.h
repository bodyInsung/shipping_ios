//
//  ReleaseListData.h
//  shipping
//
//  Created by insung on 18/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@interface ReleaseListData : JSONModel

@property (nonatomic) NSString<Optional> *makerLotNo;
@property (nonatomic) NSString<Optional> *fromSlNm;
@property (nonatomic) NSString<Optional> *giQty;
@property (nonatomic) NSString<Optional> *giftYn;
@property (nonatomic) NSString<Optional> *orderNo;
@property (nonatomic) NSString<Optional> *toSlCd;
@property (nonatomic) NSString<Optional> *itemCd;
@property (nonatomic) NSString<Optional> *serialCd;
@property (nonatomic) NSString<Optional> *plantCd;
@property (nonatomic) NSString<Optional> *procStatNm;
@property (nonatomic) NSString<Optional> *issueNo;
@property (nonatomic) NSString<Optional> *toSlNm;
@property (nonatomic) NSString<Optional> *fromSlCd;
@property (nonatomic) NSString<Optional> *reqDt;
@property (nonatomic) NSString<Optional> *itemNm;
@property (nonatomic) NSString<Optional> *contractNo;
@property (nonatomic) NSString<Optional> *movType;
@property (nonatomic) NSString<Optional> *movTypeNm;
@property (nonatomic) NSString<Optional> *erpFlag;
@property (nonatomic) NSString<Optional> *ifNo;
@end
