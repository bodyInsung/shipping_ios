//
//  UserResultData.m
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "UserResultData.h"

@implementation UserResultData

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"adminId": @"ADMIN_ID",
                                                                  @"adminNm": @"ADMIN_NM",
                                                                  @"carNumber": @"CAR_NUMBER",
                                                                  @"companyNm": @"COMPANY_NM",
                                                                  @"groupCd": @"GROUP_CD",
                                                                  @"iphoneUUID": @"IPHONE_UUID",
                                                                  @"perCd": @"PER_CD",
                                                                  @"pushRegid": @"PUSH_REGID",
                                                                  @"sId": @"SID",
                                                                  @"telNo": @"TEL_NO"}];
}
@end
