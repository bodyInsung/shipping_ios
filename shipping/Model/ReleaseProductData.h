//
//  ReleaseProductData.h
//  shipping
//
//  Created by ios on 2018. 4. 13..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@interface ReleaseProductData : JSONModel

@property (nonatomic) NSString *itemNm;
@property (nonatomic) NSString *itemCd;
@property (nonatomic) NSString<Optional> *qty;
@property (nonatomic) NSString<Optional> *rentalNm;
@property (nonatomic) NSString<Optional> *inputQty;
@property (nonatomic) NSString<Optional> *giftYn;
@property (nonatomic) NSString<Optional> *invYn;
@property (nonatomic) NSString<Optional> *lotFlag;
@end
