//
//  ProductResultData.h
//  shipping
//
//  Created by ios on 2018. 4. 2..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@protocol ProductResultData;
@interface ProductResultData : JSONModel

@property (nonatomic) NSString *productCode;
@property (nonatomic) NSString *productName;
@property (nonatomic) NSString<Optional> *productDetail;
@property (nonatomic) NSString<Optional> *productImageL;
@property (nonatomic) NSString<Optional> *productImageS;
@property (nonatomic) NSString<Optional> *productIntro;
@property (nonatomic) NSString<Optional> *productSpec;
@property (nonatomic) NSString<Optional> *productType;
@property (nonatomic) NSString<Optional> *imageOne;
@property (nonatomic) NSString<Optional> *imageTwo;
@property (nonatomic) NSString<Optional> *productCaution1;
@property (nonatomic) NSString<Optional> *productCaution1Mov;
@property (nonatomic) NSString<Optional> *productCaution2;
@property (nonatomic) NSString<Optional> *productCaution2Mov;
@property (nonatomic) NSString<Optional> *productCaution3;
@property (nonatomic) NSString<Optional> *productCaution3Mov;
@property (nonatomic) NSString<Optional> *productCaution4;
@property (nonatomic) NSString<Optional> *productCaution4Mov;
@property (nonatomic) NSString<Optional> *productCaution5;
@property (nonatomic) NSString<Optional> *productCaution5Mov;
@end
