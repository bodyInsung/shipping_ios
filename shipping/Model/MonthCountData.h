//
//  MonthCountData.h
//  shipping
//
//  Created by ios on 2018. 3. 21..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@interface MonthCountData : JSONModel

@property (nonatomic) NSString<Optional> *asOfMonth;
@property (nonatomic) NSString<Optional> *cancelOfMonth;
@property (nonatomic) NSString<Optional> *changeOfMonth;
@property (nonatomic) NSString<Optional> *deliveryOfMonth;
@property (nonatomic) NSString<Optional> *etcOfMonth;
@property (nonatomic) NSString<Optional> *faultyOfMonth;
@property (nonatomic) NSString<Optional> *moterizedBed;
@property (nonatomic) NSString<Optional> *moterizedBedEtc;
@property (nonatomic) NSString<Optional> *reinstallOfMonth;
@property (nonatomic) NSString<Optional> *relocationOfMonth0;
@property (nonatomic) NSString<Optional> *relocationOfMonth1;
@property (nonatomic) NSString<Optional> *royalFrame;
@property (nonatomic) NSString<Optional> *royalFrameEtc;

@end
