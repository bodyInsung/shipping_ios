//
//  TotalCountData.m
//  shipping
//
//  Created by ios on 2018. 3. 21..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "TotalCountData.h"

@implementation TotalCountData

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"completedCnt": @"COMPLETED_CNT",
                                                                  @"remainCnt": @"REMAIN_CNT"}];
}
@end
