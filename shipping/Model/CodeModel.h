//
//  CodeModel.h
//  shipping
//
//  Created by ios on 2018. 3. 26..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"
#import "CodeResultData.h"

@interface CodeModel : JSONModel

@property (nonatomic) NSString<Optional> *resultCode;
@property (nonatomic) NSMutableArray<CodeResultData, Optional> *resultData;
@property (nonatomic) NSString<Optional> *resultMsg;
@end
