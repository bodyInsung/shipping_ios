//
//  ScheduleData.h
//  shipping
//
//  Created by ios on 2018. 4. 27..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@protocol ScheduleData;
@interface ScheduleData : JSONModel

@property (nonatomic) NSString<Optional> *adCnt;
@property (nonatomic) NSString<Optional> *p20Cnt;
@property (nonatomic) NSString<Optional> *dueDate;
@property (nonatomic) NSString<Optional> *p10Cnt;
@property (nonatomic) NSString<Optional> *d10Cnt;
@property (nonatomic) NSString<Optional> *total;
@property (nonatomic) NSString<Optional> *d20Cnt;
@property (nonatomic) NSString<Optional> *d30Cnt;
@end
