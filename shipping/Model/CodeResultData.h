//
//  CodeResultData.h
//  shipping
//
//  Created by ios on 2018. 3. 26..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@protocol CodeResultData;
@interface CodeResultData : JSONModel

@property (nonatomic) NSString *commCd;
@property (nonatomic) NSString *commCdNm;
@property (nonatomic) NSString *desc;
@property (nonatomic) NSString *detCd;
@property (nonatomic) NSString *detCdNm;
@property (nonatomic) NSString *groupCd;
@property (nonatomic) NSString *insDt;
@property (nonatomic) NSString<Optional> *insId;
@property (nonatomic) NSString<Optional> *ref1;
@property (nonatomic) NSString<Optional> *ref2;
@property (nonatomic) NSString<Optional> *ref3;
@property (nonatomic) NSString *sortSeq;
@property (nonatomic) NSString<Optional> *updDt;
@property (nonatomic) NSString<Optional> *updId;
@property (nonatomic) NSString *useYn;

@end
