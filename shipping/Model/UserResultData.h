//
//  UserResultData.h
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@interface UserResultData : JSONModel
@property (nonatomic) NSString<Optional> *adminId;
@property (nonatomic) NSString<Optional> *adminNm;
@property (nonatomic) NSString<Optional> *carNumber;
@property (nonatomic) NSString<Optional> *companyNm;
@property (nonatomic) NSString<Optional> *groupCd;
@property (nonatomic) NSString<Optional> *iphoneUUID;
@property (nonatomic) NSString<Optional> *perCd;
@property (nonatomic) NSString<Optional> *pushRegid;
@property (nonatomic) NSString<Optional> *sId;
@property (nonatomic) NSString<Optional> *telNo;
@property (nonatomic) NSString<Optional> *loginDate;
@property (nonatomic) NSString<Optional> *resultCode;
@property (nonatomic) NSString<Optional> *resultMsg;
@property (nonatomic) NSString<Optional> *userPw;
@property (nonatomic) NSString<Optional> *autoLogin;
@property (nonatomic) NSString<Optional> *saveId;
@property (nonatomic) NSString<Optional> *certMsgId;
@end
