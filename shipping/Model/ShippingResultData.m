//
//  ShippingResultData.m
//  shipping
//
//  Created by ios on 2018. 3. 22..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ShippingResultData.h"

@implementation ShippingResultData

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"area": @"AREA",
                                                                  @"backSn": @"BACK_SN",
                                                                  @"companyCheck": @"COMPANY_CHECK",
                                                                  @"custName": @"CUST_NAME",
                                                                  @"custUpdate": @"CUST_UPDATE",
                                                                  @"deliveryMan": @"DELIVERYMAN",
                                                                  @"divDate": @"DIV_DATE",
                                                                  @"dueDate": @"DUE_DATE",
                                                                  @"expireFlag": @"EXPIRE_FLAG",
                                                                  @"freegiftOne": @"FREEGIFT_ONE",
                                                                  @"freegiftTwo": @"FREEGIFT_TWO",
                                                                  @"hasMsg": @"HAS_4_MSG",
                                                                  @"hPhoneNo": @"HPHONE_NO",
                                                                  @"insAddr": @"INSADDR",
                                                                  @"instructionCheck": @"INSTRUCTION_CHECK",
                                                                  @"insComplete": @"INS_COMPLETE",
                                                                  @"mustFlag": @"MUST_FLAG",
                                                                  @"productName": @"PRODUCT_NAME",
                                                                  @"progressNo": @"PROGRESS_NO",
                                                                  @"promise": @"PROMISE",
                                                                  @"promiseFailCd": @"PROMISE_FAIL_CD",
                                                                  @"promiseFailPs": @"PROMISE_FAIL_PS",
                                                                  @"promiseTime": @"PROMISE_TIME",
                                                                  @"purchasingOffice": @"PURCHASING_OFFICE",
                                                                  @"qty": @"QTY",
                                                                  @"relocationCheck": @"RELOCATION_CHECK",
                                                                  @"serviceMonth": @"SERVICE_MONTH",
                                                                  @"shippingPs": @"SHIPPING_PS",
                                                                  @"shippingSeq": @"SHIPPING_SEQ",
                                                                  @"shippingType": @"SHIPPING_TYPE",
                                                                  @"telNo": @"TEL_NO",
                                                                  @"deliveryMan1": @"DELIVERYMAN1",
                                                                  @"deliveryMan2": @"DELIVERYMAN2",
                                                                  @"deliveryMan3": @"DELIVERYMAN3",
                                                                  @"divStatus": @"DIV_STATUS",
                                                                  @"inDate": @"IN_DATE",
                                                                  @"phoneNo": @"PHONE_NO",
                                                                  @"progressNoNm": @"PROGRESS_NO_NM",
                                                                  @"receiveDate": @"RECEIVE_DATE",
                                                                  @"symptom": @"SYMPTOM",
                                                                  @"eval": @"EVAL",
                                                                  @"agreeName": @"AGREE_NAME",
                                                                  @"phoneNo2": @"PHONE_NO2",
                                                                  @"hPhoneNo2": @"HPHONE_NO2",
                                                                  @"addr": @"ADDR",
                                                                  @"addr2": @"ADDR2"}];
}
@end
