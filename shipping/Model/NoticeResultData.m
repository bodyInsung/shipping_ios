//
//  NoticeResultData.m
//  shipping
//
//  Created by ios on 2018. 4. 13..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "NoticeResultData.h"

@implementation NoticeResultData

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"noticeTitle": @"NOTICE_TITLE",
                                                                  @"pictureOne": @"PICTURE_ONE",
                                                                  @"rgId": @"RG_ID",
                                                                  @"noticeContents": @"NOTICE_CONTENTS",
                                                                  @"noticeSeq": @"NOTICE_SEQ",
                                                                  @"noticeNew": @"NOTICE_NEW",
                                                                  @"rgNm": @"RG_NM",
                                                                  @"rgDt": @"RG_DT",
                                                                  @"noticeScopeTarget": @"NOTICE_SCOPE_TARGET"}];
}
@end
