//
//  ProductData.h
//  shipping
//
//  Created by ios on 2018. 3. 21..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@protocol ProductData;
@interface ProductData : JSONModel

@property (nonatomic) NSString<Optional> *productName;
@property (nonatomic) NSString<Optional> *promiseTime;
@end
