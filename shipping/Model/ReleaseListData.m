//
//  ReleaseListData.m
//  shipping
//
//  Created by insung on 18/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "ReleaseListData.h"

@implementation ReleaseListData

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"makerLotNo": @"MAKER_LOT_NO",
                                                                  @"fromSlNm": @"FROM_SL_NM",
                                                                  @"giQty": @"GI_QTY",
                                                                  @"giftYn": @"GIFT_YN",
                                                                  @"orderNo": @"ORDER_NO",
                                                                  @"toSlCd": @"TO_SL_CD",
                                                                  @"itemCd": @"ITEM_CD",
                                                                  @"serialCd": @"SERIAL_CD",
                                                                  @"plantCd": @"PLANT_CD",
                                                                  @"procStatNm": @"PROC_STAT_NM",
                                                                  @"issueNo": @"ISSUE_NO",
                                                                  @"toSlNm": @"TO_SL_NM",
                                                                  @"fromSlCd": @"FROM_SL_CD",
                                                                  @"reqDt": @"REQ_DT",
                                                                  @"itemNm": @"ITEM_NM",
                                                                  @"contractNo": @"CONTRACT_NO",
                                                                  @"movType": @"MOV_TYPE",
                                                                  @"movTypeNm": @"MOV_TYPE_NM",
                                                                  @"erpFlag": @"ERP_FLAG",
                                                                  @"ifNo": @"IF_NO"}];
}
@end
