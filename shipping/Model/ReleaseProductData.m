//
//  ReleaseProductData.m
//  shipping
//
//  Created by ios on 2018. 4. 13..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ReleaseProductData.h"

@implementation ReleaseProductData

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"itemNm": @"ITEM_NM",
                                                                  @"itemCd": @"ITEM_CD",
                                                                  @"giftYn": @"GIFT_YN",
                                                                  @"qty": @"QTY",
                                                                  @"rentalNm": @"RENTAL_NM",
                                                                  @"lotFlag": @"LOT_FLG"}];
}
@end
