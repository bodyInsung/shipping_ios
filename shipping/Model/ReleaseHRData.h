//
//  ReleaseHRData.h
//  shipping
//
//  Created by ios on 2018. 4. 16..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@protocol ReleaseHRData;
@interface ReleaseHRData : JSONModel

@property (nonatomic) NSString *mdfcDttm;
@property (nonatomic) NSString *syncYn;
@property (nonatomic) NSString *sexCdNm;
@property (nonatomic) NSString *rankCd;
@property (nonatomic) NSString *rankCdNm;
@property (nonatomic) NSString *athNm;
@property (nonatomic) NSString *delYn;
@property (nonatomic) NSString *birthDt;
@property (nonatomic) NSString *mbPhn;
@property (nonatomic) NSString *orgNm;
@property (nonatomic) NSString *rmrk;
@property (nonatomic) NSString *hrDvCd;
@property (nonatomic) NSString *hrId;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *sexCd;
@property (nonatomic) NSString *email;
@property (nonatomic) NSString *sysWhsNm;
@property (nonatomic) NSString *athId;
@property (nonatomic) NSString *sysWhsCd;
@property (nonatomic) NSString *gnrlTelNo;
@property (nonatomic) NSString *addr;
@end
