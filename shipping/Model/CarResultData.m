//
//  CarResultData.m
//  shipping
//
//  Created by ios on 2018. 4. 2..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "CarResultData.h"

@implementation CarResultData

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"adminInfo": @"ADMIN_INFO",
                                                                  @"carGroup": @"CAR_GROUP",
                                                                  @"carNumber": @"CAR_NUMBER",
                                                                  @"carSeq": @"CAR_SEQ"}];
}
@end
