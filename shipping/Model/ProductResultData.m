//
//  ProductResultData.m
//  shipping
//
//  Created by ios on 2018. 4. 2..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ProductResultData.h"

@implementation ProductResultData

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"productCode": @"PRODUCT_CODE",
                                                                  @"productName": @"PRODUCT_NAME",
                                                                  @"productDetail": @"PRODUCT_DETAIL",
                                                                  @"productImageL": @"PRODUCT_IMAGE_L",
                                                                  @"productImageS": @"PRODUCT_IMAGE_S",
                                                                  @"productIntro": @"PRODUCT_INTRO",
                                                                  @"productSpec": @"PRODUCT_SPEC",
                                                                  @"productType": @"PRODUCT_TYPE",
                                                                  @"productCaution1": @"PRODUCT_CAUTION1",
                                                                  @"productCaution1Mov": @"PRODUCT_CAUTION1_MOV",
                                                                  @"productCaution2": @"PRODUCT_CAUTION2",
                                                                  @"productCaution2Mov": @"PRODUCT_CAUTION2_MOV",
                                                                  @"productCaution3": @"PRODUCT_CAUTION3",
                                                                  @"productCaution3Mov": @"PRODUCT_CAUTION3_MOV",
                                                                  @"productCaution4": @"PRODUCT_CAUTION4",
                                                                  @"productCaution4Mov": @"PRODUCT_CAUTION4_MOV",
                                                                  @"productCaution5": @"PRODUCT_CAUTION5",
                                                                  @"productCaution5Mov": @"PRODUCT_CAUTION5_MOV",
                                                                  @"imageOne": @"imageone",
                                                                  @"imageTwo": @"imagetwo"}];
}
@end
