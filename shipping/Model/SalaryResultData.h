//
//  SalaryResultData.h
//  shipping
//
//  Created by ios on 2018. 5. 8..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@protocol SalaryResultData;
@interface SalaryResultData : JSONModel

@property (nonatomic) NSString *deliveryman;
@property (nonatomic) NSString<Optional> *grandTotal;
@property (nonatomic) NSString<Optional> *telNo;
@property (nonatomic) NSString<Optional> *year;
@property (nonatomic) NSString<Optional> *departmentNm;
@property (nonatomic) NSString *deliverymanNm;
@property (nonatomic) NSString<Optional> *email;
@property (nonatomic) NSString<Optional> *month;
@property (nonatomic) NSString<Optional> *dailyCount;
@property (nonatomic) NSString<Optional> *inDate;
@property (nonatomic) NSString<Optional> *shippingType;
@property (nonatomic) NSString<Optional> *total;
@property (nonatomic) NSString<Optional> *type;
@end
