//
//  SaleHistoryData.m
//  shipping
//
//  Created by insung on 26/02/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "SaleHistoryData.h"

@implementation SaleHistoryData

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"inUserName": @"IN_USER_NAME",
                                                                  @"inTel": @"IN_TEL",
                                                                  @"salesType": @"SALES_TYPE",
                                                                  @"productName": @"PRODUCT_NAME",
                                                                  @"inHandphone": @"IN_HANDPHONE",
                                                                  @"managerName": @"MANAGER_NAME",
                                                                  @"nType": @"N_TYPE",
                                                                  @"salesSeq": @"SALES_SEQ",
                                                                  @"regDate": @"REG_DATE",
                                                                  @"etcClausal": @"ETCCLAUSAL",
                                                                  @"productType": @"PRODUCT_TYPE",
                                                                  @"salesConfirm": @"SALES_CONFIRM",
                                                                  @"receivePs": @"RECEIVE_PS",
                                                                  @"productTypeNm": @"PRODUCT_TYPE_NM",
                                                                  @"inUserNo": @"IN_USER_NO"}];
}
@end
