//
//  ProductValidationData.h
//  shipping
//
//  Created by insung on 19/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@protocol ProductValidationData;
@interface ProductValidationData : JSONModel

@property (nonatomic) NSString<Optional> *procStatNm;
@property (nonatomic) NSString<Optional> *issueNo;
@property (nonatomic) NSString<Optional> *giftYn;
@property (nonatomic) NSString<Optional> *itemNm;
@property (nonatomic) NSString<Optional> *serialCd;
@property (nonatomic) NSString<Optional> *itemCd;
@property (nonatomic) NSString<Optional> *movTypeNm;
@property (nonatomic) NSString<Optional> *slCd;
@property (nonatomic) NSString<Optional> *slNm;
@property (nonatomic) NSString<Optional> *badAmount;
@property (nonatomic) NSString<Optional> *goodAmount;
@property (nonatomic) NSString<Optional> *lotFlag;
@property (nonatomic) NSString<Optional> *tempLot;

@property (nonatomic) NSString<Optional> *productCd;
@property (nonatomic) NSString<Optional> *manufacturerCd;
@property (nonatomic) NSString<Optional> *collectYn;
@end
