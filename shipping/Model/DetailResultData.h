//
//  DetailResultData.h
//  shipping
//
//  Created by ios on 2018. 3. 28..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "JSONModel.h"

@interface DetailResultData : JSONModel

@property (nonatomic) NSString *insCompleteNm;
@property (nonatomic) NSString *freeGiftOne;
@property (nonatomic) NSString *telNo2;
@property (nonatomic) NSString<Optional> *freeGiftWarn;
@property (nonatomic) NSString *area2;
@property (nonatomic) NSString *productSn;
@property (nonatomic) NSString *isLadder;
@property (nonatomic) NSString *mustFlag;
@property (nonatomic) NSString *companyCheck;
@property (nonatomic) NSString<Optional> *picturePathFive;
@property (nonatomic) NSString *referName;
@property (nonatomic) NSString *instructionKey;
@property (nonatomic) NSString *cashReceiptCardNum;
@property (nonatomic) NSString *backSn;
@property (nonatomic) NSString *promise;
@property (nonatomic) NSString<Optional> *backPictureTwo;
@property (nonatomic) NSString *progressNoNm;
@property (nonatomic) NSString<Optional> *deliveryMan1;
@property (nonatomic) NSString *serverTime;
@property (nonatomic) NSString *promiseFailCdNm;
@property (nonatomic) NSString<Optional> *deliveryMan3;
@property (nonatomic) NSString<Optional> *deliveryMan2;
@property (nonatomic) NSString<Optional> *backImageOne;
@property (nonatomic) NSString *waterAdaptor;
@property (nonatomic) NSString<Optional> *backImageThree;
@property (nonatomic) NSString *productName;
@property (nonatomic) NSString *productType;
@property (nonatomic) NSString<Optional> *freeGiftCheckOne;
@property (nonatomic) NSString<Optional> *picturePathOne;
@property (nonatomic) NSString<Optional> *backImageTwo;
@property (nonatomic) NSString<Optional> *ladderPictureTwo;
@property (nonatomic) NSString *costnCost;
@property (nonatomic) NSString *paymentType;
@property (nonatomic) NSString *qty;
@property (nonatomic) NSString *productTypeNm;
@property (nonatomic) NSString<Optional> *pictureWAsOne;
@property (nonatomic) NSString<Optional> *imageSix;
@property (nonatomic) NSString *area;
@property (nonatomic) NSString<Optional> *deliveryManNm2;
@property (nonatomic) NSString *promiseTime;
@property (nonatomic) NSString<Optional> *deliveryManNm3;
@property (nonatomic) NSString<Optional> *deliveryManNm1;
@property (nonatomic) NSString *isSocket;
@property (nonatomic) NSString *shippingSeq;
@property (nonatomic) NSString *backDate;
@property (nonatomic) NSString<Optional> *imageFive;
@property (nonatomic) NSString *receiveDate;
@property (nonatomic) NSString *cashReceiptType;
@property (nonatomic) NSString *custUpdate;
@property (nonatomic) NSString<Optional> *picturePathThree;
@property (nonatomic) NSString<Optional> *purchasingOffice;
@property (nonatomic) NSString *insAddr2;
@property (nonatomic) NSString<Optional> *imageSign;
@property (nonatomic) NSString *colorType;
@property (nonatomic) NSString<Optional> *freeGiftAddr;
@property (nonatomic) NSString<Optional> *picturePathSix;
@property (nonatomic) NSString<Optional> *imageFour;
@property (nonatomic) NSString<Optional> *picturePathTwo;
@property (nonatomic) NSString<Optional> *freeGiftCheckTwo;
@property (nonatomic) NSString *agreeName;
@property (nonatomic) NSString *waterCooking;
@property (nonatomic) NSString *ladderPaymentType;
@property (nonatomic) NSString *dueDate;
@property (nonatomic) NSString *serviceMonth;
@property (nonatomic) NSString<Optional> *pictureWAsThree;
@property (nonatomic) NSString *ladderPrice;
@property (nonatomic) NSString<Optional> *ladderPictureOne;
@property (nonatomic) NSString *divStatus;
@property (nonatomic) NSString *custName;
@property (nonatomic) NSString<Optional> *imageTwo;
@property (nonatomic) NSString<Optional> *freeGiftTwo;
@property (nonatomic) NSString *promiseFailPs;
@property (nonatomic) NSString *faxNo;
@property (nonatomic) NSString<Optional> *imageThree;
@property (nonatomic) NSString<Optional> *picturePathSign;
@property (nonatomic) NSString<Optional> *backPictureOne;
@property (nonatomic) NSString *productCode;
@property (nonatomic) NSString *promiseFailCd;
@property (nonatomic) NSString *costAmount;
@property (nonatomic) NSString *insComplete;
@property (nonatomic) NSString<Optional> *pictureWAsTwo;
@property (nonatomic) NSString *hPhoneNo2;
@property (nonatomic) NSString *bodyNo;
@property (nonatomic) NSString *hPhoneNo;
@property (nonatomic) NSString *symptom;
@property (nonatomic) NSString<Optional> *imageOne;
@property (nonatomic) NSString<Optional> *socketPictureOne;
@property (nonatomic) NSString<Optional> *backPictureThree;
@property (nonatomic) NSString *progressNo;
@property (nonatomic) NSString *telNo;
@property (nonatomic) NSString<Optional> *picturePathFour;
@property (nonatomic) NSString *shippingType;
@property (nonatomic) NSString *insAddr;
@property (nonatomic) NSString *carNo;
@property (nonatomic) NSString *inDate;
@property (nonatomic) NSString *delYn;
@property (nonatomic) NSString *shippingPs;
@property (nonatomic) NSString<Optional> *ladderImageOne;
@property (nonatomic) NSString<Optional> *ladderImageTwo;
@property (nonatomic) NSString<Optional> *ladderImageThree;
@property (nonatomic) NSString<Optional> *socketImageOne;
@property (nonatomic) NSString<Optional> *socketImageTwo;
@property (nonatomic) NSString<Optional> *socketImageThree;
@property (nonatomic) NSString<Optional> *serviceImageOne;
@property (nonatomic) NSString<Optional> *serviceImageTwo;
@property (nonatomic) NSString<Optional> *lFreegift;
@property (nonatomic) NSString<Optional> *lFreegiftNm;
@property (nonatomic) NSString<Optional> *giftYn;
@property (nonatomic) NSString<Optional> *nameCheck;
@end
