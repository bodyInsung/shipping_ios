//
//  NSString+Util.m
//  shipping
//
//  Created by ios on 2018. 3. 30..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "NSString+Util.h"

@implementation NSString (Util)

/**
 스트링 null 체크

 @param string 스트링
 @return null 여부
 */
+ (BOOL)isEmptyString:(NSString *)string {
    if(string == nil || [string isEqualToString:@""] || [string isEqualToString:@" "]) {
        return YES;
    }
    return NO;
}

/**
 html 태그 적용

 @param string 스트링
 @param fontSize 폰트
 @return 적용된 스트링
 */
+ (NSAttributedString *)applyHtmlText:(NSString *)string fontSize:(CGFloat)fontSize {
    // 스트링 폰트를 맞추기 위함
    string = [string stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-size:%fpx;}</style>",
                                              fontSize]];
    NSData *encodeData = [string dataUsingEncoding:NSUTF8StringEncoding];    
    return [[NSAttributedString alloc] initWithData:encodeData
                                            options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                 documentAttributes:nil error:nil];
}

/**
 밑줄을 추가해 링크임을 표시
 
 @param string 주소
 @return 밑줄 친 주소
 */
+ (NSAttributedString *)applyLinkText:(NSString *)string {
    return [[NSAttributedString alloc] initWithString:string
                                           attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
}

/**
 밑줄을 추가해 링크임을 표시
 
 @param string 주소
 @param color 스트링 색상
 @return 밑줄 친 주소
 */
+ (NSAttributedString *)applyLinkText:(NSString *)string color:(UIColor *)color {
    return [[NSAttributedString alloc] initWithString:string
                                           attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                                                        NSForegroundColorAttributeName: color}];
}

/**
 숫자 문자열에 콤마 추가

 @param str 숫자 문자열
 @return 콤마 추가된 문자열
 */
+ (NSString *)addCommaFromString:(NSString *)str {
    return [self addCommaFromInteger:[str integerValue]];
}

/**
 숫자에 콤마 추가

 @param num 숫자
 @return 콤마 추가된 문자열
 */
+ (NSString *)addCommaFromInteger:(NSInteger)num {
    return [self addCommaFromNumber:[NSNumber numberWithInteger:num]];
}

/**
 number형에 콤마 추가

 @param num number형
 @return 콤마 추가된 문자열
 */
+ (NSString *)addCommaFromNumber:(NSNumber *)num {
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    return [NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:num]];
}

/**
 숫자 문자열에 콤마 제거

 @param str 숫자 문자열
 @return 콤마 제거된 int
 */
+ (NSInteger)removeCommaFromString:(NSString *)str {
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    return [[numberFormatter numberFromString:str] integerValue];
}

/**
 url 인코딩

 @param url url 스트링
 @return 인코딩된 url 스트링
 */
+ (NSString *)encodeUrl:(NSString *)url {
    return [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
}


@end
