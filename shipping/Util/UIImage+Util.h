//
//  UIImage+Util.h
//  shipping
//
//  Created by insung on 2018. 6. 1..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Util)

+ (UIImage *)resizeImage:(UIImage *)image width:(CGFloat)width;
+ (UIImage *)cropImage:(UIImage *)image size:(CGSize)size;
- (UIImage *)drawTodayText;
@end
