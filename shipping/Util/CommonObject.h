//
//  CommonObject.h
//  shipping
//
//  Created by ios on 2018. 3. 29..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    kTabBarTypeNotice,                  // 공지사항
    kTabBarTypeShipping,                // 배송리스트
    kTabBarTypeHome,                    // 홈
    kTabBarTypeComplete,                // 완료리스트
    kTabBarTypeRelease                  // 출고지시서
} TabBarType;

typedef enum {
    kShippingTypeNone,                  // None
    kShippingTypeDelivery,              // 배송
    kShippingTypeChange,                // 맞교체
    kShippingTypePartChange,            // 부분교체
    kShippingTypeCancel,                // 계약철회
    kShippingTypeMoveRequest,           // 이전요청
    kShippingTypeASRequest,             // 수리요청
    kShippingTypeAS,                    // AS
    kShippingTypeGift,                  // 사은품
    kShippingTypeReinstall,             // 재설치
    kShippingTypeDraft,                 // 기안건
    kShippingTypeDraftReturn,           // 기안회수
    kShippingTypeDisplaySetup,          // 전시장설치
    kShippingTypeDisplayReturn,         // 전시장회수
    kShippingTypeASSetup,               // 수리요청 - 설치
    kShippingTypeASReturn,              // 수리요청 - 회수
    kShippingTypeMoveSetup,             // 이전요청 - 설치
    kShippingTypeMoveReturn,            // 이전요청 - 회수
    kShippingTypeAssemble,              // 조립
    kShippingTypeDecomposition          // 분해
} ShippingType;

typedef enum {
    kReleaseDataTypeWarehouse,          // 창고 리스트
    kReleaseDataTypeProduct,            // 제품구분 리스트
    kReleaseDataTypeWarehouseMove,      // 창고 입고구분 리스트
    kReleaseDataTypeDeliverymanMove     // 기사 입고구분 리스트
} ReleaseDataType;

typedef enum {
    kPhotoTypeCamera,                   // 카메라
    kPhotoTypeLibrary,                  // 사진 라이브러리
    kPhotoTypeBarcode                   // 바코드
} PhotoType;

@interface CommonObject : NSObject

+ (NSArray *)ShippingMonthArray;
+ (NSArray *)SearchShippingBaseDateTypeArray;
+ (NSArray *)SearchCompletionBaseDateTypeArray;
+ (NSArray *)SearchReturnBaseDateTypeArray;

+ (NSArray *)DetailChairShippingArray;
+ (NSArray *)DetailChairMoveShippingArray;
+ (NSArray *)DetailLatexShippingArray;
+ (NSArray *)DetailLatexMoveShippingArray;
+ (NSArray *)DetailCompleteArray;
+ (NSArray *)DetailReturnCompleteArray;
+ (NSArray *)DetailCompleteConfirmArray;

+ (NSArray *)ProductSetupAttachArray;
+ (NSArray *)ProductFaultyAttachArray;
+ (NSArray *)ProductCancelAttachArray;
+ (NSArray *)ChangeProductAttachArray;
+ (NSArray *)ChangeProductReturnAttachArray;
+ (NSArray *)ProductReturnAttachArray;
+ (NSArray *)ProductHalfAttachArray;
+ (NSArray *)ProductAttachArray;
+ (NSArray *)LadderAttachArray;
+ (NSArray *)CraneAttachArray;
+ (NSArray *)ServiceAttachArray;
+ (NSArray *)ReturnAttachArray;

+ (NSArray *)PhotoTypeArray;
+ (NSArray *)PhotoAddBarcodeTypeArray;

+ (NSArray *)ReceiptTypeArray;
+ (NSArray *)LadderPaymentTypeArray;
+ (NSArray *)DepositTypeArray;
+ (NSArray *)CostTypeArray;
+ (NSArray *)ReleaseProductNameArray;
+ (NSArray *)ReleaseProductCodeArray;
+ (NSArray *)WarehousingNameArray;
+ (NSArray *)WarehousingCodeArray;
+ (NSArray *)DeliverymanMoveNameArray;
+ (NSArray *)DeliverymanMoveCodeArray;
+ (NSArray *)PromiseTimeArray;
+ (NSArray *)MenuListArray;
+ (NSArray *)CarCleanAttachArray;
+ (NSArray *)RefuelListArray;
+ (NSArray *)SaleTypeArray;
+ (NSArray *)SaleNewTypeArray;
+ (NSArray *)LoadingImageArray;

+ (NSString *)imageNameFromProduct:(NSString *)pName;
+ (NSString *)stringShippingType:(NSString *)sType pNo:(NSString *)pNo;
+ (ShippingType)shippingType:(NSString *)sType pNo:(NSString *)pNo;
+ (NSString *)divisionFromShippingType:(NSString *)sType pNo:(NSString *)pNo;
+ (NSInteger)baseDateTypeFromTitle:(NSString *)title;
+ (NSString *)codeFromReleaseDataType:(ReleaseDataType)type name:(NSString *)name;
+ (NSString *)codeNamesFromCode:(NSArray *)datas code:(NSString *)code;
+ (NSString *)codeFromData:(NSArray *)datas codeName:(NSString *)codeName;
+ (NSArray *)codeNamesFromData:(NSArray *)datas;
+ (NSArray *)titlesFromData:(NSArray <ProductResultData>*)datas;

+ (BOOL)isOutsourcing;
@end
