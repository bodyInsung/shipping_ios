//
//  UIImageView+Util.h
//  shipping
//
//  Created by ios on 2018. 4. 13..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Util)

- (void)setImageWithBaseUrl:(NSString *)url;
- (void)setImageWithUrlForDynamic:(NSString *)url;
@end
