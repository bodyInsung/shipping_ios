//
//  UIImage+Util.m
//  shipping
//
//  Created by insung on 2018. 6. 1..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "UIImage+Util.h"

@implementation UIImage (Util)

/**
  너비 사이즈에 맞춰 이미지 리사이즈

 @param image 이미지
 @param width 너비
 @return 리사이즈 이미지
 */
+ (UIImage *)resizeImage:(UIImage *)image width:(CGFloat)width {
    CGFloat oldWidth = image.size.width;
    CGFloat scaleFactor = width / oldWidth;
    CGFloat newHeight = image.size.height * scaleFactor;
    CGFloat newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [image drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

/**
 사이즈에 맞춰 사진 자르기
 
 @param image 이미지
 @param size 사이즈
 @return 자른 이미지
 */
+ (UIImage *)cropImage:(UIImage *)image size:(CGSize)size {
    CGRect clippedRect  = CGRectMake(0, image.size.height/2 - size.height/2, size.width, size.height);
    CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, clippedRect);
    UIImage *newImage   = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return newImage;
}

/**
 워터마크 적용 (금일 날짜로 표기)

 @return 워터마크 적용된 이미지
 */
- (UIImage *)drawTodayText {
    UIFont *font = [UIFont boldSystemFontOfSize:30.0f];
    UIColor *textColor = [UIColor redColor];
    NSString *today = [CommonUtil todayFromDateFormat:@"MM/dd HH:mm"];
    NSDictionary *attribute = @{NSForegroundColorAttributeName : textColor, NSFontAttributeName : font};
    CGSize textSize = [today sizeWithAttributes:attribute];
    
    UIGraphicsBeginImageContext(self.size);
    [self drawInRect:CGRectMake(0, 0, self.size.width, self.size.height)];
    CGRect rect = CGRectMake(self.size.width - textSize.width-10, self.size.height - textSize.height-10, self.size.width, self.size.height);
    [today drawInRect:CGRectIntegral(rect) withAttributes:attribute];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
