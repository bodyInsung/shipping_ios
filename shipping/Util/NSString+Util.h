//
//  NSString+Util.h
//  shipping
//
//  Created by ios on 2018. 3. 30..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Util)

+ (BOOL)isEmptyString:(NSString *)string;
+ (NSAttributedString *)applyHtmlText:(NSString *)string fontSize:(CGFloat)fontSize;
+ (NSAttributedString *)applyLinkText:(NSString *)string;
+ (NSAttributedString *)applyLinkText:(NSString *)string color:(UIColor *)color;

+ (NSString *)addCommaFromString:(NSString *)str;
+ (NSString *)addCommaFromInteger:(NSInteger)num;
+ (NSString *)addCommaFromNumber:(NSNumber *)num;
+ (NSInteger)removeCommaFromString:(NSString *)str;
+ (NSString *)encodeUrl:(NSString *)url;
@end
