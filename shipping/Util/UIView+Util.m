//
//  UIView+Util.m
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "UIView+Util.h"

@implementation UIView (Util)

/**
 뷰안의 모든 뷰들 제거
 */
- (void)removeAllSubviews {
    for(UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
}

/**
 라운드 처리

 @param radius 지름
 */
- (void)applyRoundBorder:(CGFloat)radius {
    [self applyRoundBorder:radius color:nil width:0.0];
}

/**
 라운드 처리 (보더)

 @param radius 지름
 @param color 보더 색
 @param width 보더 두께
 */
- (void)applyRoundBorder:(CGFloat)radius color:(UIColor *)color width:(CGFloat)width {
    self.layer.borderWidth = width;
    self.layer.cornerRadius = radius;
    if(color != nil) {
        self.layer.borderColor = color.CGColor;
    }
}

/**
 오토레이아웃 적용 (슈퍼뷰와 동일하게 맞출시)

 @param superview 슈퍼뷰
 */
- (void)applyAutoLayoutFromSuperview:(UIView *)superview {
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.leading.trailing.equalTo(superview);
    }];
}

/**
 뷰 캡쳐 이미지

 @return 캡쳐 이미지
 */
- (UIImage *)imageCaptureView {
    UIGraphicsBeginImageContext(self.bounds.size);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *screenImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [screenImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    return screenImage;
}
@end
