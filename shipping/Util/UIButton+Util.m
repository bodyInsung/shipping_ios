//
//  UIButton+Util.m
//  shipping
//
//  Created by ios on 2018. 3. 23..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "UIButton+Util.h"
#import <objc/runtime.h>

static NSString * const kIndexPathKey = @"indexPath";
static NSString * const kStringTagKey = @"stringTag";

@implementation UIButton (Util)

/**
 indexPath 키값 등록

 @param indexPath indexPath
 */
- (void)setIndexPath:(NSIndexPath *)indexPath {
    objc_setAssociatedObject(self, &kIndexPathKey, indexPath, OBJC_ASSOCIATION_ASSIGN);
}

/**
 indexPath 값 반환

 @return indexPath
 */
- (NSIndexPath *)indexPath {
    return objc_getAssociatedObject(self, &kIndexPathKey);
}

/**
 stringTag 키값 등록

 @param stringTag stringTag
 */
- (void)setStringTag:(NSString *)stringTag {
    objc_setAssociatedObject(self, &kStringTagKey, stringTag, OBJC_ASSOCIATION_ASSIGN);
}

/**
 stringTag 값 반환

 @return stringTag
 */
- (NSString *)stringTag {
    return objc_getAssociatedObject(self, &kStringTagKey);
}


/**
 버튼 그룹에서 선택된 버튼 체크

 @param sBtn 선택된 버튼
 @param group 버튼 그룹
 */
+ (void)selectBtnGroup:(UIButton *)sBtn group:(NSArray *)group {
    for(UIButton *btn in group) {
        btn.selected = [sBtn isEqual:btn];
    }
}

/**
 버튼 그룹에서 선택된 버튼 반환
 
 @param group 버튼 그룹
 @return btn
 */
+ (UIButton *)selectedBtn:(NSArray *)group {
    for(UIButton *btn in group) {
        if(btn.isSelected) {
            return btn;
        }
    }
    return nil;
}

@end
