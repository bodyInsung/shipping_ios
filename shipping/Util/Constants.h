//
//  Constants.h
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#endif /*constants_h */

#if DEBUG
//#define BASE_URL @"http://172.30.40.37:8082"            // 재영대리님
//#define BASE_URL @"http://192.168.1.131:8081"
//#define BASE_URL @"http://121.138.34.30:8080"         // 이전운영
#define BASE_URL @"http://172.30.40.53:8081"
#define BASE_SERVICE @"http://172.30.40.29:8280"        // 유경대리님
#else
////#define BASE_URL @"http://121.138.34.32:8081"
////#define BASE_URL @"https://bfs1.bfservice.co.kr"
#define BASE_URL @"https://bfs1.bfservice.co.kr"
#define BASE_SERVICE @"https://svc.bfservice.co.kr"

#endif
#define GOOGLE_MAP_API_KEY              @"AIzaSyDTVgpdm1sPVicoz0mVaPzALcrAyBxFTZY"
#define MAP_CLIENT_KEY                  @"59aaf6b2-0521-402b-88b4-7d3f53b76458"
#define TEST_LOGIN_ID                   @"testtt"

#define BASE_CERTIFICATION_URL          @"http://121.138.34.14"
//#define BASE_CERTIFICATION_URL          @"http://172.30.40.35:8080"
#define CERTIFICATION_AES_KEY           @"bfservicekey!@12"
#define CERTIFICATION_SERVICE_CODE      @"SHIPPING"
#define CERTIFICATION_SECRET_KEY        @"27e924340df23acc1776b1d07f9cd304356a4814"

#define WAREHOUSE_BASE_URL              @"http://121.138.34.92"
#define APP_VERSION_URL                 @"/mobile/api/versionCheck.json"
#define LOGIN_URL                       @"/login/appLogin.json"
#define CERTIFICATION_SEND_URL          @"/auth/sendMessage"
#define CERTIFICATION_CONFIRM_URL       @"/auth/checkAuthCode"
#define VALIDATION_URL                  @"/auth/checkMsgid"
#define TEMPKEY_URL                     @"/login/reqTempKey.json"
#define VALIDATION_TEMPKEY_URL          @"/login/selectTempKey.json"
#define UPDATE_PASSWORD_URL             @"/login/updateAdminPwd.json"
#define LOGOUT_URL                      @"/login/appLogout.json"
#define APP_LOG_URL                     @"/mobile/api/appInsertLog.json"
#define HOME_URL                        @"/mobile/api/appShippingMainSummaryNew.json"
#define NOTICE_URL                      @"/mobile/api/appNoticeList.json"
#define NOTICE_LOG_URL                  @"/mobile/api/appNoticeLog.json"
#define SHIPPING_LIST_URL               @"/mobile/api/appShippingDivList.json"
#define CALENDAR_SHIPPING_LIST_URL      @"/mobile/api/appShippingDivListCal.json"
#define SEARCH_CODE_URL                 @"/mobile/api/appSearchCodeList.json"
#define RETURN_LIST_URL                 @"/mobile/api/appShippingBackList.json"
#define COMPLETION_LIST_URL             @"/mobile/api/appShippingDoneList.json"
#define DETAIL_URL                      @"/mobile/api/appShippingMasterDetail.json"
#define MODIFY_ADDRESS_URL              @"/mobile/api/modifyAddress.json"
#define MODIFY_ADDRESS_CONFIRM_URL      @"/mobile/api/ModifyAddressConfirm.json"
#define COMPANY_CHECK_URL               @"/ShippingMgt/ShippingTeamMgt/updateShippingCompcheck.json"
#define CAR_LIST_URL                    @"/mobile/api/appCarList.json"
#define DELIVERYMAN_LIST_URL            @"/mobile/api/appSelectDeliveryMan2List.json"
#define DELIVERYMAN_CHANGE_URL          @"/mobile/api/modifyDeliveryMan2.json"
#define LATEX_GIFT_CODE_URL             @"/mobile/api/appSelectLatexCode.json"
#define UPDATE_FREEGIFT_URL             @"/mobile/api/appUpdateLFreegift.json"
#define SHIPPING_DUE_DATE_MODIFY_URL    @"/mobile/api/appModifyDueDate.json"
#define SHIPPING_MODIFY_URL             @"/mobile/api/appModifyShippingDiv.json"
#define RELEASE_LIST_URL                @"/mobile/api/wms/selectOutLoiList.json"
#define RELEASE_DELETE_URL              @"/mobile/api/wms/delOutLoi.json"
#define WAREHOUSE_LIST_URL              @"/mobile/api/wms/selectWarehouseList.json"
#define RELEASE_GROUP_LIST_URL          @"/mobile/api/wms/selectItemGroupList.json"
#define GIFT_STOCK_LIST_URL             @"/mobile/api/wms/selectGiftStockList.json"
#define GIFT_LIST_URL                   @"/mobile/api/wms/selectGiftList.json"
#define RELEASE_STOCK_LIST_URL          @"/mobile/api/wms/selectItemStockList.json"
#define ALL_PRODUCT_LIST_URL            @"/mobile/api/wms/selectItemList.json"
#define CREATE_BARCODE_URL              @"/mobile/api/wms/createBarcode.json"
#define RELEASE_REGISTER_URL            @"/mobile/api/wms/createOutLoi.json"
#define DELIVERYMAN_STOCK_LIST_URL      @"/mobile/api/wms/selectDelivManStockList.json"
#define WAREHOUSING_URL                 @"/mobile/api/wms/createInLoi.json"
#define WAREHOUSING_SEND_HISTORY_URL    @"/mobile/api/wms/selectInLoiList.json"
#define WAREHOUSING_RECEIVE_HISTORY_URL @"/mobile/api/wms/selectInLoiReceiveList.json"
#define PRODUCT_VALIDATION_URL          @"/mobile/api/wms/checkProductSerialType.json"
#define PRODUCT_LIST_URL                @"/mobile/api/appProductList.json"
#define PRODUCT_DETAIL_URL              @"/mobile/api/appProductDetail.json"
#define TRANSFER_CONFIRM_URL            @"/mobile/api/wms/confirmTransferReq.json"
#define CAR_CLEAN_URL                   @"/stats/admin/insertCarStatus.json"
#define SCHEDULE_URL                    @"/mobile/api/appShippingDivCalendar.json"
#define SALARY_MONTHLY_LIST_URL         @"/ShippingMgt/ShippingMonthlyList/selectMonthlyReportList.json"
#define SALARY_DAILY_LIST_URL           @"/ShippingMgt/ShippingMonthlyList/selectSummaryApiList.json"
#define SALE_REGISTER_URL               @"/mobile/api/createBizSale.json"
#define SALE_HISTORY_URL                @"/mobile/api/selectBizSales.json"

#define MAP_GEOCODE_URL                 @"https://api2.sktelecom.com/tmap/geo/fullAddrGeo?version=1&format=json&callback=result"
#define SURVEY_URL                      @"https://goo.gl/forms/5RUZzqlEe2JL1RZ73"
#define GUIDE_PDF_URL                   @"http://www.bfservice.co.kr/images/pdf/latex_as_guide.pdf"
#define APP_DOWNLOAD_URL                @"https://www.bfservice.co.kr/"
#define GOOGLE_SHEET_REFUEL_URL         @"https://script.google.com/macros/s/AKfycbx_5J-8rL29PM1_2heE-fyYLMOPAkHf02uJh_qEH1hTPNCas8GA/exec?action=insert"
#define PROMOTION_URL                   @"https://www.bfservice.co.kr/ShippingMgt/ShippingTeamMgt/promotionNotice.view"

#define AUTO_LOGIN_KEY                          @"AutoLogin"
#define SAVE_ID_KEY                             @"SaveID"
#define LOGIN_DATE_KEY                          @"LoginDate"
#define USER_DATA_KEY                           @"UserResultData"
#define USER_CAR_KEY                            @"UserCar"
#define USER_REGISTER_KEY                       @"UserRegister"
#define API_RESPONSE_KEY                        @"resultData"

#define STORYBOARD_MAIN_NAME                    @"Main"
#define STORYBOARD_LOGIN_ID                     @"LoginStoryBoard"
#define STORYBOARD_TABBAR_ID                    @"TabBarStoryBoard"
#define STORYBOARD_ROOT_ID                      @"RootStoryBoard"

#define STORYBOARD_CERTIFICATION_SEGUE          @"CertificationSegue"
#define STORYBOARD_NOTICE_DETAIL_SEGUE          @"NoticeDetailSegue"
#define STORYBOARD_SHIPPING_LIST_MAP_SEGUE      @"ListMapSegue"
#define STORYBOARD_SHIPPING_MAP_SEGUE           @"MapSegue"
#define STORYBOARD_SHIPPING_DETAIL_SEGUE        @"ShippingDetailSegue"
#define STORYBOARD_COMPLETION_DETAIL_SEGUE      @"CompletionDetailSegue"
#define STORYBOARD_LIST_POPUP_CAR_SEGUE         @"CarPopupSegue"
#define STORYBOARD_LIST_POPUP_DELIVERYMAN_SEGUE @"DeliverymanPopupSegue"
#define STORYBOARD_LIST_POPUP_WAREHOUSE_SEGUE   @"WarehousePopupSegue"
#define STORYBOARD_LIST_POPUP_PRODUCT_SEGUE     @"ProductPopupSegue"
#define STORYBOARD_SIGN_POPUP_SEGUE             @"SignPopupSegue"
#define STORYBOARD_SIGN_SEGUE                   @"SignSegue"
#define STORYBOARD_IMAGE_SEGUE                  @"ImageSegue"
#define STORYBOARD_BARCODE_SEGUE                @"BarcodeSegue"
#define STORYBOARD_RELEASE_ADD_SEGUE            @"ReleaseAddSegue"
#define STORYBOARD_REASON_POPUP_SEGUE           @"ReasonPopupSegue"
#define STORYBOARD_INPUT_POPUP_SEGUE            @"InputPopupSegue"
#define STORYBOARD_LIST_POPUP_STOCK_SEGUE       @"StockPopupSegue"
#define STORYBOARD_LIST_POPUP_GIFT_SEGUE        @"GiftPopupSegue"
#define STORYBOARD_SHIPPING_LIST_SEGUE          @"ShippingListSegue"
#define STORYBOARD_SALARY_SEGUE                 @"SalarySegue"
#define STORYBOARD_REFUEL_SEGUE                 @"RefuelSegue"
#define STORYBOARD_SALE_SEGUE                   @"SaleSegue"
#define STORYBOARD_SALE_HISTORY_SEGUE           @"SaleHistorySegue"
#define STORYBOARD_SCHEDULE_SEGUE               @"ScheduleSegue"
#define STORYBOARD_STOCK_SEGUE                  @"StockSegue"
#define STORYBOARD_STOCK_HISTORY_SEGUE          @"StockHistorySegue"
#define STORYBOARD_BARCODE_GENERATE_SEGUE       @"BarcodeGenerateSegue"
#define STORYBOARD_CLEAN_SEGUE                  @"CleanSegue"
#define STORYBOARD_PRODUCT_SEGUE                @"ProductSegue"
#define STORYBOARD_PRODUCT_DETAIL_SEGUE         @"ProductDetailSegue"
#define STORYBOARD_PROMOTION_SEGUE              @"PromotionSegue"

#define LOGIN_SESSION_CODE                      @"309"      // 로그인 세션 코드
#define CONNECT_FAIL_CODE                       @"-1"       // 통신 실패 코드
#define CONNECT_SUCCESS_CODE                    @"200"      // 통신 성공 코드
#define STOCK_NONE_REGISTER_CODE                @"930"      // 재고 등록되지 않음 코드

#define SEARCH_AREA_CODE                        @"400"      // 지역코드
#define SHIPPING_DIVISION_CODE                  @"9100"     // 배송구분
#define CONTRACTOR_CODE                         @"9300"     // 본사외주
#define PROMISE_IMPOSSIBLE_CODE                 @"9400"     // 약속 불가 사유
#define GIFT_CODE                               @"9600"     // 사은품
#define RELATION_CODE                           @"9700"     // 관계데이터
#define TERMS_CODE                              @"9800"     // 약관데이터
#define CHAIR_DELIVERYMAN_CODE                  @"9900"     // 안마의자 기사 정보

#define DATE_HYPHEN_YEAR_MONTH_DAY              @"yyyy-MM-dd"
#define DATE_KOREAN_YEAR_MONTH_DAY              @"yyyy년 MM월 dd일"
#define DATE_KOREAN_YEAR_MONTH                  @"yyyy년 MM월"
#define DATE_KOREAN_MONTH_DAY                   @"MM월 dd일"
#define DATE_KOREAN_MONTH_DAY_WEEK              @"MM월 dd일 EEEE"
#define DATE_DOT_YEAR_MONTH_DAY                 @"yyyy.MM.dd"
#define DATE_DOT_YEAR_MONTH                     @"yyyy.MM"
#define DATE_YEAR_MONTH_DAY                     @"yyyyMMdd"

#define COUNT_ATTACH_LADDER                     3
#define COUNT_ATTACH_CRANE                      3
#define COUNT_ATTACH_SERVICE                    2
#define COUNT_ATTACH_CAR_CLEAN                  6

#define IMAGE_PRODUCT_IROBO             @"chair_irobo"
#define IMAGE_PRODUCT_PHANTOM_BLACK     @"chair_phantom_black"
#define IMAGE_PRODUCT_PHANTOM_BROWN     @"chair_phantom_brown"
#define IMAGE_PRODUCT_PHANTOM_RED       @"chair_phantom_red"
#define IMAGE_PRODUCT_PHARAOH           @"chair_pharaoh"
#define IMAGE_PRODUCT_PHARAOH_S         @"chair_pharaoh_s"
#define IMAGE_PRODUCT_PRESIDENT         @"chair_president"
#define IMAGE_PRODUCT_PRESIDENT_PLUS    @"chair_president_plus"
#define IMAGE_PRODUCT_REGINA            @"chair_regina"
#define IMAGE_PRODUCT_REXL              @"chair_rexl"
#define IMAGE_PRODUCT_REXL_BLACK        @"chair_rexl_black"
#define IMAGE_PRODUCT_ELIZABETH         @"chair_elizabeth"
#define IMAGE_PRODUCT_NEWCRUZE          @"chair_newcruze"
#define IMAGE_PRODUCT_ROSEMARY          @"chair_rosemary"
#define IMAGE_PRODUCT_SELENE            @"chair_selene"
#define IMAGE_PRODUCT_FRAME_BLACK       @"latex_black_frame"
#define IMAGE_PRODUCT_FRAME_FLOATING    @"latex_floating_frame"
#define IMAGE_PRODUCT_MAT               @"latex_mat"
#define IMAGE_PRODUCT_FRAME_WHITE       @"latex_white_frame"
#define IMAGE_PRODUCT_CAPTAIN           @"chair_captain"
#define IMAGE_PRODUCT_DARTH_AUTO        @"chair_darth_auto"
#define IMAGE_PRODUCT_DARTH             @"chair_darth"
#define IMAGE_PRODUCT_IRONMAN           @"chair_ironman"
#define IMAGE_PRODUCT_LADY              @"chair_lady"
#define IMAGE_PRODUCT_PALACE            @"chair_palace"
#define IMAGE_PRODUCT_SPIDERMAN         @"chair_spiderman"
#define IMAGE_PRODUCT_STORM             @"chair_storm"
#define IMAGE_PRODUCT_RUG               @"rug"

#define TITLE_PROMOTION_ACTIONBAR           @"프로모션"
#define TITLE_NOTICE_ACTIONBAR              @"공지사항"
#define TITLE_NOTICE_DETAIL_ACTIONBAR       @"공지사항 상세"
#define TITLE_SHIPPING_LIST_ACTIONBAR       @"배송리스트"
#define TITLE_SHIPPING_DETAIL_ACTIONBAR     @"상세내역"
#define TITLE_COMPLETE_ACTIONBAR            @"완료리스트"
#define TITLE_COMPLETE_DETAIL_ACTIONBAR     @"배송완료"
#define TITLE_RELEASE_ACTIONBAR             @"출고지시서"
#define TITLE_RELEASE_ADD_ACTIONBAR         @"출고지시서 작성"
#define TITLE_RETURN_LIST_ACTIONBAR         @"회수리스트"
#define TITLE_SALARY_ACTIONBAR              @"수당 계산"
#define TITLE_SHIPPING_SCHEDULE_ACTIONBAR   @"배송 스케줄"
#define TITLE_STOCK_ACTIONBAR               @"재고관리"
#define TITLE_WAREHOUSING_HISTORY_ACTIONBAR @"입고 요청내역"
#define TITLE_CAR_CLEAN_ACTIONBAR           @"세차사진등록"
#define TITLE_REFUEL_ACTIONBAR              @"주유비 등록"
#define TITLE_SALE_ACTIONBAR                @"제품판매"
#define TITLE_SALE_HISTORY_ACTIONBAR        @"제품판매 조회"
#define TITLE_PRODUCT_ACTIONBAR             @"제품설명"
#define TITLE_PRODUCT_DETAIL_ACTIONBAR      @"제품상세"

#define DEFAULT_POSITION CLLocationCoordinate2DMake(37.6, 127)

static NSString *const kTitleBodyNo = @"BODY NO";
static NSString *const kTitleContract = @"계약자 동일여부";
static NSString *const kTitleDivision = @"구분";
static NSString *const kTitleCustomerNm = @"고객명";
static NSString *const kTitleContactNo1 = @"연락처1";
static NSString *const kTitleContactNo2 = @"연락처2";
static NSString *const kTitleContactNo3 = @"연락처3";
static NSString *const kTitleTelephone = @"집전화";
static NSString *const kTitleCellPhone = @"핸드폰";
static NSString *const kTitleAddress = @"주소";
static NSString *const kTitleProductDivision = @"제품구분";
static NSString *const kTitleManuFacturer = @"제조사";
static NSString *const kTitleProductNm = @"제품명";
static NSString *const kTitlePurchaseOffice = @"구매처";
static NSString *const kTitleQuantity = @"수량";
static NSString *const kTitleService = @"서비스";
static NSString *const kTitleLatexGift = @"라텍스 사은품";
static NSString *const kTitleFreeGift1 = @"사은품";
static NSString *const kTitleFreeGift2 = @"사은품2";
static NSString *const kTitlePresentFreeGift1 = @"사은품 증정여부";
static NSString *const kTitlePresentFreeGift2 = @"사은품2 증정여부";
static NSString *const kTitleCompanyNote = @"본사비고";
static NSString *const kTitleNoteAdd = @"비고추가";
static NSString *const kTitleDivideState = @"분배상태";
static NSString *const kTitleDeliveryMan = @"기사";
static NSString *const kTitleDeliveryMan1 = @"기사1";
static NSString *const kTitleDeliveryMan2 = @"기사2";
static NSString *const kTitleDeliveryMan3 = @"기사3";
static NSString *const kTitleCarSelection = @"차량선택";
static NSString *const kTitleMoveSetupInfo = @"이전 설치 정보";
static NSString *const kTitleSetupNm = @"설치자";
static NSString *const kTitleSetupTelNo = @"설치자 전화번호";
static NSString *const kTitleSetupPhoneNo = @"설치자 핸드폰번호";
static NSString *const kTitleSetupLocation = @"설치지역";
static NSString *const kTitleSetupAddress = @"설치주소";
static NSString *const kTitleCollectionNm = @"회수자";
static NSString *const kTitleCollectionTelNo = @"회수자 전화번호";
static NSString *const kTitleCollectionPhoneNo = @"회수자 핸드폰번호";
static NSString *const kTitleCollectionLocation = @"회수지역";
static NSString *const kTitleCollectionAddress = @"회수주소";
static NSString *const kTitleMove = @"이전";
static NSString *const kTitleDeliveryAdd = @"부사수 추가";
static NSString *const kTitleDeliveryChange = @"부사수 변경";
static NSString *const kTitleCarChange = @"차량변경";
static NSString *const kTitleCodeConfirm = @"제품 확인";
static NSString *const kTitleSetupCodeConfirm = @"설치 제품 확인";
static NSString *const kTitleReturnCodeConfirm = @"회수 제품 확인";
static NSString *const kTitleReturnWarehouse = @"회수 창고";
static NSString *const kTitleWarehouse = @"창고 선택";
static NSString *const kTitleProductCode = @"제품 코드";
static NSString *const kTitleGiftCode = @"사은품 코드";
static NSString *const kTitleStockDeliveryman = @"기사 선택";

static NSString *const kTitleMoveRequest = @"이전요청";
static NSString *const kTitleASRequest = @"수리요청";
static NSString *const kTitleAS = @"AS";
static NSString *const kTitleDraftReturn = @"기안회수건";

static NSString *const kTitleMoterizedBed = @"전동침대";
static NSString *const kTitleMoterizedBedEtc = @"전동기타";
static NSString *const kTitleRoyalFrame = @"고급프레임";
static NSString *const kTitleRoyalFrameEtc = @"고급프레임기타";
static NSString *const kTitleDelivery = @"배송";
static NSString *const kTitleChange = @"맞교체";
static NSString *const kTitleCancel = @"계약철회";
static NSString *const kTitleRelocationOfMonth1 = @"이전회수";
static NSString *const kTitleRelocationOfMonth0 = @"이전설치";
static NSString *const kTitleReinstall = @"재설치";
static NSString *const kTitleAsOfMonth = @"수리";
static NSString *const kTitleFaultyOfMonth = @"초도불량";
static NSString *const kTitleEtcOfMonth = @"기타";

static NSString *const kTitleSetupDate = @"설치일";
static NSString *const kTitleDivideDate = @"분배일";
static NSString *const kTitleReceiveDate = @"접수일";
static NSString *const kTitleDueDate = @"지정일";
static NSString *const kTitleReturnDate = @"회수일";

static NSString *const kTitleUndifine = @"미정";
static NSString *const kTitleUnspecified = @"미지정";
static NSString *const kTitleSetup = @"설치";
static NSString *const kTitleSetupReturn = @"설치-";
static NSString *const kTitleReturn = @"회수";
static NSString *const kTitleTransfer = @"이동";
static NSString *const kTitleReturnComplte = @"회수완료";
static NSString *const kTitleReturnIncomplte = @"회수미완료";
static NSString *const kTitleLocationTotal = @"전체";
static NSString *const kTitleUnselected = @"미선택";
static NSString *const kTitleFaulty = @"초도불량";

static NSString *const kTitleProductNo = @"제품 S/N";
static NSString *const kTitleSetupTotal = @"설치(전체)";
static NSString *const kTitleSetupAttach = @"설치첨부";
static NSString *const kTitleReturnNo = @"회수 S/N";
static NSString *const kTitleReturnAttach = @"회수첨부";

static NSString *const kTitleAttach1 = @"첨부1";
static NSString *const kTitleAttach2 = @"첨부2";
static NSString *const kTitleAttach3 = @"첨부3";
static NSString *const kTitleAttach4 = @"첨부4";
static NSString *const kTitleLadderAttach1 = @"수작업불가(줄자)";
static NSString *const kTitleLadderAttach2 = @"사다리 인증";
static NSString *const kTitleLadderAttach3 = @"영수증";
static NSString *const kTitleCraneAttach1 = @"건물외관";
static NSString *const kTitleCraneAttach2 = @"복도1";
static NSString *const kTitleCraneAttach3 = @"복도2";
static NSString *const kTitleServiceAttach1 = @"내림서비스1";
static NSString *const kTitleServiceAttach2 = @"내림서비스2";
static NSString *const kTitleReturnAttach1 = @"회수1(제품 필수)";
static NSString *const kTitleReturnAttach2 = @"회수2(제품 필수)";

static NSString *const kTitlePaymentType = @"결제구분";
static NSString *const kTitleDepositType = @"입금유형";
static NSString *const kTitleCard = @"카드";
static NSString *const kTitleAccount = @"계좌";
static NSString *const kTitleCash = @"현금";
static NSString *const kTitleCashReceipt = @"현금 영수증";

static NSString *const kTitleSetupMemo = @"설치비고";
static NSString *const kTitleSymptom = @"증상";
static NSString *const kTitleSetupState = @"설치여부";
static NSString *const kTitleDelete = @"삭제";
static NSString *const kTitleCarPopup = @"차량 선택";
static NSString *const kTitleSubDeleveryManPopup = @"부사수 선택";
static NSString *const kTitleProductPopup = @"제품 선택";
static NSString *const kTitleProductGroupPopup = @"제품그룹 선택";

static NSString *const kTitleMenuSalary = @"수당 계산";
static NSString *const kTitleMenuRefuel = @"주유비 등록";
static NSString *const kTitleMenuSale = @"제품판매";
static NSString *const kTitleMenuSaleHistory = @"제품판매 조회";
static NSString *const kTitleMenuSchedule = @"배송 스케줄";
static NSString *const kTitleMenuShippingList = @"배송 리스트";
static NSString *const kTitleMenuReturnList = @"배송 회수리스트";
static NSString *const kTitleMenuCompleteList = @"배송 완료리스트";
static NSString *const kTitleMenuStock = @"재고관리";
static NSString *const kTitleMenuBarcode = @"바코드 생성";
static NSString *const kTitleMenuRelease = @"출고지시서";
static NSString *const kTitleMenuCarRegister = @"차량등록";
static NSString *const kTitleMenuCarClean = @"세차사진등록";
static NSString *const kTitleMenuProduct = @"제품설명";
static NSString *const kTitleMenuNotice = @"공지사항";
static NSString *const kTitleMenuSurvey = @"개선 요청";
static NSString *const kTitleMenuLogout = @"로그아웃";
static NSString *const kTitleUpdate = @"업데이트";
static NSString *const kTitleImpossibleContact = @"접속제한";
static NSString *const kTitleCamera = @"카메라";
static NSString *const kTitlePromiseTime = @"약속 시간";
static NSString *const kTitlePromiseTimeSet = @"약속 시간 설정";
static NSString *const kTitlePromiseImpossible = @"약속 불가";
static NSString *const kTitlePromiseTimeChange = @"약속 시간 변경";
static NSString *const kTitlePromiseCancel = @"약속 취소";
static NSString *const kTitlePromiseImpossibleCancel = @"약속 불가 취소";
static NSString *const kTitleImpossibleContent = @"불가 내용 보기";
static NSString *const kTitleDueDateSet = @"지정일 지정";
static NSString *const kTitleDueDateCancel = @"지정일 지정 해제";
static NSString *const kTitleDueDateChange = @"지정일 변경";
static NSString *const kTitleCertification = @"휴대폰인증";
static NSString *const kTitleRefuelCard = @"카드소유자(본인이면 필요 없음)";
static NSString *const kTitleRefuelLiter = @"주유량(L)";
static NSString *const kTitleRefuelPrice = @"주유금액(₩)";
static NSString *const kTitleRefuelMeter = @"주행누계(km)";
static NSString *const kTitleRefuelDieselExhaustLiter = @"요소수량(L)";
static NSString *const kTitleRefuelDieselExhaust = @"요소수(₩)";
static NSString *const kTitleRefuelEtc = @"세차비/워셔(₩)";
static NSString *const kTitleRental = @"렌탈";
static NSString *const kTitleSale = @"판매";
static NSString *const kTitleExistent = @"기존";
static NSString *const kTitleNew = @"신규";
static NSString *const kTitleBarcode = @"바코드 확인";

static NSString *const kDescShippingMsg = @"[바디프랜드 배송팀 안내]\n%@고객님! 바디프랜드\n배송팀기사 %@입니다.\n\n배송예정시간은\n%@ 사이 예정입니다.\n감사합니다.";
static NSString *const kDescSignNeed = @"서명을 입력해주세요";
static NSString *const kDescGiftNeed = @"사은품을 선택해주세요";
static NSString *const kDescMemoNeed = @"설치비고란에 분해, 조립 정보를 입력해주세요";
static NSString *const kDescProductNoNeed = @"제품 S/N을 입력해주세요";
static NSString *const kDescReturnNoNeed = @"회수 S/N을 입력해주세요";
static NSString *const kDescSetupNeed = @"설치여부를 선택해주세요";
static NSString *const kDescHousingNeed = @"주거형태를 선택해주세요";
static NSString *const kDescLoadImageCant = @"이미지를 불러올 수 없습니다";
static NSString *const kDescSearchAddressCant = @"해당 주소를 찾을 수 없습니다";
static NSString *const kDescProductNameNeed = @"제품명을 입력해주세요";
static NSString *const kDescWarehouseNameNeed = @"창고명을 입력해주세요";
static NSString *const kDescStockDeliverymanNameNeed = @"이름을 입력해주세요";
static NSString *const kDescQuantityNeed = @"수량을 입력헤주세요";
static NSString *const kDescStockEnoughNeed = @"재고가 부족합니다";
static NSString *const kDescProductAddNeed = @"제품을 추가해주세요";
static NSString *const kDescPhoneNumberNeed = @"등록된 전화번호가 없습니다";
static NSString *const kDescMessageCant = @"메세지를 전송할 수 없습니다";
static NSString *const kDescPromiseDateNeed = @"지정된 약속 날짜가 없습니다";
static NSString *const kDescPromiseTimeNeed = @"지정된 약속 시간이 없습니다";
static NSString *const kDescDeleteQuestion = @"삭제 하시겠습니까?";
static NSString *const kDescReLoginNeed = @"세션이 종료되어 재로그인 합니다";
static NSString *const kDescCarSelectionNeed = @"차량 번호를 입력해주세요";
static NSString *const kDescSubDeliveryManNeed = @"찾으실 부사수의 이름을 입력하세요";
static NSString *const kDescModifySuccess = @"수정에 성공 하였습니다";
static NSString *const kDescCarRegisterNeed = @"차량이 등록되어 있지 않습니다.\n차량 등록하시겠습니까?";
static NSString *const kDescLogoutQuestion = @"로그아웃 하시겠습니까?";
static NSString *const kDescImageAttachNeed = @"사진을 첨부해주세요";
static NSString *const kDescRegisterSuccess = @"등록이 완료되었습니다";
static NSString *const kDescPromiseCancelQuestion = @"약속 취소 하시겠습니까?";
static NSString *const kDescPromiseImpossibleCancelQuestion = @"약속 불가 취소 하시겠습니까?";
static NSString *const kDescDateCancelQuestion = @"지정일 지정 해제 하시겠습니까?";
static NSString *const kDescDateImpossibleChange = @"고객 배송 요청일입니다 (%@)";
static NSString *const kDescLoginFailed = @"로그인에 실패했습니다";
static NSString *const kDescConnectFailed = @"통신에 실패했습니다";
static NSString *const kDescIDInputNeed = @"아이디를 입력하세요";
static NSString *const kDescPWInputNeed = @"비밀번호를 입력하세요";
static NSString *const kDescPWInputSameNeed = @"입력한 비밀번호가 일치하지 않습니다";
static NSString *const kDescPhoneInputNeed = @"휴대폰 번호를 입력하세요";
static NSString *const kDescImpossibleContact = @"서버 작업으로 인해 \n금일 오전 9시부터 접속이 가능합니다";
static NSString *const kDescCameraCant = @"카메라를 사용할 수 없습니다";
static NSString *const kDescKakaoNaviNeed = @"카카오 내비가 설치되어 있지 않습니다";
static NSString *const kDescLoginPermissionNeed = @"사용 권한이 없습니다";
static NSString *const kDescReloadNeed = @"기존에 작성중이던 이미지가 있습니다.\n다시 로드 하시겠습니까?";
static NSString *const kDescSaveNeed = @"작성중인 이미지가 있습니다.\n저장 하시겠습니까?";
static NSString *const kDescRequestFailed = @"데이터를 불러오지 못했습니다";
static NSString *const kDescSentMessage = @"메세지를 전송 했습니다";
static NSString *const kDescAlreadyAdded = @"동일 제품이 존재합니다";
static NSString *const kDescCertificationResendNeed = @"인증번호 재전송 버튼을 눌러주세요";
static NSString *const kDescCertificationSuccess = @"인증이 완료되었습니다";
static NSString *const kDescCertificationNumberNeed = @"인증번호를 입력하세요";
static NSString *const kDescCertificationSend = @"인증번호가 전송되었습니다";
static NSString *const kDescCertificationResend = @"인증번호가 재전송되었습니다";
static NSString *const kDescTempKeySend = @"임시 비밀번호가 전송되었습니다";
static NSString *const kDescTempKeyInputNeed = @"임시 비밀번호를 입력하세요";
static NSString *const kDescTempKeySendFailed = @"임시 비밀번호를 전송하지 못했습니다";
static NSString *const kDescTempKeyValidation = @"임시 비밀번호가 일치합니다";
static NSString *const kDescTempKeyValidationFailed = @"임시 비밀번호가 일치하지 않습니다";
static NSString *const kDescPasswordUpdate = @"비밀번호가 변경되었습니다";
static NSString *const kDescPasswordUpdateFailed = @"비밀번호를 변경하지 못했습니다";
static NSString *const kDescPromiseNeed = @"약속 날짜 설정이 필요합니다";
static NSString *const kDescWarehouseNeed = @"회수창고를 선택하세요";
static NSString *const kDescProductCodeValidNeed = @"제품 코드 확인이 필요합니다";
static NSString *const kDescProductCodeNeed = @"제품 코드를 입력해주세요";
static NSString *const kDescSetupProductCodeNeed = @"설치 제품 코드를 입력해주세요";
static NSString *const kDescReturnProductCodeNeed = @"회수 제품 코드를 입력해주세요";
static NSString *const kDescGiftCodeValidNeed = @"사은품 코드 확인이 필요합니다";
static NSString *const kDescStockNone = @"재고가 존재하지 않습니다";
static NSString *const kDescExistBarcode = @"제품 바코드가 존재하면 촬영해주세요";
static NSString *const kDescUseProduct = @"제품 사용설명을 기사에게 받았습니다.";

static NSString *const kFormatCase = @"%zd건";
static NSString *const kFormatCaseRound = @"(%zd건)";
static NSString *const kFormatImageAttachNeed = @"%@ 사진을 첨부해주세요";

static NSString *const kFormatLadderImage = @"ladder_sample%d";
static NSString *const kFormatCraneImage = @"crane_sample%d";
static NSString *const kFormatServiceImage = @"service_sample%d";

static NSString *const kProductTypeMassage = @"M";
static NSString *const kProductTypeLatex = @"L";
static NSString *const kProductTypeWellness = @"W";


static NSString *const kAppLogTypeCall = @"T";
static NSString *const kAppLogTypeShipping = @"U";
