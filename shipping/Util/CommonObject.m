//
//  CommonObject.m
//  shipping
//
//  Created by ios on 2018. 3. 29..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "CommonObject.h"

@implementation CommonObject

/**
 당월 완료건 array
 
 @return 당월 완료건 array
 */
+ (NSArray *)ShippingMonthArray {
    return @[kTitleMoterizedBed, kTitleMoterizedBedEtc, kTitleRoyalFrame, kTitleRoyalFrameEtc,
             kTitleDelivery, kTitleChange, kTitleCancel, kTitleRelocationOfMonth1,
             kTitleRelocationOfMonth0, kTitleReinstall, kTitleAsOfMonth, kTitleFaultyOfMonth, kTitleEtcOfMonth];
}

/**
 검색 기준일 array (배송)

 @return 기준일 array
 */
+ (NSArray *)SearchShippingBaseDateTypeArray {
    return @[kTitleDivideDate, kTitleReceiveDate, kTitleDueDate];
}

/**
 검색 기준일 array (완료)

 @return 기준일 array
 */
+ (NSArray *)SearchCompletionBaseDateTypeArray {
    return @[kTitleSetupDate, kTitleDivideDate, kTitleReceiveDate, kTitleDueDate];
}

/**
 검색 기준일 array (회수)

 @return 기준일 array
 */
+ (NSArray *)SearchReturnBaseDateTypeArray {
    return @[kTitleReturnDate, kTitleDivideDate, kTitleDueDate, kTitleReceiveDate];
}

/**
 안마의자 배송상세 타이틀 array (배송)

 @return 타이틀 array
 */
+ (NSMutableArray *)DetailChairShippingArray {
    return [NSMutableArray arrayWithArray:@[kTitleBodyNo, kTitleContract, kTitleDivision, kTitleReceiveDate, kTitleCustomerNm, kTitleTelephone, kTitleCellPhone,
                                            kTitleAddress, kTitleProductDivision, kTitleProductNm, kTitlePurchaseOffice, kTitleQuantity,
                                            kTitleService, kTitleFreeGift1, kTitleFreeGift2, kTitleCompanyNote, kTitleNoteAdd, kTitleDivideState,
                                            kTitleDeliveryMan1, kTitleDeliveryMan2, kTitleDeliveryMan3, kTitleDueDate, kTitleCarSelection]];
}

/**
 안마의자 배송상세 타이틀 array (이전)
 
 @return 타이틀 array
 */
+ (NSMutableArray *)DetailChairMoveShippingArray {
    return [NSMutableArray arrayWithArray:@[kTitleBodyNo, kTitleContract, kTitleDivision, kTitleReceiveDate, kTitleCollectionNm, kTitleCollectionTelNo,
                                            kTitleCollectionPhoneNo, kTitleCollectionLocation, kTitleCollectionAddress, kTitleSetupNm,
                                            kTitleSetupTelNo, kTitleSetupPhoneNo, kTitleSetupLocation, kTitleSetupAddress, kTitleProductDivision,
                                            kTitleProductNm, kTitlePurchaseOffice, kTitleQuantity, kTitleService, kTitleFreeGift1, kTitleFreeGift2,
                                            kTitleCompanyNote, kTitleNoteAdd, kTitleDivideState, kTitleDeliveryMan1, kTitleDeliveryMan2,
                                            kTitleDeliveryMan3, kTitleDueDate, kTitleCarSelection]];
}

/**
 라텍스 배송상세 타이틀 array (배송)
 
 @return 타이틀 array
 */
+ (NSMutableArray *)DetailLatexShippingArray {
    return [NSMutableArray arrayWithArray:@[kTitleBodyNo, kTitleContract, kTitleDivision, kTitleReceiveDate, kTitleCustomerNm, kTitleTelephone, kTitleCellPhone,
                                            kTitleAddress, kTitleProductDivision, kTitleProductNm, kTitlePurchaseOffice, kTitleQuantity,
                                            kTitleService, kTitleLatexGift, kTitleFreeGift1, kTitleFreeGift2, kTitleCompanyNote, kTitleNoteAdd,
                                            kTitleDivideState, kTitleDeliveryMan1, kTitleDeliveryMan2, kTitleDeliveryMan3, kTitleDueDate,
                                            kTitleCarSelection]];
}

/**
 라텍스 배송상세 타이틀 array (이전)
 
 @return 타이틀 array
 */
+ (NSMutableArray *)DetailLatexMoveShippingArray {
    return [NSMutableArray arrayWithArray:@[kTitleBodyNo, kTitleContract, kTitleDivision, kTitleReceiveDate, kTitleCollectionNm, kTitleCollectionTelNo,
                                            kTitleCollectionPhoneNo, kTitleCollectionLocation, kTitleCollectionAddress, kTitleSetupNm,
                                            kTitleSetupTelNo, kTitleSetupPhoneNo, kTitleSetupLocation, kTitleSetupAddress, kTitleProductDivision,
                                            kTitleProductNm, kTitlePurchaseOffice, kTitleQuantity, kTitleService, kTitleLatexGift,
                                            kTitleFreeGift1, kTitleFreeGift2, kTitleCompanyNote, kTitleNoteAdd, kTitleDivideState,
                                            kTitleDeliveryMan1, kTitleDeliveryMan2, kTitleDeliveryMan3, kTitleDueDate, kTitleCarSelection]];
}

/**
 배송완료상세 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)DetailCompleteArray {
    return @[kTitleBodyNo, kTitleDivision, kTitleReceiveDate, kTitleCustomerNm, kTitleContactNo1, kTitleContactNo2,
             kTitleContactNo3, kTitleAddress, kTitleProductDivision, kTitleManuFacturer, kTitleProductNm,
             kTitlePurchaseOffice, kTitleQuantity, kTitleService, kTitleFreeGift1, kTitleFreeGift2, kTitleCompanyNote,
             kTitleDivideState, kTitleDeliveryMan1, kTitleDeliveryMan2, kTitleDeliveryMan3, kTitleDueDate];
}

/**
 이전설치-회수 상세 타이틀 array

 @return 타이틀 array
 */
+ (NSArray *)DetailReturnCompleteArray {
    return @[kTitleBodyNo, kTitleDivision, kTitleReceiveDate, kTitleCustomerNm, kTitleContactNo1, kTitleContactNo2,
             kTitleContactNo3, kTitleAddress, kTitleProductDivision, kTitleManuFacturer, kTitleProductNm,
             kTitlePurchaseOffice, kTitleQuantity, kTitleService, kTitleFreeGift1, kTitleFreeGift2, kTitleCompanyNote,
             kTitleDivideState, kTitleDeliveryMan1, kTitleDeliveryMan2, kTitleDeliveryMan3, kTitleDueDate,
             kTitleMoveSetupInfo, kTitleSetupNm, kTitleSetupTelNo, kTitleSetupPhoneNo, kTitleSetupAddress];
}

/**
 배송완료확인 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)DetailCompleteConfirmArray {
    return @[kTitleSetupDate, kTitleSetupState, kTitleSetupMemo, kTitleProductNo, kTitlePresentFreeGift1, kTitlePresentFreeGift2];
}

/**
 배송상세 제품설치 첨부 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)ProductSetupAttachArray {
    return @[kTitleProductNo, kTitleSetup];
}

/**
 배송상세 초도불량 첨부 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)ProductFaultyAttachArray {
    return @[kTitleProductNo, kTitleSetupTotal];
}

/**
 배송상세 계약철회 첨부 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)ProductCancelAttachArray {
    return @[kTitleProductNo, kTitleReturn];
}

/**
 배송상세 이전설치 첨부 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)ChangeProductAttachArray {
    return @[kTitleProductNo, kTitleSetup, kTitleSetupAttach];
}

/**
 배송상세 회수 첨부 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)ChangeProductReturnAttachArray {
    return @[kTitleReturnNo, kTitleReturn, kTitleReturnAttach];
}

/**
 배송상세 회수 첨부 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)ProductReturnAttachArray {
    return @[kTitleProductNo, kTitleReturn];
}

/**
 배송상세 첨부 타이틀 array (2개)
 
 @return 타이틀 array
 */
+ (NSArray *)ProductHalfAttachArray {
    return @[kTitleAttach1, kTitleAttach2];
}

/**
 배송상세 첨부 타이틀 array (4개)
 
 @return 타이틀 array
 */
+ (NSArray *)ProductAttachArray {
    return @[kTitleAttach1, kTitleAttach2, kTitleAttach3, kTitleAttach4];
}

/**
 배송상세 사다리 첨부 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)LadderAttachArray {
    return @[kTitleLadderAttach1, kTitleLadderAttach2, kTitleLadderAttach3];
}

/**
 배송상세 양중 첨부 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)CraneAttachArray {
    return @[kTitleCraneAttach1, kTitleCraneAttach2, kTitleCraneAttach3];
}

/**
 배송상세 내림서비스 첨부 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)ServiceAttachArray {
    return @[kTitleServiceAttach1, kTitleServiceAttach2];
}

/**
 배송상세 회수서비스 첨부 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)ReturnAttachArray {
    return @[kTitleReturnAttach1, kTitleReturnAttach2];
}

/**
 배송상세 사다리 결제구분 타이틀 array

 @return 타이틀 array
 */
+ (NSArray *)LadderPaymentTypeArray {
    return @[kTitlePaymentType, kTitleCard, kTitleAccount, kTitleCash];
}

/**
 배송상세 입금타입 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)DepositTypeArray {
    return @[kTitleDepositType, kTitleCard, kTitleCash, kTitleAccount];
}

/**
 사진 타입 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)PhotoTypeArray {
    return @[@"카메라", @"앨범"];
}

/**
 사진 타입 타이틀 array (바코드 포함)
 
 @return 타이틀 array
 */
+ (NSArray *)PhotoAddBarcodeTypeArray {
    return @[@"카메라", @"앨범", @"바코드"];
}

/**
 영수증 타입 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)ReceiptTypeArray {
    return @[@"핸드폰번호", @"카드번호"];
}

/**
 유/무상 타입 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)CostTypeArray {
    return @[@"무상", @"유상"];
}

/**
 출고지시서 제품 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)ReleaseProductNameArray {
    return @[@"안마의자", @"라클라우드", @"정수기", @"사은품"];
}

/**
 출고지시서 제품 코드 array
 
 @return 코드 array
 */
+ (NSArray *)ReleaseProductCodeArray {
    return @[@"A10000", @"L10000", @"W10000", @"Z10000"];
}

/**
 창고 입고 구분 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)WarehousingNameArray {
    return @[@"초도불량", @"미배송", @"계약철회(양품)", @"계약철회(불량)", @"고객출고요청", @"여유재고반납", @"자재이동요청", @"창고간이동", @"설치불가", @"설치후반품", @"수취거부", @"기안회수", @"양품전환", @"부분교체", @"맞교체", @"전시장회수(양품)", @"전시장회수(불량)", @"A/S기사 이동요청"];
}

/**
 창고 입고 구분 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)WarehousingCodeArray {
    return @[@"T02", @"T03", @"T04", @"T05", @"T06", @"T07", @"T08", @"T09", @"T10", @"T11", @"T12", @"T13", @"T14", @"T15", @"T16", @"T17", @"T18", @"T20"];
}

/**
 기사 입고 구분 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)DeliverymanMoveNameArray {
    return @[@"기사이동"];
}

/**
 기사 입고 구분 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)DeliverymanMoveCodeArray {
    return @[@"T01"];
}

/**
 약속시간 array
 
 @return 약속시간 array
 */
+ (NSArray *)PromiseTimeArray {
    return @[@"08:00 ~ 08:30", @"08:30 ~ 09:00", @"09:00 ~ 09:30", @"09:30 ~ 10:00", @"10:00 ~ 10:30", @"10:30 ~ 11:00", @"11:00 ~ 11:30", @"11:30 ~ 12:00", @"12:00 ~ 12:30", @"12:30 ~ 13:00", @"13:00 ~ 13:30", @"13:30 ~ 14:00", @"14:00 ~ 14:30", @"14:30 ~ 15:00", @"15:00 ~ 15:30", @"15:30 ~ 16:00", @"16:00 ~ 16:30", @"16:30 ~ 17:00", @"17:00 ~ 17:30", @"17:30 ~ 18:00", @"18:00 ~ 18:30", @"18:30 ~ 19:00", @"19:00 ~ 19:30", @"19:30 ~ 20:00"];
}

/**
 사이드메뉴 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)MenuListArray {
    return @[[NSMutableArray arrayWithObjects:kTitleMenuRefuel, kTitleMenuSale, kTitleMenuSaleHistory, kTitleMenuSchedule, kTitleMenuShippingList, kTitleMenuReturnList, kTitleMenuCompleteList, kTitleMenuRelease, kTitleMenuStock, nil],
    [NSMutableArray arrayWithObjects:kTitleMenuCarRegister, kTitleMenuCarClean, kTitleMenuProduct, kTitleMenuNotice, kTitleMenuSurvey, nil],
    [NSMutableArray arrayWithObjects:kTitleMenuLogout, nil]];
}

/**
 세차 첨부 타이틀 array
 
 @return 타이틀 array
 */
+ (NSArray *)CarCleanAttachArray {
    return @[@"전면", @"후면", @"휠", @"좌측면", @"우측면", @"내부"];
}

+ (NSArray *)RefuelListArray {
    return @[kTitleRefuelCard, kTitleRefuelLiter, kTitleRefuelPrice, kTitleRefuelMeter, kTitleRefuelDieselExhaustLiter, kTitleRefuelDieselExhaust, kTitleRefuelEtc];
}

+ (NSArray *)SaleTypeArray {
    return @[kTitleRental, kTitleSale];
}

+ (NSArray *)SaleNewTypeArray {
    return @[kTitleExistent, kTitleNew];
}

/**
 로딩 이미지 array

 @return 이미지 array
 */
+ (NSArray *)LoadingImageArray {
    return @[[UIImage imageNamed:@"progress_loading_01"], [UIImage imageNamed:@"progress_loading_02"], [UIImage imageNamed:@"progress_loading_03"], [UIImage imageNamed:@"progress_loading_04"], [UIImage imageNamed:@"progress_loading_05"],
             [UIImage imageNamed:@"progress_loading_06"], [UIImage imageNamed:@"progress_loading_07"], [UIImage imageNamed:@"progress_loading_08"], [UIImage imageNamed:@"progress_loading_09"], [UIImage imageNamed:@"progress_loading_10"],
             [UIImage imageNamed:@"progress_loading_11"], [UIImage imageNamed:@"progress_loading_12"]];
}

/**
 제품명에 따른 이미지 네임

 @param pName 제품명
 @return 이미지 네임
 */
+ (NSString *)imageNameFromProduct:(NSString *)pName {
    if([pName containsString:@"로보"]) {
        return IMAGE_PRODUCT_IROBO;
    } else if([pName containsString:@"팬텀"]) {
        if([pName containsString:@"브라운"]) {
            return IMAGE_PRODUCT_PHANTOM_BROWN;
        } else if([pName containsString:@"레드"]) {
            return IMAGE_PRODUCT_PHANTOM_RED;
        } else {
            return IMAGE_PRODUCT_PHANTOM_BLACK;
        }
    } else if([pName containsString:@"파라오"]) {
        if([pName containsString:@"S"]) {
            return IMAGE_PRODUCT_PHARAOH_S;
        }
        return IMAGE_PRODUCT_PHARAOH;
    } else if([pName containsString:@"프레지던트"]) {
        if([pName containsString:@"플러스"]) {
            return IMAGE_PRODUCT_PRESIDENT_PLUS;
        }
        return IMAGE_PRODUCT_PRESIDENT;
    } else if([pName containsString:@"렉스"]) {
        if([pName containsString:@"블랙"]) {
            return IMAGE_PRODUCT_REXL_BLACK;
        }
        return IMAGE_PRODUCT_REXL;
    } else if([pName containsString:@"레지나"]) {
        return IMAGE_PRODUCT_REGINA;
    } else if([pName containsString:@"프레임"]) {
        if([pName containsString:@"플로팅"]) {
            return IMAGE_PRODUCT_FRAME_FLOATING;
        } else if([pName containsString:@"초코"]) {
            return IMAGE_PRODUCT_FRAME_BLACK;
        } else {
            return IMAGE_PRODUCT_FRAME_WHITE;
        }
    } else if([pName containsString:@"크루즈"]) {
        return IMAGE_PRODUCT_NEWCRUZE;
    } else if([pName containsString:@"로즈"]) {
        return IMAGE_PRODUCT_ROSEMARY;
    } else if([pName containsString:@"셀레네"]) {
        return IMAGE_PRODUCT_SELENE;
    } else if([pName containsString:@"엘리자베스"]) {
        return IMAGE_PRODUCT_ELIZABETH;
    } else if([pName containsString:@"캡틴"]) {
        return IMAGE_PRODUCT_CAPTAIN;
    } else if([pName containsString:@"다스베이더"]) {
        if([pName containsString:@"오토만"]) {
            return IMAGE_PRODUCT_DARTH_AUTO;
        }
        return IMAGE_PRODUCT_DARTH;
    } else if([pName containsString:@"아이언맨"]) {
        return IMAGE_PRODUCT_IRONMAN;
    } else if([pName containsString:@"퍼스트레이디"]) {
        return IMAGE_PRODUCT_LADY;
    } else if([pName containsString:@"팰리스"]) {
        return IMAGE_PRODUCT_PALACE;
    } else if([pName containsString:@"스파이더맨"]) {
        return IMAGE_PRODUCT_SPIDERMAN;
    } else if([pName containsString:@"스톰"]) {
        return IMAGE_PRODUCT_STORM;
    } else if([pName containsString:@"러그"]) {
        return IMAGE_PRODUCT_RUG;
    } else if([pName containsString:@"공통"] ||
              [pName containsString:@"전동"]) {
        return IMAGE_PRODUCT_MAT;
    }
    return @"";
}

/**
 배송 타입에 따른 배송 구분 타이틀 (사용 안함)

 @param sType 배송 타입
 @param pNo 상태 타입
 @return 배송 구분 타이틀
 */
+ (NSString *)stringShippingType:(NSString *)sType pNo:(NSString *)pNo {
    NSMutableString *shippingType = [NSMutableString string];
    if([sType isEqualToString:@"D10"]) {
        shippingType.string = kTitleDelivery;
    } else if([sType isEqualToString:@"D20"]) {
        shippingType.string = kTitleChange;
    } else if([sType isEqualToString:@"D30"]) {
        shippingType.string = kTitleCancel;
    } else if([sType isEqualToString:@"P10"]) {
        shippingType.string = kTitleMoveRequest;
        if(![NSString isEmptyString:pNo]) {
            [shippingType appendString:@" - "];
            if([pNo isEqualToString:@"0"]) {
                [shippingType appendString:kTitleSetup];
            } else if([pNo isEqualToString:@"1"]) {
                [shippingType appendString:kTitleReturn];
            } else if([pNo isEqualToString:@"2"]) {
                [shippingType appendString:kTitleTransfer];
            }
        }
    } else if([sType isEqualToString:@"P20"]) {
        shippingType.string = kTitleASRequest;
    } else if([sType isEqualToString:@"D70"]) {
        shippingType.string = kTitleAS;
    } else if([sType isEqualToString:@"D90"]) {
        shippingType.string = kTitleReinstall;
    } else if([sType isEqualToString:@"D140"]) {
        shippingType.string = kTitleDraftReturn;
    }
    return shippingType;
}

/**
 배송 타입에 따른 배송 구분
 
 @param sType 배송 타입
 @param pNo 상태 타입
 @return 배송 구분
 */
+ (ShippingType)shippingType:(NSString *)sType pNo:(NSString *)pNo {
    if([sType isEqualToString:@"D10"]) {
        return kShippingTypeDelivery;
    } else if([sType isEqualToString:@"D20"]) {
        return kShippingTypeChange;
    } else if([sType isEqualToString:@"D21"]) {
        return kShippingTypePartChange;
    } else if([sType isEqualToString:@"D30"]) {
        return kShippingTypeCancel;
    } else if([sType isEqualToString:@"D40"]) {
        return kShippingTypeDraft;
    } else if([sType isEqualToString:@"P10"]) {
        if([pNo isEqualToString:@"0"]) {
            return kShippingTypeMoveSetup;
        } else if([pNo isEqualToString:@"1"]) {
            return kShippingTypeMoveReturn;
        }
        return kShippingTypeMoveRequest;
    } else if([sType isEqualToString:@"P20"]) {
        return kShippingTypeASRequest;
    } else if([sType isEqualToString:@"D70"]) {
        return kShippingTypeAS;
    } else if([sType isEqualToString:@"D80"]) {
        return kShippingTypeGift;
    } else if([sType isEqualToString:@"D90"]) {
        return kShippingTypeReinstall;
    } else if([sType isEqualToString:@"D50"]) {
        return kShippingTypeDisplaySetup;
    } else if([sType isEqualToString:@"D130"]) {
        return kShippingTypeDisplayReturn;
    } else if([sType isEqualToString:@"D140"]) {
        return kShippingTypeDraftReturn;
    } else if([sType isEqualToString:@"SV10"]) {
        return kShippingTypeDecomposition;
    } else if([sType isEqualToString:@"SV11"]) {
        return kShippingTypeAssemble;
    }
    return kShippingTypeNone;
}

/**
 배송타입에 따른 구분 (ex: 이전요청 - 설치)

 @param sType 배송 타입
 @param pNo 진행 번호
 @return 배송구분
 */
+ (NSString *)divisionFromShippingType:(NSString *)sType pNo:(NSString *)pNo {
    NSMutableString *division = [NSMutableString stringWithString:sType];
    if(![NSString isEmptyString:pNo]) {
        [division appendString:@" - "];
        if([pNo isEqualToString:@"0"]) {
            [division appendString:kTitleSetup];
        } else if([pNo isEqualToString:@"1"]) {
            [division appendString:kTitleReturn];
        } else if([pNo isEqualToString:@"2"]) {
            [division appendString:kTitleTransfer];
        }
    }
    return division;
}

/**
 기준일 타이틀에 따른 파라미터 값

 @return 기준일 타이틀
 */
+ (NSInteger)baseDateTypeFromTitle:(NSString *)title {
    if([title isEqualToString:kTitleDivideDate]) {
        return 0;
    } else if([title isEqualToString:kTitleReceiveDate]) {
        return 1;
    } else if([title isEqualToString:kTitleDueDate]) {
        return 2;
    } else if([title isEqualToString:kTitleReturnDate] ||
              [title isEqualToString:kTitleSetupDate]) {
        return 3;
    }
    return 0;
}

/**
 창고명에 따른 코드 값

 @param type 출고지시서 데이터 타입
 @param name 창고명
 @return 코드 값
 */
+ (NSString *)codeFromReleaseDataType:(ReleaseDataType)type name:(NSString *)name {
    NSArray *nameArray = nil;
    NSArray *codeArray = nil;
    if(type == kReleaseDataTypeProduct) {
        nameArray = [CommonObject ReleaseProductNameArray];
        codeArray = [CommonObject ReleaseProductCodeArray];
    } else if(type == kReleaseDataTypeWarehouseMove) {
        nameArray = [CommonObject WarehousingNameArray];
        codeArray = [CommonObject WarehousingCodeArray];
    } else if(type == kReleaseDataTypeDeliverymanMove) {
        nameArray = [CommonObject DeliverymanMoveNameArray];
        codeArray = [CommonObject DeliverymanMoveCodeArray];
    }
    return codeArray[[nameArray indexOfObject:name]];
}

/**
 코드 값에 따른 코드명

 @param datas 검색할 데이터
 @param code 코드 값
 @return 코드명
 */
+ (NSString *)codeNamesFromCode:(NSArray *)datas code:(NSString *)code {
    for(CodeResultData *data in datas) {
        if([data.detCd isEqualToString:code]) {
            return data.detCdNm;
        }
    }
    return @"";
}

/**
 코드명에 따른 코드 값

 @param datas 검색할 데이터
 @param codeName 코드명
 @return 코드 값
 */
+ (NSString *)codeFromData:(NSArray *)datas codeName:(NSString *)codeName {
    for(CodeResultData *data in datas) {
        if([data.detCdNm isEqualToString:codeName]) {
            return data.detCd;
        }
    }
    return @"";
}

/**
 코드 타이틀 array

 @param datas 검색할 데이터
 @return 코드 타이틀 array
 */
+ (NSArray *)codeNamesFromData:(NSArray *)datas {
    NSMutableArray *restoreDatas = [NSMutableArray array];
    for(CodeResultData *data in datas) {
        [restoreDatas addObject:data.detCdNm];
    }
    return restoreDatas;
}

+ (NSArray *)titlesFromData:(NSArray <ProductResultData>*)datas {
    NSMutableArray *titles = [NSMutableArray array];
    for(ProductResultData *data in datas) {
        [titles addObject:data.productName];
    }
    return titles;
}

/**
 외주 업체 여부
 
 @return 외주 업체 여부
 */
+ (BOOL)isOutsourcing {
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    if([userInfo.perCd isEqualToString:@"105"]) {
        return YES;
    }
    return NO;
}

@end
