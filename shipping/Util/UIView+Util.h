//
//  UIView+Util.h
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Util)

- (void)removeAllSubviews;

- (void)applyRoundBorder:(CGFloat)radius;
- (void)applyRoundBorder:(CGFloat)radius color:(UIColor *)color width:(CGFloat)width;
- (void)applyAutoLayoutFromSuperview:(UIView *)superview;

- (UIImage *)imageCaptureView;

@end
