//
//  UIButton+Util.h
//  shipping
//
//  Created by ios on 2018. 3. 23..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Util)

@property (nonatomic) NSIndexPath *indexPath;
@property (nonatomic) NSString *stringTag;

+ (void)selectBtnGroup:(UIButton *)sBtn group:(NSArray *)group;
+ (UIButton *)selectedBtn:(NSArray *)group;
@end
