//
//  UIImageView+Util.m
//  shipping
//
//  Created by ios on 2018. 4. 13..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "UIImageView+Util.h"

@implementation UIImageView (Util)


/**
 url을 통한 이미지 설정

 @param url url
 */
- (void)setImageWithBaseUrl:(NSString *)url {
    // url에 '/svc'가 있으면 지움
    url = [url stringByReplacingOccurrencesOfString:@"/svc" withString:@""];
    NSString *fullUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, [NSString encodeUrl:url]];
    [self setImageWithURL:[NSURL URLWithString:fullUrl]];
}

/**
 이미지 설정 후 이미지뷰의 높이 설정

 @param url url
 */
- (void)setImageWithUrlForDynamic:(NSString *)url {
    // url에 '/svc'가 있으면 지움
    url = [url stringByReplacingOccurrencesOfString:@"/svc" withString:@""];
    NSString *fullUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, [NSString encodeUrl:url]];
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:fullUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    __block UIImageView *iv = self;
    [self setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            CGFloat ratio = image.size.height / image.size.width;
            // 이미지의 가로 길이에 따라 비율로 높이를 정함.
            [iv mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(@(ratio * self.bounds.size.width));
            }];
            iv.image = image;
        });
    } failure:nil];
}

@end
