//
//  CommonUtil.m
//  shipping
//
//  Created by ios on 2018. 3. 23..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "CommonUtil.h"

@implementation CommonUtil

/**
 오늘 날짜 스트링 형태로 반환

 @param format 데이트 포맷
 @return 오늘 날짜 스트링
 */
+ (NSString *)todayFromDateFormat:(NSString *)format {
    return [self stringFromDate:[NSDate date] format:format];
}

/**
 데이트 포맷에 따른 데이트 스트링

 @param date 데이트
 @param format 데이트 포맷
 @return 데이트 스트링
 */
+ (NSString *)stringFromDate:(NSDate *)date format:(NSString *)format {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    return [formatter stringFromDate:date];
}

/**
 데이트 포맷 변경

 @param preFormat 이전 데이트 포맷
 @param dateString 데이트 스트링
 @param changeFormat 변경할 데이트 포맷
 @return 변경된 데이트 스트링
 */
+ (NSString *)changeDateFormat:(NSString *)preFormat dateString:(NSString *)dateString changeFormat:(NSString *)changeFormat {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:preFormat];
    NSDate *date = [formatter dateFromString:dateString];
    [formatter setDateFormat:changeFormat];
    return [formatter stringFromDate:date];
}


/**
 스트링형을 데이트형으로 변경

 @param date 데이트 스트링
 @param format 데이트 포맷
 @return 변경된 데이트형
 */
+ (NSDate *)dateFromString:(NSString *)date format:(NSString *)format {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    return [formatter dateFromString:date];
}

/**
 해당 월의 1일 반환

 @param date 데이트
 @return 해당 월의 1일 데이트
 */
+ (NSDate *)firstDateFromFromDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:date];
    [components setDay:1];
    return [calendar dateFromComponents:components];
}

/**
 해당 데이트로 부터 한달 전 데이트 반환

 @param date 데이트
 @return 한달 전 데이트
 */
+ (NSDate *)preOneMonthDateFromDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setMonth:-1];
    return [calendar dateByAddingComponents:components toDate:date options:0];
}

/**
 오늘 날짜로부터 몇일 후 날짜

 @param afterDay 몇일 후
 @return 날짜
 */
+ (NSDate *)afterDayFromToday:(NSInteger)afterDay {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:afterDay];
    return [calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
}

/**
 해당 날짜의 일수

 @param date 날짜
 @return 날짜 일수
 */
+ (NSInteger)numberOfDaysInMonth:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSRange range = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date];
    return range.length;
}

/**
 오늘 날짜 요일 반환

 @return 요일
 */
+ (NSInteger)weekdayFromToday {
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* dc = [cal components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    return [dc weekday];
}

/**
 테이블뷰 갱신 후 최상단

 @param tableView 테이블뷰
 */
+ (void)reloadTableViewToTop:(UITableView *)tableView {
    [UIView animateWithDuration:0 animations:^{
        [tableView setContentOffset:CGPointZero];
    } completion:^(BOOL finished) {
        [tableView reloadData];
    }];
}

/**
 전화번호 링크

 @param num 전화번호
 */
+ (void)openLinkCall:(NSString *)num {
    NSString *fullNum = [NSString stringWithFormat:@"tel://%@", num];
    dispatch_async(dispatch_get_main_queue(), ^{
        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:fullNum]]) {
            if (@available(iOS 10.0, *)) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fullNum] options:@{} completionHandler:nil];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fullNum]];
            }
        }
    });
}

/**
 웹주소 링크

 @param url 웹주소
 */
+ (void)openLinkWeb:(NSString *)url {
    NSString *fullUrl = url;
    if(![url hasPrefix:@"http"]) {
        fullUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, url];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:fullUrl]]) {
            if(@available(iOS 10.0, *)) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fullUrl] options:@{} completionHandler:nil];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fullUrl]];
            }
        }
    });
    
    if([url isEqualToString:APP_DOWNLOAD_URL]) { // 앱 업데이트 이동 시 강제 종료
        [self exitApplication];
    }
}

/**
 앱 설정 페이지
 */
+ (void)openAppSetting {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (@available(iOS 10.0, *)) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    });
}

/**
 앱 강제 종료
 */
+ (void)exitApplication {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        exit(0);
    });
}

/**
 메인 스토리보드에 해당 ID의 뷰컨트롤러 반환

 @param storyboardId 뷰컨트롤러 스토리보드 ID
 @return 해당 뷰컨트롤러
 */
+ (id)controllerFromMainStoryboard:(NSString *)storyboardId {
    return [[UIStoryboard storyboardWithName:STORYBOARD_MAIN_NAME bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:storyboardId];
}

/**
 하단 액션시트 보여짐

 @param datas 타이틀 데이터
 @param target 보여질 뷰컨트롤러
 @param selected 아이템 선택시 블럭함수
 */
+ (void)showActionSheet:(NSArray *)datas target:(UIViewController *)target selected:(void (^)(NSInteger index))selected {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    for(int i = 0; i < datas.count; i++) {
        NSString *data = datas[i];
        UIAlertAction *action = [UIAlertAction actionWithTitle:data
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                           selected(i);
                                                       }];
        [alert addAction:action];
    }
    [alert addAction:[UIAlertAction actionWithTitle:@"닫기"
                                              style:UIAlertActionStyleCancel
                                            handler:nil]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [target presentViewController:alert animated:YES completion:nil];
    });
}

/**
 alert 보여짐

 @param title 타이틀
 @param msg 메세지
 @param type alert 타입 (one, two)
 @param target 보여질 뷰컨트롤러
 @param cAction 확인버튼 선택시 블럭함수
 */
+ (void)showAlertTitle:(NSString *)title
                   msg:(NSString *)msg
                  type:(AlertType)type
                target:(UIViewController *)target
               cAction:(void (^)(void))cAction {
    [self showAlertTitle:title
                     msg:msg
                    type:type
                  cancel:@"취소"
                 confirm:@"확인"
                  target:target
            cancelAction:nil confirmAction:cAction];
}

/**
 alert 보여짐

 @param title 타이틀
 @param msg 메세지
 @param type alert 타입 (one, two)
 @param cancel 취소 타이틀
 @param confirm 확인 타이틀
 @param target 보여질 뷰컨트롤러
 @param ccAction 취소버튼 선택시 블럭함수
 @param cfAction 확인버튼 선택시 블럭함수
 */
+ (void)showAlertTitle:(NSString *)title
                   msg:(NSString *)msg
                  type:(AlertType)type
                cancel:(NSString *)cancel
               confirm:(NSString *)confirm
                target:(UIViewController *)target
          cancelAction:(void (^)(void))ccAction
         confirmAction:(void (^)(void))cfAction {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    if(type == kAlertTypeTwo) {
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancel
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 if(ccAction != nil) {
                                                                     ccAction();
                                                                 }
                                                             }];
        [alert addAction:cancelAction];
    }
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:confirm
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * _Nonnull action) {
                                                              if(cfAction != nil) {
                                                                  cfAction();
                                                              }
                                                          }];
    [alert addAction:confirmAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [target presentViewController:alert animated:YES completion:nil];
    });
}

/**
 윈도우에 토스트 메세지 보여짐

 @param message 메세지
 */
+ (void)showToastMessage:(NSString *)message {
    [self hideKeyboard];
    [[APP_DELEGATE window] makeToast:message duration:1.0 position:CSToastPositionBottom];
}

/**
 슈퍼뷰 가로 너비에 따른 해당 뷰의 너비

 @param superview 슈퍼뷰
 @param count 해당 뷰의 갯수
 @param padding 해당 뷰간의 간격
 @return 해당 뷰의 너비
 */
+ (NSInteger)widthFromSuperview:(UIView *)superview count:(NSInteger)count padding:(CGFloat)padding {
    return (superview.bounds.size.width - (padding * (count-1))) / count;
}

/**
 키보드 내림
 */
+ (void)hideKeyboard {
    [[APP_DELEGATE window] endEditing:YES];
}

/**
 사이드 메뉴 보여짐
 */
+ (void)showSideMenu {
    LGSideMenuController *smC = (LGSideMenuController *)[APP_DELEGATE window].rootViewController;
    [smC showLeftViewAnimated];
}

/**
 사이드 메뉴 숨김
 */
+ (void)hideSideMenu {
    LGSideMenuController *smC = (LGSideMenuController *)[APP_DELEGATE window].rootViewController;
    [smC hideLeftViewAnimated];
}

/**
 탭바 선택

 @param index 인덱스
 */
+ (void)selectTabBarController:(NSInteger)index {
    [APP_DELEGATE tabBarC].selectedIndex = index;
}

/**
 디바이스 비율을 적용한 전체 너비 값

 @return 너비 값
 */
+ (CGFloat)widthToFullscreen {
    return [APP_DELEGATE window].bounds.size.width * [[UIScreen mainScreen] scale];
}

/**
 앨범에 이미지 저장 요청

 @param image 저장할 이미지
 */
+ (void)requestSaveImage:(UIImage *)image {
    NSString *title = [NSBundle mainBundle].infoDictionary[(__bridge NSString *)kCFBundleNameKey];
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.predicate = [NSPredicate predicateWithFormat:@"localizedTitle = %@", title];
    PHFetchResult *fetchResult = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:fetchOptions];
    if(fetchResult.count > 0) {
        [self saveImageIntoAlbum:image collection:fetchResult.firstObject];
    } else {
        __block PHObjectPlaceholder *albumPlaceholder;
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetCollectionChangeRequest *changeRequest = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:title];
            albumPlaceholder = changeRequest.placeholderForCreatedAssetCollection;
        } completionHandler:^(BOOL success, NSError *error) {
            if(success) {
                PHFetchResult *fetchResult = [PHAssetCollection fetchAssetCollectionsWithLocalIdentifiers:@[albumPlaceholder.localIdentifier] options:nil];
                [self saveImageIntoAlbum:image collection:fetchResult.firstObject];
            } else {
                NSLog(@"error : %@", error);
                [CommonUtil showToastMessage:error.description];
            }
        }];
    }
}

/**
 앨범에 이미지 저장

 @param image 저장할 이미지
 @param collection 앨범 collection
 */
+ (void)saveImageIntoAlbum:(UIImage *)image collection:(PHAssetCollection *)collection {
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:image];
        PHAssetCollectionChangeRequest *assetCollectionChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:collection];
        [assetCollectionChangeRequest addAssets:@[[assetChangeRequest placeholderForCreatedAsset]]];
    } completionHandler:^(BOOL success, NSError *error) {
        if(!success) {
            NSLog(@"Error creating asset: %@", error);
        }
    }];
}

/**
 바코드 생성

 @param string 입력 문자열
 @return 바코드 이미지
 */
+ (CIImage *)generateBarcode:(NSString *)string {
    CIFilter *barCodeFilter = [CIFilter filterWithName:@"CICode128BarcodeGenerator"];
    NSData *barCodeData = [string dataUsingEncoding:NSASCIIStringEncoding];
    [barCodeFilter setValue:barCodeData forKey:@"inputMessage"];
    [barCodeFilter setValue:[NSNumber numberWithFloat:0] forKey:@"inputQuietSpace"];
    
    CIImage *barCodeImage = barCodeFilter.outputImage;
    return barCodeImage;
}

/**
 세션이 종료 되어 로그인페이지로 이동시
 */
+ (void)openLoginViewController:(UIViewController *)target {
    LoginViewController *loginVC = [CommonUtil controllerFromMainStoryboard:STORYBOARD_LOGIN_ID];
    loginVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    loginVC.preVC = target;
    [target presentViewController:loginVC animated:YES completion:nil];
}
@end
