//
//  CommonUtil.h
//  shipping
//
//  Created by ios on 2018. 3. 23..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <Foundation/Foundation.h>

#define APP_DELEGATE (AppDelegate *)[[UIApplication sharedApplication] delegate]

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define UIColorFromRGBA(rgbValue, a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

#define DEGREES_TO_RADIANS(degrees)((M_PI * degrees)/180)

#ifdef DEBUG
#define NSLog( s, ... ) NSLog( @"<%@:%d> %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__,  [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define NSLog( s, ... )
#endif

typedef enum {
    kAlertTypeOne,
    kAlertTypeTwo
} AlertType;

@interface CommonUtil : NSObject

+ (NSString *)todayFromDateFormat:(NSString *)format;
+ (NSString *)stringFromDate:(NSDate *)date format:(NSString *)format;
+ (NSString *)changeDateFormat:(NSString *)preFormat dateString:(NSString *)dateString changeFormat:(NSString *)changeFormat;
+ (NSDate *)dateFromString:(NSString *)date format:(NSString *)format;
+ (NSDate *)firstDateFromFromDate:(NSDate *)date;
+ (NSDate *)preOneMonthDateFromDate:(NSDate *)date;
+ (NSDate *)afterDayFromToday:(NSInteger)afterDay;
+ (NSInteger)numberOfDaysInMonth:(NSDate *)date;
+ (NSInteger)weekdayFromToday;

+ (void)reloadTableViewToTop:(UITableView *)tableView;

+ (void)openLinkCall:(NSString *)num;
+ (void)openLinkWeb:(NSString *)url;
+ (void)openAppSetting;
+ (void)exitApplication;

+ (id)controllerFromMainStoryboard:(NSString *)storyboardId;

+ (void)showActionSheet:(NSArray *)datas target:(UIViewController *)target selected:(void (^)(NSInteger index))selected;
+ (void)showAlertTitle:(NSString *)title msg:(NSString *)msg type:(AlertType)type target:(UIViewController *)target cAction:(void (^)(void))cAction;
+ (void)showAlertTitle:(NSString *)title msg:(NSString *)msg type:(AlertType)type cancel:(NSString *)cancel confirm:(NSString *)confirm target:(UIViewController *)target cancelAction:(void (^)(void))ccAction confirmAction:(void (^)(void))cfAction;
+ (void)showToastMessage:(NSString *)message;

+ (NSInteger)widthFromSuperview:(UIView *)superview count:(NSInteger)count padding:(CGFloat)padding;

+ (void)hideKeyboard;

+ (void)showSideMenu;
+ (void)hideSideMenu;
+ (void)selectTabBarController:(NSInteger)index;

+ (void)requestSaveImage:(UIImage *)image;
+ (CGFloat)widthToFullscreen;
+ (CIImage *)generateBarcode:(NSString *)string;
+ (void)openLoginViewController:(UIViewController *)target;
@end
