//
//  UserManager.h
//  shipping
//
//  Created by ios on 2018. 4. 16..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserManager : NSObject

+ (void)saveData:(NSObject *)data key:(NSString *)key;
+ (void)removeDataForKey:(NSString *)key;
+ (id)loadUserData:(NSString *)key;
+ (void)removeUserData;
@end
