//
//  ApiManager.m
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ApiManager.h"

@implementation ApiManager

/**
 POST 요청 (로딩뷰)

 @param url url
 @param parameters parameters
 @param success 성공시 블럭함수
 @param failure 실패시 블럭함수
 */
+ (void)requestToPost:(NSString *)url
           parameters:(NSDictionary *)parameters
              success:(void (^)(id responseObject))success
              failure:(void (^)(NSString *errorMsg))failure {
    [self requestToPost:url parameters:parameters isLoading:YES success:success failure:failure];
}

/**
 POST 요청

 @param url url
 @param parameters parameters
 @param isLoading 로딩 여부
 @param success 성공시 블럭함수
 @param failure 실패시 블럭함수
 */
+ (void)requestToPost:(NSString *)url
           parameters:(NSDictionary *)parameters
            isLoading:(BOOL)isLoading
              success:(void (^)(id responseObject))success
              failure:(void (^)(NSString *errorMsg))failure {

    dispatch_async(dispatch_get_main_queue(), ^{
        [CommonUtil hideKeyboard];
        if(isLoading) {
            [LoadingView showLoadingView];
        }
    });
    
    NSString *fullUrl = url;
    if(![url hasPrefix:@"http"]) {
        fullUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, url];
    }
    NSLog(@"url : %@", fullUrl);
    NSLog(@"parameters : %@", parameters);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    [manager POST:fullUrl parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        ResultModel *result = [[ResultModel alloc] initWithDictionary:responseObject error:nil];
        if([result.resultCode isEqualToString:LOGIN_SESSION_CODE]) {
            if(![url containsString:LOGIN_URL]) {
                UINavigationController *naviC = [APP_DELEGATE tabBarC].selectedViewController;
                UIViewController *baseVC = naviC.topViewController.presentedViewController;
                if(baseVC == nil) { // presentedViewController 가 존재 하지 않으면
                    baseVC = naviC.topViewController;
                }
                [CommonUtil showToastMessage:kDescReLoginNeed];
                [CommonUtil openLoginViewController:baseVC];
            }
        } else if([result.resultCode isEqualToString:CONNECT_FAIL_CODE]) {
            if(failure != nil) {
                failure(result.resultMsg);
            } else {
                NSLog(@"failure");
            }
        } else {
            if(success != nil) {
                success(responseObject);
            } else {
                NSLog(@"success");
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error : %@", error);
        if(isLoading) {
            [CommonUtil showToastMessage:kDescConnectFailed];
            [LoadingView hideLoadingView];
        }
    }];
}

+ (void)requestToGoogleSheet:(NSString *)url
                  parameters:(NSDictionary *)parameters
                     success:(void (^)(id responseObject))success
                     failure:(void (^)(NSString *errorMsg))failure {
    dispatch_async(dispatch_get_main_queue(), ^{
        [CommonUtil hideKeyboard];
        [LoadingView showLoadingView];
    });
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(((NSHTTPURLResponse *)task.response).statusCode == 200) {
            success(@"200");
        } else {
            [CommonUtil showToastMessage:kDescConnectFailed];
            [LoadingView hideLoadingView];
        }
    }];
}

/**
 POST 요청 (Body에 파라미터 추가, 인증시에만 사용)
 
 @param url url
 @param parameters parameters
 @param success 성공시 블럭함수
 @param failure 실패시 블럭함수
 */
+ (void)requestToCertification:(NSString *)url
                    parameters:(NSDictionary *)parameters
                       success:(void (^)(id responseObject))success
                       failure:(void (^)(NSString *errorMsg))failure {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [CommonUtil hideKeyboard];
        [LoadingView showLoadingView];
    });
    
    NSString *fullUrl = url;
    if(![url hasPrefix:@"http"]) {
        fullUrl = [NSString stringWithFormat:@"%@%@", BASE_CERTIFICATION_URL, url];
    }
    NSLog(@"url : %@", fullUrl);
    NSLog(@"parameters : %@", parameters);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:CERTIFICATION_SERVICE_CODE forHTTPHeaderField:@"serviceCode"];
    [manager.requestSerializer setValue:CERTIFICATION_SECRET_KEY forHTTPHeaderField:@"secretKey"];
    
    [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString *(NSURLRequest *request, NSDictionary *parameters, NSError *__autoreleasing *error) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return jsonString;
    }];
    
    [manager POST:fullUrl parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        CertificationData *result = [[CertificationData alloc] initWithDictionary:responseObject error:nil];
        if([result.status.code isEqualToString:CONNECT_SUCCESS_CODE]) {
            success(responseObject);
        } else {
            failure(result.status.message);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error : %@", error);
        [CommonUtil showToastMessage:kDescConnectFailed];
        [LoadingView hideLoadingView];
    }];
}

/**
 MULTIPART 요청

 @param url url
 @param parameters parameters
 @param uploadImages 업로드 할 이미지
 @param success 성공시 블럭함수
 @param failure 실패시 블럭함수
 */
+ (void)requestToMultipart:(NSString *)url
                parameters:(NSMutableDictionary *)parameters
               uploadImages:(NSDictionary *)uploadImages
                   success:(void (^)(id responseObject))success
                   failure:(void (^)(NSString *errorMsg))failure {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [LoadingView showLoadingView];
    });
    
    NSString *fullUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, url];
    NSLog(@"url : %@", fullUrl);
    NSLog(@"parameters : %@", parameters);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    [manager POST:fullUrl parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for(NSString *key in uploadImages.allKeys) {
            UIImage *image = uploadImages[key];
            if(![image isEqual:[NSNull null]]) {
                NSData* imageData = UIImageJPEGRepresentation(image, 0.5);
                NSString* fileName = [NSString stringWithFormat:@"%@.jpg", key];
                [formData appendPartWithFileData:imageData name:key fileName:fileName mimeType:@"image/jpeg"];
            }
        }
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        ResultModel *result = [[ResultModel alloc] initWithDictionary:responseObject error:nil];
        if([result.resultCode isEqualToString:CONNECT_FAIL_CODE]) {
            failure(result.resultMsg);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error : %@", error);
        [CommonUtil showToastMessage:kDescConnectFailed];
        [LoadingView hideLoadingView];
    }];
}

@end
