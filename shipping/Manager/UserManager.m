//
//  UserManager.m
//  shipping
//
//  Created by ios on 2018. 4. 16..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "UserManager.h"

@implementation UserManager

/**
 UserDefaults에 저장

 @param data 데이터
 @param key 키
 */
+ (void)saveData:(NSObject *)data key:(NSString *)key {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:data forKey:key];
    [userDefaults synchronize];
}

/**
 UserDefaults애서 제거 (배송상세 작성 데이터)
 
 @param key 키
 */
+ (void)removeDataForKey:(NSString *)key {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:key];
    [userDefaults synchronize];
}

/**
 UserDefaults 로드

 @param key 키 값
 @return 데이터
 */
+ (id)loadUserData:(NSString *)key {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if([key isEqualToString:USER_DATA_KEY]) {
        return [[UserResultData alloc] initWithDictionary:[userDefaults objectForKey:key] error:nil];
    } else if([key isEqualToString:USER_CAR_KEY]) {
        return [[CarResultData alloc] initWithDictionary:[userDefaults objectForKey:key] error:nil];
    }
    return [userDefaults objectForKey:key];
}

/**
 UserDefaults 값 초기화
 */
+ (void)removeUserData {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:USER_DATA_KEY];
    [userDefaults removeObjectForKey:USER_CAR_KEY];
    [userDefaults removeObjectForKey:USER_REGISTER_KEY];
    [userDefaults synchronize];
}
@end
