//
//  ApiManager.h
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApiManager : NSObject

+ (void)requestToPost:(NSString *)url
           parameters:(NSDictionary *)parameters
              success:(void (^)(id responseObject))success
              failure:(void (^)(NSString *errorMsg))failure;

+ (void)requestToPost:(NSString *)url
           parameters:(NSDictionary *)parameters
            isLoading:(BOOL)isLoading
              success:(void (^)(id responseObject))success
              failure:(void (^)(NSString *errorMsg))failure;

+ (void)requestToGoogleSheet:(NSString *)url
                  parameters:(NSDictionary *)parameters
                     success:(void (^)(id responseObject))success
                     failure:(void (^)(NSString *errorMsg))failure;
    
+ (void)requestToCertification:(NSString *)url
                    parameters:(NSDictionary *)parameters
                       success:(void (^)(id responseObject))success
                       failure:(void (^)(NSString *errorMsg))failure;

+ (void)requestToMultipart:(NSString *)url
                parameters:(NSDictionary *)parameters
              uploadImages:(NSDictionary *)uploadImages
                   success:(void (^)(id responseObject))success
                   failure:(void (^)(NSString *errorMsg))failure;
@end
