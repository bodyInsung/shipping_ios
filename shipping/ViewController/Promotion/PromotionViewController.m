//
//  PromotionViewController.m
//  shipping
//
//  Created by insung on 11/12/2018.
//  Copyright © 2018 bodyfiend. All rights reserved.
//

#import "PromotionViewController.h"
#import <WebKit/WebKit.h>

@interface PromotionViewController ()

@end

@implementation PromotionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initLayout {
    [super initLayoutForTitle:TITLE_PROMOTION_ACTIONBAR];
    
    WKWebView *webV = [[WKWebView alloc] init];
    [_contentV addSubview:webV];
    
    [webV applyAutoLayoutFromSuperview:_contentV];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:PROMOTION_URL]];
    [webV loadRequest:request];
}

@end
