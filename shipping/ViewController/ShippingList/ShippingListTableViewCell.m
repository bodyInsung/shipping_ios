//
//  ShippingListTableViewCell.m
//  shipping
//
//  Created by ios on 2018. 3. 21..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ShippingListTableViewCell.h"

@implementation ShippingListTableViewCell

- (void)awakeFromNib {
     [super awakeFromNib];
     [self initLayout];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
     [super setSelected:selected animated:animated];
     
     // Configure the view for the selected state
}

- (void)initLayout {
     [_statusV applyRoundBorder:5.0];
     [_timeBtn applyRoundBorder:10.0];
}

/**
 데이터에 따른 셀 설정
 
 @param data 데이터
 */
- (void)setCellFromData:(ShippingResultData *)data {
     NSString *imageName = [CommonObject imageNameFromProduct:data.productName];
     _productIV.image = [UIImage imageNamed:imageName];
     _receiveDateLb.text = data.receiveDate;
     if([data.shippingType hasPrefix:kTitleMove]) {
          if([data.progressNo isEqualToString:@"1"]) {
               _nameLb.text = data.agreeName;
               _addressLb.text = data.addr;
          } else {
               _nameLb.text = data.custName;
               _addressLb.text = data.addr2;
          }
     } else {
          _nameLb.text = data.custName;
          _addressLb.text = data.insAddr;
     }
     _productLb.text = data.productName;
     _starIV.hidden = !data.mustFlag;
     
     if([data.custUpdate isEqualToString:@"AD"]) {
          _backV.backgroundColor = UIColorFromRGB(0xF9F6D0);
     } else {
          _backV.backgroundColor = [UIColor whiteColor];
     }
     
     NSMutableString *gift = [NSMutableString string];
     NSString *giftOne = data.freegiftOne;
     NSString *giftTwo = data.freegiftTwo;
     if([NSString isEmptyString:giftOne] == NO ||
        [NSString isEmptyString:giftTwo] == NO) { // 증정품이 있으면
          [gift appendString:giftOne];
          if([NSString isEmptyString:giftTwo] == NO) { // 증정품2가 있으면
               if([NSString isEmptyString:giftOne] == NO) {
                    [gift appendString:@", "];
               }
               [gift appendString:giftTwo];
          }
          _giftHeightLC.active = NO;
     } else {
          _giftHeightLC.active = YES;
     }
     _giftLb.text = gift;
     
     NSString *promise = nil;
     if([NSString isEmptyString:data.promise]) {
          promise = kTitleUndifine;
     } else {
          if([data.promise boolValue]) {
               promise = [CommonUtil changeDateFormat:@"HHmm" dateString:data.promiseTime changeFormat:@"HH:mm"];
          } else {
               promise = kTitlePromiseImpossible;
          }
     }
     [_timeBtn setTitle:promise forState:UIControlStateNormal];
     // promiseTime 값이 nil 경우만 노출 하지 않음 (nil이 아닌 빈값의 경우는 미정)
     _timeBtn.hidden = data.promiseTime == nil;
     
     UIColor *statusColor = UIColorFromRGB(0x8F82BC);
     if([data.shippingType hasPrefix:kTitleMove]) {
          if([data.progressNo isEqualToString:@"0"]) {
               _progressLb.text = kTitleSetupReturn;
               if(data.relocationCheck.boolValue) {
                    _completeLb.textColor = [UIColor blueColor];
                    _completeLb.text = kTitleReturnComplte;
               } else {
                    _completeLb.textColor = [UIColor redColor];
                    _completeLb.text = kTitleReturnIncomplte;
               }
          } else if([data.progressNo isEqualToString:@"1"]) {
               _progressLb.text = kTitleReturn;
          } else if([data.progressNo isEqualToString:@"2"]) {
               _progressLb.text = kTitleTransfer;
          } else {
               _progressLb.text = @"";
          }
          statusColor = UIColorFromRGB(0xBC8282);
     } else if([data.shippingType containsString:kTitleChange]) {
          statusColor = UIColorFromRGB(0x8298BC);
     } else if([data.shippingType containsString:kTitleCancel]) {
          statusColor = UIColorFromRGB(0xBCB082);
     } else if([data.shippingType containsString:kTitleAS] ||
               [data.shippingType containsString:kTitleASRequest] ||
               [data.shippingType containsString:kTitleReinstall]) {
          statusColor = UIColorFromRGB(0x9DBC80);
     }
     _statusV.backgroundColor = statusColor;
     _statusLb.text = data.shippingType;
     
     NSMutableString *note = [NSMutableString stringWithString:data.companyCheck];
     NSString *symptom = data.symptom;
     if(![NSString isEmptyString:symptom]) {
          if(![NSString isEmptyString:note]) {
               [note appendString:@"<br>"];
          }
          [note appendFormat:@"증상 : %@", symptom];
     }
     _descriptionLb.attributedText = [NSString applyHtmlText:note fontSize:_descriptionLb.font.pointSize];
}

- (IBAction)pressedTime:(UIButton *)sender {
     _timeHandler();
}

- (IBAction)pressedCall:(UIButton *)sender {
     _callHandler();
}

- (IBAction)pressedMap:(UIButton *)sender {
     _mapHandler();
}

- (IBAction)pressedMessage:(UIButton *)sender {
     _messageHandler();
}

- (void)prepareForReuse {
     [super prepareForReuse];
     _productIV.image = nil;
     _nameLb.text = nil;
     _productLb.text = nil;
     _addressLb.text = nil;
     _progressLb.text = nil;
     _completeLb.text = nil;
     _statusLb.text = nil;
     _descriptionLb.text = nil;
}

@end
