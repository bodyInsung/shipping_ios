//
//  ShippingListViewController.m
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ShippingListViewController.h"
#import "ShippingListHeaderView.h"
#import "ShippingListTableViewCell.h"
#import "ListMapViewController.h"
#import "MapViewController.h"
#import "ShippingDetailViewController.h"
#import "CompletionDetailViewController.h"

static NSString *kShippingListHeaderIdentifier = @"ShippingListHeader";
static NSString *kShippingListCellIdentifier = @"ShippingListCell";

@interface ShippingListViewController ()

@end

@implementation ShippingListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestCode:SEARCH_AREA_CODE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:STORYBOARD_SHIPPING_DETAIL_SEGUE]) {
        ShippingDetailViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        ShippingResultData *data = (ShippingResultData *)sender;
        vc.shippingSeq = data.shippingSeq;
    } else if([[segue identifier] isEqualToString:STORYBOARD_COMPLETION_DETAIL_SEGUE]) {
        CompletionDetailViewController *vc = [segue destinationViewController];
        ShippingResultData *data = (ShippingResultData *)sender;
        vc.shippingSeq = data.shippingSeq;
    } else if([[segue identifier] isEqualToString:STORYBOARD_SHIPPING_LIST_MAP_SEGUE]) {
        ListMapViewController *vc = [segue destinationViewController];
        ShippingResultData *data = (ShippingResultData *)sender;
        vc.address = data.insAddr;
    } else if([[segue identifier] isEqualToString:STORYBOARD_REASON_POPUP_SEGUE]) {
        ReasonPopupViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        ShippingResultData *data = (ShippingResultData *)sender;
        vc.shippingSeq = data.shippingSeq;
    }
}

- (void)reloadViewController {
    [self requestCode:SEARCH_AREA_CODE];
}

- (void)initLayout {
    [super initLayout];
    
    if(_searchType == kSearchTypeNone) { // default : shipping
        _searchType = kSearchTypeShipping;
    }
    if(_searchType == kSearchTypeSchedule) { // 스케쥴을 통해 진입시
        [super initLayoutForTitle:[CommonUtil stringFromDate:_selectedDate format:DATE_HYPHEN_YEAR_MONTH_DAY]];
    } else {
        if(_searchType == kSearchTypeReturn) {
            [super initLayoutForTitle:TITLE_RETURN_LIST_ACTIONBAR];
        } else {
            [super initLayoutForTitle:TITLE_SHIPPING_LIST_ACTIONBAR];
        }
        [self showSearchButton];
        [self showMapButton];
    }
    
    [self setBaseSV:_listTbV];
    
    UINib *headerNib = [UINib nibWithNibName:@"ShippingListHeaderView" bundle:nil];
    [_listTbV registerNib:headerNib forHeaderFooterViewReuseIdentifier:kShippingListHeaderIdentifier];
    UINib *cellNib = [UINib nibWithNibName:@"ShippingListTableViewCell" bundle:nil];
    [_listTbV registerNib:cellNib forCellReuseIdentifier:kShippingListCellIdentifier];
    
    _listTbV.estimatedSectionHeaderHeight = UITableViewAutomaticDimension;
    
    [self addSearchView];
    [self addPullToRefresh];
    
    inputTF = [[CTTextField alloc] init];
    [self.view addSubview:inputTF];
    inputTF.accessoryDelegate = self;
}

/**
 검색 뷰 추가
 */
- (void)addSearchView {
    [self hideSearchView];
    
    searchV = [[ListSearchView alloc] init];
    searchV.delegate = self;
    searchV.target = self;
    [_searchBackV addSubview:searchV];
    [searchV applyAutoLayoutFromSuperview:_searchBackV];
}

/**
 당겨서 새로고침 추가
 */
- (void)addPullToRefresh {
    UIRefreshControl *pullToRC = [[UIRefreshControl alloc] init];
    [pullToRC addTarget:self action:@selector(refreshRequestData:) forControlEvents:UIControlEventValueChanged];
    [_listTbV addSubview:pullToRC];
}

/**
 코드 요청

 @param code 요청할 코드 값
 */
- (void)requestCode:(NSString *)code {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:kProductTypeMassage forKey:@"s_producttype"];
    [parameters setValue:code forKey:@"code"];
    
    [ApiManager requestToPost:SEARCH_CODE_URL parameters:parameters success:^(id responseObject) {
        NSArray<CodeResultData> *codeData = (NSArray<CodeResultData> *)[CodeResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        if([code isEqualToString:SEARCH_AREA_CODE]) {
            [searchV initValueFromData:codeData type:_searchType date:_selectedDate];
            [self requestList:[searchV sendParameters]];
        }
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 리스트 요청 (회수, 해당 날짜, 배송)

 @param parameters 설정된 파라미터
 */
- (void)requestList:(NSMutableDictionary *)parameters {
    if(parameters != nil) {
        [parameters setValue:@"N" forKey:@"s_oneself"];
    }
    NSString *url = nil;
    if(_searchType == kSearchTypeReturn) { // 회수 리스트
        url = RETURN_LIST_URL;
    } else if(_selectedDate != nil) { // 캘린더 날짜 검색
        url = CALENDAR_SHIPPING_LIST_URL;
    } else { // 배송 리스트
        url = SHIPPING_LIST_URL;
    }
    [ApiManager requestToPost:url parameters:parameters success:^(id responseObject) {
        [self restoreResponseData:responseObject];
        [_listTbV reloadData];
        [LoadingView hideLoadingViewToDelay];        
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [CommonUtil reloadTableViewToTop:_listTbV];
        [LoadingView hideLoadingView];
    }];
}

/**
 약속 시간, 지정일 변경 요청
 
 @param parameters 설정된 파라미터
 @param isRecord 등록여부
 */
- (void)requestModify:(NSMutableDictionary *)parameters isRecord:(BOOL)isRecord {
    NSArray *keys = parameters.allKeys;
    if([keys containsObject:parameters[@"m_shippingseq"]] &&
       [keys containsObject:parameters[@"m_promisetime"]] &&
       [keys containsObject:parameters[@"m_promise"]]) {
        NSString *logData = [NSString stringWithFormat:@"%@, %@, %@", parameters[@"m_shippingseq"], parameters[@"m_promisetime"], parameters[@"m_promise"]];
        // 앱로그 전송
        [self requestAppLog:logData type:kAppLogTypeShipping];
    }
    
    NSString *url = nil;
    if(isRecord) {
        url = SHIPPING_DUE_DATE_MODIFY_URL;
    } else {
        url = SHIPPING_MODIFY_URL;
    }
    
    [ApiManager requestToPost:url parameters:parameters success:^(id responseObject) {
        [CommonUtil showToastMessage:kDescModifySuccess];
        [self requestList:[searchV sendParameters]];
        [LoadingView hideLoadingViewToDelay];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [CommonUtil reloadTableViewToTop:_listTbV];
        [LoadingView hideLoadingView];
    }];
}

/**
 데이터 재정렬 (약속 시간)

 @param responseObject responseData
 */
- (void)restoreResponseData:(id)responseObject {
    if(shippingListArray != nil) {
        [shippingListArray removeAllObjects];
        shippingListArray = nil;
    }
    
    shippingListArray = [NSMutableArray array];
    NSMutableArray *responseList = [NSMutableArray arrayWithArray:responseObject[API_RESPONSE_KEY][@"list"]];
    while(responseList.count > 0) {
        NSSortDescriptor *descriptorDate = [NSSortDescriptor sortDescriptorWithKey:@"DUE_DATE" ascending:YES comparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            if([obj1 isEqualToString:@""]) { // 미지정은 내림차순
                return NSOrderedDescending;
            } else {
                return NSOrderedAscending;
            }
        }];
        NSMutableArray *filterArray = [NSMutableArray arrayWithArray:[responseList sortedArrayUsingDescriptors:@[descriptorDate]]];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"DUE_DATE = %@", filterArray.firstObject[@"DUE_DATE"]];
        [filterArray filterUsingPredicate:predicate];
        NSSortDescriptor *descriptorTime = [NSSortDescriptor sortDescriptorWithKey:@"PROMISE_TIME" ascending:YES];
        NSArray *sortedArray = [filterArray sortedArrayUsingDescriptors:@[descriptorTime]];
        NSMutableArray *shippingList = [ShippingResultData arrayOfModelsFromDictionaries:sortedArray error:nil];
        [responseList removeObjectsInArray:sortedArray];
        [shippingListArray addObject:shippingList];
    }
}

/**
 지정일 변경 불가능 알럿

 @param title 타이틀
 @param dueDate 지정일
 */
- (void)showAlertDueDateImpossible:(NSString *)title dueDate:(NSString *)dueDate {
    NSString *message = [CommonUtil changeDateFormat:DATE_HYPHEN_YEAR_MONTH_DAY dateString:dueDate changeFormat:@"MM/dd"];
    message = [NSString stringWithFormat:kDescDateImpossibleChange, message];
    [CommonUtil showAlertTitle:title msg:message type:kAlertTypeOne target:self cAction:nil];
}

#pragma mark - Action Event
- (void)refreshRequestData:(UIRefreshControl *)sender {
    [self requestList:[searchV sendParameters]];
    [sender endRefreshing];
}

- (IBAction)pressedRetry:(UIButton *)sender {
    [self requestList:[searchV sendParameters]];
}

- (void)pressedTime:(ShippingResultData *)data {
    NSMutableArray *settingArray = [NSMutableArray array];
    if([NSString isEmptyString:data.promise]) {
        if([NSString isEmptyString:data.promiseTime]) { // 약속 시간이 미정인 경우
            [settingArray addObject:kTitlePromiseTimeSet];
            [settingArray addObject:kTitlePromiseImpossible];
        } else { // 약속 시간이 정해진 경우
            [settingArray addObject:kTitlePromiseTimeChange];
            [settingArray addObject:kTitlePromiseCancel];
        }
    } else {
        if(![data.promise boolValue]) { // 약속 불가인 경우
            [settingArray addObject:kTitleImpossibleContent];
            [settingArray addObject:kTitlePromiseImpossibleCancel];
        } else { // 약속 시간이 정해진 경우
            [settingArray addObject:kTitlePromiseTimeChange];
            [settingArray addObject:kTitlePromiseCancel];
        }
    }
    if([NSString isEmptyString:data.dueDate]) { // 지정일이 미정인 경우
        [settingArray addObject:kTitleDueDateSet];
    } else { // 지정일이 지정된 경우
        [settingArray addObject:kTitleDueDateCancel];
        [settingArray addObject:kTitleDueDateChange];
    }

    [CommonUtil showActionSheet:settingArray target:self selected:^(NSInteger index) {
        NSString *title = settingArray[index];
        if([title hasPrefix:kTitlePromiseTime]) {
            [inputTF showPickerView:self];
        } else if([title isEqualToString:kTitlePromiseCancel]) {
            [CommonUtil showAlertTitle:title msg:kDescPromiseCancelQuestion type:kAlertTypeTwo target:self cAction:^{
                NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
                [parameters setValue:data.shippingSeq forKey:@"m_shippingseq"];
                [parameters setValue:@"" forKey:@"m_promisetime"];
                [parameters setValue:@"" forKey:@"m_promise"];
                [parameters setValue:data.promiseFailCd forKey:@"m_promisefailcd"];
                [parameters setValue:data.promiseFailPs forKey:@"m_promisefailps"];
                [self requestModify:parameters isRecord:NO];
            }];
        } else if([title isEqualToString:kTitleImpossibleContent]) {
            [CommonUtil showAlertTitle:title msg:data.promiseFailPs type:kAlertTypeOne target:self cAction:nil];
        } else if([title isEqualToString:kTitlePromiseImpossible]) {
            [self openViewController:STORYBOARD_REASON_POPUP_SEGUE sender:data];
        } else if([title isEqualToString:kTitlePromiseImpossibleCancel]) {
            [CommonUtil showAlertTitle:title msg:kDescPromiseImpossibleCancelQuestion type:kAlertTypeTwo target:self cAction:^{
                NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
                [parameters setValue:data.shippingSeq forKey:@"m_shippingseq"];
                [parameters setValue:@"" forKey:@"m_promisetime"];
                [parameters setValue:@"" forKey:@"m_promise"];
                [parameters setValue:data.promiseFailCd forKey:@"m_promisefailcd"];
                [parameters setValue:data.promiseFailPs forKey:@"m_promisefailps"];
                [self requestModify:parameters isRecord:NO];
            }];
        } else if([title isEqualToString:kTitleDueDateSet] ||
                  [title isEqualToString:kTitleDueDateChange]) {
            if([data.custUpdate isEqualToString:@"AD"]) {
                [self showAlertDueDateImpossible:title dueDate:data.dueDate];
            } else {
                [inputTF showDatePicker:kDatePickerDisableTypeMinimum];
            }
        } else if([title isEqualToString:kTitleDueDateCancel]) {
            if([data.custUpdate isEqualToString:@"AD"]) {
                [self showAlertDueDateImpossible:title dueDate:data.dueDate];
            } else {
                [CommonUtil showAlertTitle:title msg:kDescDateCancelQuestion type:kAlertTypeTwo target:self cAction:^{
                    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
                    [parameters setValue:data.shippingSeq forKey:@"m_shippingseq"];
                    [parameters setValue:@"" forKey:@"m_duedate"];
                    [self requestModify:parameters isRecord:YES];
                }];
            }
        }
    }];
}

- (void)pressedCall:(ShippingResultData *)data {
    NSString *phoneNum = nil;
    NSString *custName = nil;
    if([data.shippingType hasPrefix:kTitleMove] &&
       [data.progressNo isEqualToString:@"1"]) {
        phoneNum = data.hPhoneNo2;
        custName = data.agreeName;
    } else {
        if(![NSString isEmptyString:data.hPhoneNo]) {
            phoneNum = data.hPhoneNo;
        } else if(![NSString isEmptyString:data.telNo]) {
            phoneNum = data.telNo;
        }
        custName = data.custName;
    }
    
    if([NSString isEmptyString:phoneNum]) {
        [CommonUtil showToastMessage:kDescPhoneNumberNeed];
    } else {
        NSString *logData = [NSString stringWithFormat:@"%@, %@, %@", data.shippingSeq, custName, phoneNum];
        [self requestAppLog:logData type:kAppLogTypeCall];
        [CommonUtil openLinkCall:phoneNum];
    }
}

- (void)pressedMessage:(ShippingResultData *)data {
    NSString *phoneNum = nil;
    NSString *custName = nil;
    if([data.shippingType hasPrefix:kTitleMove] &&
       [data.progressNo isEqualToString:@"1"]) {
        phoneNum = data.hPhoneNo2;
        custName = data.agreeName;
    } else {
        if(![NSString isEmptyString:data.hPhoneNo]) {
            phoneNum = data.hPhoneNo;
        } else if(![NSString isEmptyString:data.telNo]) {
            phoneNum = data.telNo;
        }
        custName = data.custName;
    }
    
    if([NSString isEmptyString:phoneNum]) {
        [CommonUtil showToastMessage:kDescPhoneNumberNeed];
    } else if([NSString isEmptyString:data.dueDate]) {
        [CommonUtil showToastMessage:kDescPromiseDateNeed];
    } else if([NSString isEmptyString:data.promiseTime]) {
        [CommonUtil showToastMessage:kDescPromiseTimeNeed];
    } else {
        [LoadingView showLoadingView];
        dispatch_async(dispatch_get_main_queue(), ^{
            if([MFMessageComposeViewController canSendText]) {
                [self removeKeyboardObserver];
                MFMessageComposeViewController *messageComposeVC = [[MFMessageComposeViewController alloc] init];
                messageComposeVC.messageComposeDelegate = self;
                messageComposeVC.recipients = @[phoneNum];
                
                NSString *date = [CommonUtil changeDateFormat:DATE_HYPHEN_YEAR_MONTH_DAY dateString:data.dueDate changeFormat:DATE_KOREAN_MONTH_DAY];
                NSString *startTime = [CommonUtil changeDateFormat:@"HHmm" dateString:data.promiseTime changeFormat:@"a hh시 mm분"];
                NSString *sinceTime = [NSString stringWithFormat:@"%d", data.promiseTime.intValue + 200];
                NSString *endTime = [CommonUtil changeDateFormat:@"HHmm" dateString:sinceTime changeFormat:@"a hh시 mm분"];
                NSString *promiseTime = [NSString stringWithFormat:@"%@. %@~%@", date, startTime, endTime];
                NSString *message = [NSString stringWithFormat:kDescShippingMsg, custName, data.deliveryMan, promiseTime];
                [messageComposeVC setBody:message];
                
                [self presentViewController:messageComposeVC animated:YES completion:^{
                    [LoadingView hideLoadingView];
                }];
            } else {
                [CommonUtil showToastMessage:kDescMessageCant];
                [LoadingView hideLoadingView];
            }
        });
    }
}

#pragma mark - ActionBarDelegate
- (void)showSearchView {
    _searchVHeightLC.active = NO;
    [self.view layoutIfNeeded];
}

- (void)hideSearchView {
    [CommonUtil hideKeyboard];
    _searchVHeightLC.active = YES;
    [self.view layoutIfNeeded];
}

- (void)pressedMap {
    [self openViewController:STORYBOARD_SHIPPING_MAP_SEGUE sender:self];
}

#pragma mark - ReasonPopupViewDelegate
/**
 뷰컨트롤러 리로드 토스트 메세지 보여짐
 
 @param msg 메세지
 */
- (void)reloadViewController:(NSString *)msg {
    [CommonUtil showToastMessage:msg];
    [self requestList:[searchV sendParameters]];
}

#pragma mark - ListSearchDelegate
- (void)pressedSearch {
    [self requestList:[searchV sendParameters]];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    tableView.hidden = !([shippingListArray count] > 0);
    _noDataV.hidden = !tableView.isHidden;
    return [shippingListArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *listArray = shippingListArray[section];
    return [listArray count];
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    ShippingListHeaderView *headerV = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kShippingListHeaderIdentifier];
    [headerV setHeaderFromData:shippingListArray[section]];
    return headerV;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ShippingListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kShippingListCellIdentifier];
    ShippingResultData *data = shippingListArray[indexPath.section][indexPath.row];
    cell.timeHandler = ^{
        indexPathOfModify = indexPath;
        [self pressedTime:data];
    };
    cell.callHandler = ^{
        [self pressedCall:data];
    };
    cell.mapHandler = ^{
        [self openViewController:STORYBOARD_SHIPPING_LIST_MAP_SEGUE sender:data];
    };
    cell.messageHandler = ^{
        [self pressedMessage:data];
    };
    
    [cell setCellFromData:data];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ShippingResultData *data = shippingListArray[indexPath.section][indexPath.row];
    if([data.insComplete isEqualToString:@"ZZ"]) {
        [self openViewController:STORYBOARD_COMPLETION_DETAIL_SEGUE sender:data];
    } else {
        [self openViewController:STORYBOARD_SHIPPING_DETAIL_SEGUE sender:data];
    }
}

#pragma mark - MFMessageComposeViewControllerDelegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    if(result == MessageComposeResultSent) {
        [CommonUtil showToastMessage:kDescSentMessage];
    }
    [self addKeyboardObserver];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIPickerViewDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSArray *timeArray = [CommonObject PromiseTimeArray];
    return timeArray.count;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSArray *timeArray = [CommonObject PromiseTimeArray];
    return timeArray[row];
}

#pragma mark - CTTextFieldDelegate
- (void)pressedDone:(id)sender {
    ShippingResultData *data = shippingListArray[indexPathOfModify.section][indexPathOfModify.row];
    if([sender isKindOfClass:[UIPickerView class]]) {
        NSArray *timeArray = [CommonObject PromiseTimeArray];
        UIPickerView *pv = (UIPickerView *)sender;
        NSInteger index = [pv selectedRowInComponent:0];
        NSString *time = [timeArray[index] componentsSeparatedByString:@"~"][0];
        time = [CommonUtil changeDateFormat:@"HH:mm" dateString:time changeFormat:@"HHmm"];
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:data.shippingSeq forKey:@"m_shippingseq"];
        [parameters setValue:time forKey:@"m_promisetime"];
        [parameters setValue:@"Y" forKey:@"m_promise"];
        [self requestModify:parameters isRecord:NO];
    } else if([sender isKindOfClass:[UIDatePicker class]]) {
        UIDatePicker *dp = (UIDatePicker *)sender;
        NSString *date = [CommonUtil stringFromDate:[dp date] format:DATE_HYPHEN_YEAR_MONTH_DAY];
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:data.shippingSeq forKey:@"m_shippingseq"];
        [parameters setValue:date forKey:@"m_duedate"];
        [self requestModify:parameters isRecord:YES];
    }
}

@end
