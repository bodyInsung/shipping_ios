//
//  DetailValueChangeTableViewCell.m
//  shipping
//
//  Created by ios on 2018. 3. 30..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "DetailValueChangeTableViewCell.h"

@implementation DetailValueChangeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initLayout];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initLayout {
    [_changeBtn applyRoundBorder:4.0];
}

- (void)setCellFromData:(NSString *)data title:(NSString *)title {
    _titleLb.text = title;
    
    if([title hasPrefix:kTitleDeliveryMan]) {
        _valueLb.text = data;
        NSString *btnTitle = nil;
        if([NSString isEmptyString:data]) {
            btnTitle = kTitleDeliveryAdd;
        } else {
            btnTitle = kTitleDeliveryChange;
        }
        [_changeBtn setTitle:btnTitle forState:UIControlStateNormal];
    } else if([title isEqualToString:kTitleCarSelection]) {
        CarResultData *carData = [UserManager loadUserData:USER_CAR_KEY];
        _valueLb.text = carData.carNumber;
        [_changeBtn setTitle:kTitleCarChange forState:UIControlStateNormal];
    } else if([title isEqualToString:kTitleProductCode] ||
              [title isEqualToString:kTitleGiftCode]) {
        [_changeBtn setTitle:data forState:UIControlStateNormal];
    } else if([title isEqualToString:kTitleReturnWarehouse]) {
        _valueLb.text = data;
        [_changeBtn setTitle:kTitleWarehouse forState:UIControlStateNormal];
    }
}

- (IBAction)pressedChnage:(UIButton *)sender {
    _changeHandler();
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _titleLb.text = nil;
    _valueLb.text = nil;
}

@end
