//
//  ShippingDetailViewController.m
//  shipping
//
//  Created by ios on 2018. 3. 28..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ShippingDetailViewController.h"
#import "DetailBaseTableViewCell.h"
#import "DetailLinkTextTableViewCell.h"
#import "DetailTextInputTableViewCell.h"
#import "DetailValueChangeTableViewCell.h"
#import "DetailButtonTableViewCell.h"
#import "DetailTitleTableViewCell.h"
#import "DetailCodeConfirmTableViewCell.h"
#import "DetailGiftConfirmTableViewCell.h"
#import "SignPopupViewController.h"
#import "BarcodeViewController.h"
#import "ProductValidationData.h"

static NSString *kDetailBaseCellIdentifier = @"BaseCell";
static NSString *kDetailLinkTextCellIdentifier = @"LinkTextCell";
static NSString *kDetailTextInputCellIdentifier = @"TextInputCell";
static NSString *kDetailValueChangeCellIdentifier = @"ValueChangeCell";
static NSString *kDetailButtonCellIdentifier = @"ButtonCell";
static NSString *kDetailTitleCellIdentifier = @"TitleCell";
static NSString *kDetailCodeCellIdentifier = @"CodeCell";
static NSString *kDetailGiftCellIdentifier = @"GiftCell";

@interface ShippingDetailViewController ()

@end

@implementation ShippingDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestCode:CONTRACTOR_CODE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:STORYBOARD_LIST_POPUP_DELIVERYMAN_SEGUE]) {
        ListPopupViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.datas = sender;
        vc.isSearch = YES;
        vc.popupType = kListPopupTypeDeliveryman;
    } else if([[segue identifier] isEqualToString:STORYBOARD_LIST_POPUP_CAR_SEGUE]) {
        ListPopupViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.datas = sender;
        vc.isSearch = YES;
        vc.popupType = kListPopupTypeCar;
    } else if([[segue identifier] isEqualToString:STORYBOARD_LIST_POPUP_WAREHOUSE_SEGUE]) {
        ListPopupViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.datas = sender;
        vc.isSearch = YES;
        vc.popupType = kListPopupTypeWarehouse;
    } else if([[segue identifier] isEqualToString:STORYBOARD_LIST_POPUP_STOCK_SEGUE]) {
        ListPopupViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.datas = sender;
        vc.isSearch = YES;
        vc.popupType = kListPopupTypeStock;
    } else if([[segue identifier] isEqualToString:STORYBOARD_LIST_POPUP_PRODUCT_SEGUE]) {
        ListPopupViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.datas = sender;
        vc.isSearch = YES;
        vc.popupType = kListPopupTypeProduct;
    } else if([[segue identifier] isEqualToString:STORYBOARD_SIGN_POPUP_SEGUE]) {
        SignPopupViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        NSString *name = nil;
        if([resultData.progressNo isEqualToString:@"1"]) {
            name = resultData.agreeName;
        } else {
            name = resultData.custName;
        }
        vc.custName = name;
        vc.datas = sender;
    } else if([[segue identifier] isEqualToString:STORYBOARD_INPUT_POPUP_SEGUE]) {
        InputPopupViewController *vc = [segue destinationViewController];
        vc.delegate = self;
    } else if([[segue identifier] isEqualToString:STORYBOARD_BARCODE_SEGUE]) {
        BarcodeViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.isUseImage = selectedPhotoV != nil;
    }
}

- (void)reloadViewController {
    [self requestCode:CONTRACTOR_CODE];
}

- (void)initValue {
    ladderBtnGroup = @[_ladderUseBtn, _ladderUnuseBtn];
    craneBtnGroup = @[_craneUseBtn, _craneUnuseBtn];
    setupBtnGroup = @[_setupCompleteBtn, _faultyBtn, _impossibleBtn, _rejectBtn, _returnBtn, _cancelBtn];
    housingBtnGroup = @[_apartBtn, _villaBtn, _houseBtn, _shareBtn];
    serviceBtnGroup = @[_addCostBtn, _noneCostBtn];
    returnBtnGroup = @[_returnUseBtn, _returnUnuseBtn];
    
    uploadImages = [NSMutableDictionary dictionary];
}

- (void)initLayout {
    [super initLayoutForTitle:TITLE_SHIPPING_DETAIL_ACTIONBAR];
    
    [self showDetailView:0];
    
    UINib *baseCellNib = [UINib nibWithNibName:@"DetailBaseTableViewCell" bundle:nil];
    [_detailTbV registerNib:baseCellNib forCellReuseIdentifier:kDetailBaseCellIdentifier];
    UINib *linkTextCellNib = [UINib nibWithNibName:@"DetailLinkTextTableViewCell" bundle:nil];
    [_detailTbV registerNib:linkTextCellNib forCellReuseIdentifier:kDetailLinkTextCellIdentifier];
    UINib *textInputCellNib = [UINib nibWithNibName:@"DetailTextInputTableViewCell" bundle:nil];
    [_detailTbV registerNib:textInputCellNib forCellReuseIdentifier:kDetailTextInputCellIdentifier];
    UINib *valueChangeCellNib = [UINib nibWithNibName:@"DetailValueChangeTableViewCell" bundle:nil];
    [_detailTbV registerNib:valueChangeCellNib forCellReuseIdentifier:kDetailValueChangeCellIdentifier];
    UINib *buttonCellNib = [UINib nibWithNibName:@"DetailButtonTableViewCell" bundle:nil];
    [_detailTbV registerNib:buttonCellNib forCellReuseIdentifier:kDetailButtonCellIdentifier];
    UINib *titleCellNib = [UINib nibWithNibName:@"DetailTitleTableViewCell" bundle:nil];
    [_detailTbV registerNib:titleCellNib forCellReuseIdentifier:kDetailTitleCellIdentifier];
    UINib *codeCellNib = [UINib nibWithNibName:@"DetailCodeConfirmTableViewCell" bundle:nil];
    [_detailTbV registerNib:codeCellNib forCellReuseIdentifier:kDetailCodeCellIdentifier];
    UINib *giftCellNib = [UINib nibWithNibName:@"DetailGiftConfirmTableViewCell" bundle:nil];
    [_detailTbV registerNib:giftCellNib forCellReuseIdentifier:kDetailGiftCellIdentifier];
    
    _detailTbV.estimatedSectionHeaderHeight = UITableViewAutomaticDimension;
    
    [self setConfirmView];
}

/**
 작성 중인 이미지가 있었는지 체크
 */
- (void)checkRegisterData {
    NSDictionary *registerData = [UserManager loadUserData:USER_REGISTER_KEY];
    if(registerData != nil) {
        [CommonUtil showAlertTitle:@"" msg:kDescReloadNeed type:kAlertTypeTwo cancel:@"아니요" confirm:@"네" target:self cancelAction:^{
            [self setReloadData:NO];
        } confirmAction:^{
            [self setReloadData:YES];
        }];
    } else {
        [self setReloadData:NO];
    }
}

/**
 데이터 리로드 여부

 @param isReload 리로드 여부
 */
- (void)setReloadData:(BOOL)isReload {
    if(isReload) { // 리로드시
        NSDictionary *registerData = [UserManager loadUserData:USER_REGISTER_KEY];
        selectedPhotoDatas = [[NSMutableDictionary alloc] initWithDictionary:registerData];
    } else { // 리로드 아닐시
        selectedPhotoDatas = [NSMutableDictionary dictionary];
    }
    [self setGroupAttachView:NO];
    [UserManager removeDataForKey:USER_REGISTER_KEY];
}

/**
 설치확인서 설정
 */
- (void)setConfirmView {
    [_attachBackV applyRoundBorder:4.0];
    [_ladderBackV applyRoundBorder:4.0];
    [_craneBackV applyRoundBorder:4.0];
    [_serviceBackV applyRoundBorder:4.0];
    [_returnBackV applyRoundBorder:4.0];
    [_setupMemoTV applyRoundBorder:4.0 color:UIColorFromRGB(0xB5B5B5) width:0.4];
    [_signBtn applyRoundBorder:4.0];
    [_registerBtn applyRoundBorder:4.0];
    
    [_ladderPriceTF setAccessoryView];
    [_addCostTF setAccessoryView];
    [_costMemoTF setAccessoryView];
    [_setupMemoTV setAccessoryView];
    [_productNoTF setAccessoryView];
    [_returnNoTF setAccessoryView];
    [_asTotalCostTF setAccessoryView];
    [_receiptTF setAccessoryView];
    [_depositTF setAccessoryView];
    
    [self pressedGroupLadder:_ladderUnuseBtn];
    [self pressedGroupCrane:_craneUseBtn];
    [self pressedGroupService:_addCostBtn];
    [self pressedGroupReturn:_returnUseBtn];
    
    [self setPaymentType:0];
}

/**
 코드 리퀘스트

 @param code 코드네임
 */
- (void)requestCode:(NSString *)code {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:code forKey:@"code"];
    [ApiManager requestToPost:SEARCH_CODE_URL parameters:parameters success:^(id responseObject) {
        if([code isEqualToString:CONTRACTOR_CODE]) { // 본사외주
            if(contractorCodeDatas != nil) {
                contractorCodeDatas = nil;
            }
            contractorCodeDatas = [CodeResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
            [self requestCode:SEARCH_AREA_CODE];
        } else if([code isEqualToString:SEARCH_AREA_CODE]) { // 지역코드
            if(areaCodeDatas != nil) {
                areaCodeDatas = nil;
            }
            areaCodeDatas = [CodeResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
            [self requestCode:SHIPPING_DIVISION_CODE];
        } else if ([code isEqualToString:SHIPPING_DIVISION_CODE]) { // 배송구분
            if(divisionCodeDatas != nil) {
                divisionCodeDatas = nil;
            }
            divisionCodeDatas = [CodeResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
            [self requestDetail];
        } else if([code isEqualToString:TERMS_CODE]) { // 약관데이터
            NSArray *codeDatas = [CodeResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
            [self openViewController:STORYBOARD_SIGN_POPUP_SEGUE sender:[self restoreResponseData:codeDatas]];
            [LoadingView hideLoadingView];
        }
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 상세 데이터 요청
 */
- (void)requestDetail {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"n" forKey:@"deleteFlag"];
    [parameters setValue:_shippingSeq forKey:@"shipping_seq"];
    [ApiManager requestToPost:DETAIL_URL parameters:parameters success:^(id responseObject) {
        if(resultData != nil) {
            resultData = nil;
        }
        resultData = [[DetailResultData alloc] initWithDictionary:responseObject[API_RESPONSE_KEY] error:nil];
        if(resultData == nil) {
            [self requestFailed];
        } else {
            [self setDetailTitles];
            [self checkRegisterData];
            [_detailTbV reloadData];
        }
        [LoadingView hideLoadingViewToDelay];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
        [self requestFailed];
    }];
}

/**
 라텍스 증정품 코드
 */
- (void)requestLatexGiftCode {
    [ApiManager requestToPost:LATEX_GIFT_CODE_URL parameters:nil success:^(id responseObject) {
        NSArray *datas = [ProductResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        NSMutableArray *names = [NSMutableArray array];
        for(ProductResultData *data in datas) {
            [names addObject:data.productName];
        }
        
        [CommonUtil showActionSheet:names target:self selected:^(NSInteger index) {
            ProductResultData *data = datas[index];
            latexGiftCode = data.productCode;
            latexGiftName = data.productName;
            [_detailTbV reloadData];
        }];
        [LoadingView hideLoadingViewToDelay];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 주소 변경

 @param address 변경할 주소
 */
- (void)requestModifyAddress:(NSString *)address {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:_shippingSeq forKey:@"m_shippingseq"];
    [parameters setValue:address forKey:@"m_address"];
    [ApiManager requestToPost:MODIFY_ADDRESS_URL parameters:parameters success:^(id responseObject) {
        [self requestDetail];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 주소 변경 확인 요청
 */
- (void)requestModifyAddressConfirm {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:_shippingSeq forKey:@"m_shippingseq"];
    [ApiManager requestToPost:MODIFY_ADDRESS_CONFIRM_URL parameters:parameters success:^(id responseObject) {
        [self requestDetail];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 비고 추가 요청

 @param value 비고추가
 */
- (void)requestAddCompanyCheck:(NSString *)value {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:_shippingSeq forKey:@"shipping_seq"];
    [parameters setValue:resultData.companyCheck forKey:@"m_compcheck"];
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    NSString *inputDate = [CommonUtil todayFromDateFormat:@"MM/dd HH:mm"];
    NSString *noteAdd = [NSString stringWithFormat:@"(%@ %@) %@", userInfo.adminNm, inputDate, value];
    [parameters setValue:noteAdd forKey:@"m_compcheck1"];
    [ApiManager requestToPost:COMPANY_CHECK_URL parameters:parameters success:^(id responseObject) {
        [self requestDetail];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 배송기사 리스트 요청
 */
- (void)requestDeliverymanList {
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:userInfo.adminId forKey:@"code"];
    [ApiManager requestToPost:DELIVERYMAN_LIST_URL parameters:parameters success:^(id responseObject) {
        NSArray *datas = [UserResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        [self openViewController:STORYBOARD_LIST_POPUP_DELIVERYMAN_SEGUE sender:datas];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 배송기사 교체 요청

 @param adminId 로그인 아이디
 */
- (void)requestChangeDeliveryman:(NSString *)adminId {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:_shippingSeq forKey:@"m_shippingseq"];
    [parameters setValue:resultData.dueDate forKey:@"m_duedate"];
    NSString *key = nil;
    if([selectedDeliveryman isEqualToString:kTitleDeliveryMan3]) {
        key = @"m_deliveryman3";
    } else {
        key = @"m_deliveryman2";
    }
    [parameters setValue:adminId forKey:key];
    [ApiManager requestToPost:DELIVERYMAN_CHANGE_URL parameters:parameters success:^(id responseObject) {
        [self requestDetail];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 자동차 리스트 요청
 */
- (void)requestCarList {
    [ApiManager requestToPost:CAR_LIST_URL parameters:nil success:^(id responseObject) {
        NSArray *datas = [CarResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        [self openViewController:STORYBOARD_LIST_POPUP_CAR_SEGUE sender:datas];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 창고 리스트
 */
- (void)requestWarehouseList {
    [ApiManager requestToPost:WAREHOUSE_LIST_URL parameters:nil success:^(id responseObject) {
        NSArray *datas = [ReleaseWarehouseData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        [self openViewController:STORYBOARD_LIST_POPUP_WAREHOUSE_SEGUE sender:datas];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 제품 유효성 체크
 */
- (void)requestValidateCode {
    ProductValidationData *data = validDatas[selectedValidIndex];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:data.productCd forKey:@"serialCd"];
    [parameters setValue:data.itemCd forKey:@"itemCd"];
    [ApiManager requestToPost:PRODUCT_VALIDATION_URL parameters:parameters success:^(id responseObject) {
        ResultModel *resultData = [[ResultModel alloc] initWithDictionary:responseObject error:nil];
        if([resultData.resultCode isEqualToString:CONNECT_SUCCESS_CODE] == NO) {
            [CommonUtil showAlertTitle:@"" msg:resultData.resultMsg type:kAlertTypeOne target:self cAction:nil];
        }
        [_detailTbV reloadData];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}


/**
 기사 재고 리스트
 */
- (void)requestStockList:(BOOL)isGift {
    [ApiManager requestToPost:DELIVERYMAN_STOCK_LIST_URL parameters:nil success:^(id responseObject) {
        NSArray *datas = [ProductValidationData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        NSPredicate *predicate = nil;
        if(isGift == YES) {
            predicate = [NSPredicate predicateWithFormat:@"giftYn = %@", @"S"];
        } else {
            predicate = [NSPredicate predicateWithFormat:@"giftYn = %@ OR giftYn = %@", @"N", @"S"];
        }
        NSArray *giftDatas = [datas filteredArrayUsingPredicate:predicate];
        if(giftDatas.count > 0) {
            [self openViewController:STORYBOARD_LIST_POPUP_STOCK_SEGUE sender:giftDatas];
        } else {
            [CommonUtil showToastMessage:kDescStockNone];
        }
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 사은품 리스트
 */
- (void)requestGiftList {
    [ApiManager requestToPost:GIFT_LIST_URL parameters:nil success:^(id responseObject) {
        NSArray *data = [ReleaseProductData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        [self openViewController:STORYBOARD_LIST_POPUP_PRODUCT_SEGUE sender:data];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 창고 재고 리스트 (외주 기사 경우에만)
 */
- (void)requestWarehouseStockList:(BOOL)isGift {
    [ApiManager requestToPost:RELEASE_STOCK_LIST_URL parameters:nil success:^(id responseObject) {
        NSArray *datas = [ReleaseProductData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        NSPredicate *predicate = nil;
        if(isGift == YES) {
            predicate = [NSPredicate predicateWithFormat:@"giftYn = %@", @"S"];
        } else {
            predicate = [NSPredicate predicateWithFormat:@"giftYn = %@ OR giftYn = %@", @"N", @"S"];
        }
        NSArray *giftDatas = [datas filteredArrayUsingPredicate:predicate];
        if(giftDatas.count > 0) {
            [self openViewController:STORYBOARD_LIST_POPUP_PRODUCT_SEGUE sender:giftDatas];
        } else {
            [CommonUtil showToastMessage:kDescStockNone];
        }
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 증정품 업데이트 요청
 */
- (void)requestUpdateFreegift {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:_shippingSeq forKey:@"shipping_seq"];
    [parameters setValue:latexGiftCode forKey:@"l_freegift"];
    [ApiManager requestToPost:UPDATE_FREEGIFT_URL parameters:parameters success:^(id responseObject) {
        [self requestDetail];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 증정품 코드 요청

 @param sender 선택된 버튼
 */
- (void)requestGiftCode:(UIButton *)sender {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:GIFT_CODE forKey:@"code"];
    [ApiManager requestToPost:SEARCH_CODE_URL parameters:parameters success:^(id responseObject) {
        giftCodeDatas = [CodeResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        [self addNoneData];
        NSArray *codeNames = [CommonObject codeNamesFromData:giftCodeDatas];
        [CommonUtil showActionSheet:codeNames target:self selected:^(NSInteger index) {
            [sender setTitle:codeNames[index] forState:UIControlStateNormal];
        }];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 모든 제품 리스트 (회수 바코드 없을 시)
 */
- (void)requestAllProductList {
    NSString *itemGroup = nil;
    if([resultData.productType isEqualToString:kProductTypeMassage]) {
        itemGroup = @"A10000";
    } else if([resultData.productType isEqualToString:kProductTypeLatex]) {
        itemGroup = @"L10000";
    } else {
        itemGroup = @"W10000";
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:itemGroup forKey:@"itemGroup"];
    
    [ApiManager requestToPost:ALL_PRODUCT_LIST_URL parameters:parameters success:^(id responseObject) {
        NSArray *datas = [ReleaseProductData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        if(datas.count > 0) {
            [self openViewController:STORYBOARD_LIST_POPUP_PRODUCT_SEGUE sender:datas];
        }
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 회수 제품 임시 바코드 생성

 @param code 아이템 코드
 */
- (void)requestCreateBarcode:(NSString *)code {
    ProductValidationData *validData = validDatas[selectedValidIndex];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:code forKey:@"itemCd"];
    
    [ApiManager requestToPost:CREATE_BARCODE_URL parameters:parameters success:^(id responseObject) {
        ProductValidationData *data = [[ProductValidationData alloc] initWithDictionary:responseObject[API_RESPONSE_KEY] error:nil];
        validData.productCd = data.tempLot;
        [_detailTbV reloadData];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 선택 안됨 첫 인덱스에 강제 추가
 */
- (void)addNoneData {
    CodeResultData *data = [[CodeResultData alloc] init];
    data.detCdNm = @"--";
    [giftCodeDatas insertObject:data atIndex:0];
}

/**
 완료처리 상태 반환

 @return 완료처리 상태
 */
- (NSString *)setupStatusCode {
    if(_setupCompleteBtn.isSelected) {
        return @"ZZ";
    } else if(_faultyBtn.isSelected) {
        return @"F2";
    } else if(_impossibleBtn.isSelected) {
        return @"X1";
    } else if(_rejectBtn.isSelected) {
        return @"X2";
    } else if(_returnBtn.isSelected) {
        return @"X4";
    } else if(_cancelBtn.isSelected) {
        return @"X3";
    } else {
        return @"";
    }
}

/**
 주거형태 반환
 
 @return 완료처리 상태
 */
- (NSString *)housingTypeCode {
    if(_apartBtn.isSelected) {
        return @"APT";
    } else if(_shareBtn.isSelected) {
        return @"FAC";
    } else if(_houseBtn.isSelected) {
        return @"HS";
    } else if(_villaBtn.isSelected) {
        return @"VIL";
    } else {
        return @"";
    }
}

/**
 선택된 버튼 타이틀에 따른 인덱스 반환

 @param array 버튼에 적용된 타이틀 배열
 @param btn 해당 버튼
 @return 선택된 인덱스
 */
- (NSInteger)btnTypeFromArray:(NSArray *)array btn:(UIButton *)btn {
    for(int i = 0; i < array.count; i++) {
        if([btn.currentTitle isEqualToString:array[i]]) {
            return i;
        }
    }
    return NSNotFound;
}

- (void)checkParameters {
    [uploadImages removeAllObjects];
    NSMutableArray *attachArray = [NSMutableArray array];
    [attachArray addObjectsFromArray:[_productV subviews]];
    [attachArray addObjectsFromArray:[_attachV subviews]];
    [attachArray addObjectsFromArray:[_ladderV subviews]];
    [attachArray addObjectsFromArray:[_craneV subviews]];
    [attachArray addObjectsFromArray:[_serviceV subviews]];
    [attachArray addObjectsFromArray:[_returnV subviews]];
    
    NSString *message = nil;
    for(PhotoView *view in attachArray) {
        NSString *title = view.titleLb.text;
        NSData *imageData = [selectedPhotoDatas objectForKey:title];
        if(imageData != nil) { // 이미지가 존재 하면
            UIImage *image = [UIImage imageWithData:imageData];
            if([self isShippingTypeChange] == YES) { // 맞교체 시
                if([title hasPrefix:kTitleSetupAttach]) { // 설치첨부
                    [uploadImages setValue:image forKey:@"screen3"];
                } else if([title isEqualToString:kTitleReturnNo]) { // 회수 S/N
                    [uploadImages setValue:image forKey:@"screen4"];
                } else if([title isEqualToString:kTitleReturn]) { // 회수
                    [uploadImages setValue:image forKey:@"screen5"];
                } else if([title isEqualToString:kTitleReturnAttach]) { // 회수첨부
                    [uploadImages setValue:image forKey:@"screen6"];
                }
            }
            if([title isEqualToString:kTitleProductNo]) { // 제품 S/N
                [uploadImages setValue:image forKey:@"screen1"];
            } else if([title isEqualToString:kTitleSetup] ||
                      [title isEqualToString:kTitleSetupTotal] ||
                      [title isEqualToString:kTitleReturn]) { // 설치 or 설치전체 or 회수
                [uploadImages setValue:image forKey:@"screen2"];
            } else if([title hasPrefix:@"첨부"]) { // 첨부 (1, 2, 3, 4)
                if(uploadImages[@"screen3"] == nil) {
                    [uploadImages setValue:image forKey:@"screen3"];
                } else if(uploadImages[@"screen4"] == nil) {
                    [uploadImages setValue:image forKey:@"screen4"];
                } else if(uploadImages[@"screen5"] == nil) {
                    [uploadImages setValue:image forKey:@"screen5"];
                } else if(uploadImages[@"screen6"] == nil) {
                    [uploadImages setValue:image forKey:@"screen6"];
                }
            } else if([title isEqualToString:kTitleLadderAttach1]) { // 수작업불가(줄자)
                [uploadImages setValue:image forKey:@"screen_ladder_one"];
            } else if([title isEqualToString:kTitleLadderAttach2]) { // 사다리 인증
                [uploadImages setValue:image forKey:@"screen_ladder_two"];
            } else if([title isEqualToString:kTitleLadderAttach3]) { // 영수증
                [uploadImages setValue:image forKey:@"screen_ladder_three"];
            } else if([title isEqualToString:kTitleCraneAttach1]) { // 건물외관
                [uploadImages setValue:image forKey:@"screen_socket_one"];
            } else if([title isEqualToString:kTitleCraneAttach2]) { // 복도1
                [uploadImages setValue:image forKey:@"screen_socket_two"];
            } else if([title isEqualToString:kTitleCraneAttach3]) { // 복도2
                [uploadImages setValue:image forKey:@"screen_socket_three"];
            } else if([title hasPrefix:kTitleServiceAttach1]) { // 내림서비스1
                [uploadImages setValue:image forKey:@"service_picture1"];
            } else if([title hasPrefix:kTitleServiceAttach2]) { // 내림서비스2
                [uploadImages setValue:image forKey:@"service_picture2"];
            } else if([title hasPrefix:kTitleReturnAttach1]) { // 회수서비스1
                [uploadImages setValue:image forKey:@"screen_collect_one"];
            } else if([title hasPrefix:kTitleReturnAttach2]) { // 회수서비스2
                [uploadImages setValue:image forKey:@"screen_collect_two"];
            }
        } else { // 해당 뷰에 사진이 등록 되지 않았으면
            if([title isEqualToString:kTitleProductNo]) { // 제품 S/N
                message = kTitleProductNo;
                break;
            } else if([title isEqualToString:kTitleSetup]) { // 설치
                message = kTitleSetup;
                break;
            } else if([title isEqualToString:kTitleSetupTotal]) { // 설치 전체
                message = kTitleSetupTotal;
                break;
            } else if([title isEqualToString:kTitleReturn]) { // 회수 사진
                message = kTitleReturn;
                break;
            }
            
            if([self isShippingTypeChange] == YES) { // 맞교체 시
                if([title isEqualToString:kTitleReturnNo]) { // 회수 S/N
                    message = kTitleReturnNo;
                    break;
                }
            }
            if([title isEqualToString:kTitleLadderAttach1] ||
               [title isEqualToString:kTitleLadderAttach2]) { // 사다리 사용시 이미지 모두 사용필요
                message = title;
                break;
            } else if([title isEqualToString:kTitleCraneAttach1] ||
                      [title isEqualToString:kTitleCraneAttach2] ||
                      [title isEqualToString:kTitleCraneAttach3]) { // 양중 설치시 이미지 한개 이상 사용필요
                message = title;
                break;
            } else if([title isEqualToString:kTitleServiceAttach1] ||
                      [title isEqualToString:kTitleServiceAttach2]) { // 내림서비스 이용시 이미지 모두 사용필요
                message = title;
                break;
            } else if([title isEqualToString:kTitleReturnAttach1] ||
                      [title isEqualToString:kTitleReturnAttach2]) { // 회수서비스 이용시 이미지 모두 사용필요
                message = title;
                break;
            }
        }
    }

    if(message != nil) { // 에러메세지가 있으면
        [CommonUtil showToastMessage:[NSString stringWithFormat:kFormatImageAttachNeed, message]];
        return;
    }

    if(shippingType == kShippingTypeAssemble ||
       shippingType == kShippingTypeDecomposition) { // 분해, 조립
        if([NSString isEmptyString:_setupMemoTV.text]) {
            message = kDescMemoNeed;
        }
    }

    CarResultData *carData = [UserManager loadUserData:USER_CAR_KEY];
    if([NSString isEmptyString:carData.carNumber]) { // 등록된 차량이 없으면
        [CommonUtil showAlertTitle:kTitleCarPopup msg:kDescCarSelectionNeed type:kAlertTypeTwo target:self cAction:^{
            [self requestCarList];
        }];
        return;
    }

    NSString *gift1Code = [CommonObject codeFromData:giftCodeDatas codeName:_gift1Btn.currentTitle];
    if(![NSString isEmptyString:resultData.freeGiftOne]) { // 증정품이 있으면
        if([NSString isEmptyString:gift1Code]) { // 증정품의 코드가 없으면
            message = kDescGiftNeed;
        }
    }
    NSString *gift2Code = [CommonObject codeFromData:giftCodeDatas codeName:_gift2Btn.currentTitle];
    if(![NSString isEmptyString:resultData.freeGiftTwo]) { // 증정품2가 있으면
        if([NSString isEmptyString:gift2Code]) { // 증정품2의 코드가 없으면
            message = kDescGiftNeed;
        }
    }

    NSString *backSn = _returnNoTF.text;
    if(_returnNoHeightLC.isActive == NO && [NSString isEmptyString:backSn]) { // 회수 S/N가 없으면
        message = kDescReturnNoNeed;
    }
    NSString *productSn = _productNoTF.text;
    if(_productNoHeightLC.isActive == NO && [NSString isEmptyString:productSn]) { // 제품 S/N가 없으면
        message = kDescProductNoNeed;
    }

    UIButton *selectedSetupBtn = [UIButton selectedBtn:setupBtnGroup];
    if(selectedSetupBtn == nil) { // 설치여부 선택된 버튼이 없으면
        message = kDescSetupNeed;
    }
    
    UIButton *selectedHousingBtn = [UIButton selectedBtn:housingBtnGroup];
    if(selectedHousingBtn == nil) { // 주거형태 선택된 버튼이 없으면
        message = kDescHousingNeed;
    }
    
    if(signImage != nil) { // 싸인 이미지가 있으면
        [uploadImages setValue:signImage forKey:@"screen_sign"];
    } else { // 싸인 이미지가 없으면
        message = kDescSignNeed;
    }

    if([resultData.promise isEqualToString:@"Y"] == NO) { // 약속 날짜 설정이 안되어 있으면
        message = kDescPromiseNeed;
    }

    if([self isShowCodeConfirm] == YES) {
        BOOL isInputSetupProduct = NO;
        BOOL isInputReturnProduct = NO;
        for(ProductValidationData *data in validDatas) {
            if([NSString isEmptyString:data.itemNm] == NO) { // 제품이 선택된 경우
                if([NSString isEmptyString:data.productCd]) { // 필수 코드 값 없음
                    if([data.giftYn isEqualToString:@"N"]) { // 제품 코드 값 필수
                        message = kDescProductCodeValidNeed;
                        break;
                    } else {
                        if([data.lotFlag isEqualToString:@"Y"]) { // 사은품 코드 값 (lotFlag == Y 경우 필수)
                            message = kDescGiftCodeValidNeed;
                            break;
                        }
                    }
                }
            }
            if([self isShippingTypeChange] == YES) { // 맞교체(전체교체)인 경우
                if([NSString isEmptyString:data.productCd] == NO) {
                    if([data.collectYn isEqualToString:@"N"] &&
                       isInputSetupProduct == NO) { // 설치 제품이 선택되지 않았으면
                        isInputSetupProduct = YES;
                    } else if([data.collectYn isEqualToString:@"Y"] &&
                              isInputReturnProduct == NO) { // 회수 제품이 선택되지 않았으면
                        isInputReturnProduct = YES;
                    }
                }

                if([NSString isEmptyString:data.itemCd] &&
                   [NSString isEmptyString:data.productCd] &&
                   [NSString isEmptyString:data.serialCd]) { // 맞교체는 값 없어도 통과
                    continue;
                }
            }

            if([NSString isEmptyString:data.itemNm] &&
               [data.giftYn isEqualToString:@"N"]) { // 제품이 선택되지 않았으면
                message = kDescProductCodeValidNeed;
                break;
            }
        }

        if(shippingType == kShippingTypeChange &&
           [NSString isEmptyString:message]) { // 맞교체(전체교체)인데 제품선택 하나도 선택되지 않은 경우
            if(isInputSetupProduct == NO) {
                message = kDescSetupProductCodeNeed;
            } else if(isInputReturnProduct == NO) {
                message = kDescReturnProductCodeNeed;
            }
        }
    }
    
    if(message != nil) { // 에러메세지가 존재하면
        [CommonUtil showToastMessage:message];
        return;
    }

    if([self isNeedWarehouse] == YES) { // 회수 제품을 선택 했을 경우 사유 팝업
        [self openViewController:STORYBOARD_INPUT_POPUP_SEGUE sender:nil];
    } else { // 사유 입력 필요 없이 완료 처리
        [self requestRegister:nil];
    }
}

/**
 완료 처리 요청
 */
- (void)requestRegister:(NSString *)returnMsg {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:_shippingSeq forKey:@"m_shippingseq"];
    [parameters setValue:@"Y" forKey:@"m_promise"];
    [parameters setValue:_setupMemoTV.text forKey:@"m_shippingps"];
    [parameters setValue:[self setupStatusCode] forKey:@"m_complete"];
    [parameters setValue:[self housingTypeCode] forKey:@"m_houseType"];
    [parameters setValue:[CommonUtil todayFromDateFormat:DATE_HYPHEN_YEAR_MONTH_DAY] forKey:@"m_completedt"];
    NSString *productSn = _productNoTF.text;
    if([NSString isEmptyString:productSn] == NO) {
        [parameters setValue:productSn forKey:@"m_productsn"];
    }
    NSString *gift1Code = [CommonObject codeFromData:giftCodeDatas codeName:_gift1Btn.currentTitle];
    [parameters setValue:gift1Code forKey:@"m_freegiftcheck1"];
    NSString *gift2Code = [CommonObject codeFromData:giftCodeDatas codeName:_gift2Btn.currentTitle];
    [parameters setValue:gift2Code forKey:@"m_freegiftcheck2"];
    [parameters setValue:_craneUseBtn.isSelected ? @"Y" : @"N" forKey:@"m_is_socket"];
    [parameters setValue:_ladderUseBtn.isSelected ? @"Y" : @"N" forKey:@"m_is_ladder"];
    [parameters setValue:resultData.productName forKey:@"m_productname"];
    [parameters setValue:resultData.productCode forKey:@"m_productcode"];
    NSString *name = nil;
    if([resultData.progressNo isEqualToString:@"1"]) {
        name = resultData.agreeName;
    } else {
        name = resultData.custName;
    }
    [parameters setValue:name forKey:@"m_custname"];
    [parameters setValue:signMemo forKey:@"m_confirmps"];
    [parameters setValue:relationData.detCd forKey:@"m_relationship"];
    [parameters setValue:relationData.detCdNm forKey:@"m_relationshipnm"];
    NSString *backSn = _returnNoTF.text;
    if([NSString isEmptyString:backSn] == NO) {
        [parameters setValue:backSn forKey:@"m_backsn"];
    }
    CarResultData *carData = [UserManager loadUserData:USER_CAR_KEY];
    [parameters setValue:carData.carNumber forKey:@"car_number"];

    if(_ladderUseBtn.isSelected) { // 사다리 사용시
        [parameters setValue:@([NSString removeCommaFromString:_ladderPriceTF.text]) forKey:@"m_ladder_price"];
        NSInteger paymentType = [self btnTypeFromArray:[CommonObject LadderPaymentTypeArray] btn:_paymentTypeBtn];
        [parameters setValue:@(paymentType) forKey:@"ladder_payment_type"];
    }
    if(_addCostBtn.isSelected) { // 내림 서비스 사용시
        [parameters setValue:@([NSString removeCommaFromString:_addCostTF.text]) forKey:@"m_extra_cost"];
        [parameters setValue:_costMemoTF.text forKey:@"m_extra_cost_memo"];
    }
    if(_returnUseBtn.isSelected) { // 회수 서비스 사용시
        [parameters setValue:_returnUseBtn.isSelected ? @"Y" : @"N" forKey:@"m_iscollect"];
        [parameters setValue:_returnProductBtn.isSelected ? @"Y" : @"N" forKey:@"m_is_bodyproduct"];
    }

    UIButton *selectedSetupBtn = [UIButton selectedBtn:setupBtnGroup];
    if([selectedSetupBtn isEqual:_faultyBtn]) { // 초도불량
        [parameters setValue:kTitleFaulty forKey:@"m_completenm"];
    } else if([selectedSetupBtn isEqual:_setupCompleteBtn] && [self isShippingTypeChange]) { // 맞교체 완료
        [parameters setValue:kTitleChange forKey:@"m_completenm"];
    }

    if(shippingType == kShippingTypeAS ||
       shippingType == kShippingTypeAssemble ||
       shippingType == kShippingTypeDecomposition) { // AS, 조립, 분해 경우
        NSString *asType = [_asBtn.currentTitle isEqualToString:@"유상"] ? @"1" : @"2"; // 인덱스와 다름
        [parameters setValue:asType forKey:@"m_costncost"];
        if(shippingType == kShippingTypeAS) {
            NSInteger receiptType = [self btnTypeFromArray:[CommonObject ReceiptTypeArray] btn:_receiptBtn];
            [parameters setValue:@(receiptType) forKey:@"m_cashreceipttype"];
            [parameters setValue:_receiptTF.text forKey:@"m_cashreceiptcardnum"];
            NSInteger depositType = [self btnTypeFromArray:[CommonObject DepositTypeArray] btn:_depositTypeBtn];
            [parameters setValue:@(depositType) forKey:@"m_paymenttype"];
            [parameters setValue:_depositTF.text forKey:@"m_depositdetailhistory"];
            [parameters setValue:@([NSString removeCommaFromString:_asTotalCostTF.text]) forKey:@"m_costamount"];
        }
    }
    
    if([self isShowCodeConfirm] == YES) { // 제품 코드를 입력 할수있는 경우
        NSMutableDictionary *validItems = [NSMutableDictionary dictionary];
        NSMutableArray *jsonDatas = [NSMutableArray array];
        for(ProductValidationData *data in validDatas) {
            if((shippingType == kShippingTypeChange ||
                [data.giftYn isEqualToString:@"Y"] ||
                [data.collectYn isEqualToString:@"Y"]) &&
               [NSString isEmptyString:data.itemCd] &&
               [NSString isEmptyString:data.productCd] &&
               [NSString isEmptyString:data.serialCd]) { // 맞교체(전체교체) or 사은품 or 회수인 경우 제품 선택값이 비어 있으면 통과
                continue;
            }
            
            NSMutableDictionary *validData = [NSMutableDictionary dictionary];
            [validData setValue:data.issueNo forKey:@"issueNo"];
            NSString *itemCd = data.itemCd;
            [validData setValue:[NSString isEmptyString:itemCd] == YES ? @"" : itemCd forKey:@"item"];
            NSString *productCd = data.productCd;
            [validData setValue:[NSString isEmptyString:productCd] == YES ? @"" : productCd forKey:@"serialCd"];
            NSString *makerCd = data.manufacturerCd;
            [validData setValue:[NSString isEmptyString:makerCd] == YES ? @"" : makerCd forKey:@"makerCd"];
            [validData setValue:data.collectYn forKey:@"collectYn"];
            [validData setValue:data.giftYn forKey:@"giftYn"];
            [jsonDatas addObject:validData];
        }
        if(jsonDatas.count > 0) { // 제품 코드를 입력하지 않은 경우
            [validItems setValue:jsonDatas forKey:@"itemList"];
            NSString *slCd = nil;
            if([self isNeedWarehouse] == YES) { // 회수 창고 입력이 필요한 경우
                slCd = selectedWarehouseData.slCd;
            }
            [validItems setValue:slCd ? slCd : @"" forKey:@"tSlCd"];
            [validItems setValue:returnMsg ? returnMsg : @"" forKey:@"tReason"];
            
            NSData* jsonData = [NSJSONSerialization dataWithJSONObject:validItems options:kNilOptions error:nil];
            NSString* jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [parameters setValue:jsonString forKey:@"jsonData"];
        }
    }
    
    // 앱로그 전송
    [self requestAppLog];
    [ApiManager requestToMultipart:SHIPPING_MODIFY_URL parameters:parameters uploadImages:uploadImages success:^(id responseObject) {
        [UserManager removeDataForKey:USER_REGISTER_KEY];
        [_delegate reloadViewController:kDescRegisterSuccess];
        [LoadingView hideLoadingView];
        [self closeViewController];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [CommonUtil showToastMessage:errorMsg];
        [LoadingView hideLoadingView];
    }];
}

/**
 완료처리 요청시 앱로그 요청
 */
- (void)requestAppLog {
    NSString *logData = nil;
    if(shippingType == kShippingTypeASReturn ||
       shippingType == kShippingTypeMoveReturn) {
        logData = [NSString stringWithFormat:@"mobile/api/appModifyShippingDiv.json : m_shippingseq = %@", _shippingSeq];
    } else {
        logData = [NSString stringWithFormat:@"%@, %@, %@, %@, %@", _shippingSeq, resultData.promise, resultData.shippingPs, resultData.insComplete, resultData.inDate];
    }
    [self requestAppLog:logData type:kAppLogTypeShipping];
}

/**
 배송 구분 이전 요청 여부

 @return 이전 여부
 */
- (BOOL)isShippingTypeMove {
    return (shippingType == kShippingTypeMoveRequest ||
            shippingType == kShippingTypeMoveSetup ||
            shippingType == kShippingTypeMoveReturn);
}

/**
 배송 구분 회수 여부

 @return 회수 여부
 */
- (BOOL)isShippingTypeReturn {
    return (shippingType == kShippingTypeMoveReturn ||
            shippingType == kShippingTypeDisplayReturn ||
            shippingType == kShippingTypeDraftReturn ||
            shippingType == kShippingTypeCancel);
}

/**
 제품 코드 확인 필요

 @return 필요 여부
 */
- (BOOL)isShowCodeConfirm {
    if(_cancelBtn.isSelected == YES) { // 취소는 불필요
        return NO;
    } else if(shippingType == kShippingTypeDelivery ||
              shippingType == kShippingTypeDraft ||
              shippingType == kShippingTypeDisplaySetup ||
              shippingType == kShippingTypeGift) { // 설치 건
        return YES;
    } else if((shippingType == kShippingTypeASReturn ||
               shippingType == kShippingTypeDisplayReturn ||
               shippingType == kShippingTypeDraftReturn ||
               shippingType == kShippingTypeMoveReturn ||
               shippingType == kShippingTypeCancel ||
               shippingType == kShippingTypeChange ||
               shippingType == kShippingTypePartChange) &&
              (_faultyBtn.isSelected == YES ||
               _impossibleBtn.isSelected == YES ||
               _rejectBtn.isSelected == YES ||
               _returnBtn.isSelected == YES) == NO) { // 회수, 맞교체 건 중 초도불량, 회수불가, 수취거부, 설치후반품은 처리 안함
        return YES;
    }
    return NO;
}

/**
 회수창고 선택 필요
 
 @return 필요 여부
 */
- (BOOL)isNeedWarehouse {
    if(_cancelBtn.isSelected == YES) { // 취소는 불필요
        return NO;
    }
    if(shippingType == kShippingTypeASReturn ||
       shippingType == kShippingTypeDisplayReturn ||
       shippingType == kShippingTypeDraftReturn ||
       shippingType == kShippingTypeMoveReturn ||
       shippingType == kShippingTypeCancel ||
       shippingType == kShippingTypeChange ||
       shippingType == kShippingTypePartChange) { // 회수 건
        if((_faultyBtn.isSelected == YES ||
            _impossibleBtn.isSelected == YES ||
            _rejectBtn.isSelected == YES ||
            _returnBtn.isSelected == YES) == NO) { // 회수 건 중 초도불량, 회수불가, 수취거부, 설치후반품은 처리 안함
            return YES;
        }
    } else {
        if(_faultyBtn.isSelected == YES ||
           _impossibleBtn.isSelected == YES ||
           _rejectBtn.isSelected == YES ||
           _returnBtn.isSelected == YES) { // 설치 건의 경우 설치 불가의 경우
            return YES;
        }
    }
    return NO;
}

/**
 배송 구분 사은품 여부
 
 @return 사은품 여부
 */
- (BOOL)isShippingTypeGift {
    return shippingType == kShippingTypeGift;
}

/**
 맞교체

 @return 맞교체
 */
- (BOOL)isShippingTypeChange {
    return shippingType == kShippingTypeChange;
}

/**
 배송내역 뷰 설정
 */
- (void)setDetailTitles {
    if(detailTitles != nil) {
        [detailTitles removeAllObjects];
        detailTitles = nil;
    }
    shippingType = [CommonObject shippingType:resultData.shippingType pNo:resultData.progressNo];

    if([resultData.productType isEqualToString:kProductTypeLatex]) {
        if([self isShippingTypeMove] == YES) {
            detailTitles = [NSMutableArray arrayWithArray:[CommonObject DetailLatexMoveShippingArray]];
        } else {
            detailTitles = [NSMutableArray arrayWithArray:[CommonObject DetailLatexShippingArray]];
        }
        latexGiftCode = resultData.lFreegift;
    } else {
        if([self isShippingTypeMove] == YES) {
            detailTitles = [NSMutableArray arrayWithArray:[CommonObject DetailChairMoveShippingArray]];
        } else {
            detailTitles = [NSMutableArray arrayWithArray:[CommonObject DetailChairShippingArray]];
        }
    }
    
    _productHeightLC.active = NO;
    _setupHeightLC.active = NO;
    _productNoHeightLC.active = NO;
    
    if(![NSString isEmptyString:resultData.serverTime]) {
        _setupDateLb.text = [CommonUtil changeDateFormat:DATE_YEAR_MONTH_DAY dateString:resultData.serverTime changeFormat:DATE_KOREAN_YEAR_MONTH_DAY];
        _setupDateHeightLC.active = NO;
    } else {
        _setupDateHeightLC.active = YES;
    }
    
    if(![NSString isEmptyString:resultData.freeGiftOne]) {
        _gift1Lb.text = [NSString stringWithFormat:@"사은품 %@", resultData.freeGiftOne];
        _gift1HeightLC.active = NO;
    } else {
        _gift1HeightLC.active = YES;
    }
    
    if(![NSString isEmptyString:resultData.freeGiftTwo]) {
        _gift2Lb.text = [NSString stringWithFormat:@"사은품 %@", resultData.freeGiftTwo];
        _gift2HeightLC.active = NO;
    } else {
        _gift2HeightLC.active = YES;
    }
    
    if([self isShippingTypeChange] == YES) {
        _returnNoHeightLC.active = NO;
    } else if(shippingType == kShippingTypeAS ||
              shippingType == kShippingTypeAssemble ||
              shippingType == kShippingTypeDecomposition) { // AS, 조립, 분해 경우
        _asHeightLC.active = NO;
        NSArray *costArray = [CommonObject CostTypeArray];
        NSString *costType = [resultData.costnCost isEqualToString:@"1"] ? costArray[1] : costArray[0];
        [_asBtn setTitle:costType forState:UIControlStateNormal];
    }
    
    NSString *type = nil;
    if([self isShippingTypeReturn] == YES) {
        type = @"회수";
        _returnNoHeightLC.active = YES;
    } else {
        type = @"설치";
    }
    
    [_detailSC setTitle:[NSString stringWithFormat:@"%@확인서", type] forSegmentAtIndex:1];
    _setupLb.text = [NSString stringWithFormat:@"%@일", type];
    _setupMemoLb.text = [NSString stringWithFormat:@"%@비고", type];
    _setupStatusLb.text = [NSString stringWithFormat:@"%@여부", type];
    [_setupCompleteBtn setTitle:[NSString stringWithFormat:@"%@완료", type] forState:UIControlStateNormal];
    [_impossibleBtn setTitle:[NSString stringWithFormat:@"%@불가", type] forState:UIControlStateNormal];

    if([self isShowCodeConfirm] == YES) { // 제품 코드 확인 필요
        [self addProductCode];
    }
}

/**
 제품 코드 확인 리스트 추가
 */
- (void)addProductCode {
    // 초기화
    [self removeProductCode];
    
    if([self isShippingTypeGift] == YES) {
        [self addCodeConfirmTitle:kTitleCodeConfirm];
        [self addProductCode:kTitleGiftCode];
    } else {
        if([self isShippingTypeChange] == YES) {
            [self addCodeConfirmTitle:kTitleSetupCodeConfirm];
            [self addValidData:kTitleProductCode];
            [self addValidData:kTitleProductCode];
            [self addValidData:kTitleGiftCode];
            [self addValidData:kTitleGiftCode];
            [self addCodeConfirmTitle:kTitleReturnCodeConfirm];
        } else {
            [self addCodeConfirmTitle:kTitleCodeConfirm];
        }

        if([self isNeedWarehouse] == YES) { // 회수 선택 창고 선택 필요
            [detailTitles addObject:kTitleReturnWarehouse];
            codeTitleCount++;
        }
        NSString *productName = resultData.productName;
        // 제품명에 '+' 기호가 있으면 각각 제품으로 분리
        NSArray *products = [productName componentsSeparatedByString:@"+"];
        for(NSString *_ in products) {
            [self addProductCode:kTitleProductCode];
        }
    }
    
    NSString *freeGiftOne = resultData.freeGiftOne;
    // 사은품1이 존재하면
    if([NSString isEmptyString:freeGiftOne] == NO) {
        [self addProductCode:kTitleGiftCode];
        if([freeGiftOne containsString:@"2EA"]) {
            [self addProductCode:kTitleGiftCode];
        }
    }
    NSString *freeGiftTwo = resultData.freeGiftTwo;
    // 사은품2가 존재하면
    if([NSString isEmptyString:freeGiftTwo] == NO) {
        [self addProductCode:kTitleGiftCode];
        if([freeGiftTwo containsString:@"2EA"]) {
            [self addProductCode:kTitleGiftCode];
        }
    }
}

- (void)removeProductCode {
    codeTitleCount = 0;
    [detailTitles removeObject:kTitleCodeConfirm];
    [detailTitles removeObject:kTitleSetupCodeConfirm];
    [detailTitles removeObject:kTitleReturnCodeConfirm];
    [detailTitles removeObject:kTitleReturnWarehouse];
    [detailTitles removeObject:kTitleProductCode];
    [detailTitles removeObject:kTitleGiftCode];
}

- (void)addCodeConfirmTitle:(NSString *)title {
    [detailTitles addObject:title];
    codeTitleCount++;
}

- (void)addProductCode:(NSString *)title {
    [detailTitles addObject:title];
    [self addValidData:title isCollect:[self isShippingTypeReturn] || [self isShippingTypeChange]];
}

- (void)addValidData:(NSString *)title {
    [detailTitles addObject:title];
    [self addValidData:title isCollect:NO];
}

- (void)addValidData:(NSString *)title isCollect:(BOOL)isCollect {
    if(validDatas == nil) {
        validDatas = [NSMutableArray array];
    }
    
    BOOL isExist = NO;
    for(UIButton *btn in setupBtnGroup) {
        if(btn.isSelected == YES) {
            isExist = YES;
            break;
        }
    }
    if(isExist == NO) {
        ProductValidationData *data = [[ProductValidationData alloc] init];
        data.giftYn = [title containsString:@"사은품"] ? @"Y" : @"N";
        data.collectYn = isCollect == YES ? @"Y" : @"N";
        [validDatas addObject:data];
    }
}

- (void)checkBarcode:(NSInteger)index isProduct:(BOOL)isProduct {
    selectedPhotoV = nil;
    selectedValidIndex = index;
    isProductCode = isProduct;
    [self requestCameraPermission:kPhotoTypeBarcode];
}

/**
 segment 뷰 전환

 @param index 인덱스
 */
- (void)showDetailView:(NSInteger)index {
    [CommonUtil hideKeyboard];
    if(index == 0) {
        _certificationSV.hidden = YES;
        _detailTbV.hidden = NO;
        [self setBaseSV:_detailTbV];
    } else if(index == 1) {
        _detailTbV.hidden = YES;
        _certificationSV.hidden = NO;
        [self setBaseSV:_certificationSV];
    }
}

/**
 파일첨부 뷰 생성

 @param datas 첨부파일 타이틀
 @param view 상단 뷰
 */
- (void)setAttachViewFromData:(NSArray *)datas superview:(UIView *)view {
    [view removeAllSubviews];
    
    UIView *preView = nil;
    for(int i = 0; i < datas.count; i++) {
        PhotoView *photoV = [[PhotoView alloc] init];
        photoV.delegate = self;
        photoV.titleLb.text = datas[i];
        [view addSubview:photoV];
        
        CGFloat width = [CommonUtil widthFromSuperview:view count:datas.count padding:8.0];
        [photoV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(view);
            make.width.equalTo(@(width)).priorityHigh();
            make.width.lessThanOrEqualTo(@90);
            if(preView == nil) {
                make.leading.equalTo(view);
            } else {
                make.leading.equalTo(preView.mas_trailing).offset(8);
            }
            
            if(i == datas.count - 1) {
                make.trailing.lessThanOrEqualTo(view);
            }
        }];
        preView = photoV;
        
        NSData *imageData = [selectedPhotoDatas objectForKey:photoV.titleLb.text];
        if(imageData != nil) { // 선택된 이미지가 있으면 이미지 적용
            UIImage *selectedImage = [UIImage imageWithData:imageData];
            [photoV.plusBtn setImage:selectedImage forState:UIControlStateNormal];
        } else {
            NSString *imageName = nil;
            if([view isEqual:_ladderV]) { // 사다리 샘플 이미지
                imageName = [NSString stringWithFormat:kFormatLadderImage, i+1];
            } else if([view isEqual:_craneV]) { // 양중 샘플 이미지
                imageName = [NSString stringWithFormat:kFormatCraneImage, i+1];
            } else if([view isEqual:_serviceV]) { // 서비스 샘플 이미지
                imageName = [NSString stringWithFormat:kFormatServiceImage, i+1];
            }
            
            if(![NSString isEmptyString:imageName]) { // 샘플 이미지명이 있으면 샘플이미지 적용
                UIImage *sampleImage = [UIImage imageNamed:imageName];
                [photoV.plusBtn setImage:sampleImage forState:UIControlStateNormal];
            }
        }
    }
}

/**
 설치확인서의 사진 첨부뷰 설정

 @param isFaulty 초도불량 여부
 */
- (void)setGroupAttachView:(BOOL)isFaulty {
    NSArray *attach1Array = nil;
    NSArray *attach2Array = nil;
    
    if([self isShippingTypeChange] == YES) {
        attach1Array = [CommonObject ChangeProductAttachArray];
        attach2Array = [CommonObject ChangeProductReturnAttachArray];
    } else if(shippingType == kShippingTypeCancel) {
        attach1Array = [CommonObject ProductCancelAttachArray];
        attach2Array = [CommonObject ProductAttachArray];
    } else if([self isShippingTypeReturn] == YES) {
        attach1Array = [CommonObject ProductReturnAttachArray];
        attach2Array = [CommonObject ProductAttachArray];
    } else {
        if(isFaulty) {
            attach1Array = [CommonObject ProductFaultyAttachArray];
            attach2Array = [CommonObject ProductHalfAttachArray];
        } else {
            attach1Array = [CommonObject ProductSetupAttachArray];
            attach2Array = [CommonObject ProductAttachArray];
        }
    }
    [self setAttachViewFromData:attach1Array superview:_productV];
    [self setAttachViewFromData:attach2Array superview:_attachV];
}

/**
 지불 방법 설정

 @param index 인덱스
 */
- (void)setPaymentType:(NSInteger)index {
    [_paymentTypeBtn setTitle:[CommonObject LadderPaymentTypeArray][index] forState:UIControlStateNormal];
}

/**
 데이터 순서 재정의

 @param datas 데이터
 @return 재정렬 된 데이터
 */
- (NSMutableArray *)restoreResponseData:(NSArray *)datas {
    NSString *productType = nil;
    if([resultData.productType isEqualToString:kProductTypeMassage]) {
        productType = kProductTypeMassage;
    } else if([resultData.productType isEqualToString:kProductTypeLatex]) {
        productType = kProductTypeLatex;
    } else if([resultData.productType isEqualToString:kProductTypeWellness]) {
        productType = kProductTypeWellness;
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"detCd BEGINSWITH[c] %@", productType];
    return [NSMutableArray arrayWithArray:[datas filteredArrayUsingPredicate:predicate]];
}

- (void)checkLadderBtn:(UIButton *)sender {
    if([sender isEqual:_ladderUseBtn]) {
        [self setAttachViewFromData:[CommonObject LadderAttachArray] superview:_ladderV];
    } else {
        [_ladderV removeAllSubviews];
    }
    _ladderHeightLC.active = !([sender isEqual:_ladderUseBtn]);
    [UIButton selectBtnGroup:sender group:ladderBtnGroup];
}

- (void)checkCraneBtn:(UIButton *)sender {
    if([sender isEqual:_craneUseBtn]) {
        [self setAttachViewFromData:[CommonObject CraneAttachArray] superview:_craneV];
    } else {
        [_craneV removeAllSubviews];
    }
    _craneHeightLC.active = !([sender isEqual:_craneUseBtn]);
    [UIButton selectBtnGroup:sender group:craneBtnGroup];
}

- (void)checkCodeConfirm {
    if([self isShowCodeConfirm] == YES) {
        [self addProductCode];
    } else {
        [self removeProductCode];
    }
    [_detailTbV reloadData];
}


/**
 바코드 촬영 타입에 따른 처리

 @param isGift 사은품 여부
 @param collect 회수 여부
 */
- (void)checkBarcodeConfirm:(BOOL)isGift collect:(NSString *)collect {
    if(isGift) {
        if([collect isEqualToString:@"Y"]) {
            [self requestAllProductList];
        } else {
            if([CommonObject isOutsourcing]) {
                [self requestWarehouseStockList:NO];
            } else {
                [self requestStockList:NO];
            }
        }
    } else {
        if([collect isEqualToString:@"Y"]) {
            [self requestGiftList];
        } else {
            if([CommonObject isOutsourcing]) {
                [self requestWarehouseStockList:YES];
            } else {
                [self requestStockList:YES];
            }
        }
    }
}

/**
 제품코드, 사은품 코드 배열 인덱스 계산 (validDatas)

 @param index cell index
 */
- (NSInteger)checkValidIndex:(NSInteger)index {
    NSInteger idx = (validDatas.count + codeTitleCount) - (detailTitles.count - index) ;
    for(NSInteger i = 0; i < index; i++) {
        NSString *title = detailTitles[i];
        if([title isEqualToString:kTitleCodeConfirm] ||
           [title isEqualToString:kTitleSetupCodeConfirm] ||
           [title isEqualToString:kTitleReturnCodeConfirm] ||
           [title isEqualToString:kTitleReturnWarehouse]) {
            idx--;
        }
    }
    return idx;
}

/**
 선택된 이미지 저장

 @param image 선택된 이미지
 */
- (void)setSelectedPhoto:(UIImage *)image {
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    // UserDefaults에 저장하기 위해 NSData형으로 전환
    [selectedPhotoDatas setValue:imageData forKey:selectedPhotoV.titleLb.text];
    // UserDefaults에 저장
    [UserManager saveData:selectedPhotoDatas key:USER_REGISTER_KEY];
}

#pragma mark - Action Event
- (IBAction)changedDetail:(UISegmentedControl *)sender {
    if(sender.selectedSegmentIndex == 0) {
        _detailTbV.hidden = resultData == nil;
    }
    [self showDetailView:sender.selectedSegmentIndex];
}

- (IBAction)pressedGroupLadder:(UIButton *)sender {
    if(sender.isSelected) {
        return;
    }
    [self checkLadderBtn:sender];
    if([sender isEqual:_ladderUseBtn]) {
        [self checkCraneBtn:_craneUnuseBtn];
    }
}

- (IBAction)pressedGroupCrane:(UIButton *)sender {
    if(sender.isSelected) {
        return;
    }
    [self checkCraneBtn:sender];
    if([sender isEqual:_craneUseBtn]) {
        [self checkLadderBtn:_ladderUnuseBtn];
    }
}

- (IBAction)pressedGroupSetup:(UIButton *)sender {
    if(sender.isSelected) {
        return;
    }
    if([sender isEqual:_faultyBtn]) {
        _setupMemoLb.text = kTitleSymptom;
    } else {
        _setupMemoLb.text = kTitleSetupMemo;
    }
    [self setGroupAttachView:[sender isEqual:_faultyBtn]];
    [UIButton selectBtnGroup:sender group:setupBtnGroup];
    
    [self checkCodeConfirm];
}

- (IBAction)pressedGroupHousing:(UIButton *)sender {
    if(sender.isSelected) {
        return;
    }
    [UIButton selectBtnGroup:sender group:housingBtnGroup];
}
    
- (IBAction)pressedGroupService:(UIButton *)sender {
    if(sender.isSelected) {
        return;
    }
    if([sender isEqual:_addCostBtn]){
        [self setAttachViewFromData:[CommonObject ServiceAttachArray] superview:_serviceV];
    } else {
        [_serviceV removeAllSubviews];
    }
    
    if([CommonObject isOutsourcing] == NO) { // 본사 기사 경우
        _addCostTF.text = [NSString addCommaFromInteger:10000];
    } else { // 외주 기사 경우
        _addCostTF.text = [NSString addCommaFromInteger:20000];
    }
    _serviceHeightLC.active = !(sender == _addCostBtn);
    [UIButton selectBtnGroup:sender group:serviceBtnGroup];
}

- (IBAction)pressedGroupReturn:(UIButton *)sender {
    if(sender.isSelected) {
        return;
    }
    if([sender isEqual:_returnUseBtn]){
        [self setAttachViewFromData:[CommonObject ReturnAttachArray] superview:_returnV];
    } else {
        [_returnV removeAllSubviews];
    }
    
    _returnHeightLC.active = !(sender == _returnUseBtn);
    [UIButton selectBtnGroup:sender group:returnBtnGroup];
}

- (IBAction)pressedReturnProduct:(UIButton *)sender {
    sender.selected = !sender.isSelected;
}

- (IBAction)pressedPaymentType:(UIButton *)sender {
    [CommonUtil showActionSheet:[CommonObject LadderPaymentTypeArray] target:self selected:^(NSInteger index) {
        [self setPaymentType:index];
    }];
}

- (IBAction)pressedGift:(UIButton *)sender {
    [self requestGiftCode:sender];
}

- (IBAction)pressedAS:(UIButton *)sender {
    NSArray *costArray = [CommonObject CostTypeArray];
    [CommonUtil showActionSheet:costArray target:self selected:^(NSInteger index) {
        [sender setTitle:costArray[index] forState:UIControlStateNormal];
        if(shippingType == kShippingTypeAS) {
            _asSubHeightLC.active = index == 0;
        }
    }];
}

- (IBAction)pressedSign:(id)sender {
    [self requestCode:TERMS_CODE];
}

- (IBAction)pressedDeposit:(id)sender {
    NSArray *depositArray = [CommonObject DepositTypeArray];
    [CommonUtil showActionSheet:depositArray target:self selected:^(NSInteger index) {
        [sender setTitle:depositArray[index] forState:UIControlStateNormal];
    }];
}

- (IBAction)pressedReceipt:(id)sender {
    NSArray *receiptArray = [CommonObject ReceiptTypeArray];
    [CommonUtil showActionSheet:receiptArray target:self selected:^(NSInteger index) {
        [sender setTitle:receiptArray[index] forState:UIControlStateNormal];
    }];
}

- (IBAction)pressedRegister:(id)sender {
    [self checkParameters];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(_detailSC.selectedSegmentIndex == 0) {
        tableView.hidden = resultData == nil;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return resultData != nil ? detailTitles.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *title = detailTitles[indexPath.row];
    if([title isEqualToString:kTitleProductCode]) {
        // ValidDatas의 인덱스 계산
        NSInteger index = [self checkValidIndex:indexPath.row];
        ProductValidationData *data = validDatas[index];
        if([NSString isEmptyString:data.itemNm] == NO ||
           [NSString isEmptyString:data.productCd] == NO) { // 바코드 정보가 있을 경우
            DetailCodeConfirmTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailCodeCellIdentifier];
            cell.collectCodeHandler = ^(UIButton *sender) {
                sender.selected = !sender.isSelected;
                data.collectYn = sender.isSelected ? @"Y" : @"N";
            };
            cell.productCodeHandler = ^{
                selectedValidIndex = index;
                [self checkBarcodeConfirm:YES collect:data.collectYn];
            };
            cell.manufacturerCodeHandler = ^{
                selectedValidIndex = index;
                [self checkBarcode:index isProduct:NO];
            };
            [cell setCellFromData:data];
            return cell;
        } else { // 바코드 정보가 없을 경우
            DetailValueChangeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailValueChangeCellIdentifier];
            cell.changeHandler = ^{
                selectedValidIndex = index;
                [self checkBarcodeConfirm:YES collect:data.collectYn];
            };
            [cell setCellFromData:@"제품 선택" title:title];
            return cell;
        }
    } else if([title isEqualToString:kTitleGiftCode]) {
        NSInteger index = [self checkValidIndex:indexPath.row];
        ProductValidationData *data = validDatas[index];
        if([NSString isEmptyString:data.itemNm] == NO) { // 사은품 이름이 있을 경우
            DetailGiftConfirmTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailGiftCellIdentifier];
            cell.popupHandler = ^{
                selectedValidIndex = index;
                [self checkBarcodeConfirm:NO collect:data.collectYn];
            };
            cell.barcodeHandler = ^{
                selectedValidIndex = index;
                [self checkBarcode:index isProduct:YES];
            };
            [cell setCellFromData:data];
            return cell;
        } else {
            DetailValueChangeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailValueChangeCellIdentifier];
            cell.changeHandler = ^{
                selectedValidIndex = index;
                [self checkBarcodeConfirm:NO collect:data.collectYn];
            };
            [cell setCellFromData:@"사은품 선택" title:title];
            return cell;
        }
    } else {
        if([title isEqualToString:kTitleAddress] ||
           [title isEqualToString:kTitleNoteAdd] ||
           [title isEqualToString:kTitleCollectionAddress]) {
            __weak DetailTextInputTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailTextInputCellIdentifier];
            if([title isEqualToString:kTitleAddress] ||
               [title isEqualToString:kTitleCollectionAddress]) {
                cell.confirmHandler = ^{
                    [self requestModifyAddressConfirm];
                };
                cell.textAddHandler = ^{
                    [self requestModifyAddress:cell.valueTV.text];
                };
                [cell setCellFromData:resultData title:title];
            } else {
                cell.textAddHandler = ^{
                    [self requestAddCompanyCheck:cell.valueTV.text];
                };
                [cell setCellFromTitle:title];
            }
            return cell;
        } else if([title isEqualToString:kTitleTelephone] ||
                  [title isEqualToString:kTitleCellPhone] ||
                  [title isEqualToString:kTitleCollectionTelNo] ||
                  [title isEqualToString:kTitleCollectionPhoneNo] ||
                  [title isEqualToString:kTitleSetupTelNo] ||
                  [title isEqualToString:kTitleSetupPhoneNo]) {
            DetailLinkTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailLinkTextCellIdentifier];
            cell.valueLb.delegate = self;
            [cell setCellFromData:resultData title:title];
            return cell;
        } else if(([title isEqualToString:kTitleDeliveryMan2]) ||
                  ([title isEqualToString:kTitleDeliveryMan3] && ![NSString isEmptyString:resultData.deliveryMan2]) ||
                  [title isEqualToString:kTitleCarSelection] ||
                  [title isEqualToString:kTitleProductCode] ||
                  [title isEqualToString:kTitleGiftCode] ||
                  [title isEqualToString:kTitleReturnWarehouse]) {
            DetailValueChangeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailValueChangeCellIdentifier];
            cell.changeHandler = ^{
                NSString *title = detailTitles[indexPath.row];
                if([title isEqualToString:kTitleDeliveryMan2] ||
                   [title isEqualToString:kTitleDeliveryMan3]) {
                    selectedDeliveryman = title;
                    [self requestDeliverymanList];
                } else if([title isEqualToString:kTitleCarSelection]) {
                    [self requestCarList];
                } else if([title isEqualToString:kTitleReturnWarehouse]) {
                    [self requestWarehouseList];
                }
            };
            NSString *data = @"";
            if([title isEqualToString:kTitleDeliveryMan2] && ![NSString isEmptyString:resultData.deliveryMan2]) {
                data = [NSString stringWithFormat:@"%@(%@)", resultData.deliveryManNm2, resultData.deliveryMan2];
            } else if([title isEqualToString:kTitleDeliveryMan3]) {
                if(![NSString isEmptyString:resultData.deliveryMan3]) {
                    data = [NSString stringWithFormat:@"%@(%@)", resultData.deliveryManNm3, resultData.deliveryMan3];
                }
            } else if([title isEqualToString:kTitleReturnWarehouse]) {
                data = selectedWarehouseData.slNm;
            }
            [cell setCellFromData:data title:title];
            return cell;
        } else if([title isEqualToString:kTitleLatexGift]) {
            DetailButtonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailButtonCellIdentifier];
            cell.optionHandler = ^{
                [self requestLatexGiftCode];
            };
            cell.saveHandler = ^{
                [self requestUpdateFreegift];
            };
            [cell setCellFromData:latexGiftName title:title];
            return cell;
        } else if([title isEqualToString:kTitleCodeConfirm] ||
                  [title isEqualToString:kTitleSetupCodeConfirm] ||
                  [title isEqualToString:kTitleReturnCodeConfirm]) {
            DetailTitleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailTitleCellIdentifier];
            [cell setCellFromTitle:title];
            return cell;
        } else {
            DetailBaseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailBaseCellIdentifier];
            if([title isEqualToString:kTitleDivision]) {
                NSString *division = [CommonObject codeNamesFromCode:divisionCodeDatas code:resultData.shippingType];
                [cell setCellFromTitle:title value:[CommonObject divisionFromShippingType:division pNo:resultData.progressNo]];
            } else if([title isEqualToString:kTitleDivideState]) {
                [cell setCellFromTitle:title value:[CommonObject codeNamesFromCode:contractorCodeDatas code:resultData.divStatus]];
            } else if([title isEqualToString:kTitleCollectionLocation]) {
                [cell setCellFromTitle:title value:[CommonObject codeNamesFromCode:areaCodeDatas code:resultData.area2]];
            } else if([title isEqualToString:kTitleSetupLocation]) {
                [cell setCellFromTitle:title value:[CommonObject codeNamesFromCode:areaCodeDatas code:resultData.area]];
            } else {
                [cell setCellFromData:resultData title:title];
            }
            return cell;
        }
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"^[-a-zA-Z0-9]"];
    return [string isEqualToString:@""] || [predicate evaluateWithObject:string];
}

#pragma mark - CTLabelDelegate
- (void)pressedLink:(NSString *)string {
    [CommonUtil openLinkCall:string];
}

#pragma mark - PhotoViewDelegate
- (void)pressedPhotoView:(PhotoView *)sender {
    selectedPhotoV = sender;
    NSString *title = sender.titleLb.text;
    NSArray *photoTypeArray = nil;
    if([title isEqualToString:kTitleProductNo] ||
       [title isEqualToString:kTitleReturnNo]) { // 제품 번호 사진은 바코드 사용가능
        photoTypeArray = [CommonObject PhotoAddBarcodeTypeArray];
    } else {
        photoTypeArray = [CommonObject PhotoTypeArray];
    }
    [CommonUtil showActionSheet:photoTypeArray target:self selected:^(NSInteger index) {
        [self requestCameraPermission:(PhotoType)index];
    }];
}

#pragma mark - SignPopupViewDelegate
- (void)completeSign:(UIImage *)image data:(CodeResultData *)data memo:(NSString *)memo {
    if(image != nil) {
        _signBtn.backgroundColor = UIColorFromRGB(0xB5B5B5);
        _signBtn.enabled = NO;
        signImage = image;
    }
    if(data != nil) {
        relationData = data;
    }
    signMemo = memo;
}

#pragma mark - InputPopupViewDelegate
- (void)didInputData:(NSString *)string {
    [self requestRegister:string];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    __block UIImage *image = info[UIImagePickerControllerOriginalImage];
    NSURL *referenceUrl = info[UIImagePickerControllerReferenceURL];
    [picker dismissViewControllerAnimated:YES completion:^{
        // 이미지 리사이징 & 크롭
        image = [UIImage cropImage:[UIImage resizeImage:image width:500] size:CGSizeMake(500, 500)];
        if(referenceUrl == nil) { // 카메라로 촬영한걸로 간주
            // 워터마크 처리
            image = [image drawTodayText];
            [self requestSavePhotoPermission:image];
        }
        [self setSelectedPhoto:image];
        [selectedPhotoV.plusBtn setImage:image forState:UIControlStateNormal];
    }];
}

#pragma mark - ListPopupViewDelegate
- (void)selectedItem:(JSONModel *)data type:(ListPopupType)type {
    if(type == kListPopupTypeDeliveryman) {
        UserResultData *userData = (UserResultData *)data;
        NSString *adminId = @"";
        if(userData.adminId != nil) {
            adminId = userData.adminId;
        }
        [self requestChangeDeliveryman:adminId];
    } else if(type == kListPopupTypeCar) {
        CarResultData *carData = (CarResultData *)data;
        [UserManager saveData:carData.toDictionary key:USER_CAR_KEY];
        [_detailTbV reloadData];
    } else if(type == kListPopupTypeStock) { // 기사 재고 선택시
        ProductValidationData *validData = validDatas[selectedValidIndex];
        ProductValidationData *productData = (ProductValidationData *)data;
        // 선택시 이전 선택된 코드 값 초기화
        productData.productCd = nil;
        // 값 유지
        productData.collectYn = validData.collectYn;
        productData.giftYn = validData.giftYn;
        productData.manufacturerCd = validData.manufacturerCd;
        validDatas[selectedValidIndex] = productData;
        [_detailTbV reloadData];
        if([productData.giftYn isEqualToString:@"N"] ||
           [productData.lotFlag isEqualToString:@"Y"]) { // 제품인 경우
            if([productData.serialCd isEqualToString:@"*"] ||
               (shippingType == kShippingTypePartChange)) {
                productData.productCd = productData.serialCd;
                validDatas[selectedValidIndex] = productData;
                [_detailTbV reloadData];
            } else {
                [self checkBarcode:selectedValidIndex isProduct:YES];
            }
        }
    } else if(type == kListPopupTypeProduct) {
        ProductValidationData *validData = validDatas[selectedValidIndex];
        ReleaseProductData *productData = (ReleaseProductData *)data;
        // 선택시 이전 선택된 코드 값 초기화
        validData.productCd = nil;
        // 값 대입
        validData.itemCd = productData.itemCd;
        validData.itemNm = productData.itemNm;
        validData.lotFlag = productData.lotFlag;
        [_detailTbV reloadData];
        NSString *code = productData.itemCd;
        if([NSString isEmptyString:productData.giftYn]) { // 제품 리스트에서 선택 시
            dispatch_async(dispatch_get_main_queue(), ^{
                [CommonUtil showAlertTitle:kTitleBarcode msg:kDescExistBarcode type:kAlertTypeTwo cancel:@"존재하지 않음" confirm:@"촬영" target:self cancelAction:^{
                    [self requestCreateBarcode:code];
                } confirmAction:^{
                    [self checkBarcode:selectedValidIndex isProduct:YES];
                }];
            });
        } else if([productData.giftYn isEqualToString:@"N"]) { // 제품인 경우
            [self checkBarcode:selectedValidIndex isProduct:YES];
        } else { // 사은품인 경우
            if([productData.lotFlag isEqualToString:@"Y"]) { // 사은품 바코드 촬영이 필요한 경우
                [self checkBarcode:selectedValidIndex isProduct:YES];
            }
        }
    } else if(type == kListPopupTypeWarehouse) {
        ReleaseWarehouseData *warehouseData = (ReleaseWarehouseData *)data;
        selectedWarehouseData = warehouseData;
        [_detailTbV reloadData];
    }
}

#pragma mark - BarcodeViewDelegate
/**
 제품 코드 촬영 시 (바코드 촬영)

 @param code 코드 값
 */
- (void)scanBarcode:(NSString *)code {
    ProductValidationData *data = validDatas[selectedValidIndex];
    if(isProductCode == NO) {
        data.manufacturerCd = code;
    } else {
        data.productCd = code;
        [self requestValidateCode];
    }
    [_detailTbV reloadData];
}

/**
 사진 촬영 시 (바코드 촬영)

 @param code 코드 값
 @param image 바코드 이미지
 */
- (void)scanBarcode:(NSString *)code image:(UIImage *)image {
    NSString *title = selectedPhotoV.titleLb.text;
    if([title isEqualToString:kTitleReturnNo] &&
       _returnNoHeightLC.isActive == NO) {
        _returnNoTF.text = code;
    } else if([title isEqualToString:kTitleReturnNo] ||
              _productNoHeightLC.isActive == NO) {
        _productNoTF.text = code;
    }
    // 이미지 리사이징 & 크롭
    image = [UIImage cropImage:[UIImage resizeImage:image width:500] size:CGSizeMake(500, 500)];
    // 워터마크 처리
    image = [image drawTodayText];
    [self setSelectedPhoto:image];
    [selectedPhotoV.plusBtn setImage:image forState:UIControlStateNormal];
    [self requestSavePhotoPermission:image];
}

@end
