//
//  DetailBaseTableViewCell.m
//  shipping
//
//  Created by ios on 2018. 3. 28..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "DetailBaseTableViewCell.h"

@implementation DetailBaseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellFromTitle:(NSString *)title value:(NSString *)value {
    _titleLb.text = title;
    _valueLb.text = value;
}

- (void)setCellFromData:(DetailResultData *)data title:(NSString *)title {
    NSString *value = @"";
    NSAttributedString *attributedValue = nil;
    if([title isEqualToString:kTitleBodyNo]) {
        value = data.bodyNo;
    } else if([title isEqualToString:kTitleReceiveDate]) {
        value = data.receiveDate;
    } else if([title isEqualToString:kTitleCustomerNm]) {
        if([data.progressNo isEqualToString:@"1"] ||
           [data.progressNoNm isEqualToString:@"회수"]) {
            value = data.agreeName;
        } else {
            value = data.custName;
        }
//    } else if([title isEqualToString:kTitleContactNo1]) {
//        value = data.telNo;
//    } else if([title isEqualToString:kTitleContactNo2]) {
//        value = data.hPhoneNo;
//    } else if([title isEqualToString:kTitleContactNo3]) {
//        // TODO: 연락처3
    } else if([title isEqualToString:kTitleAddress]) {
        if([data.progressNo isEqualToString:@"0"] ||
           [data.progressNoNm isEqualToString:@"설치"]) {
            value = data.insAddr2;
        } else {
            value = data.insAddr;
        }
    } else if([title isEqualToString:kTitleProductDivision]) {
        value = data.productTypeNm;
    } else if([title isEqualToString:kTitleManuFacturer]) {
        // TODO: 제조사
    } else if([title isEqualToString:kTitleProductNm]) {
        value = data.productName;
    } else if([title isEqualToString:kTitlePurchaseOffice]) {
        value = data.purchasingOffice;
    } else if([title isEqualToString:kTitleQuantity]) {
        value = data.qty;
    } else if([title isEqualToString:kTitleService]) {
        value = data.serviceMonth;
    } else if([title isEqualToString:kTitleFreeGift1]) {
        value = data.freeGiftOne;
    } else if([title isEqualToString:kTitleFreeGift2]) {
        value = data.freeGiftTwo;
    } else if([title isEqualToString:kTitleCompanyNote]) {
        NSMutableString *note = [NSMutableString stringWithString:data.companyCheck];
        NSString *symptom = data.symptom;
        if(![NSString isEmptyString:symptom]) {
            if(![NSString isEmptyString:note]) {
                [note appendString:@"<br>"];
            }
            [note appendFormat:@"증상 : %@", symptom];
        }
        attributedValue = [NSString applyHtmlText:note fontSize:_valueLb.font.pointSize];
    } else if([title isEqualToString:kTitleDeliveryMan1]) {
        if(![data.deliveryManNm1 isEqualToString:@""] && ![data.deliveryMan1 isEqualToString:@""]) {
            value = [NSString stringWithFormat:@"%@(%@)", data.deliveryManNm1, data.deliveryMan1];
        }
    } else if([title isEqualToString:kTitleDeliveryMan2]) {
        if(![data.deliveryManNm2 isEqualToString:@""] && ![data.deliveryMan2 isEqualToString:@""]) {
            value = [NSString stringWithFormat:@"%@(%@)", data.deliveryManNm2, data.deliveryMan2];
        }
    } else if([title isEqualToString:kTitleDeliveryMan3]) {
        if(![data.deliveryManNm3 isEqualToString:@""] && ![data.deliveryMan3 isEqualToString:@""]) {
            value = [NSString stringWithFormat:@"%@(%@)", data.deliveryManNm3, data.deliveryMan3];
        }
    } else if([title isEqualToString:kTitleDueDate]) {
        value = data.dueDate;
    } else if([title isEqualToString:kTitleCarSelection]) {
        value = data.carNo;
    } else if([title isEqualToString:kTitleCollectionNm]) {
        value = data.agreeName;
    } else if([title isEqualToString:kTitleCollectionLocation]) {
        value = data.area;
        // TODO: 회수지역
    } else if([title isEqualToString:kTitleCollectionAddress]) {
        value = data.insAddr;
    } else if([title isEqualToString:kTitleSetupNm]) {
        value = data.custName;
    } else if([title isEqualToString:kTitleSetupLocation]) {
        value = data.area2;
        // TODO: 설치지역
    } else if([title isEqualToString:kTitleSetupAddress]) {
        value = data.insAddr2;
    } else if([title isEqualToString:kTitleSetupDate]) {
        value = data.inDate;
    } else if([title isEqualToString:kTitleSetupState]) {
        value = data.insCompleteNm;
    } else if([title isEqualToString:kTitleSetupMemo]) {
        value = data.shippingPs;
    } else if([title isEqualToString:kTitleProductNo]) {
        value = data.productSn;
    } else if([title isEqualToString:kTitlePresentFreeGift1]) {
        value = [self isPresentedFreeGift:data.freeGiftCheckOne];
    } else if([title isEqualToString:kTitlePresentFreeGift2]) {
        value = [self isPresentedFreeGift:data.freeGiftCheckTwo];
    } else if([title isEqualToString:kTitleContract]) {
        value = [data.nameCheck isEqualToString:@"Y"] ? @"동일" : @"상이";
    }
    _titleLb.text = title;
    if(attributedValue != nil) {
        _valueLb.attributedText = attributedValue;
    } else {
        _valueLb.text = value;
    }
}

- (NSString *)isPresentedFreeGift:(NSString *)freeGift {
    if([freeGift isEqualToString:@"Y"]) {
        return @"증정";
    } else if([freeGift isEqualToString:@"N"]) {
        return @"미증정";
    }
    return @"";
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _titleLb.text = nil;
    _valueLb.text = nil;
    _valueLb.attributedText = nil;
}

@end
