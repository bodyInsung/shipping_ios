//
//  DetailButtonTableViewCell.h
//  shipping
//
//  Created by ios on 2018. 4. 2..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailButtonTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet UIButton *optionBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;

@property (strong, nonatomic) void(^optionHandler)(void);
@property (strong, nonatomic) void(^saveHandler)(void);

- (void)setCellFromData:(NSString *)data title:(NSString *)title;
@end
