//
//  DetailCodeConfirmTableViewCell.m
//  shipping
//
//  Created by insung on 19/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "DetailCodeConfirmTableViewCell.h"

@implementation DetailCodeConfirmTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initLayout];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initLayout {
    [_productCodeBtn applyRoundBorder:4.0];
    [_manufacturerCodeBtn applyRoundBorder:4.0];
}

- (void)setCellFromData:(ProductValidationData *)data {
    _nameLb.text = data.itemNm;
    _nameHeightLC.active = [NSString isEmptyString:data.itemNm];
    _collectBtn.hidden = YES;

    NSString *productCd = data.productCd;
    if([productCd isEqualToString:@"*"]) {
        _productCodeLb.text = @"확인 불필요";
    } else {
        _productCodeLb.text = productCd;
    }
    _manufacturerLb.text = data.manufacturerCd;
}

- (IBAction)pressedCollect:(UIButton *)sender {
    _collectCodeHandler(sender);
}

- (IBAction)pressedProductCode:(UIButton *)sender {
    _productCodeHandler();
}

- (IBAction)pressedManufacturerCode:(UIButton *)sender {
    _manufacturerCodeHandler();
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    _nameLb.text = nil;
    _productCodeLb.text = nil;
    _manufacturerLb.text = nil;
}

@end
