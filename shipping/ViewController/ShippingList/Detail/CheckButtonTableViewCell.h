//
//  CheckButtonTableViewCell.h
//  shipping
//
//  Created by ios on 2018. 4. 4..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckButtonTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *checkBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLb;

@property (strong, nonatomic) void(^checkHandler)(void);

- (void)setCellFromData:(CodeResultData *)data isSelected:(BOOL)isSelected;
- (void)setLastCell:(BOOL)isSelected;
@end
