//
//  DetailTitleTableViewCell.h
//  shipping
//
//  Created by insung on 27/03/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailTitleTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLb;

- (void)setCellFromTitle:(NSString *)title;

@end
