//
//  DetailTextInputTableViewCell.m
//  shipping
//
//  Created by ios on 2018. 3. 30..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "DetailTextInputTableViewCell.h"

@implementation DetailTextInputTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initLayout];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initLayout {
    [_confirmBtn applyRoundBorder:4.0];
    [_textAddBtn applyRoundBorder:4.0];
    [_valueTV setAccessoryView];
    [_valueTV applyRoundBorder:4.0 color:UIColorFromRGB(0xB5B5B5) width:0.4];
}

- (void)setCellFromTitle:(NSString *)title {
    [self setCellFromData:nil title:title];
}

- (void)setCellFromData:(DetailResultData *)data title:(NSString *)title {
    _titleLb.text = title;

    BOOL isUpdate = NO;
    if(data != nil) {
        isUpdate = [data.custUpdate isEqualToString:@"AD"];
        if([title isEqualToString:kTitleAddress] ||
           [title isEqualToString:kTitleCollectionAddress]) {
            _valueTV.text = data.insAddr;
            [_textAddBtn setTitle:@"주소 변경" forState:UIControlStateNormal];
        } else if([title isEqualToString:kTitleSetupAddress]) {
            _valueTV.text = data.insAddr2;
        }
    } else {
        _valueTV.text = @"";
        [_textAddBtn setTitle:@"비고추가" forState:UIControlStateNormal];
    }    
    _confirmBtn.hidden = !isUpdate;
}
- (IBAction)pressedConfirm:(UIButton *)sender {
    _confirmHandler();
}

- (IBAction)pressedTextAdd:(UIButton *)sender {
    _textAddHandler();
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _titleLb.text = nil;
    _valueTV.text = nil;
}

@end
