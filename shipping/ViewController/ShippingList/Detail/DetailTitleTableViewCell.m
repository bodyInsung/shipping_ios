//
//  DetailTitleTableViewCell.m
//  shipping
//
//  Created by insung on 27/03/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "DetailTitleTableViewCell.h"

@implementation DetailTitleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellFromTitle:(NSString *)title {
    _titleLb.text = title;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _titleLb.text = nil;
}
@end
