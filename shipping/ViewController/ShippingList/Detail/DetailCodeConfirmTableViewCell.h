//
//  DetailCodeConfirmTableViewCell.h
//  shipping
//
//  Created by insung on 19/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailCodeConfirmTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UIButton *collectBtn;
@property (weak, nonatomic) IBOutlet UILabel *productCodeLb;
@property (weak, nonatomic) IBOutlet UIButton *productCodeBtn;
@property (weak, nonatomic) IBOutlet UILabel *manufacturerLb;
@property (weak, nonatomic) IBOutlet UIButton *manufacturerCodeBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *nameHeightLC;

@property (strong, nonatomic) void(^collectCodeHandler)(UIButton *sender);
@property (strong, nonatomic) void(^productCodeHandler)(void);
@property (strong, nonatomic) void(^manufacturerCodeHandler)(void);

- (void)setCellFromData:(ProductValidationData *)data;
@end
