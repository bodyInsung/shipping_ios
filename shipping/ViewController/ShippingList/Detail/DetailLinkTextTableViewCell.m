//
//  DetailLinkTextTableViewCell.m
//  shipping
//
//  Created by ios on 2018. 3. 30..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "DetailLinkTextTableViewCell.h"

@implementation DetailLinkTextTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellFromData:(DetailResultData *)data title:(NSString *)title {
    NSString *value = @"";
    if([title isEqualToString:kTitleTelephone] ||
       [title isEqualToString:kTitleSetupTelNo]) {
        value = data.telNo;
    } else if([title isEqualToString:kTitleCellPhone] ||
              [title isEqualToString:kTitleSetupPhoneNo]) {
        value = data.hPhoneNo;
    } else if([title isEqualToString:kTitleCollectionTelNo]) {
        value = data.telNo2;
    } else if([title isEqualToString:kTitleCollectionPhoneNo]) {
        value = data.hPhoneNo2;
    } else if([title isEqualToString:kTitleContactNo1]) {
        if([data.progressNo isEqualToString:@"1"] ||
           [data.progressNoNm isEqualToString:@"회수"]) {
            value = data.telNo2;
        } else {
            value = data.telNo;
        }
    } else if([title isEqualToString:kTitleContactNo2]) {
        if([data.progressNo isEqualToString:@"1"] ||
           [data.progressNoNm isEqualToString:@"회수"]) {
            value = data.hPhoneNo2;
        } else {
            value = data.hPhoneNo;
        }
    } else if([title isEqualToString:kTitleContactNo3]) {
        // TODO: 연락처3
    }
    
    NSAttributedString *linkText = [NSString applyLinkText:value color:UIColorFromRGB(0x6DA89C)];
    _titleLb.text = title;
    _valueLb.attributedText = linkText;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _titleLb.text = nil;
    _valueLb.attributedText = nil;
}
@end
