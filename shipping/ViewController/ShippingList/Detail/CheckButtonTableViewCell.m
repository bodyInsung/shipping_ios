//
//  CheckButtonTableViewCell.m
//  shipping
//
//  Created by ios on 2018. 4. 4..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "CheckButtonTableViewCell.h"

@implementation CheckButtonTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellFromData:(CodeResultData *)data isSelected:(BOOL)isSelected {
    _checkBtn.selected = isSelected;
    _titleLb.text = data.desc;
}

- (void)setLastCell:(BOOL)isSelected {
    _checkBtn.selected = isSelected;
    _titleLb.text = @"전체선택";
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _titleLb.text = nil;
}

@end
