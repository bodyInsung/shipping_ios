//
//  DetailGiftConfirmTableViewCell.h
//  shipping
//
//  Created by insung on 28/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailGiftConfirmTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UIButton *popupBtn;
@property (weak, nonatomic) IBOutlet UILabel *codeLb;
@property (weak, nonatomic) IBOutlet UIButton *barcodeBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *codeHeightLC;

@property (strong, nonatomic) void(^popupHandler)(void);
@property (strong, nonatomic) void(^barcodeHandler)(void);

- (void)setCellFromData:(ProductValidationData *)data;
@end
