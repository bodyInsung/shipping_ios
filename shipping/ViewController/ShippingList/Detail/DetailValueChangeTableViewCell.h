//
//  DetailValueChangeTableViewCell.h
//  shipping
//
//  Created by ios on 2018. 3. 30..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailValueChangeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet UILabel *valueLb;
@property (weak, nonatomic) IBOutlet UIButton *changeBtn;

@property (strong, nonatomic) void(^changeHandler)(void);

- (void)setCellFromData:(NSString *)data title:(NSString *)title;

@end
