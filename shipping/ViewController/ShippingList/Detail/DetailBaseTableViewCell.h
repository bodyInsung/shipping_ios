//
//  DetailBaseTableViewCell.h
//  shipping
//
//  Created by ios on 2018. 3. 28..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailBaseTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet UILabel *valueLb;

- (void)setCellFromTitle:(NSString *)title value:(NSString *)value;
- (void)setCellFromData:(DetailResultData *)data title:(NSString *)title;

@end
