//
//  DetailTextInputTableViewCell.h
//  shipping
//
//  Created by ios on 2018. 3. 30..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailTextInputTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet CTTextView *valueTV;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIButton *textAddBtn;

@property (strong, nonatomic) void(^confirmHandler)(void);
@property (strong, nonatomic) void(^textAddHandler)(void);

- (void)setCellFromTitle:(NSString *)title;
- (void)setCellFromData:(DetailResultData *)data title:(NSString *)title;
@end
