//
//  DetailButtonTableViewCell.m
//  shipping
//
//  Created by ios on 2018. 4. 2..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "DetailButtonTableViewCell.h"

@implementation DetailButtonTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initLayout];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initLayout {
    [_saveBtn applyRoundBorder:4.0];
}

- (void)setCellFromData:(NSString *)data title:(NSString *)title {
    _titleLb.text = title;
    
    if([NSString isEmptyString:data]) {
        [_optionBtn setTitle:kTitleUnselected forState:UIControlStateNormal];
    } else {
        [_optionBtn setTitle:data forState:UIControlStateNormal];
    }
}

- (IBAction)pressedOption:(UIButton *)sender {
    _optionHandler();
}

- (IBAction)pressedSave:(UIButton *)sender {
    _saveHandler();
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _titleLb.text = nil;
}

@end
