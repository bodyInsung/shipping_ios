//
//  DetailGiftConfirmTableViewCell.m
//  shipping
//
//  Created by insung on 28/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "DetailGiftConfirmTableViewCell.h"

@implementation DetailGiftConfirmTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initLayout];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initLayout {
    [_popupBtn applyRoundBorder:4.0];
    [_barcodeBtn applyRoundBorder:4.0];
}

- (void)setCellFromData:(ProductValidationData *)data {
    _nameLb.text = data.itemNm;
    _codeLb.text = data.productCd;
    _codeHeightLC.active = ![data.lotFlag isEqualToString:@"Y"];
}

- (IBAction)pressedPopup:(UIButton *)sender {
    _popupHandler();
}

- (IBAction)pressedBarcode:(UIButton *)sender {
    _barcodeHandler();
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _nameLb.text = nil;
    _codeLb.text = nil;
}
@end
