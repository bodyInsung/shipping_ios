//
//  DetailLinkTextTableViewCell.h
//  shipping
//
//  Created by ios on 2018. 3. 30..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailLinkTextTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet CTLabel *valueLb;

- (void)setCellFromData:(DetailResultData *)data title:(NSString *)title;
@end
