//
//  ShippingDetailViewController.h
//  shipping
//
//  Created by ios on 2018. 3. 28..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"
#import "ListPopupViewController.h"
#import "SignPopupViewController.h"
#import "InputPopupViewController.h"
#import "BarcodeOverlayView.h"
#import "BarcodeViewController.h"

@protocol ShippingDetailViewDelegate
- (void)reloadViewController:(NSString *)msg;
@end
@interface ShippingDetailViewController : BaseViewController <CTLabelDelegate, ListPopupViewDelegate, PhotoViewDelegate, SignPopupViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, BarcodeViewDelegate, InputPopupViewDelegate> {
    
    ShippingType                    shippingType;
    DetailResultData                *resultData;
    NSArray                         *contractorCodeDatas;
    NSArray                         *areaCodeDatas;
    NSArray                         *divisionCodeDatas;
    NSMutableArray                  *giftCodeDatas;
    NSMutableArray                  *detailTitles;
    NSString                        *latexGiftCode;
    NSString                        *latexGiftName;
    
    NSArray                         *ladderBtnGroup;
    NSArray                         *craneBtnGroup;
    NSArray                         *setupBtnGroup;
    NSArray                         *housingBtnGroup;
    NSArray                         *serviceBtnGroup;
    NSArray                         *returnBtnGroup;
    
    NSMutableDictionary             *selectedPhotoDatas;
    NSMutableDictionary             *uploadImages;
    PhotoView                       *selectedPhotoV;
    CodeResultData                  *relationData;
    UIImage                         *signImage;
    NSString                        *signMemo;
    NSString                        *selectedDeliveryman;
    ReleaseWarehouseData            *selectedWarehouseData;

    NSMutableArray                  *productDatas;
    NSMutableArray                  *validDatas;
    NSInteger                       selectedValidIndex;
    BOOL                            isProductCode;
    NSInteger                       codeTitleCount;
}

@property (weak) id<ShippingDetailViewDelegate> delegate;
@property (strong, nonatomic) NSString *shippingSeq;
@property (weak, nonatomic) IBOutlet UISegmentedControl *detailSC;
@property (weak, nonatomic) IBOutlet UITableView *detailTbV;
@property (weak, nonatomic) IBOutlet UIScrollView *certificationSV;
@property (weak, nonatomic) IBOutlet UIView *attachBackV;
@property (weak, nonatomic) IBOutlet UIView *productV;
@property (weak, nonatomic) IBOutlet UIView *attachV;

@property (weak, nonatomic) IBOutlet UIView *ladderBackV;
@property (weak, nonatomic) IBOutlet UIView *ladderV;
@property (weak, nonatomic) IBOutlet UIButton *ladderUseBtn;
@property (weak, nonatomic) IBOutlet UIButton *ladderUnuseBtn;
@property (weak, nonatomic) IBOutlet CTTextField *ladderPriceTF;
@property (weak, nonatomic) IBOutlet UIButton *paymentTypeBtn;

@property (weak, nonatomic) IBOutlet UIButton *craneUseBtn;
@property (weak, nonatomic) IBOutlet UIButton *craneUnuseBtn;
@property (weak, nonatomic) IBOutlet UIView *craneBackV;
@property (weak, nonatomic) IBOutlet UIView *craneV;

@property (weak, nonatomic) IBOutlet UILabel *setupLb;
@property (weak, nonatomic) IBOutlet UILabel *setupDateLb;

@property (weak, nonatomic) IBOutlet UIView *setupStatusV;
@property (weak, nonatomic) IBOutlet UILabel *setupStatusLb;
@property (weak, nonatomic) IBOutlet UIButton *setupCompleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *faultyBtn;
@property (weak, nonatomic) IBOutlet UIButton *impossibleBtn;
@property (weak, nonatomic) IBOutlet UIButton *rejectBtn;
@property (weak, nonatomic) IBOutlet UIButton *returnBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (weak, nonatomic) IBOutlet UIButton *apartBtn;
@property (weak, nonatomic) IBOutlet UIButton *villaBtn;
@property (strong, nonatomic) IBOutlet UIButton *houseBtn;
@property (weak, nonatomic) IBOutlet UIButton *shareBtn;

@property (weak, nonatomic) IBOutlet UIView *serviceBackV;
@property (weak, nonatomic) IBOutlet UIView *serviceV;
@property (weak, nonatomic) IBOutlet UIButton *addCostBtn;
@property (weak, nonatomic) IBOutlet UIButton *noneCostBtn;
@property (weak, nonatomic) IBOutlet CTTextField *addCostTF;
@property (weak, nonatomic) IBOutlet CTTextField *costMemoTF;
@property (weak, nonatomic) IBOutlet UIView *returnBackV;
@property (weak, nonatomic) IBOutlet UIView *returnV;
@property (weak, nonatomic) IBOutlet UIButton *returnUseBtn;
@property (weak, nonatomic) IBOutlet UIButton *returnUnuseBtn;
@property (weak, nonatomic) IBOutlet UIButton *returnProductBtn;

@property (weak, nonatomic) IBOutlet UILabel *setupMemoLb;
@property (weak, nonatomic) IBOutlet CTTextView *setupMemoTV;

@property (weak, nonatomic) IBOutlet UIView *productNoV;
@property (weak, nonatomic) IBOutlet CTTextField *productNoTF;
@property (weak, nonatomic) IBOutlet UIView *returnNoV;
@property (weak, nonatomic) IBOutlet CTTextField *returnNoTF;

@property (weak, nonatomic) IBOutlet UIView *gift1V;
@property (weak, nonatomic) IBOutlet UILabel *gift1Lb;
@property (weak, nonatomic) IBOutlet UIButton *gift1Btn;
@property (weak, nonatomic) IBOutlet UIView *gift2V;
@property (weak, nonatomic) IBOutlet UILabel *gift2Lb;
@property (weak, nonatomic) IBOutlet UIButton *gift2Btn;

@property (weak, nonatomic) IBOutlet UIView *asBackV;
@property (weak, nonatomic) IBOutlet UIButton *asBtn;
@property (weak, nonatomic) IBOutlet UIView *asSubV;
@property (weak, nonatomic) IBOutlet CTTextField *asTotalCostTF;
@property (weak, nonatomic) IBOutlet UIButton *depositTypeBtn;
@property (weak, nonatomic) IBOutlet UIButton *receiptBtn;
@property (weak, nonatomic) IBOutlet CTTextField *receiptTF;
@property (weak, nonatomic) IBOutlet CTTextField *depositTF;

@property (weak, nonatomic) IBOutlet UIButton *signBtn;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *productHeightLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *ladderHeightLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *craneHeightLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *serviceHeightLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *returnHeightLC;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *setupDateHeightLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *setupHeightLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *productNoHeightLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *returnNoHeightLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *gift1HeightLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *gift2HeightLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *asHeightLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *asSubHeightLC;

@end
