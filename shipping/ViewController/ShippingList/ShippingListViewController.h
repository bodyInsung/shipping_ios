//
//  ShippingListViewController.h
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"
#import "ReasonPopupViewController.h"

@interface ShippingListViewController : BaseViewController <ShippingDetailViewDelegate, MFMessageComposeViewControllerDelegate, ListSearchDelegate, UIPickerViewDelegate, CTTextFieldDelegate, ReasonPopupViewDelegate> {
    
    NSMutableArray                  *shippingListArray;
    ListSearchView                  *searchV;
    CTTextField                     *inputTF;
    NSIndexPath                     *indexPathOfModify;
}

@property (assign) SearchType searchType;
@property (strong, nonatomic) NSDate *selectedDate;
@property (weak, nonatomic) IBOutlet UIView *searchBackV;
@property (weak, nonatomic) IBOutlet UITableView *listTbV;
@property (weak, nonatomic) IBOutlet UIView *noDataV;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *searchVHeightLC;
@end
