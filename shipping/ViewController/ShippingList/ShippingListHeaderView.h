//
//  ShippingListHeaderView.h
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShippingListHeaderView : UITableViewHeaderFooterView

@property (weak, nonatomic) IBOutlet UILabel *dateLb;
@property (weak, nonatomic) IBOutlet UILabel *countLb;

- (void)setHeaderFromData:(NSArray *)datas;
@end
