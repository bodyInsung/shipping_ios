//
//  ShippingListHeaderView.m
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ShippingListHeaderView.h"

@implementation ShippingListHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setHeaderFromData:(NSArray *)datas {
    ShippingResultData *data = datas.firstObject;
    if(![data.dueDate isEqualToString:@""]) {
        _dateLb.text = data.dueDate;
    } else {
        _dateLb.text = kTitleUnspecified;
    }
    _countLb.text = [NSString stringWithFormat:kFormatCaseRound, datas.count];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _dateLb.text = nil;
    _countLb.text = nil;
}
@end
