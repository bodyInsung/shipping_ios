//
//  ShippingListTableViewCell.h
//  shipping
//
//  Created by ios on 2018. 3. 21..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShippingListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *backV;
@property (weak, nonatomic) IBOutlet UIImageView *productIV;
@property (weak, nonatomic) IBOutlet UIImageView *starIV;
@property (weak, nonatomic) IBOutlet UILabel *receiveDateLb;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UILabel *productLb;
@property (weak, nonatomic) IBOutlet UILabel *giftLb;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *giftHeightLC;
@property (weak, nonatomic) IBOutlet UILabel *addressLb;
@property (weak, nonatomic) IBOutlet UIView *progressV;
@property (weak, nonatomic) IBOutlet UILabel *progressLb;
@property (weak, nonatomic) IBOutlet UILabel *completeLb;
@property (weak, nonatomic) IBOutlet UIView *statusV;
@property (weak, nonatomic) IBOutlet UILabel *statusLb;
@property (weak, nonatomic) IBOutlet UIButton *timeBtn;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLb;
@property (weak, nonatomic) IBOutlet UIButton *callBtn;
@property (weak, nonatomic) IBOutlet UIButton *mapBtn;
@property (weak, nonatomic) IBOutlet UIButton *messageBtn;

@property (strong, nonatomic) void(^timeHandler)(void);
@property (strong, nonatomic) void(^callHandler)(void);
@property (strong, nonatomic) void(^mapHandler)(void);
@property (strong, nonatomic) void(^messageHandler)(void);

- (void)setCellFromData:(ShippingResultData *)data;
@end
