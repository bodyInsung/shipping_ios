//
//  BasePopupViewController.m
//  shipping
//
//  Created by ios on 2018. 4. 4..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BasePopupViewController.h"

@interface BasePopupViewController ()

@end

@implementation BasePopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initValue];
    [self initLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self addKeyboardObserver];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self removeKeyboardObserver];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [CommonUtil hideKeyboard];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initValue {
}

- (void)initLayout {
}

/**
 키보드 옵저버 등록
 */
- (void)addKeyboardObserver {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

/**
 키보드 옵저버 해제
 */
- (void)removeKeyboardObserver {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

/**
 가로로 강제 회전시
 */
- (void)landscapeViewController {
    CGAffineTransform transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
    self.view.transform = transform;
    [UIView commitAnimations];
}

/**
 스토리보드 segue로 페이지 이동
 
 @param segue 설정된 segue
 @param sender sender
 */
- (void)openViewController:(NSString *)segue sender:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:segue sender:sender];
    });
}

/**
 뷰컨트롤러 닫음
 */
- (void)closeViewController {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}

#pragma mark - Keyboard Notification
- (void)keyboardDidShow:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGRect originalKeyboardFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect keyboardFrame = [[[UIApplication sharedApplication] keyWindow] convertRect:originalKeyboardFrame toView:_baseV];
    CGFloat keyboardHeight = CGRectIntersection(keyboardFrame, _baseSV.frame).size.height;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardHeight, 0.0);
    _baseSV.contentInset = contentInsets;
    _baseSV.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillHide:(NSNotification *)notification {
    _baseSV.contentInset = UIEdgeInsetsZero;
    _baseSV.scrollIndicatorInsets = UIEdgeInsetsZero;
}


@end
