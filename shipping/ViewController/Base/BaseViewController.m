//
//  BaseViewController.m
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setActionBarView];
    [self initValue];
    [self initLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self addKeyboardObserver];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self removeKeyboardObserver];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [CommonUtil hideKeyboard];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/**
 키보드 옵저버 등록
 */
- (void)addKeyboardObserver {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

/**
 키보드 옵저버 해제
 */
- (void)removeKeyboardObserver {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

/**
 뷰컨트롤러 리로드시
 */
- (void)reloadViewController {
}

/**
 초기 값 설정시
 */
- (void)initValue {
}

/**
 초기 레이아웃 설정시
 */
- (void)initLayout {
}

/**
 액션바 숨김
 */
- (void)initLayoutForNoneActionBar {
    actionBarView.hidden = YES;
}

/**
 액션바 초기 설정 (로고)
 */
- (void)initLayoutForLogo {
    [self showActionBarTitle:NO];
    [self showMenuButton:YES];
    [self showLogoImageView:YES];
}

/**
 액션바 초기 설정 (타이틀)

 @param title 타이틀
 */
- (void)initLayoutForTitle:(NSString *)title {
    [self showLogoImageView:NO];
    if([title isEqualToString:TITLE_NOTICE_ACTIONBAR] ||
       [title isEqualToString:TITLE_SHIPPING_LIST_ACTIONBAR] ||
       [title isEqualToString:TITLE_COMPLETE_ACTIONBAR] ||
       [title isEqualToString:TITLE_RELEASE_ACTIONBAR]) {
        [self showMenuButton:YES];
    } else {
        [self showBackButton:YES];
    }
    [self setActionBarTitle:title];
}

/**
 액션바 생성
 */
- (void)setActionBarView {
    actionBarView = [[TopActionBarView alloc] init];
    actionBarView.delegate = self;
    [self.view addSubview:actionBarView];

    // 액션바를 뷰컨트롤러 상단에 고정
    [actionBarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.equalTo(self.view);
    }];
}

/**
 액션바 로고 보여짐

 @param show 보여짐 여부
 */
- (void)showLogoImageView:(BOOL)show {
    actionBarView.logoIV.hidden = !show;
}

/**
 액션바 타이틀 보여짐
 
 @param show 보여짐 여부
 */
- (void)showActionBarTitle:(BOOL)show {
    actionBarView.titleLb.hidden = !show;
}

/**
 액션바 메뉴 버튼 보여짐

 @param show 보여짐 여부
 */
- (void)showMenuButton:(BOOL)show {
    actionBarView.menuBtn.hidden = !show;
    actionBarView.backBtn.hidden = show;
}

/**
 액션바 뒤로 버튼 보여짐

 @param show 보여짐 여부
 */
- (void)showBackButton:(BOOL)show {
    actionBarView.backBtn.hidden = !show;
    actionBarView.menuBtn.hidden = show;
}

/**
 액션바 뒤로 버튼 보여짐
 */
- (void)showPromotionButton {
    actionBarView.promotionBtn.hidden = NO;
}

/**
 액션바 검색 버튼 보여짐
 */
- (void)showSearchButton {
    actionBarView.searchBtn.hidden = NO;
}

/**
 액션바 추가 버튼 보여짐
 */
- (void)showAddButton {
    actionBarView.addBtn.hidden = NO;
}

/**
 액션바 지도 버튼 보여짐
 */
- (void)showMapButton {
    actionBarView.mapBtn.hidden = NO;
}

/**
 지도 부분 선택 버튼 보여짐
 */
- (void)showEditMapButton {
    actionBarView.editMapBtn.hidden = NO;
}

/**
 입고 요청 내역 버튼 보여짐
 */
- (void)showHistoryButton {
    actionBarView.historyBtn.hidden = NO;
}

/**
 액션바 타이틀 설정

 @param title 타이틀
 */
- (void)setActionBarTitle:(NSString *)title {
    [self showActionBarTitle:![title isEqualToString:@""]];
    actionBarView.titleLb.text = title;
}

/**
 스토리보드 segue로 페이지 이동

 @param segue 설정된 segue
 @param sender sender
 */
- (void)openViewController:(NSString *)segue sender:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:segue sender:sender];
    });
}

/**
 뷰컨트롤러 닫음
 */
- (void)closeViewController {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}

/**
 기본 루트뷰컨트롤러를 탭바로 설정 (default : home)
 */
- (void)openRootTabBarController {
    LGSideMenuController *smc = [CommonUtil controllerFromMainStoryboard:STORYBOARD_ROOT_ID];
    UITabBarController *tabBarC = (UITabBarController *)smc.rootViewController;
    tabBarC.selectedIndex = 2;
    // 탭바 컨트롤이 용이하게 객체를 담아둠
    [APP_DELEGATE setTabBarC:tabBarC];
    [APP_DELEGATE window].rootViewController = smc;
}

/**
 로그인 페이지 종료 후 이전 뷰컨트롤러 갱신

 @param vc 이전 뷰컨트롤러
 */
- (void)closePresentViewController:(UIViewController *)vc {
    [self dismissViewControllerAnimated:YES completion:^{
        if([vc respondsToSelector:@selector(reloadViewController)]) {
            [(BaseViewController *)vc reloadViewController];
        }
    }];
}

/**
 앱 로그 전송 (로딩뷰 없이)

 @param data parameter
 @param type 로그 타입 (call: 'T' shipping: 'U')
 */
- (void)requestAppLog:(NSString *)data type:(NSString *)type {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:type forKey:@"logtype"];
    [parameters setValue:data forKey:@"logtdata"];
    if([type isEqualToString:kAppLogTypeCall]) { // 전화
        [parameters setValue:@"TB_LOG" forKey:@"logtable"];
    } else if([type isEqualToString:kAppLogTypeShipping]) { // 배송
        [parameters setValue:@"TB_SHIPPING_INFO" forKey:@"logtable"];
    }
    [ApiManager requestToPost:APP_LOG_URL parameters:parameters isLoading:NO success:nil failure:nil];
}

/**
 로그 아웃 요청
 */
- (void)requestLogout {
    [ApiManager requestToPost:LOGOUT_URL parameters:nil success:^(id responseObject) {
        [UserManager removeUserData];
        [self openRootLoginViewController];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        [LoadingView hideLoadingView];
    }];
}


/**
 중요 데이터 요청 실패시 이전 페이지로 이동
 */
- (void)requestFailed {
    [CommonUtil showToastMessage:kDescRequestFailed];
    [self closeViewController];
}

/**
 로그아웃 시 루트뷰컨트롤러를 로그인뷰컨트롤러로 변경
 */
- (void)openRootLoginViewController {
    // 탭바 컨트롤 객체 해제
    [APP_DELEGATE setTabBarC:nil];
    LoginViewController *loginVC = [CommonUtil controllerFromMainStoryboard:STORYBOARD_LOGIN_ID];
    [APP_DELEGATE window].rootViewController = loginVC;
}

/**
 카메라 권한 요청 여부

 @param type photoType (0: 카메라 1: 라이브러리 2: 바코드)
 */
- (void)requestCameraPermission:(PhotoType)type {
    if(type == kPhotoTypeCamera || type == kPhotoTypeBarcode) { // 카메라 사용 시
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        switch (authStatus) {
            case AVAuthorizationStatusAuthorized: // 이전에 권한 허용
                [self showCameraController:type];
                break;
            case AVAuthorizationStatusNotDetermined: { // 권한 요청
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    if(granted) { // 권한 승인
                        [self showCameraController:type];
                    } else { // 권한 거절
                        [CommonUtil showAlertTitle:kTitleCamera msg:kDescCameraCant type:kAlertTypeOne target:self cAction:nil];
                    }
                }];
            }
                break;
            case AVAuthorizationStatusRestricted: // 권한 없음
                [CommonUtil showAlertTitle:kTitleCamera msg:kDescCameraCant type:kAlertTypeOne target:self cAction:nil];
                break;
            case AVAuthorizationStatusDenied: // 이전에 권한 거절
                [CommonUtil openAppSetting];
                break;
            default:
                break;
        }
    } else if(type == kPhotoTypeLibrary) { // 사진 라이브러리 사용 시
        UIImagePickerController *imagePC = [[UIImagePickerController alloc] init];
        imagePC.delegate = self;
        imagePC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePC animated:YES completion:nil];
    }
}

/**
 카메라 컨트롤러 보여짐

 @param type photoType
 */
- (void)showCameraController:(PhotoType)type {
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) { // 카메라 사용 가능 시
        if(type == kPhotoTypeCamera) {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImagePickerController *imagePC = [[UIImagePickerController alloc] init];
                imagePC.delegate = self;
                imagePC.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePC animated:YES completion:nil];
            });
        } else if(type == kPhotoTypeBarcode) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self openViewController:STORYBOARD_BARCODE_SEGUE sender:self];
            });
        }
    } else { // 카메라 사용 불가능 시
        [CommonUtil showAlertTitle:kTitleCamera msg:kDescCameraCant type:kAlertTypeOne target:self cAction:nil];
    }
}

/**
 사진 저장 권한 여부

 @param image 저장할 이미지
 */
- (void)requestSavePhotoPermission:(UIImage *)image {
    PHAuthorizationStatus oldStatus = [PHPhotoLibrary authorizationStatus];
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        if(status == PHAuthorizationStatusDenied) { // 거절
            if(oldStatus != PHAuthorizationStatusNotDetermined) { // 설정 페이지로
                [CommonUtil openAppSetting];
            }
        } else if(status == PHAuthorizationStatusAuthorized) { // 허용
            [CommonUtil requestSaveImage:image];
        } else if(status == PHAuthorizationStatusRestricted) { // 권한 없음
            [CommonUtil openAppSetting];
        }
    }];
}

#pragma mark - ActionBarDelegate
- (void)showSearchView {
}

- (void)hideSearchView {
}

/**
 메뉴 버튼 눌렀을시 사이드메뉴 오픈
 */
- (void)showMenuViewController {
    [CommonUtil showSideMenu];
}

#pragma mark - Keyboard Notification
- (void)keyboardDidShow:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height - tabBarHeight, 0.0);
    _baseSV.contentInset = contentInsets;
    _baseSV.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillHide:(NSNotification *)notification {
    _baseSV.contentInset = UIEdgeInsetsZero;
    _baseSV.scrollIndicatorInsets = UIEdgeInsetsZero;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
