//
//  BaseViewController.h
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopActionBarView.h"

@interface BaseViewController : UIViewController<ActionBarDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    TopActionBarView            *actionBarView;
}

@property (weak, nonatomic) UIScrollView *baseSV;

- (void)addKeyboardObserver;
- (void)removeKeyboardObserver;
- (void)reloadViewController;
- (void)initValue;
- (void)initLayout;
- (void)initLayoutForNoneActionBar;
- (void)initLayoutForLogo;
- (void)initLayoutForTitle:(NSString *)title;
- (void)setActionBarView;
- (void)showLogoImageView:(BOOL)show;
- (void)showActionBarTitle:(BOOL)show;
- (void)showMenuButton:(BOOL)show;
- (void)showBackButton:(BOOL)show;
- (void)showPromotionButton;
- (void)showSearchButton;
- (void)showAddButton;
- (void)showMapButton;
- (void)showEditMapButton;
- (void)showHistoryButton;
- (void)setActionBarTitle:(NSString *)title;
- (void)openViewController:(NSString *)segue sender:(id)sender;
- (void)closeViewController;
- (void)openRootTabBarController;
- (void)requestLogout;
- (void)requestFailed;
- (void)closePresentViewController:(UIViewController *)vc;
- (void)requestCameraPermission:(PhotoType)type;
- (void)requestSavePhotoPermission:(UIImage *)image;

- (void)requestAppLog:(NSString *)data type:(NSString *)type;
@end
