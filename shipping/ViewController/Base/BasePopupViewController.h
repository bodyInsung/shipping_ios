//
//  BasePopupViewController.h
//  shipping
//
//  Created by ios on 2018. 4. 4..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BasePopupViewController : UIViewController

@property (weak, nonatomic) UIScrollView *baseSV;
@property (weak, nonatomic) UIView *baseV;

- (void)initValue;
- (void)initLayout;
- (void)landscapeViewController;
- (void)openViewController:(NSString *)segue sender:(id)sender;
- (void)closeViewController;

@end
