//
//  CompletionTableViewCell.m
//  shipping
//
//  Created by ios on 2018. 3. 21..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "CompletionTableViewCell.h"

@implementation CompletionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellFromData:(ShippingResultData *)data {
    NSString *productImageName = [CommonObject imageNameFromProduct:data.productName];
    _productIV.image = [UIImage imageNamed:productImageName];
    if([data.shippingType hasPrefix:kTitleMove]) {
        if([data.progressNoNm isEqualToString:@"회수"]) {
            _nameLb.text = data.agreeName;
            _addressLb.text = data.addr;
        } else {
            _nameLb.text = data.custName;
            _addressLb.text = data.addr2;
        }
    } else {
        _nameLb.text = data.custName;
        _addressLb.text = data.insAddr;
    }
    _productLb.text = data.productName;
    
    _setupLb.text = [CommonUtil changeDateFormat:DATE_HYPHEN_YEAR_MONTH_DAY dateString:data.inDate changeFormat:DATE_KOREAN_MONTH_DAY_WEEK];
    NSString *description = [NSString isEmptyString:data.shippingType] ? @"배송" : data.shippingType;
    _descriptionLb.text = [NSString stringWithFormat:@"%@완료", description];
    NSString *surveyImageName = [data.eval boolValue] ? @"ic_survey_p" : @"ic_survey_n";
    _surveyIV.image = [UIImage imageNamed:surveyImageName];
}

- (IBAction)pressedCall:(UIButton *)sender {
    _callHandler();
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _productIV.image = nil;
    _nameLb.text = @"";
    _productLb.text = @"";
    _addressLb.text = @"";
    _setupLb.text = @"";
}
@end
