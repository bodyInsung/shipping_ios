//
//  CompletionTableViewCell.h
//  shipping
//
//  Created by ios on 2018. 3. 21..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompletionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *productIV;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UILabel *productLb;
@property (weak, nonatomic) IBOutlet UILabel *addressLb;
@property (weak, nonatomic) IBOutlet UILabel *setupLb;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLb;
@property (weak, nonatomic) IBOutlet UIButton *callBtn;
@property (weak, nonatomic) IBOutlet UIImageView *surveyIV;

@property (strong, nonatomic) void(^callHandler)(void);

- (void)setCellFromData:(ShippingResultData *)data;

@end
