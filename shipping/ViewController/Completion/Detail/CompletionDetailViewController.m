//
//  CompletionDetailViewController.m
//  shipping
//
//  Created by ios on 2018. 3. 28..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "CompletionDetailViewController.h"
#import "DetailBaseTableViewCell.h"
#import "DetailLinkTextTableViewCell.h"
#import "DetailTitleTableViewCell.h"
#import "ImageViewController.h"

static NSString *kDetailBaseCellIdentifier = @"BaseCell";
static NSString *kDetailLinkTextCellIdentifier = @"LinkTextCell";
static NSString *kDetailTitleCellIdentifier = @"TitleCell";

@interface CompletionDetailViewController ()

@end

@implementation CompletionDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestCode:CONTRACTOR_CODE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:STORYBOARD_IMAGE_SEGUE]) {
        ImageViewController *vc = [segue destinationViewController];
        vc.imageUrl = sender;
    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    _confirmHeightLC.constant = _confirmTbV.contentSize.height;
}

- (void)reloadViewController {
    [self requestCode:CONTRACTOR_CODE];
}

- (void)initLayout {
    [super initLayoutForTitle:TITLE_COMPLETE_DETAIL_ACTIONBAR];
    
    [self showDetailView:0];
    
    [_confirmBtn applyRoundBorder:4.0];
    [_attachBackV applyRoundBorder:4.0];
    
    UINib *baseCellNib = [UINib nibWithNibName:@"DetailBaseTableViewCell" bundle:nil];
    [_detailTbV registerNib:baseCellNib forCellReuseIdentifier:kDetailBaseCellIdentifier];
    [_confirmTbV registerNib:baseCellNib forCellReuseIdentifier:kDetailBaseCellIdentifier];
    UINib *linkTextCellNib = [UINib nibWithNibName:@"DetailLinkTextTableViewCell" bundle:nil];
    [_detailTbV registerNib:linkTextCellNib forCellReuseIdentifier:kDetailLinkTextCellIdentifier];
    UINib *titleCellNib = [UINib nibWithNibName:@"DetailTitleTableViewCell" bundle:nil];
    [_detailTbV registerNib:titleCellNib forCellReuseIdentifier:kDetailTitleCellIdentifier];
    
    _confirmTbV.tableFooterView = [[UIView alloc] init];
}

/**
 segment에 선택된 뷰를 보여줌

 @param index 선택된 인덱스
 */
- (void)showDetailView:(NSInteger)index {
    [CommonUtil hideKeyboard];
    if(index == 0) {
        _contentSV.hidden = YES;
        _detailTbV.hidden = NO;
    } else if(index == 1) {
        _detailTbV.hidden = YES;
        _contentSV.hidden = NO;
    }
}

/**
 코드에 따른 요청

 @param code 요청 코드
 */
- (void)requestCode:(NSString *)code {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:code forKey:@"code"];
    [ApiManager requestToPost:SEARCH_CODE_URL parameters:parameters success:^(id responseObject) {
        if([code isEqualToString:CONTRACTOR_CODE]) {
            if(contractorCodeDatas != nil) {
                contractorCodeDatas = nil;
            }
            contractorCodeDatas = [CodeResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
            [self requestCode:SHIPPING_DIVISION_CODE];
        } else if([code isEqualToString:SHIPPING_DIVISION_CODE]) {
            if(divisionCodeDatas != nil) {
                divisionCodeDatas = nil;
            }
            divisionCodeDatas = [CodeResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
            [self requestDetail];
        }
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 상세 데이터 요청
 */
- (void)requestDetail {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"n" forKey:@"deleteFlag"];
    [parameters setValue:_shippingSeq forKey:@"shipping_seq"];
    [ApiManager requestToPost:DETAIL_URL parameters:parameters success:^(id responseObject) {
        resultData = [[DetailResultData alloc] initWithDictionary:responseObject[API_RESPONSE_KEY] error:nil];
        if(resultData == nil) {
            [self requestFailed];
        } else {
            [self setGroupAttachView];
            [self setDetailTitles];
            [CommonUtil reloadTableViewToTop:_detailTbV];
            [_confirmTbV reloadData];
        }
        [LoadingView hideLoadingViewToDelay];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
        [self requestFailed];
    }];
}

/**
 첨부 뷰 설정

 */
- (void)setGroupAttachView {
    [self setAttachViewFromData:[CommonObject ProductSetupAttachArray] superview:_productV];
    [self setAttachViewFromData:[CommonObject ProductAttachArray] superview:_attachV];
    
    _ladderHeightLC.active = YES;
    _craneHeightLC.active = YES;
    _serviceHeightLC.active = YES;
    
    if(![NSString isEmptyString:[self imageNameFromTitle:kTitleLadderAttach1]] ||
       ![NSString isEmptyString:[self imageNameFromTitle:kTitleLadderAttach2]] ||
       ![NSString isEmptyString:[self imageNameFromTitle:kTitleLadderAttach3]]) {
        _ladderHeightLC.active = NO;
        [self setAttachViewFromData:[CommonObject LadderAttachArray] superview:_ladderV];
    }
    if(![NSString isEmptyString:[self imageNameFromTitle:kTitleCraneAttach1]] ||
       ![NSString isEmptyString:[self imageNameFromTitle:kTitleCraneAttach2]] ||
       ![NSString isEmptyString:[self imageNameFromTitle:kTitleCraneAttach3]]) {
        _craneHeightLC.active = NO;
        [self setAttachViewFromData:[CommonObject CraneAttachArray] superview:_craneV];
    }
    if(![NSString isEmptyString:[self imageNameFromTitle:kTitleServiceAttach1]] ||
       ![NSString isEmptyString:[self imageNameFromTitle:kTitleServiceAttach2]]) {
        _serviceHeightLC.active = NO;
        [self setAttachViewFromData:[CommonObject ServiceAttachArray] superview:_serviceV];
    }
}

/**
 데이터에 따른 첨부 뷰 설정

 @param datas 데이터
 @param view 슈퍼뷰
 */
- (void)setAttachViewFromData:(NSArray *)datas superview:(UIView *)view {
    [view removeAllSubviews];
    
    UIView *preView = nil;
    for(int i = 0; i < datas.count; i++) {
        NSString *title = datas[i];
        PhotoView *photoV = [[PhotoView alloc] init];
        [photoV setPhotoMode:NO];
        photoV.titleLb.text = title;
        NSString *imageUrl = [self imageNameFromTitle:title];
        [photoV.photoIV setImageWithBaseUrl:imageUrl];
        [view addSubview:photoV];
        
        CGFloat width = [CommonUtil widthFromSuperview:view count:datas.count padding:8.0];
        [photoV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(view);
            make.width.equalTo(@(width)).priorityHigh();
            make.width.lessThanOrEqualTo(@90);
            if(preView == nil) {
                make.leading.equalTo(view);
            } else {
                make.leading.equalTo(preView.mas_trailing).offset(8);
            }
            
            if(i == datas.count - 1) {
                make.trailing.lessThanOrEqualTo(view);
            }
        }];
        preView = photoV;
    }
}

/**
 이미지 첨부 타이틀에 따른 이미지 데이터 설정

 @param title 이미지 첨부 타이틀
 @return 이미지 데이터
 */
- (NSString *)imageNameFromTitle:(NSString *)title {
    if([title isEqualToString:kTitleProductNo]) {
        return resultData.imageOne;
    } else if([title isEqualToString:kTitleSetup] ||
              [title isEqualToString:kTitleSetupTotal]) {
        return resultData.imageTwo;
    } else if([title isEqualToString:kTitleAttach1]) {
        return resultData.imageThree;
    } else if([title isEqualToString:kTitleAttach2]) {
        return resultData.imageFour;
    } else if([title isEqualToString:kTitleAttach3]) {
        return resultData.imageFive;
    } else if([title isEqualToString:kTitleAttach4]) {
        return resultData.imageSix;
    } else if([title isEqualToString:kTitleLadderAttach1]) {
        return resultData.ladderImageOne;
    } else if([title isEqualToString:kTitleLadderAttach2]) {
        return resultData.ladderImageTwo;
    } else if([title isEqualToString:kTitleLadderAttach3]) {
        return resultData.ladderImageThree;
    } else if([title isEqualToString:kTitleCraneAttach1]) {
        return resultData.socketImageOne;
    } else if([title isEqualToString:kTitleCraneAttach2]) {
        return resultData.socketImageTwo;
    } else if([title isEqualToString:kTitleCraneAttach3]) {
        return resultData.socketImageThree;
    } else if([title isEqualToString:kTitleServiceAttach1]) {
        return resultData.serviceImageOne;
    } else if([title isEqualToString:kTitleServiceAttach2]) {
        return resultData.serviceImageTwo;
    }
    return @"";
}

- (void)setDetailTitles {
    if(detailTitles != nil) {
        [detailTitles removeAllObjects];
        detailTitles = nil;
    }
    ShippingType shippingType = [CommonObject shippingType:resultData.shippingType pNo:resultData.progressNo];
    
    if(shippingType == kShippingTypeMoveReturn) {
        detailTitles = [NSMutableArray arrayWithArray:[CommonObject DetailReturnCompleteArray]];
    } else {
        detailTitles = [NSMutableArray arrayWithArray:[CommonObject DetailCompleteArray]];
    }
}

#pragma mark - Action Event
- (IBAction)pressedConfirm:(id)sender {
    NSString *imageName = resultData.imageSign;
    if([NSString isEmptyString:imageName]) {
        [CommonUtil showToastMessage:kDescLoadImageCant];
    } else {
        [self openViewController:STORYBOARD_IMAGE_SEGUE sender:resultData.imageSign];
    }
}

- (IBAction)changedDetail:(UISegmentedControl *)sender {
    if(sender.selectedSegmentIndex == 0) {
        _detailTbV.hidden = resultData == nil;
    }
    [self showDetailView:sender.selectedSegmentIndex];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(_detailSC.selectedSegmentIndex == 0) {
        tableView.hidden = resultData == nil;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *detailConfirmTitles = [CommonObject DetailCompleteConfirmArray];
    if([tableView isEqual:_detailTbV]) {
        return resultData != nil ? detailTitles.count : 0;
    } else if([tableView isEqual:_confirmTbV]) {
        return resultData != nil ? detailConfirmTitles.count : 0;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *detailConfirmTitles = [CommonObject DetailCompleteConfirmArray];
    NSString *title = nil;
    if([tableView isEqual:_detailTbV]) {
        title = detailTitles[indexPath.row];;
    } else if([tableView isEqual:_confirmTbV]) {
        title = detailConfirmTitles[indexPath.row];
    }
    
    if([title isEqualToString:kTitleContactNo1] ||
       [title isEqualToString:kTitleContactNo2] ||
       [title isEqualToString:kTitleContactNo3] ||
       [title isEqualToString:kTitleSetupTelNo] ||
       [title isEqualToString:kTitleSetupPhoneNo]) {
        DetailLinkTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailLinkTextCellIdentifier];
        cell.valueLb.delegate = self;
        [cell setCellFromData:resultData title:title];
        return cell;
    } else if([title isEqualToString:kTitleMoveSetupInfo]) {
        DetailTitleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailTitleCellIdentifier];
        [cell setCellFromTitle:title];
        return cell;
    } else {
        DetailBaseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailBaseCellIdentifier];
        if([title isEqualToString:kTitleDivision]) {
            NSString *division = [CommonObject codeNamesFromCode:divisionCodeDatas code:resultData.shippingType];
            [cell setCellFromTitle:title value:[CommonObject divisionFromShippingType:division pNo:resultData.progressNo]];
        } else if([title isEqualToString:kTitleDivideState]) {
            [cell setCellFromTitle:title value:[CommonObject codeNamesFromCode:contractorCodeDatas code:resultData.divStatus]];
        } else {
            [cell setCellFromData:resultData title:title];
        }
        return cell;
    }
}

#pragma mark - CTLabelDelegate
- (void)pressedLink:(NSString *)string {
    [CommonUtil openLinkCall:string];
}

@end
