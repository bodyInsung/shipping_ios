//
//  CompletionDetailViewController.h
//  shipping
//
//  Created by ios on 2018. 3. 28..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"

@interface CompletionDetailViewController : BaseViewController <CTLabelDelegate> {
    DetailResultData                *resultData;
    NSArray                         *contractorCodeDatas;
    NSArray                         *divisionCodeDatas;
    NSMutableArray                  *detailTitles;
}

@property (strong, nonatomic) NSString *shippingSeq;
@property (strong, nonatomic) NSString *shippingDivision;
@property (weak, nonatomic) IBOutlet UISegmentedControl *detailSC;
@property (weak, nonatomic) IBOutlet UITableView *detailTbV;
@property (weak, nonatomic) IBOutlet UIScrollView *contentSV;
@property (weak, nonatomic) IBOutlet UITableView *confirmTbV;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIView *attachBackV;
@property (weak, nonatomic) IBOutlet UIView *productV;
@property (weak, nonatomic) IBOutlet UIView *attachV;
@property (weak, nonatomic) IBOutlet UIView *ladderV;
@property (weak, nonatomic) IBOutlet UIView *craneV;
@property (weak, nonatomic) IBOutlet UIView *serviceV;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *ladderHeightLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *craneHeightLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *serviceHeightLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *confirmHeightLC;
@end
