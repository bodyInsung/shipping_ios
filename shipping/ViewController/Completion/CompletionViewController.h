//
//  CompletionViewController.h
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"

@interface CompletionViewController : BaseViewController<ListSearchDelegate> {
    
    NSMutableArray                  *shippingListArray;
    ListSearchView                  *searchV;
}

@property (weak, nonatomic) IBOutlet UIView *searchBackV;
@property (weak, nonatomic) IBOutlet UITableView *listTbV;
@property (weak, nonatomic) IBOutlet UIView *noDataV;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *searchVHeightLC;
@end
