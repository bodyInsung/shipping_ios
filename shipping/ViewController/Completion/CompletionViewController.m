//
//  CompletionViewController.m
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "CompletionViewController.h"
#import "ShippingListHeaderView.h"
#import "CompletionTableViewCell.h"
#import "CompletionDetailViewController.h"

static NSString *kShippingListHeaderIdentifier = @"ShippingListHeader";
static NSString *kCompletionCellIdentifier = @"CompletionCell";

@interface CompletionViewController ()

@end

@implementation CompletionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestCodeArea];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:STORYBOARD_COMPLETION_DETAIL_SEGUE]) {
        CompletionDetailViewController *vc = [segue destinationViewController];
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        ShippingResultData *data = shippingListArray[indexPath.section][indexPath.row];
        vc.shippingSeq = data.shippingSeq;
    }
}

- (void)reloadViewController {
    [self requestCodeArea];
}

- (void)initLayout {
    [super initLayoutForTitle:TITLE_COMPLETE_ACTIONBAR];
    [self showSearchButton];
    [self setBaseSV:_listTbV];
    
    UINib *headerNib = [UINib nibWithNibName:@"ShippingListHeaderView" bundle:nil];
    [_listTbV registerNib:headerNib forHeaderFooterViewReuseIdentifier:kShippingListHeaderIdentifier];
    UINib *cellNib = [UINib nibWithNibName:@"CompletionTableViewCell" bundle:nil];
    [_listTbV registerNib:cellNib forCellReuseIdentifier:kCompletionCellIdentifier];
    
    _listTbV.estimatedSectionHeaderHeight = UITableViewAutomaticDimension;
    
    [self addSearchView];
    [self addPullToRefresh];
}

/**
 검색 뷰 추가
 */
- (void)addSearchView {
    [self hideSearchView];
    
    searchV = [[ListSearchView alloc] init];
    searchV.delegate = self;
    searchV.target = self;
    [_searchBackV addSubview:searchV];
    [searchV applyAutoLayoutFromSuperview:_searchBackV];
}

/**
 당겨서 새로고침 추가
 */
- (void)addPullToRefresh {
    UIRefreshControl *pullToRC = [[UIRefreshControl alloc] init];
    [pullToRC addTarget:self action:@selector(refreshRequestData:) forControlEvents:UIControlEventValueChanged];
    [_listTbV addSubview:pullToRC];
    [_listTbV sendSubviewToBack:pullToRC];
}

/**
 지역코드 요청
 */
- (void)requestCodeArea {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:SEARCH_AREA_CODE forKey:@"code"];
    [parameters setValue:kProductTypeMassage forKey:@"s_producttype"];
    
    [ApiManager requestToPost:SEARCH_CODE_URL parameters:parameters success:^(id responseObject) {
        CodeModel *codeModel = [[CodeModel alloc] initWithDictionary:responseObject error:nil];
        [searchV initValueFromData:codeModel.resultData type:kSearchTypeCompletion date:nil];
        [self requestList:[searchV sendParameters]];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 리스트 데이터 요청

 @param parameters parameters
 */
- (void)requestList:(NSMutableDictionary *)parameters {
    if(parameters != nil) {
        [parameters setValue:@"Y" forKey:@"s_oneself"];
        [parameters setValue:kProductTypeMassage forKey:@"s_producttype"];
    }
    [ApiManager requestToPost:COMPLETION_LIST_URL parameters:parameters success:^(id responseObject) {
        [self restoreResponseData:responseObject];
        
        [_listTbV reloadData];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [CommonUtil reloadTableViewToTop:_listTbV];
        [LoadingView hideLoadingView];
    }];
}

/**
 데이터 재정렬

 @param responseObject 데이터
 */
- (void)restoreResponseData:(id)responseObject {
    if(shippingListArray != nil) {
        [shippingListArray removeAllObjects];
        shippingListArray = nil;
    }
    
    shippingListArray = [NSMutableArray array];
    NSMutableArray *responseList = [NSMutableArray arrayWithArray:responseObject[API_RESPONSE_KEY][@"list"]];
    // DUE_DATE 로 오름차순
    while(responseList.count > 0) {
        NSSortDescriptor *descriptorDate = [NSSortDescriptor sortDescriptorWithKey:@"DUE_DATE" ascending:NO];
        NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:[responseList sortedArrayUsingDescriptors:@[descriptorDate]]];
        // 같은 날짜끼리 묶음
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"DUE_DATE = %@", sortedArray.firstObject[@"DUE_DATE"]];
        [sortedArray filterUsingPredicate:predicate];
        NSMutableArray *shippingList = [ShippingResultData arrayOfModelsFromDictionaries:sortedArray error:nil];
        [responseList removeObjectsInArray:sortedArray];
        [shippingListArray addObject:shippingList];
    }
}

#pragma mark - Action Event
- (void)refreshRequestData:(UIRefreshControl *)sender {
    [self requestList:[searchV sendParameters]];
    [sender endRefreshing];
}

- (IBAction)pressedRetry:(id)sender {
    [self requestList:[searchV sendParameters]];
}

- (void)pressedCall:(UIButton *)sender {
    ShippingResultData *data = shippingListArray[sender.indexPath.section][sender.indexPath.row];
    NSString *phoneNum = nil;
    if([data.shippingType hasPrefix:kTitleMove] &&
       [data.progressNoNm isEqualToString:@"회수"]) {
        phoneNum = data.phoneNo2;
    } else {
        if(![NSString isEmptyString:data.phoneNo]) {
            phoneNum = data.phoneNo;
        } else if(![NSString isEmptyString:data.telNo]) {
            phoneNum = data.telNo;
        }
    }
    
    if([NSString isEmptyString:phoneNum]) {
        [CommonUtil showToastMessage:kDescPhoneNumberNeed];
    } else {
        [CommonUtil openLinkCall:phoneNum];
    }
}

#pragma mark - ActionBarDelegate
- (void)showSearchView {
    _searchVHeightLC.active = NO;
    [self.view layoutIfNeeded];
}

- (void)hideSearchView {
    [CommonUtil hideKeyboard];
    _searchVHeightLC.active = YES;
    [self.view layoutIfNeeded];
}

#pragma mark - ListSearchDelegate
- (void)pressedSearch {
    [self requestList:[searchV sendParameters]];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    tableView.hidden = !(shippingListArray.count > 0);
    _noDataV.hidden = !tableView.isHidden;
    return shippingListArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *listArray = shippingListArray[section];
    return [listArray count];
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    ShippingListHeaderView *headerV = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kShippingListHeaderIdentifier];
    [headerV setHeaderFromData:shippingListArray[section]];
    return headerV;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CompletionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCompletionCellIdentifier];
    
    cell.callHandler = ^{
        ShippingResultData *data = shippingListArray[indexPath.section][indexPath.row];
        NSString *phoneNum = nil;
        if([data.shippingType hasPrefix:kTitleMove] &&
           [data.progressNoNm isEqualToString:@"회수"]) {
            phoneNum = data.phoneNo2;
        } else {
            if(![NSString isEmptyString:data.phoneNo]) {
                phoneNum = data.phoneNo;
            } else if(![NSString isEmptyString:data.telNo]) {
                phoneNum = data.telNo;
            }
        }
        
        if([NSString isEmptyString:phoneNum]) {
            [CommonUtil showToastMessage:kDescPhoneNumberNeed];
        } else {
            [CommonUtil openLinkCall:phoneNum];
        }
    };
    
    [cell setCellFromData:shippingListArray[indexPath.section][indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self openViewController:STORYBOARD_COMPLETION_DETAIL_SEGUE sender:indexPath];
}

@end
