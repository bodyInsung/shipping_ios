//
//  RefuelTableViewCell.h
//  shipping
//
//  Created by insung on 2018. 8. 24..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RefuelTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet CTTextField *valueTF;

- (void)setCell:(NSString *)title;
@end
