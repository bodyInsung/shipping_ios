//
//  RefuelTableViewCell.m
//  shipping
//
//  Created by insung on 2018. 8. 24..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "RefuelTableViewCell.h"

@implementation RefuelTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initLayout];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initLayout {
    [_valueTF setAccessoryView];
}

- (void)setCell:(NSString *)title {
    _titleLb.text = title;
    if([title isEqualToString:kTitleRefuelCard]) {
        _valueTF.keyboardType = UIKeyboardTypeDefault;
    } else {
        _valueTF.keyboardType = UIKeyboardTypeNumberPad;
    }
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _titleLb.text = @"";
}
@end
