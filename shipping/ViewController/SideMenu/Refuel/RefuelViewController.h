//
//  RefuelViewController.h
//  shipping
//
//  Created by insung on 2018. 8. 24..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"

@interface RefuelViewController : BaseViewController <ListPopupViewDelegate> {
    
    NSMutableDictionary         *cellDic;
}
@property (weak, nonatomic) IBOutlet UIScrollView *contentSV;
@property (weak, nonatomic) IBOutlet UILabel *carLb;
@property (weak, nonatomic) IBOutlet UIButton *changeBtn;
@property (weak, nonatomic) IBOutlet UITableView *listTbV;
@property (weak, nonatomic) IBOutlet UIButton *checkBtn;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *listHeightLC;

@end
