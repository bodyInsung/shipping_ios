//
//  RefuelViewController.m
//  shipping
//
//  Created by insung on 2018. 8. 24..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "RefuelViewController.h"
#import "RefuelTableViewCell.h"

static NSString *kRefuelCellIdentifier = @"RefuelCell";
@interface RefuelViewController ()

@end

@implementation RefuelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:STORYBOARD_LIST_POPUP_CAR_SEGUE]) {
        ListPopupViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.datas = sender;
        vc.isSearch = YES;
        vc.popupType = kListPopupTypeCar;
    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    _listHeightLC.constant = _listTbV.contentSize.height;
}

- (void)initValue {
    cellDic = [NSMutableDictionary dictionary];
}

- (void)initLayout {
    [super initLayoutForTitle:TITLE_REFUEL_ACTIONBAR];
    [self setBaseSV:_contentSV];
    
    [_changeBtn applyRoundBorder:4.0];
    [_registerBtn applyRoundBorder:4.0];
    
    UINib *cellNib = [UINib nibWithNibName:@"RefuelTableViewCell" bundle:nil];
    [_listTbV registerNib:cellNib forCellReuseIdentifier:kRefuelCellIdentifier];
    
    [_listTbV reloadData];
    
    [self setCarNumber];
}

- (void)setCarNumber {
    UserResultData *carData = [UserManager loadUserData:USER_CAR_KEY];
    NSString *carNum = carData.carNumber;
    if([NSString isEmptyString:carNum]) {
        _carLb.text = @"차량번호 미선택";
    } else {
        _carLb.text = [NSString stringWithFormat:@"차량번호 %@", carNum];
    }
}

/**
 자동차 리스트 요청
 */
- (void)requestCarList {
    [ApiManager requestToPost:CAR_LIST_URL parameters:nil success:^(id responseObject) {
        NSArray *datas = [CarResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        [self openViewController:STORYBOARD_LIST_POPUP_CAR_SEGUE sender:datas];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)requestRegister {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    UserResultData *carData = [UserManager loadUserData:USER_CAR_KEY];
    NSString *carNum = carData.carNumber;
    NSString *message = nil;
    if([NSString isEmptyString:carNum]) {
        message = kDescCarSelectionNeed;
    } else {
        for(NSString *key in cellDic.allKeys) {
            NSString *parameterKey = nil;
            RefuelTableViewCell *cell = cellDic[key];
            NSString *value = cell.valueTF.text;
            if([key isEqualToString:kTitleRefuelMeter]) { // 주행누계
                if([NSString isEmptyString:value]) { // 주행누계 필수
                    message = [NSString stringWithFormat:@"%@ 입력이 필요합니다", key];
                    break;
                } else {
                    parameterKey = @"dashboard";
                }
            } else if([key isEqualToString:kTitleRefuelLiter]) {                // 주유량
                parameterKey = @"gasLiter";
            } else if([key isEqualToString:kTitleRefuelPrice]) {                // 주유금액
                parameterKey = @"gasPrice";
            } else if([key isEqualToString:kTitleRefuelDieselExhaust]) {        // 요소수
                parameterKey = @"dieselExhaustFluid";
            } else if([key isEqualToString:kTitleRefuelDieselExhaustLiter]) {   // 요소수 리터
                parameterKey = @"dieselExhaustFluidLiter";
            } else if([key isEqualToString:kTitleRefuelEtc]) {                  // 세차비/워셔
                parameterKey = @"ectPrice";
            } else if([key isEqualToString:kTitleRefuelCard]) {                 // 카드소유자
                parameterKey = @"ownerCard";
            }
        
            [parameters setValue:value forKey:parameterKey];
        }
    }
    
    if(message != nil) {
        [CommonUtil showToastMessage:message];
        return;
    }

    [parameters setValue:userInfo.adminNm forKey:@"name"];
    [parameters setValue:carData.carNumber forKey:@"carNumber"];
    [parameters setValue:[CommonUtil stringFromDate:[NSDate date] format:@"yyyyMMddHHmm"] forKey:@"date"];
    [parameters setValue:_checkBtn.isSelected ? @"Y" : @"N" forKey:@"carChang"];
    
    [ApiManager requestToGoogleSheet:GOOGLE_SHEET_REFUEL_URL parameters:parameters success:^(id responseObject) {
        [CommonUtil showToastMessage:kDescRegisterSuccess];
        [self closeViewController];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        [LoadingView hideLoadingView];
    }];
}

#pragma mark - Action Event
- (IBAction)pressedChange:(UIButton *)sender {
    [self requestCarList];
}

- (IBAction)pressedCheck:(UIButton *)sender {
    sender.selected = !sender.isSelected;
}

- (IBAction)pressedRegister:(id)sender {
    [self requestRegister];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [CommonObject RefuelListArray].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RefuelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRefuelCellIdentifier];
    NSString *title = [CommonObject RefuelListArray][indexPath.row];
    [cell setCell:title];
    [cellDic setValue:cell forKey:title];
    return cell;
}

#pragma mark - ListPopupViewDelegate
- (void)selectedItem:(JSONModel *)data type:(ListPopupType)type {
    if(type == kListPopupTypeCar) {
        CarResultData *carData = (CarResultData *)data;
        [UserManager saveData:carData.toDictionary key:USER_CAR_KEY];
        [self setCarNumber];
    }
}
@end
