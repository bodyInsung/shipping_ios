//
//  ScheduleViewController.h
//  shipping
//
//  Created by ios on 2018. 4. 24..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"
#import "ScheduleData.h"

@interface ScheduleViewController : BaseViewController {
    NSArray<ScheduleData>               *schduleData;
}

@property (weak, nonatomic) IBOutlet UIView *description01V;
@property (weak, nonatomic) IBOutlet UIView *description02V;
@property (weak, nonatomic) IBOutlet UISwitch *completionSw;
@property (weak, nonatomic) IBOutlet FSCalendar *calendar;

@end
