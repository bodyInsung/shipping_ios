//
//  ScheduleViewController.m
//  shipping
//
//  Created by ios on 2018. 4. 24..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ScheduleViewController.h"
#import "ScheduleCell.h"
#import "ShippingListViewController.h"

@interface ScheduleViewController ()

@end

@implementation ScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestSchedule:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:STORYBOARD_SHIPPING_LIST_SEGUE]) {
        UINavigationController *naviVC = [segue destinationViewController];
        ShippingListViewController *vc = (ShippingListViewController *)naviVC.topViewController;
        vc.selectedDate = sender;
        vc.searchType = kSearchTypeSchedule;
    }
 }

- (void)reloadViewController {
    [self requestSchedule:nil];
}

- (void)initLayout {
    [super initLayoutForTitle:TITLE_SHIPPING_SCHEDULE_ACTIONBAR];
    
    [_description01V applyRoundBorder:3.0];
    [_description02V applyRoundBorder:3.0];
    [_calendar registerClass:[ScheduleCell class] forCellReuseIdentifier:@"ScheduleCell"];
}

- (void)requestSchedule:(NSDate *)date {
    if(date == nil) {
        date = [NSDate date];
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NSDate *startDate = [CommonUtil firstDateFromFromDate:_calendar.currentPage];
    [parameters setValue:[CommonUtil stringFromDate:startDate format:DATE_DOT_YEAR_MONTH_DAY] forKey:@"dateStart"];
    NSString *currentDate = [CommonUtil stringFromDate:_calendar.currentPage format:DATE_DOT_YEAR_MONTH];
    [parameters setValue:[NSString stringWithFormat:@"%@.%zd", currentDate, [CommonUtil numberOfDaysInMonth:_calendar.currentPage]] forKey:@"dateEnd"];
    [parameters setValue:_completionSw.isOn ? @"Y" : @"N" forKey:@"view_complete"];
    [ApiManager requestToPost:SCHEDULE_URL parameters:parameters success:^(id responseObject) {
        schduleData = (NSArray<ScheduleData> *)[ScheduleData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY][@"list"] error:nil];
        [_calendar reloadData];
        [LoadingView hideLoadingViewToDelay];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

#pragma mark - Action Event
- (IBAction)changedValueCompletion:(id)sender {
    [self requestSchedule:_calendar.currentPage];
}

#pragma mark - FSCalendarDelegate & FSCalendarDataSource
- (void)calendarCurrentPageDidChange:(FSCalendar *)calendar {
    [self requestSchedule:calendar.currentPage];
}

- (__kindof FSCalendarCell *)calendar:(FSCalendar *)calendar cellForDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)position {
    ScheduleCell *cell = [calendar dequeueReusableCellWithIdentifier:@"ScheduleCell" forDate:date atMonthPosition:position];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dueDate = %@", [CommonUtil stringFromDate:date format:DATE_HYPHEN_YEAR_MONTH_DAY]];
    ScheduleData *data = [[schduleData filteredArrayUsingPredicate:predicate] firstObject];
    if(data != nil) {
        cell.sub01Lb.text = [NSString stringWithFormat:@"%@건", data.total];
        cell.sub02Lb.text = [NSString stringWithFormat:@"%@건", data.adCnt];
    } else {
        cell.sub01Lb.text = @"";
        cell.sub02Lb.text = @"";
    }
    cell.sub01V.hidden = data == nil;
    cell.sub02V.hidden = data == nil;
    return cell;
}

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition {
    [calendar deselectDate:date];
    [self openViewController:STORYBOARD_SHIPPING_LIST_SEGUE sender:date];
}

@end
