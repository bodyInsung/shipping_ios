//
//  ScheduleCell.m
//  shipping
//
//  Created by ios on 2018. 4. 27..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ScheduleCell.h"

@implementation ScheduleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] firstObject];
        [self initLayout];
    }
    return self;
}

- (void)initLayout {
    
    [_dot01V applyRoundBorder:3.0];
    [_dot02V applyRoundBorder:3.0];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _sub01Lb.text = @"";
    _sub02Lb.text = @"";
}

@end
