//
//  ScheduleCell.h
//  shipping
//
//  Created by ios on 2018. 4. 27..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "FSCalendarCell.h"

@interface ScheduleCell : FSCalendarCell


@property (weak, nonatomic) IBOutlet UIView *sub01V;
@property (weak, nonatomic) IBOutlet UIView *dot01V;
@property (weak, nonatomic) IBOutlet UILabel *sub01Lb;
@property (weak, nonatomic) IBOutlet UIView *sub02V;
@property (weak, nonatomic) IBOutlet UIView *dot02V;
@property (weak, nonatomic) IBOutlet UILabel *sub02Lb;
@end
