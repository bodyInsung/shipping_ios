//
//  StockViewController.h
//  shipping
//
//  Created by ios on 2018. 4. 24..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"

@interface StockViewController : BaseViewController <ListPopupViewDelegate, BarcodeViewDelegate> {
    
    NSArray                     *radioBtnGroup;
    NSMutableArray              *stockList;
    NSArray                     *locationListData;
    NSMutableArray              *addedProductList;
}

@property (weak, nonatomic) IBOutlet UIButton *warehouseBtn;
@property (weak, nonatomic) IBOutlet UIButton *deliverymanBtn;
@property (weak, nonatomic) IBOutlet UIButton *locationBtn;
@property (weak, nonatomic) IBOutlet UILabel *locationLb;
@property (weak, nonatomic) IBOutlet UIButton *divisionBtn;
@property (weak, nonatomic) IBOutlet UIButton *stockBtn;
@property (weak, nonatomic) IBOutlet UITableView *listTbV;
@end
