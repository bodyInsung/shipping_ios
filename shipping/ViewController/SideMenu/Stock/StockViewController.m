//
//  StockViewController.m
//  shipping
//
//  Created by ios on 2018. 4. 24..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "StockViewController.h"
#import "StockTableViewCell.h"

@interface StockViewController ()

@end

static NSString *kStockCellIdentifier = @"StockCell";

@implementation StockViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:STORYBOARD_LIST_POPUP_WAREHOUSE_SEGUE]) {
        ListPopupViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.datas = sender;
        vc.isSearch = YES;
        if(_warehouseBtn.isSelected == YES) {
            vc.popupType = kListPopupTypeWarehouse;
        } else if(_deliverymanBtn.isSelected == YES) {
            vc.popupType = kListPopupTypeWarehousingDeliveryman;
        }
    } else if([[segue identifier] isEqualToString:STORYBOARD_LIST_POPUP_PRODUCT_SEGUE]) {
        ListPopupViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.datas = sender;
        vc.isSearch = YES;
        vc.popupType = kListPopupTypeStock;
    }
}
        
- (void)initValue {
    [super initValue];
    
    radioBtnGroup = @[_warehouseBtn, _deliverymanBtn];
    addedProductList = [NSMutableArray array];
}

- (void)initLayout {
    [super initLayoutForTitle:TITLE_STOCK_ACTIONBAR];
    [self showHistoryButton];
    
    [_locationBtn applyRoundBorder:4.0];
    [_divisionBtn applyRoundBorder:4.0];
    [_stockBtn applyRoundBorder:4.0];
    
    UINib *cellNib = [UINib nibWithNibName:@"StockTableViewCell" bundle:nil];
    [_listTbV registerNib:cellNib forCellReuseIdentifier:kStockCellIdentifier];
    
    _listTbV.tableFooterView = [[UIView alloc] init];
    
    [self pressedGroupRadio:_warehouseBtn];
    [self changeBtnTitle:_divisionBtn index:0];
}

- (void)requestLocationList {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    // 1000: 창고 2000: 기사
    [parameters setValue:_warehouseBtn.isSelected ? @"1000" : @"2000" forKey:@"groupCd"];
    [ApiManager requestToPost:WAREHOUSE_LIST_URL parameters:parameters success:^(id responseObject) {
        if(locationListData != nil) {
            locationListData = nil;
        }
        locationListData = [ReleaseWarehouseData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        [self changeBtnTitle:_locationBtn index:0];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 제품 재고 리스트
 */
- (void)requestStockList {
    [ApiManager requestToPost:DELIVERYMAN_STOCK_LIST_URL parameters:nil success:^(id responseObject) {
        NSArray *data = [ProductValidationData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        if(data.count > 0) {
            [self openViewController:STORYBOARD_LIST_POPUP_PRODUCT_SEGUE sender:data];
        } else {
            [CommonUtil showToastMessage:kDescStockNone];
        }
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 사은품 리스트
 */
- (void)requestGiftList {
    [ApiManager requestToPost:GIFT_LIST_URL parameters:nil success:^(id responseObject) {
        NSArray *data = [ReleaseProductData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        [self openViewController:STORYBOARD_LIST_POPUP_PRODUCT_SEGUE sender:data];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)addListValidationData:(ProductValidationData *)data {
    BOOL isContain = NO;
    for(ProductValidationData *addedData in addedProductList) {
        if([addedData.serialCd isEqualToString:@"*"] == NO &&
           [addedData.serialCd isEqualToString:data.serialCd]) {
            isContain = YES;
        }
    }
    
    if(isContain == NO) {
        data.goodAmount = @"1";
        [addedProductList addObject:data];
    }
    [_listTbV reloadData];
}

- (void)addListGiftData:(ReleaseProductData *)data {
    BOOL isAdded = NO;
    for(ProductValidationData *listData in addedProductList) {
        if([listData.itemCd isEqualToString:data.itemCd]) {
            isAdded = YES;
            break;
        }
    }
    if(isAdded == NO) {
        ProductValidationData *giftData = [[ProductValidationData alloc] init];
        giftData.itemCd = data.itemCd;
        giftData.itemNm = data.itemNm;
        giftData.giftYn = data.giftYn;
        giftData.goodAmount = @"1";
        [addedProductList addObject:giftData];
    }
    [_listTbV reloadData];
}

- (void)requestWarehousing {
    if(addedProductList.count == 0) {
        [CommonUtil showToastMessage:kDescProductAddNeed];
        return;
    }
    
    NSMutableDictionary *items = [NSMutableDictionary dictionary];
    [items setValue:[self warehouseCode:YES] forKey:@"toSlCd"];
    [items setValue:[self warehouseCode:NO] forKey:@"plantCd"];
    
    NSString *moveType = nil;
    if(_warehouseBtn.isSelected) {
        moveType = [CommonObject codeFromReleaseDataType:kReleaseDataTypeWarehouseMove name:_divisionBtn.currentTitle];
    } else if(_deliverymanBtn.isSelected) {
        moveType = [CommonObject codeFromReleaseDataType:kReleaseDataTypeDeliverymanMove name:_divisionBtn.currentTitle];
    }
    [items setValue:moveType forKey:@"movType"];
    
    NSMutableArray* datas = [NSMutableArray array];
    for(ProductValidationData *data in addedProductList) { // 재고에서 선택된 제품
        NSMutableDictionary *item = [NSMutableDictionary dictionary];
        [item setValue:data.serialCd == nil ? @"" : data.serialCd forKey:@"serialCd"];
        [item setValue:data.itemCd forKey:@"itemCd"];
        [item setValue:data.goodAmount forKey:@"qty"];
        [datas addObject:item];
    }
    
    [items setValue:datas forKey:@"itemList"];
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:items options:kNilOptions error:nil];
    NSString* jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:jsonString forKey:@"jsonData"];
    
    [ApiManager requestToPost:WAREHOUSING_URL parameters:parameters success:^(id responseObject) {
        ResultModel *resultData = [[ResultModel alloc] initWithDictionary:responseObject error:nil];
        if([resultData.resultCode isEqualToString:CONNECT_SUCCESS_CODE]) {
            [addedProductList removeAllObjects];
            [_listTbV reloadData];
        }
        [CommonUtil showToastMessage:resultData.resultMsg];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (NSString *)codeFromData:(NSArray *)datas name:(NSString *)name {
    for(JSONModel *data in datas) {
        if([data isKindOfClass:[ReleaseGroupData class]]) {
            ReleaseGroupData *groupData = (ReleaseGroupData *)data;
            if([groupData.itemGroupNm isEqualToString:name]) {
                return groupData.itemGroupCd;
            }
        }
    }
    return @"";
}

- (NSString *)warehouseCode:(BOOL)isWarehouse {
    for(ReleaseWarehouseData *data in locationListData) {
        if([data.slNm isEqualToString:_locationBtn.currentTitle]) {
            if(isWarehouse == YES) {
                return data.slCd;
            } else {
                return data.plantCd;
            }
        }
    }
    return @"";
}

- (void)changeBtnTitle:(UIButton *)btn index:(NSInteger)index {
    NSString *title = nil;
    if([btn isEqual:_locationBtn]) {
        ReleaseWarehouseData *data = locationListData[index];
        title = data.slNm;
    } else if([btn isEqual:_divisionBtn]) {
        if(_warehouseBtn.isSelected == YES) {
            NSArray *names = [CommonObject WarehousingNameArray];
            title = names[index];
        } else if(_deliverymanBtn.isSelected == YES) {
            NSArray *names = [CommonObject DeliverymanMoveNameArray];
            title = names[index];
        }
    }
    [btn setTitle:title forState:UIControlStateNormal];
}

- (void)checkLocationButton {
    if(_warehouseBtn.isSelected == YES) {
        _locationLb.text = @"창고위치";
    } else {
        _locationLb.text = @"배송기사";
    }
    
    [_listTbV reloadData];
    [self changeBtnTitle:_divisionBtn index:0];
    [self requestLocationList];
}

#pragma mark - Action Event
- (IBAction)pressedGroupRadio:(UIButton *)sender {
    if(sender.isSelected) {
        return;
    }
    [UIButton selectBtnGroup:sender group:radioBtnGroup];
    [self checkLocationButton];
}

- (IBAction)pressedLocation:(UIButton *)sender {
    [self openViewController:STORYBOARD_LIST_POPUP_WAREHOUSE_SEGUE sender:locationListData];
}

- (IBAction)pressedDivision:(UIButton *)sender {
    NSArray *names = [CommonObject WarehousingNameArray];
    if(_warehouseBtn.isSelected == YES) {
        names = [CommonObject WarehousingNameArray];
    } else if(_deliverymanBtn.isSelected == YES) {
        names = [CommonObject DeliverymanMoveNameArray];
    }
    [CommonUtil showActionSheet:names target:self selected:^(NSInteger index) {
        [self changeBtnTitle:sender index:index];
    }];
}

- (IBAction)pressedStock:(UIButton *)sender {
    [self requestStockList];
}

- (IBAction)pressedRequest:(UIButton *)sender {
    [self requestWarehousing];
}

- (void)pressedHistory {
    [self openViewController:STORYBOARD_STOCK_HISTORY_SEGUE sender:self];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return addedProductList.count;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row < addedProductList.count) {
        [addedProductList removeObjectAtIndex:indexPath.row];
    }
    [_listTbV reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    StockTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kStockCellIdentifier forIndexPath:indexPath];
    ProductValidationData *data = addedProductList[indexPath.row];;
    [cell setCellFromData:data];
    return cell;
}

#pragma mark - ListPopupViewDelegate
- (void)selectedItem:(JSONModel *)data type:(ListPopupType)type {
    if(type == kListPopupTypeStock) {
        ProductValidationData *productData = (ProductValidationData *)data;
        [self addListValidationData:productData];
    } else if(type == kListPopupTypeWarehouse ||
              type == kListPopupTypeWarehousingDeliveryman) {
        ReleaseWarehouseData *warehouseData = (ReleaseWarehouseData *)data;
        [_locationBtn setTitle:warehouseData.slNm forState:UIControlStateNormal];
    }
}
@end
