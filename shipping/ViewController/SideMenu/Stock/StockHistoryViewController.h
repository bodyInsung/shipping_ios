//
//  StockHistoryViewController.h
//  shipping
//
//  Created by insung on 24/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"
#import "StockHistoryCell.h"

@interface StockHistoryViewController : BaseViewController <BarcodeViewDelegate> {
    StockHistoryCell                  *infinitiveCell;
    NSMutableArray                          *listData;
}

@property (weak, nonatomic) IBOutlet UISegmentedControl *historySC;
@property (weak, nonatomic) IBOutlet UICollectionView *listCV;
@property (weak, nonatomic) IBOutlet UIView *noDataV;
@end
