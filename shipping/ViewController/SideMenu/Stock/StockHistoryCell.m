//
//  StockHistoryCell.m
//  shipping
//
//  Created by insung on 25/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "StockHistoryCell.h"

@implementation StockHistoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initLayout];
}

- (void)initLayout {
    [_contentV applyRoundBorder:4.0 color:UIColorFromRGB(0xB5B5B5) width:0.4];
    [_confirmBtn applyRoundBorder:4.0];
}

- (void)setCellFromData:(NSArray *)datas isRequest:(BOOL)isRequest {
    ReleaseListData *firstData = datas.firstObject;
    _dateLb.text = firstData.reqDt;
    _titleLb.text = firstData.issueNo;
    _locationLb.text = isRequest ? firstData.toSlNm : firstData.fromSlNm;
    
    _stateLb.text = firstData.movTypeNm;
    _countLb.text = [NSString stringWithFormat:@"%zd", datas.count];
    
    NSMutableString *value = [NSMutableString string];
    NSMutableArray *codes = [NSMutableArray array];
    for(ReleaseListData *data in datas) {
        if([NSString isEmptyString:value] == NO) {
            [value appendString:@"\n\n"];
        }
        NSString *itemNm = data.itemNm;
        if([NSString isEmptyString:itemNm] == NO) {
            [value appendFormat:@"제품명 : %@", itemNm];
        }

        NSString *code = nil;
        if([data.giftYn isEqualToString:@"Y"]) {
            code = data.itemCd;
        } else {
            code = [data.serialCd isEqualToString:@"*"] ? @"" : data.serialCd;
        }
        if([NSString isEmptyString:code] == NO) {
            [value appendFormat:@"\n코드 : %@", code];
            [codes addObject:code];
        }
    }
    
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:value];
    for(NSString *code in codes) {
        [attributed addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:[value rangeOfString:code]];
    }
    _valueLb.attributedText = attributed;
    _confirmBtn.hidden = isRequest;
    _confirmHeightLC.active = !isRequest;
}

- (IBAction)pressedDelete:(id)sender {
    _deleteHandler();
}

- (IBAction)pressedConfirm:(UIButton *)sender {
    _confirmHandler();
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _dateLb.text = nil;
    _titleLb.text = nil;
    _stateLb.text = nil;
    _countLb.text = nil;
    _valueLb.text = nil;
}

@end
