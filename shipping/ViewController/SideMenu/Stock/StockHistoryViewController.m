//
//  StockHistoryViewController.m
//  shipping
//
//  Created by insung on 24/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "StockHistoryViewController.h"

@interface StockHistoryViewController ()

@end

static NSString *kHistoryCellIdentifier = @"historyCell";

@implementation StockHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:STORYBOARD_BARCODE_SEGUE]) {
        BarcodeViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.isUseImage = NO;
    }
}

- (void)initLayout {
    [super initLayoutForTitle:TITLE_WAREHOUSING_HISTORY_ACTIONBAR];
    
    UINib *historyCellNib = [UINib nibWithNibName:@"StockHistoryCell" bundle:nil];
    [_listCV registerNib:historyCellNib forCellWithReuseIdentifier:kHistoryCellIdentifier];

    // 셀의 높이를 구하기 위해 셀 오브젝트를 가지고 있는다
    infinitiveCell = [[historyCellNib instantiateWithOwner:nil options:nil] firstObject];
    
    [self checkSegment:0];
}

- (void)requestSendHistory {
    [ApiManager requestToPost:WAREHOUSING_SEND_HISTORY_URL parameters:nil success:^(id responseObject) {
        [self restoreResultData:responseObject];
        [_listCV reloadData];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)requestReceiveHistory {
    [ApiManager requestToPost:WAREHOUSING_RECEIVE_HISTORY_URL parameters:nil success:^(id responseObject) {
        if(listData != nil) {
            listData = nil;
        }
        [self restoreResultData:responseObject];
        [_listCV reloadData];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)restoreResultData:(id)datas {
    if(listData != nil) {
        [listData removeAllObjects];
        listData = nil;
    }
    listData = [NSMutableArray array];
    NSMutableArray *responseList = [NSMutableArray arrayWithArray:datas[API_RESPONSE_KEY]];
    while(responseList.count > 0) {
        NSMutableArray *filterArray = [NSMutableArray arrayWithArray:responseList];
        [filterArray sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"yyyy-MM-dd"];
            NSDate *date1 = [df dateFromString:obj1[@"REQ_DT"]];
            NSDate *date2 = [df dateFromString:obj2[@"REQ_DT"]];
            return [date2 compare:date1];
        }];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ISSUE_NO = %@", filterArray.firstObject[@"ISSUE_NO"]];
        [filterArray filterUsingPredicate:predicate];
        NSMutableArray *shippingList = [ReleaseListData arrayOfModelsFromDictionaries:filterArray error:nil];
        [responseList removeObjectsInArray:filterArray];
        [listData addObject:shippingList];
    }
}

- (void)requestRemove:(NSString *)no {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:no forKey:@"issueNo"];
    
    [ApiManager requestToPost:RELEASE_DELETE_URL parameters:parameters success:^(id responseObject) {
        ResultModel *resultData = [[ResultModel alloc] initWithDictionary:responseObject error:nil];
        [CommonUtil showToastMessage:resultData.resultMsg];
        if([resultData.resultCode isEqualToString:CONNECT_SUCCESS_CODE]) {
            [self requestSendHistory];
        } else {
            [LoadingView hideLoadingView];
        }
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)requestConfirm:(NSString *)issueNo {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:issueNo forKey:@"issueNo"];
    [ApiManager requestToPost:TRANSFER_CONFIRM_URL parameters:parameters success:^(id responseObject) {
        ProductValidationModel *resultData = [[ProductValidationModel alloc] initWithDictionary:responseObject error:nil];
        if([NSString isEmptyString:resultData.resultMsg] == NO) {
            [CommonUtil showToastMessage:resultData.resultMsg];
        }
        [self requestReceiveHistory];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)checkSegment:(NSInteger)index {
    if(index == 0) {
        [self requestSendHistory];
    } else {
        [self requestReceiveHistory];
    }
}

#pragma mark - Action Event
- (IBAction)pressedRetry:(UIButton *)sender {
    [self checkSegment:_historySC.selectedSegmentIndex];
}

- (IBAction)changedHistory:(UISegmentedControl *)sender {
    [self checkSegment:sender.selectedSegmentIndex];
}

#pragma mark - UICollectionViewDataSource & UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    collectionView.hidden = !(listData.count > 0);
    _noDataV.hidden = !collectionView.isHidden;
    return listData.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    StockHistoryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kHistoryCellIdentifier forIndexPath:indexPath];
    NSArray *datas = listData[indexPath.row];
    ReleaseListData *data = datas.firstObject;
    cell.deleteHandler = ^{
        [CommonUtil showAlertTitle:kTitleDelete msg:kDescDeleteQuestion type:kAlertTypeTwo target:self cAction:^{
            [self requestRemove:data.issueNo];
        }];
    };
    cell.confirmHandler = ^{
        [self requestConfirm:data.issueNo];
    };
    [cell setCellFromData:datas isRequest:_historySC.selectedSegmentIndex == 0];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    [infinitiveCell setCellFromData:listData[indexPath.row] isRequest:_historySC.selectedSegmentIndex == 0];
    // 적용된 컨텐츠뷰의 사이즈 높이를 리턴
    CGFloat height = [infinitiveCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    UICollectionViewFlowLayout* flow = (UICollectionViewFlowLayout *)collectionView.collectionViewLayout;
    return CGSizeMake(collectionView.bounds.size.width - flow.sectionInset.left - flow.sectionInset.left, height);
}
@end
