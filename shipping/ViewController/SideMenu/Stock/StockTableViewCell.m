//
//  StockTableViewCell.m
//  shipping
//
//  Created by insung on 24/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "StockTableViewCell.h"

@implementation StockTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellFromData:(ProductValidationData *)data {
    _nameLb.text = data.itemNm;
    _nameHeightLC.active = [NSString isEmptyString:data.itemNm];
    
    if([data.giftYn isEqualToString:@"Y"]) {
        _codeLb.text = data.itemCd;
    } else {
        _codeLb.text = [data.serialCd isEqualToString:@"*"] ? @"확인 불필요" : data.serialCd;
    }
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _nameLb.text = nil;
    _codeLb.text = nil;
    _nameHeightLC.active = NO;
}

@end
