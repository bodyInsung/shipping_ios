//
//  StockHistoryCell.h
//  shipping
//
//  Created by insung on 25/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StockHistoryCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *contentV;
@property (weak, nonatomic) IBOutlet UILabel *dateLb;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet UILabel *stateLb;
@property (weak, nonatomic) IBOutlet UILabel *locationLb;
@property (weak, nonatomic) IBOutlet UILabel *countLb;
@property (weak, nonatomic) IBOutlet UILabel *valueLb;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *confirmHeightLC;

@property (strong, nonatomic) void(^deleteHandler)(void);
@property (strong, nonatomic) void(^confirmHandler)(void);

- (void)setCellFromData:(NSArray *)datas isRequest:(BOOL)isRequest;
@end
