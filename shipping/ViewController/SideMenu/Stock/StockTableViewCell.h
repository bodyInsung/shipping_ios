//
//  StockTableViewCell.h
//  shipping
//
//  Created by insung on 24/04/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StockTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UILabel *codeLb;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *nameHeightLC;

- (void)setCellFromData:(ProductValidationData *)data;
@end
