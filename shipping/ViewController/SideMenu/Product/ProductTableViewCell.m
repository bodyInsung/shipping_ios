//
//  ProductTableViewCell.m
//  shipping
//
//  Created by ios on 2018. 4. 24..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ProductTableViewCell.h"

@implementation ProductTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellFromData:(ProductResultData *)data {
    _nameLb.text = data.productName;
    UIImage *image = [UIImage imageNamed:[CommonObject imageNameFromProduct:data.productName]];
    _productIV.image = image;
    _descriptionLb.text = data.productSpec;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _productIV.image = nil;
    _nameLb.text = @"";
    _descriptionLb.text = @"";
}

@end
