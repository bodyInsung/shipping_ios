//
//  ProductDetailViewController.m
//  shipping
//
//  Created by ios on 2018. 4. 25..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ProductDetailViewController.h"

@interface ProductDetailViewController ()

@end

@implementation ProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestDetail];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)reloadViewController {
    [self requestDetail];
}

- (void)initLayout {
    [super initLayoutForTitle:TITLE_PRODUCT_DETAIL_ACTIONBAR];
}

- (void)requestDetail {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:_code forKey:@"productcode"];
    [ApiManager requestToPost:PRODUCT_DETAIL_URL parameters:parameters success:^(id responseObject) {
        ProductResultData *resultData = [[ProductResultData alloc] initWithDictionary:responseObject[API_RESPONSE_KEY] error:nil];
        [self setDetailData:resultData];
        [LoadingView hideLoadingViewToDelay];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)setDetailData:(ProductResultData *)data {
    NSString *videoId = [[data.productIntro componentsSeparatedByString:@"?v="] lastObject];
    [_playerV loadWithVideoId:videoId];
    
    if([NSString isEmptyString:videoId]) {
        _playerV.hidden = YES;
        [_mainIV setImageWithBaseUrl:data.productImageL];
    }
    
    _nameLb.text = data.productName;
    _specLb.text = data.productSpec;
    _descriptionLb.text = data.productDetail;
    NSMutableString *caution = [NSMutableString string];
    [caution appendString:data.productCaution1];
    [caution appendString:data.productCaution2];
    [caution appendString:data.productCaution3];
    [caution appendString:data.productCaution4];
    [caution appendString:data.productCaution5];
    _cautionLb.text = caution;
}

@end
