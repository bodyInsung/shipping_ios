//
//  ProductViewController.h
//  shipping
//
//  Created by ios on 2018. 4. 24..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"

@interface ProductViewController : BaseViewController {
    NSArray<ProductResultData>              *resultData;
}

@property (weak, nonatomic) IBOutlet UIButton *dataBtn;
@property (weak, nonatomic) IBOutlet UITableView *listTbV;
@property (weak, nonatomic) IBOutlet UIView *noDataV;
@end
