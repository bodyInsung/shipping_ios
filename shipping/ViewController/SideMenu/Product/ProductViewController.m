//
//  ProductViewController.m
//  shipping
//
//  Created by ios on 2018. 4. 24..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ProductViewController.h"
#import "ProductTableViewCell.h"
#import "ProductDetailViewController.h"

static NSString *kProductCellIdentifier = @"ProductCell";
@interface ProductViewController ()

@end

@implementation ProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:STORYBOARD_PRODUCT_DETAIL_SEGUE]) {
        ProductDetailViewController *vc = [segue destinationViewController];
        vc.code = sender;
    }
}

- (void)reloadViewController {
    [self requestList];
}

- (void)initLayout {
    [super initLayoutForTitle:TITLE_PRODUCT_ACTIONBAR];
    
    [_dataBtn applyRoundBorder:4.0];
    
    UINib *cellNib = [UINib nibWithNibName:@"ProductTableViewCell" bundle:nil];
    [_listTbV registerNib:cellNib forCellReuseIdentifier:kProductCellIdentifier];
}

- (void)requestList {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:kProductTypeMassage forKey:@"s_producttype"];
    
    [ApiManager requestToPost:PRODUCT_LIST_URL parameters:parameters success:^(id responseObject) {
        if(resultData != nil) {
            resultData = nil;
        }
        resultData = (NSArray<ProductResultData> *)[ProductResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY][@"list"] error:nil];
        [_listTbV reloadData];
        [LoadingView hideLoadingViewToDelay];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

#pragma mark - Action Event
- (IBAction)pressedGuidePDF:(id)sender {
    [CommonUtil openLinkWeb:GUIDE_PDF_URL];
}

- (IBAction)pressedRetry:(id)sender {
    [self requestList];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    tableView.hidden = !(resultData.count > 0);
    _noDataV.hidden = !tableView.isHidden;
    return resultData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kProductCellIdentifier];
    [cell setCellFromData:resultData[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ProductResultData *data = resultData[indexPath.row];
    [self openViewController:STORYBOARD_PRODUCT_DETAIL_SEGUE sender:data.productCode];
}

@end
