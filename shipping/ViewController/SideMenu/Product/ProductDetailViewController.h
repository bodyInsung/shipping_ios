//
//  ProductDetailViewController.h
//  shipping
//
//  Created by ios on 2018. 4. 25..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"

@interface ProductDetailViewController : BaseViewController

@property (strong, nonatomic) NSString *code;
@property (weak, nonatomic) IBOutlet YTPlayerView *playerV;
@property (weak, nonatomic) IBOutlet UIImageView *mainIV;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UILabel *specLb;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLb;
@property (weak, nonatomic) IBOutlet UILabel *cautionLb;
@end
