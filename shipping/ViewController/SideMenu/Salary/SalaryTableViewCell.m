//
//  SalaryTableViewCell.m
//  shipping
//
//  Created by ios on 2018. 5. 8..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "SalaryTableViewCell.h"

@implementation SalaryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellFromData:(NSArray<SalaryResultData> *)datas {

    NSString *date = nil;
    NSMutableString *description = [NSMutableString string];
    NSMutableArray *datasArray = [NSMutableArray arrayWithArray:datas];
    SalaryResultData *data = datas.firstObject;
    NSInteger total = 0;
    
    while(datasArray.count > 0) {
        SalaryResultData *firstData = datasArray.firstObject;
        NSString *type = firstData.type;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type = %@", type];
        NSArray *filterArray = [datasArray filteredArrayUsingPredicate:predicate];
        
        NSInteger dataTotal = 0;
        NSInteger count = 0;
        CGFloat distance = 0.0;
        
        for(SalaryResultData *data in filterArray) {
            if([data.type isEqualToString:@"거리제 수당"]) {
                dataTotal += [data.total integerValue];
                distance += [data.dailyCount floatValue];
            } else if([data.type isEqualToString:@"성과급수당"]) { // 표기 하지 않기로 함
            } else {
                dataTotal += [data.total integerValue];
                count += [data.dailyCount integerValue];
            }
        }
        [datasArray removeObjectsInArray:filterArray];
        
        NSString *strTotal = [NSString addCommaFromInteger:dataTotal];
        if(description.length > 0 &&
           (count > 0 || distance > 0)) {
            [description appendString:@"\n"];
        }
        if(count > 0) { // 거리제 수당 이외
            [description appendFormat:@"%@ : %@원 (%zd건)", type, strTotal, count];
        }
        if(distance > 0) { // 거리제 수당 (거리 표기)
            [description appendFormat:@"%@ : %@원 (%.1f)", type, strTotal, distance];
        }
        // 총 합계
        total += dataTotal;
    }
    
    if([NSString isEmptyString:date]) { // 최초 한번만 일자 표기
        date = [CommonUtil changeDateFormat:DATE_HYPHEN_YEAR_MONTH_DAY dateString:data.inDate changeFormat:@"d일"];
    }
    
    _dateLb.text = date;
    _totalLb.text = [NSString stringWithFormat:@"%@원", [NSString addCommaFromInteger:total]];
    _descriptionLb.text = description;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _dateLb.text = @"";
    _totalLb.text = @"";
    _descriptionLb.text = @"";
}

@end
