//
//  SalaryViewController.m
//  shipping
//
//  Created by ios on 2018. 5. 8..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "SalaryViewController.h"
#import "SalaryTableViewCell.h"

static NSString *kSalaryCellIdentifier = @"SalaryCell";
@interface SalaryViewController ()

@end

@implementation SalaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestSalary];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)reloadViewController {
    [self requestSalary];
}

- (void)initValue {
    NSDate *todayDate = [NSDate date];
    startDate = [CommonUtil firstDateFromFromDate:todayDate];
    endDate = todayDate;
    [_searchDateBtn setTitle:[CommonUtil stringFromDate:endDate format:DATE_KOREAN_YEAR_MONTH] forState:UIControlStateNormal];
    
    yearArray = [NSMutableArray array];
    NSInteger year = [[CommonUtil todayFromDateFormat:@"yyyy"] integerValue];
    
    for(int i = (int)(year-30); i <= year; i++) {
        [yearArray addObject:[NSString stringWithFormat:@"%d년", i]];
    }
    monthArray = [NSMutableArray array];
    for(int i = 0; i < 12; i++) {
        [monthArray addObject:[NSString stringWithFormat:@"%d월", i+1]];
    }
}

- (void)initLayout {
    [super initLayoutForTitle:TITLE_SALARY_ACTIONBAR];
    
    [_searchDateV applyRoundBorder:4.0 color:UIColorFromRGB(0xB5B5B5) width:0.4];
    [_searchBtn applyRoundBorder:4.0];
    
    UINib *cellNib = [UINib nibWithNibName:@"SalaryTableViewCell" bundle:nil];
    [_listTbV registerNib:cellNib forCellReuseIdentifier:kSalaryCellIdentifier];
    
    [self setDatePicker];
    [self addPullToRefresh];
}

/**
 데이터피커 설정
 */
- (void)setDatePicker {
    if(dateTF != nil) {
        [dateTF removeFromSuperview];
        dateTF = nil;
    }
    
    dateTF = [[CTTextField alloc] init];
    [self.view addSubview:dateTF];
    dateTF.accessoryDelegate = self;
    [dateTF setPickerView:self];
}

- (void)addPullToRefresh {
    UIRefreshControl *pullToRC = [[UIRefreshControl alloc] init];
    [pullToRC addTarget:self action:@selector(refreshRequestData:) forControlEvents:UIControlEventValueChanged];
    [_listTbV addSubview:pullToRC];
}

/**
 수당 데이터 요청
 */
- (void)requestSalary {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    [parameters setValue:userInfo.adminId forKey:@"employeeId"];
    [parameters setValue:[CommonUtil stringFromDate:startDate format:DATE_HYPHEN_YEAR_MONTH_DAY] forKey:@"dateStart"];
    [parameters setValue:[CommonUtil stringFromDate:endDate format:DATE_HYPHEN_YEAR_MONTH_DAY] forKey:@"dateEnd"];
    
    [ApiManager requestToPost:SALARY_MONTHLY_LIST_URL parameters:parameters success:^(id responseObject) {
        SalaryResultData *data = [(NSArray<SalaryResultData> *)[SalaryResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil] firstObject];
        [self setTotalValue:data];
        [self requestSalaryList];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)requestSalaryList {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    [parameters setValue:userInfo.adminId forKey:@"employeeId"];
    [parameters setValue:[CommonUtil stringFromDate:startDate format:DATE_HYPHEN_YEAR_MONTH_DAY] forKey:@"dateStart"];
    [parameters setValue:[CommonUtil stringFromDate:endDate format:DATE_HYPHEN_YEAR_MONTH_DAY] forKey:@"dateEnd"];
    
    [ApiManager requestToPost:SALARY_DAILY_LIST_URL parameters:parameters success:^(id responseObject) {
        NSLog(@"response : %@", responseObject);
        [self restoreResultData:responseObject];
        [_listTbV reloadData];
        [LoadingView hideLoadingViewToDelay];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)restoreResultData:(id)responseObject {
    if(salaryListArray != nil) {
        [salaryListArray removeAllObjects];
        salaryListArray = nil;
    }

    salaryListArray = [NSMutableArray array];
    @try {
        NSMutableArray *responseList = [NSMutableArray arrayWithArray:responseObject[API_RESPONSE_KEY]];
        // IN_DATE로 오름차순
        while(responseList.count > 0) {
            NSSortDescriptor *descriptorDate = [NSSortDescriptor sortDescriptorWithKey:@"IN_DATE" ascending:YES];
            NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:[responseList sortedArrayUsingDescriptors:@[descriptorDate]]];
            // 같은 날짜끼리 묶음
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"IN_DATE = %@", sortedArray.firstObject[@"IN_DATE"]];
            [sortedArray filterUsingPredicate:predicate];
            
            NSMutableArray *list = [SalaryResultData arrayOfModelsFromDictionaries:sortedArray error:nil];
            [responseList removeObjectsInArray:sortedArray];
            [salaryListArray addObject:list];
        }
    } @catch(NSException *error) {
    }
}

- (void)setTotalValue:(SalaryResultData *)data {
    _totalLb.text = [NSString stringWithFormat:@"%@원", [NSString addCommaFromString:data.grandTotal]];
}

#pragma mark - Action Event
- (void)refreshRequestData:(UIRefreshControl *)sender {
    [self reloadViewController];
    [sender endRefreshing];
}

- (IBAction)pressedSearchDate:(UIButton *)sender {
    NSString *todayYear = [CommonUtil stringFromDate:startDate format:@"yyyy년"];
    NSString *todayMonth = [CommonUtil stringFromDate:startDate format:@"M월"];
    [dateTF.contentPV selectRow:[yearArray indexOfObject:todayYear] inComponent:0 animated:YES];
    [dateTF.contentPV selectRow:[monthArray indexOfObject:todayMonth] inComponent:1 animated:YES];
    [dateTF becomeFirstResponder];
}

- (IBAction)pressedSearch:(id)sender {
    [self requestSalary];
}

- (IBAction)pressedRetry:(id)sender {
    [self reloadViewController];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    _listV.hidden = !([salaryListArray count] > 0);
    _noDataV.hidden = !_listV.isHidden;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return salaryListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SalaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kSalaryCellIdentifier];
    NSArray<SalaryResultData> *datas = salaryListArray[indexPath.row];
    [cell setCellFromData:datas];
    return cell;
}

#pragma mark - UIPickerViewDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(component == 0) {
        return yearArray.count;
    } else {
        return monthArray.count;
    }
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(component == 0) {
        return yearArray[row];
    } else {
        return monthArray[row];
    }
}

#pragma mark - CTTextFieldDelegate
- (void)pressedDone:(id)sender {
    if([sender isKindOfClass:[UIPickerView class]]) {
        UIPickerView *pv = (UIPickerView *)sender;
        NSInteger yearIndex = [pv selectedRowInComponent:0];
        NSInteger monthIndex = [pv selectedRowInComponent:1];
        
        NSString *selectedDateString = [NSString stringWithFormat:@"%@ %@", yearArray[yearIndex], monthArray[monthIndex]];
        NSDate *selectedDate = [CommonUtil dateFromString:selectedDateString format:DATE_KOREAN_YEAR_MONTH];
        startDate = [CommonUtil firstDateFromFromDate:selectedDate];
        NSString *endDateString = [NSString stringWithFormat:@"%@ %zd일", selectedDateString, [CommonUtil numberOfDaysInMonth:selectedDate]];
        endDate = [CommonUtil dateFromString:endDateString format:DATE_KOREAN_YEAR_MONTH_DAY];
        [_searchDateBtn setTitle:[CommonUtil stringFromDate:selectedDate format:DATE_KOREAN_YEAR_MONTH] forState:UIControlStateNormal];
    }
    [dateTF resignFirstResponder];
}
@end
