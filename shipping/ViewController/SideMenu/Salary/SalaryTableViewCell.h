//
//  SalaryTableViewCell.h
//  shipping
//
//  Created by ios on 2018. 5. 8..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalaryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLb;
@property (strong, nonatomic) IBOutlet UILabel *totalLb;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLb;

- (void)setCellFromData:(NSArray<SalaryResultData> *)datas;
@end
