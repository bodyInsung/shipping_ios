//
//  SalaryViewController.h
//  shipping
//
//  Created by ios on 2018. 5. 8..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"

@interface SalaryViewController : BaseViewController<CTTextFieldDelegate> {
    CTTextField                         *dateTF;
    NSDate                              *startDate;
    NSDate                              *endDate;
    NSMutableArray                      *yearArray;
    NSMutableArray                      *monthArray;
    
    NSMutableArray                      *salaryListArray;
}

@property (weak, nonatomic) IBOutlet UIView *searchDateV;
@property (weak, nonatomic) IBOutlet UIButton *searchDateBtn;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet UIView *noDataV;
@property (weak, nonatomic) IBOutlet UIView *listV;
@property (weak, nonatomic) IBOutlet UILabel *totalLb;
@property (weak, nonatomic) IBOutlet UITableView *listTbV;
@end
