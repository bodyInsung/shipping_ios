//
//  BarcodeGenerateViewController.m
//  shipping
//
//  Created by insung on 28/05/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "BarcodeGenerateViewController.h"

@interface BarcodeGenerateViewController ()

@end

@implementation BarcodeGenerateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initLayout {
    [super initLayoutForTitle:@"바코드 생성"];
    
    _barcodeIV.image = [UIImage imageWithCIImage:[CommonUtil generateBarcode:@"CT123481848T"]];
}

@end
