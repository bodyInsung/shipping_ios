//
//  BarcodeGenerateViewController.h
//  shipping
//
//  Created by insung on 28/05/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"

@interface BarcodeGenerateViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *barcodeIV;
@end
