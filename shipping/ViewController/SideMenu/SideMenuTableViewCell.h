//
//  SideMenuTableViewCell.h
//  shipping
//
//  Created by ios on 2018. 4. 24..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconIV;
@property (weak, nonatomic) IBOutlet UILabel *titleLb;

- (void)setCellFromData:(NSString *)title;
@end
