//
//  SideMenuViewController.h
//  shipping
//
//  Created by ios on 2018. 4. 23..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuViewController : BaseViewController <ListPopupViewDelegate> {
    
    NSArray                     *menuArray;
}

@property (weak, nonatomic) IBOutlet UILabel *mainNameLb;
@property (weak, nonatomic) IBOutlet UILabel *carNumberLb;
@property (weak, nonatomic) IBOutlet UIView *sub01V;
@property (weak, nonatomic) IBOutlet UIView *sub02V;
@property (weak, nonatomic) IBOutlet UIView *sub03V;
@property (weak, nonatomic) IBOutlet UILabel *subName01Lb;
@property (weak, nonatomic) IBOutlet UILabel *subPhone01Lb;
@property (weak, nonatomic) IBOutlet UILabel *subName02Lb;
@property (weak, nonatomic) IBOutlet UILabel *subPhone02Lb;
@property (weak, nonatomic) IBOutlet UILabel *subName03Lb;
@property (weak, nonatomic) IBOutlet UILabel *subPhone03Lb;
@property (weak, nonatomic) IBOutlet UITableView *menuTbV;
@end
