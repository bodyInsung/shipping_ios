//
//  SideMenuTableViewCell.m
//  shipping
//
//  Created by ios on 2018. 4. 24..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "SideMenuTableViewCell.h"

@implementation SideMenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellFromData:(NSString *)title {
    NSString *imageName = nil;
    if([title isEqualToString:kTitleMenuSalary]) {
        imageName = @"menu_icon9";
    } else if([title isEqualToString:kTitleMenuRefuel]) {
        imageName = @"menu_icon10";
    } else if([title isEqualToString:kTitleMenuSale]) {
        imageName = @"menu_icon11";
    } else if([title isEqualToString:kTitleMenuSaleHistory]) {
        imageName = @"menu_icon12";
    } else if([title isEqualToString:kTitleMenuSchedule]) {
        imageName = @"menu_icon1";
    } else if([title isEqualToString:kTitleMenuShippingList]) {
        imageName = @"menu_icon2";
    } else if([title isEqualToString:kTitleMenuReturnList]) {
        imageName = @"menu_icon3";
    } else if([title isEqualToString:kTitleMenuCompleteList] ||
              [title isEqualToString:kTitleMenuRelease] ||
              [title isEqualToString:kTitleMenuCarRegister]) {
        imageName = @"menu_icon4";
    } else if([title isEqualToString:kTitleMenuStock]) {
        imageName = @"menu_icon1";
    } else if([title isEqualToString:kTitleMenuBarcode]) {
        
        imageName = @"menu_icon13";
    } else if([title isEqualToString:kTitleMenuCarClean]) {
        imageName = @"menu_icon8";
    } else if([title isEqualToString:kTitleMenuProduct]) {
        imageName = @"menu_icon5";
    } else if([title isEqualToString:kTitleMenuNotice] ||
              [title isEqualToString:kTitleMenuSurvey]) {
        imageName = @"menu_icon6";
    } else if([title isEqualToString:kTitleMenuLogout]) {
        imageName = @"menu_icon7";
    }
    _iconIV.image = [UIImage imageNamed:imageName];
    _titleLb.text = title;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _iconIV.image = nil;
    _titleLb.text = nil;
}

@end
