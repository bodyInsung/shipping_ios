//
//  CleanViewController.m
//  shipping
//
//  Created by ios on 2018. 4. 24..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "CleanViewController.h"

@interface CleanViewController ()

@end

@implementation CleanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initValue {
    selectedPhotos = [NSMutableDictionary dictionary];
}

- (void)initLayout {
    [super initLayoutForTitle:TITLE_CAR_CLEAN_ACTIONBAR];
    
    [_backV applyRoundBorder:4.0];
    [_registerBtn applyRoundBorder:4.0];
    
    [self setPhotoView];
}

- (void)setPhotoView {
    [_contentV removeAllSubviews];
    
    NSArray *datas = [CommonObject CarCleanAttachArray];
    UIView *preView = nil;
    for(int i = 0; i < datas.count; i++) {
        PhotoView *photoV = [[PhotoView alloc] init];
        photoV.delegate = self;
        photoV.titleLb.text = datas[i];
        [_contentV addSubview:photoV];
        
        CGFloat width = [CommonUtil widthFromSuperview:_contentV count:datas.count/2 padding:8.0];
        [photoV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(width)).priorityHigh();
            if(preView == nil) {
                make.top.equalTo(_contentV);
                make.leading.equalTo(_contentV);
            } else {
                if(i % (datas.count/2) == 0) {
                    make.top.equalTo(preView.mas_bottom).offset(8);
                    make.leading.equalTo(_contentV);
                    make.bottom.equalTo(_contentV);
                } else {
                    make.top.equalTo(preView);
                    make.bottom.equalTo(preView);
                    make.leading.equalTo(preView.mas_trailing).offset(8);
                }
            }
            
            if(i == datas.count - 1) {
                make.trailing.lessThanOrEqualTo(_contentV);
            }
        }];
        preView = photoV;
    }
}

- (void)requestRegister {
    NSMutableDictionary *images = [NSMutableDictionary dictionary];
    BOOL isPhoto = NO;
    for(int i = 0; i < [_contentV subviews].count; i++) {
        PhotoView *view = [_contentV subviews][i];
        NSString *title = view.titleLb.text;
        UIImage *image = [selectedPhotos objectForKey:title];
        if(image != nil) {
            isPhoto = YES;
            [images setValue:image forKey:[NSString stringWithFormat:@"screen%d", (i+1)]];
        }
    }
    
    if(!isPhoto) {
        [CommonUtil showToastMessage:kDescImageAttachNeed];
        return;
    }
    
    [ApiManager requestToMultipart:CAR_CLEAN_URL parameters:nil uploadImages:images success:^(id responseObject) {
        [LoadingView hideLoadingView];
        [self closeViewController];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

#pragma mark - Action Event
- (IBAction)pressedRegister:(id)sender {
    [self requestRegister];
}

#pragma mark - PhotoViewDelegate
- (void)pressedPhotoView:(PhotoView *)sender {
    selectedPhotoV = sender;
    NSArray *array = [CommonObject PhotoTypeArray];
    [CommonUtil showActionSheet:array target:self selected:^(NSInteger index) {
        [self requestCameraPermission:(PhotoType)index];
    }];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    [selectedPhotos setValue:image forKey:selectedPhotoV.titleLb.text];
    [selectedPhotoV.plusBtn setImage:image forState:UIControlStateNormal];
    [picker dismissViewControllerAnimated:YES completion:nil];
}


@end
