//
//  CleanViewController.h
//  shipping
//
//  Created by ios on 2018. 4. 24..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"

@interface CleanViewController : BaseViewController <PhotoViewDelegate> {
    NSMutableDictionary             *selectedPhotos;
    PhotoView                       *selectedPhotoV;
}

@property (weak, nonatomic) IBOutlet UIView *backV;
@property (weak, nonatomic) IBOutlet UIView *contentV;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;
@end
