//
//  SaleViewController.m
//  shipping
//
//  Created by insung on 14/02/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "SaleViewController.h"

@interface SaleViewController ()

@end

@implementation SaleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestProductList];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initLayout {
    [super initLayoutForTitle:TITLE_SALE_ACTIONBAR];
    [self setBaseSV:_contentSV];
    
    [_modelBtn applyRoundBorder:4.0];
    [_saleTypeBtn applyRoundBorder:4.0];
    [_nTypeBtn applyRoundBorder:4.0];
    
    [_nameTF setAccessoryView];
    [_phoneTF setAccessoryView];
    [_telTF setAccessoryView];
    [_etcTF setAccessoryView];
    
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    _serviceNameLb.text = userInfo.adminNm;
}

- (void)requestProductList {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"M" forKey:@"producttype"];
    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_SERVICE, PRODUCT_LIST_URL];
    [ApiManager requestToPost:url parameters:parameters isLoading:YES success:^(id responseObject) {
        if(productData != nil) {
            productData = nil;
        }
        productData = (NSArray<ProductResultData> *)[ProductResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY][@"list"] error:nil];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)requestRegister {
    NSString *message = nil;
    if([NSString isEmptyString:_modelBtn.currentTitle] == YES) {
        message = @"제품을 선택해주세요";
    } else if([NSString isEmptyString:_nameTF.text] == YES) {
        message = @"고객명을 입력해주세요";
    } else if([NSString isEmptyString:_telTF.text] == YES) {
        message = @"연락처를 입력해주세요";
    } else if([NSString isEmptyString:_saleTypeBtn.currentTitle] == YES) {
        message = @"렌탈/판매를 선택해주세요";
    } else if([NSString isEmptyString:_nTypeBtn.currentTitle] == YES) {
        message = @"기존/신규를 선택해주세요";
    }
    
    if([NSString isEmptyString:message] == NO) {
        [CommonUtil showToastMessage:message];
        return;
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    [parameters setValue:userInfo.adminId forKey:@"managerid"];
    [parameters setValue:userInfo.adminNm forKey:@"managername"];
    [parameters setValue:@"M" forKey:@"producttype"];
    [parameters setValue:_saleTypeBtn.currentTitle forKey:@"salestype"];
    [parameters setValue:_nTypeBtn.currentTitle forKey:@"ntype"];
    [parameters setValue:_modelBtn.currentTitle forKey:@"productname"];
    [parameters setValue:_nameTF.text forKey:@"inusername"];
    [parameters setValue:_telTF.text forKey:@"intel"];
    [parameters setValue:_phoneTF.text forKey:@"inhandphone"];
    [parameters setValue:_etcTF.text forKey:@"etcclausal"];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_SERVICE, SALE_REGISTER_URL];
    [ApiManager requestToPost:url parameters:parameters isLoading:YES success:^(id responseObject) {
        CodeModel *code = [[CodeModel alloc] initWithDictionary:responseObject error:nil];
        [CommonUtil showToastMessage:code.resultMsg];
        [self closeViewController];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)changeModelTitle:(NSInteger)index {
    ProductResultData *data = productData[index];
    [_modelBtn setTitle:data.productName forState:UIControlStateNormal];
}

- (void)changeSaleTitle:(NSInteger)index {
    NSString *title = [CommonObject SaleTypeArray][index];
    [_saleTypeBtn setTitle:title forState:UIControlStateNormal];
}

- (void)changeNewTitle:(NSInteger)index {
    NSString *title = [CommonObject SaleNewTypeArray][index];
    [_nTypeBtn setTitle:title forState:UIControlStateNormal];
}

#pragma mark - Action Event
- (IBAction)pressedModel:(UIButton *)sender {
    [CommonUtil showActionSheet:[CommonObject titlesFromData:productData] target:self selected:^(NSInteger index) {
        [self changeModelTitle:index];
    }];
}

- (IBAction)pressedSaleType:(UIButton *)sender {
    [CommonUtil showActionSheet:[CommonObject SaleTypeArray] target:self selected:^(NSInteger index) {
        [self changeSaleTitle:index];
    }];
}

- (IBAction)pressedNewType:(UIButton *)sender {
    [CommonUtil showActionSheet:[CommonObject SaleNewTypeArray] target:self selected:^(NSInteger index) {
        [self changeNewTitle:index];
    }];
}

- (IBAction)pressedRegister:(UIButton *)sender {
    [self requestRegister];
}


@end
