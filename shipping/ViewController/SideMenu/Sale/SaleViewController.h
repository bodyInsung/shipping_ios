//
//  SaleViewController.h
//  shipping
//
//  Created by insung on 14/02/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"

@interface SaleViewController : BaseViewController {
    NSArray<ProductResultData>              *productData;
}

@property (weak, nonatomic) IBOutlet UIScrollView *contentSV;
@property (weak, nonatomic) IBOutlet UIButton *modelBtn;
@property (weak, nonatomic) IBOutlet UIButton *saleTypeBtn;
@property (weak, nonatomic) IBOutlet UIButton *nTypeBtn;
@property (weak, nonatomic) IBOutlet CTTextField *nameTF;
@property (weak, nonatomic) IBOutlet UILabel *serviceNameLb;
@property (weak, nonatomic) IBOutlet CTTextField *phoneTF;
@property (weak, nonatomic) IBOutlet CTTextField *telTF;
@property (weak, nonatomic) IBOutlet CTTextField *etcTF;

@end
