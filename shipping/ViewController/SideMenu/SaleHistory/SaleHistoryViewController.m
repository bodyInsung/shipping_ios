//
//  SaleHistoryViewController.m
//  shipping
//
//  Created by insung on 26/02/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "SaleHistoryViewController.h"
#import "SaleHistoryHeaderView.h"
#import "SaleHistoryTableCell.h"

static NSString *kSaleHistoryHeaderIdentifier = @"SaleHistoryHeader";
static NSString *kSaleHistoryCellIdentifier = @"SaleHistoryCell";
@interface SaleHistoryViewController ()

@end

@implementation SaleHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestHistory];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initLayout {
    [super initLayoutForTitle:TITLE_SALE_HISTORY_ACTIONBAR];
    
    UINib *headerNib = [UINib nibWithNibName:@"SaleHistoryHeaderView" bundle:nil];
    [_listTbV registerNib:headerNib forHeaderFooterViewReuseIdentifier:kSaleHistoryHeaderIdentifier];
    
    UINib *cellNib = [UINib nibWithNibName:@"SaleHistoryTableCell" bundle:nil];
    [_listTbV registerNib:cellNib forCellReuseIdentifier:kSaleHistoryCellIdentifier];
}

- (void)requestHistory {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    [parameters setValue:userInfo.adminId forKey:@"managerid"];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_SERVICE, SALE_HISTORY_URL];
    [ApiManager requestToPost:url parameters:parameters success:^(id responseObject) {
        NSLog(@"response : %@", responseObject);
        if(resultData != nil) {
            [resultData removeAllObjects];
            resultData = nil;
        }
        resultData = [SaleHistoryData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY][@"list"] error:nil];
        [_listTbV reloadData];
        [LoadingView hideLoadingViewToDelay];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

#pragma mark - Action Event
- (IBAction)pressedRetry:(id)sender {
    [self requestHistory];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SaleHistoryHeaderView *headerV = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kSaleHistoryHeaderIdentifier];
    return headerV;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    tableView.hidden = !([resultData count] > 0);
    _noDataV.hidden = !tableView.isHidden;
    return resultData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SaleHistoryTableCell *cell = [tableView dequeueReusableCellWithIdentifier:kSaleHistoryCellIdentifier];
    [cell setCellFromData:resultData[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    SaleHistoryData *data = resultData[indexPath.row];
    if([NSString isEmptyString:data.receivePs] == NO) {
        [CommonUtil showAlertTitle:@"상담내용" msg:data.receivePs type:kAlertTypeOne target:self cAction:nil];
    }
}

@end
