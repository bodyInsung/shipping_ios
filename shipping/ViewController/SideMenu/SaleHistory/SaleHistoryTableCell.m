//
//  SaleHistoryTableCell.m
//  shipping
//
//  Created by insung on 26/02/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "SaleHistoryTableCell.h"

@implementation SaleHistoryTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellFromData:(SaleHistoryData *)data {
    _dateLb.text = data.regDate;
    _nameLb.text = data.inUserName;
    _typeLb.text = data.productTypeNm;
    _numLb.text = data.inTel;
    if([NSString isEmptyString:data.salesConfirm]) {
        _stateLb.text = @"실패";
    } else {
        _stateLb.text = data.salesConfirm;
    }
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _dateLb.text = nil;
    _nameLb.text = nil;
    _typeLb.text = nil;
    _numLb.text = nil;
    _stateLb.text = nil;
}

@end
