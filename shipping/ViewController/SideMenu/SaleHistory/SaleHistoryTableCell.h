//
//  SaleHistoryTableCell.h
//  shipping
//
//  Created by insung on 26/02/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SaleHistoryData.h"

@interface SaleHistoryTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLb;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UILabel *typeLb;
@property (weak, nonatomic) IBOutlet UILabel *numLb;
@property (weak, nonatomic) IBOutlet UILabel *stateLb;

- (void)setCellFromData:(SaleHistoryData *)data;
@end
