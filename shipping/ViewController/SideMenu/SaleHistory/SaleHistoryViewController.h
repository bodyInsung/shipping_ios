//
//  SaleHistoryViewController.h
//  shipping
//
//  Created by insung on 26/02/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"
#import "SaleHistoryData.h"

@interface SaleHistoryViewController : BaseViewController {
    
    NSMutableArray               *resultData;
}

@property (weak, nonatomic) IBOutlet UITableView *listTbV;
@property (weak, nonatomic) IBOutlet UIView *noDataV;
@end

