//
//  SideMenuViewController.m
//  shipping
//
//  Created by ios on 2018. 4. 23..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "SideMenuViewController.h"
#import "SideMenuTableViewCell.h"
#import "SideMenuFooterView.h"
#import "ShippingListViewController.h"

static NSString *kMenuCellIdentifier = @"MenuCell";
static NSString *kMenuFooterIdentifier = @"MenuFooter";

@interface SideMenuViewController ()

@end

@implementation SideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestMenuInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:STORYBOARD_SHIPPING_LIST_SEGUE]) {
        UINavigationController *naviVC = [segue destinationViewController];
        ShippingListViewController *vc = (ShippingListViewController *)naviVC.topViewController;
        vc.searchType = (SearchType)[sender integerValue];
    } else if([[segue identifier] isEqualToString:STORYBOARD_LIST_POPUP_CAR_SEGUE]) {
        ListPopupViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.datas = sender;
        vc.isSearch = YES;
        vc.popupType = kListPopupTypeCar;
    }
}

- (void)initValue {
    [super initValue];
    menuArray = [CommonObject MenuListArray];
    if([CommonObject isOutsourcing]) { // 외주 기사의 경우 입고요청 메뉴 제거
        [[menuArray firstObject] removeObject:kTitleMenuStock];
    }
}

- (void)initLayout {
    [super initLayoutForNoneActionBar];
    
    UINib *cellNib = [UINib nibWithNibName:@"SideMenuTableViewCell" bundle:nil];
    [_menuTbV registerNib:cellNib forCellReuseIdentifier:kMenuCellIdentifier];
    UINib *footerlNib = [UINib nibWithNibName:@"SideMenuFooterView" bundle:nil];
    [_menuTbV registerNib:footerlNib forHeaderFooterViewReuseIdentifier:kMenuFooterIdentifier];
    
    [self setMainInfo];
}

- (void)requestMenuInfo {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:CHAIR_DELIVERYMAN_CODE forKey:@"code"];
    [ApiManager requestToPost:SEARCH_CODE_URL parameters:parameters success:^(id responseObject) {
        NSArray<CodeResultData> *resultData = (NSArray<CodeResultData> *)[CodeResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        [self setMenuInfo:resultData];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)requestCarList {
    [ApiManager requestToPost:CAR_LIST_URL parameters:nil success:^(id responseObject) {
        NSArray *datas = [CarResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        [self openViewController:STORYBOARD_LIST_POPUP_CAR_SEGUE sender:datas];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)setMainInfo {
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    _mainNameLb.text = userInfo.adminNm;
    
    CarResultData *carData = [UserManager loadUserData:USER_CAR_KEY];
    if(![NSString isEmptyString:carData.carNumber]) {
        _carNumberLb.text = carData.carNumber;
    }
}

- (void)setMenuInfo:(NSArray<CodeResultData> *)resultData {
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ref1 = %@", userInfo.perCd];
    NSArray *restoreData = [resultData filteredArrayUsingPredicate:predicate];

    NSInteger maxCount = restoreData.count;
    if(restoreData.count > 3) {
        maxCount = 3;
    }
    for(int i = 0; i < maxCount; i++) {
        CodeResultData *data = restoreData[i];
        if(i == 0) {
            _sub01V.hidden = NO;
            _subName01Lb.text = data.detCdNm;
            _subPhone01Lb.text = data.desc;
        } else if(i == 1) {
            _sub02V.hidden = NO;
            _subName02Lb.text = data.detCdNm;
            _subPhone02Lb.text = data.desc;
        } else if(i == 2) {
            _sub03V.hidden = NO;
            _subName03Lb.text = data.detCdNm;
            _subPhone03Lb.text = data.desc;
        }
    }
}

#pragma mark - Action Event
- (IBAction)pressedSub01:(id)sender {
    [CommonUtil openLinkCall:_subPhone01Lb.text];
}

- (IBAction)pressedSub02:(id)sender {
    [CommonUtil openLinkCall:_subPhone02Lb.text];
}

- (IBAction)pressedSub03:(id)sender {
    [CommonUtil openLinkCall:_subPhone03Lb.text];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [menuArray[section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.5;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SideMenuFooterView *footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kMenuFooterIdentifier];
    return footer;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SideMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kMenuCellIdentifier];
    NSString *title = menuArray[indexPath.section][indexPath.row];
    [cell setCellFromData:title];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [CommonUtil hideSideMenu];
    
    NSString *title = menuArray[indexPath.section][indexPath.row];
    if([title isEqualToString:kTitleMenuRefuel]) {
        [self openViewController:STORYBOARD_REFUEL_SEGUE sender:self];
    } else if([title isEqualToString:kTitleMenuSale]) {
        [self openViewController:STORYBOARD_SALE_SEGUE sender:self];
    } else if([title isEqualToString:kTitleMenuSaleHistory]) {
        [self openViewController:STORYBOARD_SALE_HISTORY_SEGUE sender:self];
    } else if([title isEqualToString:kTitleMenuSchedule]) {
        [self openViewController:STORYBOARD_SCHEDULE_SEGUE sender:self];
    } else if([title isEqualToString:kTitleMenuSalary]) {
        [self openViewController:STORYBOARD_SALARY_SEGUE sender:self];
    } else if([title isEqualToString:kTitleMenuShippingList]) {
        [CommonUtil selectTabBarController:kTabBarTypeShipping];
    } else if([title isEqualToString:kTitleMenuReturnList]) {
        [self openViewController:STORYBOARD_SHIPPING_LIST_SEGUE sender:@(kSearchTypeReturn)];
    } else if([title isEqualToString:kTitleMenuCompleteList]) {
        [CommonUtil selectTabBarController:kTabBarTypeComplete];
    } else if([title isEqualToString:kTitleMenuStock]) {
        [self openViewController:STORYBOARD_STOCK_SEGUE sender:self];
    } else if([title isEqualToString:kTitleMenuBarcode]) {
        [self openViewController:STORYBOARD_BARCODE_GENERATE_SEGUE sender:self];
    } else if([title isEqualToString:kTitleMenuRelease]) {
        [CommonUtil selectTabBarController:kTabBarTypeRelease];
    } else if([title isEqualToString:kTitleMenuCarRegister]) {
        [self requestCarList];
    } else if([title isEqualToString:kTitleMenuCarClean]) {
        CarResultData *carData = [UserManager loadUserData:USER_CAR_KEY];
        if([NSString isEmptyString:carData.carNumber]) {
            [CommonUtil showAlertTitle:kTitleMenuCarRegister msg:kDescCarRegisterNeed type:kAlertTypeTwo target:self cAction:^{
                [self requestCarList];
            }];
        } else {
            [self openViewController:STORYBOARD_CLEAN_SEGUE sender:self];
        }
    } else if([title isEqualToString:kTitleMenuProduct]) {
        [self openViewController:STORYBOARD_PRODUCT_SEGUE sender:self];
    } else if([title isEqualToString:kTitleMenuNotice]) {
        [CommonUtil selectTabBarController:kTabBarTypeNotice];
    } else if([title isEqualToString:kTitleMenuSurvey]) {
        [CommonUtil openLinkWeb:SURVEY_URL];
    } else if([title isEqualToString:kTitleMenuLogout]) {
        [CommonUtil showAlertTitle:kTitleMenuLogout msg:kDescLogoutQuestion type:kAlertTypeTwo target:self cAction:^{
            [self requestLogout];
        }];
    }
}

#pragma mark - ListPopupViewDelegate
- (void)selectedItem:(JSONModel *)data type:(ListPopupType)type {
    if(type == kListPopupTypeCar) {
        CarResultData *carData = (CarResultData *)data;
        [UserManager saveData:carData.toDictionary key:USER_CAR_KEY];
        [self setMainInfo];
    }
}

@end
