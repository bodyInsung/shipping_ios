//
//  PwFindViewController.m
//  shipping
//
//  Created by insung on 2018. 10. 5..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "PwFindViewController.h"

@interface PwFindViewController ()

@end

@implementation PwFindViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initLayout {
    [super initLayoutForTitle:@"비밀번호 찾기"];
    [self setBaseSV:_contentSV];
    
    [_idTF setAccessoryView];
    [_phoneTF setAccessoryView];
    [_validationTF setAccessoryView];
    [_pwTF setAccessoryView];
    [_pwConfirmTF setAccessoryView];
    [_requestBtn applyRoundBorder:4.0];

    [self showView:_requestV];
}

- (void)showView:(UIView *)view {
    _requestV.hidden = YES;
    _validationV.hidden = YES;
    _passwordV.hidden = YES;
    _requestBottomLC.active = NO;
    _validationBottomLC.active = NO;
    _pwBottomLC.active = NO;
    
    if([view isEqual:_requestV]) {
        _requestBottomLC.active = YES;
        _descriptionLb.text = @"아이디와 휴대폰 번호를 입력하시면 임시 비밀번호를 발급해 드립니다.";
        [_requestBtn setTitle:@"임시 비밀번호 요청" forState:UIControlStateNormal];
    } else if([view isEqual:_validationV]) {
        _validationBottomLC.active = YES;
        _descriptionLb.text = @"발급 받으신 임시 비밀번호를 입력해주세요.";
        [_requestBtn setTitle:@"임시 비밀번호 입력" forState:UIControlStateNormal];
    } else if([view isEqual:_passwordV]) {
        _pwBottomLC.active = YES;
        _descriptionLb.text = @"변결하실 비밀번호를 입력해주세요.";
        [_requestBtn setTitle:@"비밀번호 변경" forState:UIControlStateNormal];
    }
    view.hidden = NO;
}

- (void)requestTempKey {
    if([NSString isEmptyString:_idTF.text]) {
        [CommonUtil showToastMessage:kDescIDInputNeed];
        return;
    } else if([NSString isEmptyString:_phoneTF.text]) {
        [CommonUtil showToastMessage:kDescPhoneInputNeed];
        return;
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:_idTF.text forKey:@"admin_id"];
    [parameters setValue:_phoneTF.text forKey:@"telNo"];
    
    [ApiManager requestToPost:TEMPKEY_URL parameters:parameters success:^(id responseObject) {
        ResultModel *result = [[ResultModel alloc] initWithDictionary:responseObject error:nil];
        if([result.resultCode isEqualToString:CONNECT_SUCCESS_CODE]) {
            [self showView:_validationV];
            [CommonUtil showToastMessage:kDescTempKeySend];
        } else {
            [CommonUtil showToastMessage:result.resultMsg];
        }
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        [CommonUtil showToastMessage:kDescTempKeySendFailed];
        [LoadingView hideLoadingView];
    }];
}

- (void)requestValidation {
    if([NSString isEmptyString:_validationTF.text]) {
        [CommonUtil showToastMessage:kDescTempKeyInputNeed];
        return;
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:_idTF.text forKey:@"admin_id"];
    [parameters setValue:_validationTF.text forKey:@"tempKey"];
    
    [ApiManager requestToPost:VALIDATION_TEMPKEY_URL parameters:parameters success:^(id responseObject) {
        ResultModel *result = [[ResultModel alloc] initWithDictionary:responseObject error:nil];
        if([result.resultCode isEqualToString:CONNECT_SUCCESS_CODE]) {
            [self showView:_passwordV];
            [CommonUtil showToastMessage:kDescTempKeyValidation];
        } else {
            [CommonUtil showToastMessage:result.resultMsg];
        }
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        [CommonUtil showToastMessage:kDescTempKeyValidationFailed];
        [LoadingView hideLoadingView];
    }];
}

- (void)requestUpdatePassword {
    if([NSString isEmptyString:_pwTF.text]) {
        [CommonUtil showToastMessage:kDescPWInputNeed];
        return;
    } else if(![_pwTF.text isEqualToString:_pwConfirmTF.text]) {
        [CommonUtil showToastMessage:kDescPWInputSameNeed];
        return;
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:_idTF.text forKey:@"admin_id"];
    [parameters setValue:_pwTF.text forKey:@"admin_pwd"];
    
    [ApiManager requestToPost:UPDATE_PASSWORD_URL parameters:parameters success:^(id responseObject) {
        [self closeViewController];
        [CommonUtil showToastMessage:kDescPasswordUpdate];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        [CommonUtil showToastMessage:kDescPasswordUpdateFailed];
        [LoadingView hideLoadingView];
    }];
}

- (IBAction)pressedRequest:(UIButton *)sender {
    if(!_requestV.isHidden) {
        [self requestTempKey];
    } else if(!_validationV.isHidden) {
        [self requestValidation];
    } else if(!_passwordV.isHidden) {
        [self requestUpdatePassword];
    }
}
@end
