//
//  CertificationViewController.m
//  shipping
//
//  Created by insung on 2018. 7. 13..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "CertificationViewController.h"
#define TIMER_MAX   180

@interface CertificationViewController ()

@end

@implementation CertificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initLayout {
    [super initLayoutForTitle:kTitleCertification];
    [self showBackButton:NO];
    [_certificationTF setAccessoryView];
    [_resendBtn applyRoundBorder:4.0];
    [_certificationBtn applyRoundBorder:4.0];
    
    [self initTimer];
}

- (void)initTimer {
    if(countTimer != nil) {
        [countTimer invalidate];
        countTimer = nil;
    }
    
    startDate = [NSDate date];
    countTimer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(updateTimer:) userInfo:nil repeats:YES];
    [countTimer fire];
    [[NSRunLoop mainRunLoop] addTimer:countTimer forMode:NSRunLoopCommonModes];
}

/**
 인증 번호 시간 업데이트

 @param timer timer
 */
- (void)updateTimer:(NSTimer *)timer {
    NSTimeInterval timeInterval = [[NSDate date] timeIntervalSinceDate:startDate];
    NSInteger timerCount = TIMER_MAX - timeInterval;
    if(timerCount <= 0) {
        _timerLb.text = kDescCertificationResendNeed;
        _certificationBtn.enabled = NO;
        [timer invalidate];
    } else {
        NSInteger minute = timerCount / 60;
        NSInteger second = (timerCount - minute * 60) % 60;
        NSString *timerString = [NSString stringWithFormat:@"%.2ld:%.2ld", minute, second];
        _timerLb.text = timerString;
        _certificationBtn.enabled = YES;
    }
}

/**
 인증 요청
 */
- (void)requestCertify {
    NSString *certNo = _certificationTF.text;
    if([NSString isEmptyString:certNo]) { // 인증번호 입력이 없을시
        [CommonUtil showToastMessage:kDescCertificationNumberNeed];
        return;
    }
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:_certMsgId forKey:@"msgid"];
    [parameters setValue:certNo forKey:@"authCode"];
    
    [ApiManager requestToCertification:CERTIFICATION_CONFIRM_URL parameters:parameters success:^(id responseObject) {
        UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
        userInfo.certMsgId = _certMsgId;
        [UserManager saveData:userInfo.toDictionary key:USER_DATA_KEY];
        [self openRootTabBarController];
        [CommonUtil showToastMessage:kDescCertificationSuccess];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        [CommonUtil showToastMessage:errorMsg];
        [LoadingView hideLoadingView];
    }];
}

/**
 인증번호 재전송 요청
 */
- (void)requestResendCertification {
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    NSString *phoneNo = [userInfo.telNo encryptWithKey:CERTIFICATION_AES_KEY];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:phoneNo forKey:@"phoneNumber"];
    [parameters setValue:userInfo.adminId forKey:@"userId"];
    
    [ApiManager requestToCertification:CERTIFICATION_SEND_URL parameters:parameters success:^(id responseObject) {
        CertificationData *resultData = [[CertificationData alloc] initWithDictionary:responseObject error:nil];
        _certMsgId = resultData.data.msgid;
        [CommonUtil showToastMessage:kDescCertificationResend];
        [self initTimer];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        [CommonUtil showToastMessage:errorMsg];
        [LoadingView hideLoadingView];
    }];
}

#pragma mark - Action Event
- (IBAction)pressedCertification:(UIButton *)sender {
    [self requestCertify];
}

- (IBAction)pressedResend:(id)sender {
    [self requestResendCertification];
}
@end
