//
//  PwFindViewController.h
//  shipping
//
//  Created by insung on 2018. 10. 5..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"

@interface PwFindViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIScrollView *contentSV;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLb;
@property (weak, nonatomic) IBOutlet UIView *requestV;
@property (weak, nonatomic) IBOutlet CTTextField *idTF;
@property (weak, nonatomic) IBOutlet CTTextField *phoneTF;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *requestBottomLC;
@property (weak, nonatomic) IBOutlet UIView *validationV;
@property (weak, nonatomic) IBOutlet CTTextField *validationTF;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *validationBottomLC;
@property (weak, nonatomic) IBOutlet UIView *passwordV;
@property (weak, nonatomic) IBOutlet CTTextField *pwTF;
@property (weak, nonatomic) IBOutlet CTTextField *pwConfirmTF;
@property (weak, nonatomic) IBOutlet UIButton *requestBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *pwBottomLC;
@end
