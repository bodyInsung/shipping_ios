//
//  LoginViewController.m
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"
#import "AppVersionData.h"
#import "CertificationViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:STORYBOARD_CERTIFICATION_SEGUE]) {
        CertificationViewController *vc = [segue destinationViewController];
        vc.certMsgId = sender;
    }
}

- (void)initLayout {
    [super initLayoutForNoneActionBar];
    [self setBaseSV:_contentSV];
    
    [_idTF setAccessoryView];
    [_pwTF setAccessoryView];
    [_loginBtn applyRoundBorder:4.0];
    [_pwFindBtn applyRoundBorder:4.0];

    [self requestAppVersion];
}

/**
 페이지 진입시 앱 버전 체크
 */
- (void)requestAppVersion {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"I" forKey:@"deviceType"];
    [parameters setValue:@"ship" forKey:@"svType"];
    [ApiManager requestToPost:APP_VERSION_URL parameters:parameters success:^(id responseObject) {
        AppVersionData *appInfo = [[AppVersionData alloc] initWithDictionary:responseObject[API_RESPONSE_KEY] error:nil];
        NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
        NSInteger version = [[info objectForKey:@"CFBundleVersion"] integerValue];
        BOOL isUpdateNeed = [appInfo.updateType isEqualToString:@"Y"];
        if(version < [appInfo.version integerValue]) {
            if(isUpdateNeed == YES) {
                NSString *msg = [NSString stringWithFormat:@"앱 업데이트가 필요합니다\n\n내용 : %@", appInfo.msg];
                [CommonUtil showAlertTitle:kTitleUpdate msg:msg type:kAlertTypeOne target:self cAction:^{
                    [CommonUtil openLinkWeb:APP_DOWNLOAD_URL];
                }];
            } else {
                NSString *msg = [NSString stringWithFormat:@"앱 업데이트가 있습니다\n\n내용 : %@", appInfo.msg];
                [CommonUtil showAlertTitle:kTitleUpdate msg:msg type:kAlertTypeTwo cancel:@"취소" confirm:@"확인" target:self cancelAction:^{
                    [self checkValue];
                } confirmAction:^{
                    [CommonUtil openLinkWeb:APP_DOWNLOAD_URL];
                }];
            }
        } else {
            [self checkValue];
        }
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        [LoadingView hideLoadingView];
    }];
}

/**
 저장되어 있는 데이터 체크
 */
- (void)checkValue {
    if([self isBlockTime]) { // 접속 제한 시 종료
        [CommonUtil showAlertTitle:kTitleImpossibleContact msg:kDescImpossibleContact type:kAlertTypeOne target:self cAction:^{
            exit(0);
        }];
        return;
    }
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    if(userInfo != nil) { // 저장된 유저정보가 있을 경우
        if([userInfo.autoLogin boolValue] ||
           _preVC != nil) { // 자동로그인 (preVC이 존재 하면 세션 유지)
            _idTF.text = userInfo.adminId;
            _pwTF.text = userInfo.userPw;
            [self requestLogin];
        } else if([userInfo.saveId boolValue]) { // 아이디 저장
            _idTF.text = userInfo.adminId;
        }
    } else {
        _idTF.text = @"";
        _pwTF.text = @"";
    }
    
    _autoBtn.selected = [userInfo.autoLogin boolValue];
    _idBtn.selected = [userInfo.saveId boolValue];
}

/**
 로그인 정보 초기화
 */
- (void)initLoginValue {
    [UserManager removeUserData];
    [self checkValue];
}

/**
 로그인 요청
 */
- (void)requestLogin {
    if([NSString isEmptyString:_idTF.text]) {
        [CommonUtil showToastMessage:kDescIDInputNeed];
        return;
    } else if([NSString isEmptyString:_pwTF.text]) {
        [CommonUtil showToastMessage:kDescPWInputNeed];
        return;
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:_idTF.text forKey:@"admin_id"];
    [parameters setValue:_pwTF.text forKey:@"admin_pwd"];
    
    [ApiManager requestToPost:LOGIN_URL parameters:parameters success:^(id responseObject) {
        UserResultData *userInfo = [[UserResultData alloc] initWithDictionary:responseObject[API_RESPONSE_KEY] error:nil];
        // 패스워드 별도 저장
        userInfo.userPw = _pwTF.text;
        userInfo.autoLogin = _autoBtn.isSelected? @"Y" : @"N";
        userInfo.saveId = _idBtn.isSelected? @"Y" : @"N";
        
        if(_autoBtn.isSelected) { // 자동 로그인의 경우에만 저장되어 있던 인증서 다시 저장
            UserResultData *savedUserInfo = [UserManager loadUserData:USER_DATA_KEY];
            userInfo.certMsgId = savedUserInfo.certMsgId;
        }
        NSLog(@"userInfo : %@", userInfo);
        if([userInfo.perCd integerValue] / 100 == 1) {
            // 로그인 데이터 저장
            [UserManager saveData:userInfo.toDictionary key:USER_DATA_KEY];
            [self checkCertification];
        } else {
            [CommonUtil showToastMessage:kDescLoginPermissionNeed];
        }
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        [CommonUtil showToastMessage:kDescLoginFailed];
        [LoadingView hideLoadingView];
    }];
}

/**
 인증 여부 체크
 */
- (void)checkCertification {
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    NSString *adminId = userInfo.adminId;
    if([adminId isEqualToString:TEST_LOGIN_ID]) { // 테스트를 위한 계정
        [self showNextViewController];
        return;
    }
    
    // 인증 제거
    [self showNextViewController];
//    NSString *certMsgId = userInfo.certMsgId;
//    if([NSString isEmptyString:certMsgId]) {
//#if DEBUG
//        [self showNextViewController];
//#else
//        [self requestSendCertification];
//#endif
//    } else {
//        [self requestValidation];
//    }
}

/**
 유효성 요청
 */
- (void)requestValidation {
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    NSString *certMsgId = userInfo.certMsgId;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:certMsgId forKey:@"msgid"];
    [ApiManager requestToCertification:VALIDATION_URL parameters:parameters success:^(id responseObject) {
        [self showNextViewController];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        [self initLoginValue];
        [CommonUtil showToastMessage:errorMsg];
        [LoadingView hideLoadingView];
    }];
}

/**
 인증 요청
 */
- (void)requestSendCertification {
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    NSString *phoneNo = [userInfo.telNo encryptWithKey:CERTIFICATION_AES_KEY];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:phoneNo forKey:@"phoneNumber"];
    [parameters setValue:userInfo.adminId forKey:@"userId"];
    
    [ApiManager requestToCertification:CERTIFICATION_SEND_URL parameters:parameters success:^(id responseObject) {
        CertificationData *resultData = [[CertificationData alloc] initWithDictionary:responseObject error:nil];
        [self checkNextViewController:resultData.data.msgid];
        [CommonUtil showToastMessage:kDescCertificationSend];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        [CommonUtil showToastMessage:errorMsg];
        [LoadingView hideLoadingView];
    }];
}

/**
 접속 제한 여부
 
 @return 접속 제한 여부
 */
- (BOOL)isBlockTime {
    NSInteger hour = [[CommonUtil stringFromDate:[NSDate date] format:@"HH"] integerValue];
    if([CommonUtil weekdayFromToday] == 2 &&
       (hour >= 5 && hour < 9)) { // 월요일 서버 작업으로 5시부터 9시까지 접속 제한
        return YES;
    }
    return NO;
}

/**
 유효성 체크 후 다음 페이지 구분

 @param msgId 유효성 판별을 위한 msgId
 */
- (void)checkNextViewController:(NSString *)msgId {
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    if([NSString isEmptyString:userInfo.certMsgId] ||
       ![userInfo.certMsgId isEqualToString:msgId]) {
        [self openViewController:STORYBOARD_CERTIFICATION_SEGUE sender:msgId];
    } else {
        [self showNextViewController];
    }
}

/**
 다음 페이지 이동
 */
- (void)showNextViewController {
    if(_preVC != nil) { // 이전 페이지로 돌아가서 갱신 할 경우
        [self closePresentViewController:_preVC];
    } else { // 홈으로 이동할 경우
        [self openRootTabBarController];
    }
}

#pragma mark - Action Event
- (IBAction)pressedCheck:(UIButton *)sender {
    sender.selected = !sender.isSelected;
}

- (IBAction)pressedLogin:(UIButton *)sender {
    [CommonUtil hideKeyboard];
    [self requestLogin];
}

- (IBAction)pressedPwFind:(UIButton *)sender {
    [self openViewController:@"PwFindSegue" sender:self];
}

@end
