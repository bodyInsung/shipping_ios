//
//  LoginViewController.h
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : BaseViewController 

@property (strong, nonatomic) UIViewController *preVC;
@property (weak, nonatomic) IBOutlet UIScrollView *contentSV;
@property (weak, nonatomic) IBOutlet UIButton *autoBtn;
@property (weak, nonatomic) IBOutlet UIButton *idBtn;
@property (weak, nonatomic) IBOutlet CTTextField *idTF;
@property (weak, nonatomic) IBOutlet CTTextField *pwTF;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *pwFindBtn;

@end

