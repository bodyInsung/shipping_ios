//
//  CertificationViewController.h
//  shipping
//
//  Created by insung on 2018. 7. 13..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"

@interface CertificationViewController : BaseViewController {
    NSDate                  *startDate;
    NSTimer                 *countTimer;
}

@property (strong, nonatomic) NSString *certMsgId;
@property (weak, nonatomic) IBOutlet UIScrollView *contentSV;
@property (weak, nonatomic) IBOutlet CTTextField *certificationTF;
@property (weak, nonatomic) IBOutlet UIButton *resendBtn;
@property (weak, nonatomic) IBOutlet UILabel *timerLb;
@property (weak, nonatomic) IBOutlet UIButton *certificationBtn;
@end
