//
//  MapViewController.h
//  shipping
//
//  Created by ios on 2018. 5. 23..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"

@interface MapViewController : BaseViewController<GMSMapViewDelegate> {
    NSArray                     *dayBtnGroup;
    
    ShippingResultData          *selectedData;
    CLLocationCoordinate2D      selectedPosition;
    GMSCoordinateBounds         *mapBounds;
}

@property (weak, nonatomic) IBOutlet GMSMapView *mapV;
@property (weak, nonatomic) IBOutlet UIView *leftMenuV;
@property (weak, nonatomic) IBOutlet UIButton *day01Btn;
@property (weak, nonatomic) IBOutlet UIButton *day02Btn;
@property (weak, nonatomic) IBOutlet UIButton *day03Btn;
@property (weak, nonatomic) IBOutlet UIView *description01V;
@property (weak, nonatomic) IBOutlet UIView *description02V;
@property (weak, nonatomic) IBOutlet UIView *description03V;
@property (weak, nonatomic) IBOutlet UIView *description04V;
@property (weak, nonatomic) IBOutlet UIView *description05V;
@property (weak, nonatomic) IBOutlet UIView *bottomMenuV;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UILabel *addressLb;
@property (weak, nonatomic) IBOutlet UIButton *naviBtn;
@property (weak, nonatomic) IBOutlet UIButton *callBtn;
@property (weak, nonatomic) IBOutlet UIButton *detailBtn;
@end
