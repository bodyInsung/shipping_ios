//
//  MapViewController.m
//  shipping
//
//  Created by ios on 2018. 5. 23..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "MapViewController.h"
#import "MapMarkerView.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    UIButton *btn = [UIButton selectedBtn:dayBtnGroup];
    if(btn == nil) { // 첫 진입시
        [self pressedDay:_day01Btn];
    } else { // 뒤로 돌아와서 진입시
        [self selectedDayBtn:btn];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:STORYBOARD_SHIPPING_DETAIL_SEGUE]) {
        ShippingDetailViewController *vc = [segue destinationViewController];
        vc.shippingSeq = selectedData.shippingSeq;
    }
}

- (void)initValue {
    [super initValue];
    dayBtnGroup = @[_day01Btn, _day02Btn, _day03Btn];
    mapBounds = [[GMSCoordinateBounds alloc] init];
}

- (void)initLayout {
    [super initLayoutForTitle:@"배송 지도"];
    [self showEditMapButton];
    
    [_description01V applyRoundBorder:_description01V.bounds.size.height/2];
    [_description02V applyRoundBorder:_description02V.bounds.size.height/2];
    [_description03V applyRoundBorder:_description03V.bounds.size.height/2];
    [_description04V applyRoundBorder:_description04V.bounds.size.height/2];
    [_description05V applyRoundBorder:_description05V.bounds.size.height/2];
    
    [_naviBtn applyRoundBorder:4.0];
    [_callBtn applyRoundBorder:4.0];
    [_detailBtn applyRoundBorder:4.0];
    
    _mapV.delegate = self;
    _mapV.alpha = 0.0;
    _mapV.settings.compassButton = YES;
    _mapV.settings.myLocationButton = YES;
    _mapV.myLocationEnabled = YES;
    [self hideBottomMenu];
}

/**
 배송 리스트 요청 (지정일 기준)

 @param date 날짜
 */
- (void)requestShipping:(NSString *)date {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:date forKey:@"dateEnd"];
    [parameters setValue:date forKey:@"dateStart"];
    [parameters setValue:@"N" forKey:@"s_oneself"];
    [parameters setValue:@"2" forKey:@"s_datetype"];
    [parameters setValue:@"" forKey:@"s_addr"];
    
    [ApiManager requestToPost:SHIPPING_LIST_URL parameters:parameters success:^(id responseObject) {
        NSArray *resultData = [ShippingResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY][@"list"] error:nil];
        [_mapV clear];
        if(resultData.count > 0) {
            for(ShippingResultData *data in resultData) {
                // 지도 검색 오류로 인한 ',' 제거
                data.insAddr = [data.insAddr stringByReplacingOccurrencesOfString:@"," withString:@""];
                [self requestGeocoding:data];
            }
        } else {
            [self cameraInitPosition];
        }
        [LoadingView hideLoadingViewToDelay];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 네이버 geocode 요청 (사용안함)
 
 @param data 배송 데이터
 */
- (void)requestGeocoding:(ShippingResultData *)data {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:data.insAddr forKey:@"fullAddr"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:MAP_CLIENT_KEY forHTTPHeaderField:@"appKey"];
    manager.requestSerializer = requestSerializer;
    [manager GET:MAP_GEOCODE_URL parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        @try {
            NSArray *item = responseObject[@"coordinateInfo"][@"coordinate"];
            NSDictionary *position = [item firstObject];
            float longitude = [position[@"lon"] floatValue];
            float latitude = [position[@"lat"] floatValue];
            if([NSString isEmptyString:position[@"lon"]] == YES) {
                longitude = [position[@"newLon"] floatValue];
                latitude = [position[@"newLat"] floatValue];
            }
            [self reloadSearchMarker:CLLocationCoordinate2DMake(latitude, longitude) data:data];
        } @catch(NSException *error) {
            [self failedAddressSearch];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error : %@", error);
        [self failedAddressSearch];
    }];
}

/**
 최종 실패했을 경우
 */
- (void)failedAddressSearch {
    [CommonUtil showToastMessage:kDescSearchAddressCant];
}

/**
 검색된 곳에 마커 추가 및 카메라 이동
 
 @param position position
 @param data data
 */
- (void)reloadSearchMarker:(CLLocationCoordinate2D)position data:(ShippingResultData *)data {
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.userData = data;
    MapMarkerView *markerV = [[MapMarkerView alloc] init];
    marker.icon = [markerV iconFromData:data isProduct:NO];
    marker.position = position;
    mapBounds = [mapBounds includingCoordinate:position];
    marker.map = _mapV;
    [self cameraUpdate];
}

/**
 카메라 초기 포지션
 */
- (void)cameraInitPosition {
    GMSCameraUpdate *update = [GMSCameraUpdate setCamera:[GMSCameraPosition cameraWithTarget:DEFAULT_POSITION zoom:10]];
    [_mapV animateWithCameraUpdate:update];
    [self animationShowMap];
}

/**
 카메라 위치 업데이트
 */
- (void)cameraUpdate {
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:mapBounds withPadding:0];
    [_mapV animateWithCameraUpdate:update];
    [self animationShowMap];
}

/**
 카메라 위치가 잡히면 맵뷰 보여짐
 */
- (void)animationShowMap {
    if(_mapV.alpha < 1) {
        [UIView animateWithDuration:0.5 animations:^{
            _mapV.alpha = 1.0;
        }];
    }
}

/**
 선택된 날짜 버튼

 @param btn btn
 */
- (void)selectedDayBtn:(UIButton *)btn {
    NSString *date = nil;
    if([btn isEqual:_day01Btn]) { // 금일
        date = [CommonUtil todayFromDateFormat:@"yyyy.MM.dd"];
    } else if([btn isEqual:_day02Btn]) { // +1
        date = [CommonUtil stringFromDate:[CommonUtil afterDayFromToday:1] format:@"yyyy.MM.dd"];
    } else if([btn isEqual:_day03Btn]) { // +2
        date = [CommonUtil stringFromDate:[CommonUtil afterDayFromToday:2] format:@"yyyy.MM.dd"];
    }
    [self requestShipping:date];
}

/**
 좌측 메뉴 보여줌
 */
- (void)showLeftMenu {
    _leftMenuV.hidden = NO;
}

/**
 좌측 메뉴 숨김
 */
- (void)hideLeftMenu {
    _leftMenuV.hidden = YES;
}

/**
 하단 메뉴 보여줌
 */
- (void)showBottomMenu {
    [self hideLeftMenu];
    _bottomMenuV.hidden = NO;
}

/**
 하단 메뉴 숨김
 */
- (void)hideBottomMenu {
    [self showLeftMenu];
    _bottomMenuV.hidden = YES;
}

- (void)pressedEditMap {
    [self openViewController:@"EditMapSegue" sender:self];
}

#pragma mark - Action Event
- (IBAction)pressedDay:(UIButton *)sender {
    if(sender.isSelected) {
        return;
    }
    [self selectedDayBtn:sender];
    [UIButton selectBtnGroup:sender group:dayBtnGroup];
}

- (IBAction)pressedNavi:(UIButton *)sender {
    KNVLocation *destination = [KNVLocation locationWithName:selectedData.insAddr
                                                           x:@(selectedPosition.longitude)
                                                           y:@(selectedPosition.latitude)];
    KNVOptions *options = [KNVOptions options];
    options.coordType = KNVCoordTypeWGS84;
    KNVParams *params = [KNVParams paramWithDestination:destination options:options];
    [[KNVNaviLauncher sharedLauncher] shareDestinationWithParams:params completion:^(NSError * _Nullable error) {
        if(error != nil) {
            [CommonUtil showToastMessage:kDescKakaoNaviNeed];
        }
    }];
}

- (IBAction)pressedCall:(UIButton *)sender {
    NSString *name = nil;
    NSString *phoneNum = nil;
    if([selectedData.shippingType hasPrefix:kTitleMove] &&
       [selectedData.progressNo isEqualToString:@"1"]) {
        name = selectedData.agreeName;
        phoneNum = selectedData.hPhoneNo2;
    } else {
        name = selectedData.custName;
        if(![NSString isEmptyString:selectedData.hPhoneNo]) {
            phoneNum = selectedData.hPhoneNo;
        } else if(![NSString isEmptyString:selectedData.telNo]) {
            phoneNum = selectedData.telNo;
        }
    }
    
    if([NSString isEmptyString:phoneNum]) {
        [CommonUtil showToastMessage:kDescPhoneNumberNeed];
    } else {
        NSString *logData = [NSString stringWithFormat:@"%@, %@, %@", selectedData.shippingSeq, name, phoneNum];
        [self requestAppLog:logData type:kAppLogTypeCall];
        [CommonUtil openLinkCall:phoneNum];
    }
}

- (IBAction)pressedDetail:(UIButton *)sender {
    [self openViewController:STORYBOARD_SHIPPING_DETAIL_SEGUE sender:self];
}

#pragma mark - GMSMapViewDelegate
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    ShippingResultData *data = marker.userData;
    selectedData = data;
    selectedPosition = marker.position;
    NSString *name = nil;
    NSString *addr = nil;
    if([data.shippingType hasPrefix:kTitleMove]) {
        if([data.progressNo isEqualToString:@"1"]) {
            name = data.agreeName;
            addr = data.addr;
        } else {
            name = data.custName;
            addr = data.addr2;
        }
    } else {
        name = data.custName;
        addr = data.insAddr;
    }
    _nameLb.text = name;
    _addressLb.text = addr;
    [self showBottomMenu];
    return YES;
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    [self hideBottomMenu];
}
@end
