//
//  EditMapCell.m
//  shipping
//
//  Created by insung on 2018. 10. 19..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "EditMapCell.h"

@implementation EditMapCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setCellFromData:(ShippingResultData *)data {
    _dateLb.text = [CommonUtil changeDateFormat:@"yyyy-MM-dd" dateString:data.dueDate changeFormat:@"MM-dd"];
    _stateLb.text = data.shippingType;
    _nameLb.text = data.productName;
    
    NSMutableString *note = [NSMutableString stringWithString:data.companyCheck];
    NSString *symptom = data.symptom;
    if(![NSString isEmptyString:symptom]) {
        if(![NSString isEmptyString:note]) {
            [note appendString:@"<br>"];
        }
        [note appendFormat:@"증상 : %@", symptom];
    }
    _memoLb.attributedText = [NSString applyHtmlText:data.companyCheck fontSize:_memoLb.font.pointSize];
    _addressLb.text = data.insAddr;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _dateLb.text = nil;
    _stateLb.text = nil;
    _nameLb.text = nil;
    _memoLb.attributedText = nil;
    _addressLb.text = nil;
}

@end
