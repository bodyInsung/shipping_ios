//
//  ListMapViewController.h
//  shipping
//
//  Created by ios on 2018. 3. 27..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListMapViewController : BaseViewController {
    
    GMSCoordinateBounds         *mapBounds;
}

@property (strong, nonatomic) NSString *address;
@property (weak, nonatomic) IBOutlet GMSMapView *mapV;
@property (weak, nonatomic) IBOutlet UIView *searchV;
@property (weak, nonatomic) IBOutlet CTTextField *addressTF;
@end
