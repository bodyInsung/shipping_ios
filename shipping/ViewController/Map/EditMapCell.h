//
//  EditMapCell.h
//  shipping
//
//  Created by insung on 2018. 10. 19..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditMapCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *backV;
@property (weak, nonatomic) IBOutlet UILabel *dateLb;
@property (weak, nonatomic) IBOutlet UILabel *stateLb;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UILabel *memoLb;
@property (weak, nonatomic) IBOutlet UILabel *addressLb;

- (void)setCellFromData:(ShippingResultData *)data;
@end
