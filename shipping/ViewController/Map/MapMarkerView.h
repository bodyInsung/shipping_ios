//
//  MapMarkerView.h
//  shipping
//
//  Created by ios on 2018. 5. 23..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapMarkerView : UIView {
    UIImageView             *backIV;
    UILabel                 *titleLb;
}

- (UIImage *)iconFromData:(ShippingResultData *)data isProduct:(BOOL)isProduct;
@end
