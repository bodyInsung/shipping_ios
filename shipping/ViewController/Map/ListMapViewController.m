//
//  ListMapViewController.m
//  shipping
//
//  Created by ios on 2018. 3. 27..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ListMapViewController.h"

@interface ListMapViewController ()

@end

@implementation ListMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestGeocoding];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initValue {
    [super initValue];
    
    mapBounds = [[GMSCoordinateBounds alloc] init];
}

- (void)initLayout {
    [super initLayoutForNoneActionBar];
    
    [_searchV applyRoundBorder:4.0];
    [_addressTF setAccessoryView];
    
    _mapV.alpha = 0.0;
    _mapV.settings.compassButton = YES;
    _mapV.settings.myLocationButton = YES;
    _mapV.myLocationEnabled = YES;

    _addressTF.text = _address;    
}

/**
 검색된 곳에 마커 추가 및 카메라 이동
 
 @param position position
 */
- (void)reloadSearchMarker:(CLLocationCoordinate2D)position {
    [_mapV clear];
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = position;
    marker.title = _address;
    marker.map = _mapV;
    mapBounds = [mapBounds includingCoordinate:position];
    [self cameraUpdate:position zoom:16];
}

/**
 카메라 초기 포지션
 */
- (void)cameraInitPosition {
    GMSCameraUpdate *update = [GMSCameraUpdate setCamera:[GMSCameraPosition cameraWithTarget:DEFAULT_POSITION zoom:10]];
    [_mapV animateWithCameraUpdate:update];
}

/**
 카메라 위치 업데이트

 @param position 카메라 포지션
 @param zoom zoom
 */
- (void)cameraUpdate:(CLLocationCoordinate2D)position zoom:(CGFloat)zoom {
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:mapBounds withPadding:60.0f];
//    GMSCameraUpdate *update = [GMSCameraUpdate setCamera:[GMSCameraPosition cameraWithTarget:position zoom:zoom]];
    [_mapV animateWithCameraUpdate:update];
    
    if(_mapV.alpha < 1) {
        [UIView animateWithDuration:0.5 animations:^{
            _mapV.alpha = 1.0;
        }];
    }
}

/**
 입력된 주소의 네이버 geocode 요청
 */
- (void)requestGeocoding {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:_address forKey:@"fullAddr"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:MAP_CLIENT_KEY forHTTPHeaderField:@"appKey"];
    manager.requestSerializer = requestSerializer;
    [manager GET:MAP_GEOCODE_URL parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        @try {
            NSArray *item = responseObject[@"coordinateInfo"][@"coordinate"];
            NSDictionary *position = [item firstObject];
            double longitude = [position[@"lon"] doubleValue];
            double latitude = [position[@"lat"] doubleValue];
            if([NSString isEmptyString:position[@"lon"]] == YES) {
                longitude = [position[@"newLon"] doubleValue];
                latitude = [position[@"newLat"] doubleValue];
            }
            [self reloadSearchMarker:CLLocationCoordinate2DMake(latitude, longitude)];
        } @catch(NSException *error) {
            [self failedAddressSearch];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self failedAddressSearch];
    }];
}

/**
 최종 실패했을 경우
 */
- (void)failedAddressSearch {
    [self cameraInitPosition];
    [CommonUtil showToastMessage:kDescSearchAddressCant];
}

#pragma mark - Action Event
- (IBAction)pressedBack:(UIButton *)sender {
    [self closeViewController];
}

- (IBAction)pressedSearch:(id)sender {
    [CommonUtil hideKeyboard];
    _address = _addressTF.text;
    [self requestGeocoding];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    _address = textField.text;
    [textField resignFirstResponder];
    [self requestGeocoding];
    return YES;
}

@end
