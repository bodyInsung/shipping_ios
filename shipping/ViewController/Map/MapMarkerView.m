//
//  MapMarkerView.m
//  shipping
//
//  Created by ios on 2018. 5. 23..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "MapMarkerView.h"

#define TEXT_PADDING 4
#define TEXT_PADDING_BOTTOM 10
@implementation MapMarkerView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (instancetype)init {
    self = [super init];
    if(self) {
        [self initLayout];
    }
    return self;
}

- (void)initLayout {
    backIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_marker"]];
    [self addSubview:backIV];
    titleLb = [[UILabel alloc] init];
    titleLb.numberOfLines = 0;
    titleLb.textColor = [UIColor whiteColor];
    titleLb.font = [UIFont boldSystemFontOfSize:12.0];
    titleLb.textAlignment = NSTextAlignmentCenter;
    [backIV addSubview:titleLb];
}

- (UIImage *)iconFromData:(ShippingResultData *)data isProduct:(BOOL)isProduct {
    NSString *shippingType = data.shippingType;
    if([shippingType isEqualToString:kTitleChange]) {
        backIV.tintColor = UIColorFromRGB(0x8298BC);
    } else if([shippingType isEqualToString:kTitleCancel]) {
        backIV.tintColor = UIColorFromRGB(0xBCB082);
    } else if([shippingType hasPrefix:kTitleMove]) {
        backIV.tintColor = UIColorFromRGB(0xBC8282);
    } else if([shippingType isEqualToString:kTitleReinstall]) {
        backIV.tintColor = UIColorFromRGB(0x9DBC80);
    } else {
        backIV.tintColor = UIColorFromRGB(0x8F82BC);
    }
    
    NSMutableString *title = [NSMutableString string];
    if(data.mustFlag) {
        [title appendString:@"지정건\n"];
    }
    NSString *name = nil;
    if([data.shippingType hasPrefix:kTitleMove]) {
        if([data.progressNo isEqualToString:@"1"]) {
            name = data.agreeName;
        } else {
            name = data.custName;
        }
    } else {
        name = data.custName;
    }
    [title appendFormat:@"%@\n", name];
    if(isProduct) {
        [title appendFormat:@"%@\n", data.productName];
    }
    NSString *promise = nil;
    if([NSString isEmptyString:data.promise]) {
        promise = kTitleUndifine;
    } else {
        if([data.promise boolValue]) {
            promise = [CommonUtil changeDateFormat:@"HHmm" dateString:data.promiseTime changeFormat:@"HH:mm"];
        } else {
            promise = kTitlePromiseImpossible;
        }
    }
    [title appendFormat:@"%@", promise];
    titleLb.text = title;
    
    CGSize size = [titleLb.text sizeWithAttributes:@{NSFontAttributeName:titleLb.font}];
    titleLb.frame = CGRectMake(TEXT_PADDING, TEXT_PADDING, size.width, size.height);
    backIV.frame = CGRectMake(0, 0, titleLb.frame.size.width + TEXT_PADDING*2, titleLb.frame.size.height + TEXT_PADDING + TEXT_PADDING_BOTTOM);
    UIImage *markerImage = backIV.image;
    markerImage = [markerImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    backIV.image = [markerImage resizableImageWithCapInsets:UIEdgeInsetsMake(TEXT_PADDING, 4, TEXT_PADDING_BOTTOM, TEXT_PADDING) resizingMode:UIImageResizingModeStretch];

    UIGraphicsBeginImageContextWithOptions(backIV.bounds.size, NO, 0.0);
    [backIV.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *icon = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return icon;
}

@end
