//
//  EditMapViewController.m
//  shipping
//
//  Created by insung on 2018. 10. 19..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "EditMapViewController.h"
#import "EditMapCell.h"
#import "MapMarkerView.h"

static NSString *kEditMapCellIdentifier = @"EditMapCell";

@interface EditMapViewController ()

@end

@implementation EditMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self requestShipping];
    });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initValue {
    [super initValue];
    mapBounds = [[GMSCoordinateBounds alloc] init];
    selectedData = [[NSMutableArray alloc] init];
}

- (void)initLayout {
    [super initLayoutForTitle:@"배송 경로"];
    
    UINib *cellNib = [UINib nibWithNibName:@"EditMapCell" bundle:nil];
    [_listCV registerNib:cellNib forCellWithReuseIdentifier:kEditMapCellIdentifier];

    _mapV.settings.compassButton = YES;
    _mapV.settings.myLocationButton = YES;
    _mapV.myLocationEnabled = YES;
    
    [self cameraInitPosition];
}

/**
 배송 리스트 요청 (지정일 기준)
 */
- (void)requestShipping {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"N" forKey:@"s_oneself"];
    [parameters setValue:@"2" forKey:@"s_datetype"];
    [parameters setValue:@"" forKey:@"s_addr"];
    
    [ApiManager requestToPost:SHIPPING_LIST_URL parameters:parameters success:^(id responseObject) {
        if(resultData != nil) {
            resultData = nil;
        }
        resultData = [ShippingResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY][@"list"] error:nil];
        if(resultData.count > 0) {
            _listHeightLC.active = NO;
        } else {
            [CommonUtil showToastMessage:@"배송 데이터가 존재하지 않습니다"];
        }
        [_listCV reloadData];
        [LoadingView hideLoadingViewToDelay];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)reloadMapView {
    [_mapV clear];
    if(selectedData.count > 0) {
        for(ShippingResultData *data in selectedData) {
            // 지도 검색 오류로 인한 ',' 제거
            data.insAddr = [data.insAddr stringByReplacingOccurrencesOfString:@"," withString:@""];
            [self requestGeocoding:data];
        }
    } else {
        [self cameraInitPosition];
    }
}

/**
 네이버 geocode 요청
 
 @param data 배송 데이터
 */
- (void)requestGeocoding:(ShippingResultData *)data {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:data.insAddr forKey:@"fullAddr"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:MAP_CLIENT_KEY forHTTPHeaderField:@"appKey"];
    manager.requestSerializer = requestSerializer;
    [manager GET:MAP_GEOCODE_URL parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        @try {
            NSArray *item = responseObject[@"coordinateInfo"][@"coordinate"];
            NSDictionary *position = [item firstObject];
            double longitude = [position[@"lon"] doubleValue];
            double latitude = [position[@"lat"] doubleValue];
            if([NSString isEmptyString:position[@"lon"]] == YES) {
                longitude = [position[@"newLon"] doubleValue];
                latitude = [position[@"newLat"] doubleValue];
            }
            [self reloadSearchMarker:CLLocationCoordinate2DMake(latitude, longitude) data:data];
        } @catch(NSException *error) {
            [self failedAddressSearch];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self failedAddressSearch];
    }];
}

/**
 최종 실패했을 경우
 */
- (void)failedAddressSearch {
    [CommonUtil showToastMessage:kDescSearchAddressCant];
}

/**
 검색된 곳에 마커 추가 및 카메라 이동
 
 @param position position
 @param data data
 */
- (void)reloadSearchMarker:(CLLocationCoordinate2D)position data:(ShippingResultData *)data {
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.userData = data;
    MapMarkerView *markerV = [[MapMarkerView alloc] init];
    marker.icon = [markerV iconFromData:data isProduct:YES];
    marker.position = position;
    mapBounds = [mapBounds includingCoordinate:position];
    marker.map = _mapV;
    [self cameraUpdate];
}

/**
 카메라 초기 포지션
 */
- (void)cameraInitPosition {
    GMSCameraUpdate *update = [GMSCameraUpdate setCamera:[GMSCameraPosition cameraWithTarget:DEFAULT_POSITION zoom:10]];
    [_mapV animateWithCameraUpdate:update];
}

/**
 카메라 위치 업데이트
 */
- (void)cameraUpdate {
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:mapBounds withPadding:30.0f];
    [_mapV animateWithCameraUpdate:update];
}


#pragma mark - UICollectionViewDataSource & UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return resultData.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    EditMapCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kEditMapCellIdentifier forIndexPath:indexPath];
    [cell setCellFromData:resultData[indexPath.row]];
    
    ShippingResultData *data = resultData[indexPath.row];
    if([selectedData containsObject:data]) {
        [cell applyRoundBorder:4.0 color:UIColorFromRGB(0x8F82BC) width:3.0];
    } else {
        [cell applyRoundBorder:4.0 color:UIColorFromRGB(0xB5B5B5) width:3.0];
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat screenWidth = [APP_DELEGATE window].bounds.size.width;
    return CGSizeMake((screenWidth - 30) / 2, 120);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ShippingResultData *data = resultData[indexPath.row];
    if([selectedData containsObject:data]) {
        [selectedData removeObject:data];
    } else {
        [selectedData addObject:data];
    }
    [_listCV reloadData];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self reloadMapView];
    });
}

@end
