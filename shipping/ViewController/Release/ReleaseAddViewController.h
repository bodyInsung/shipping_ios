//
//  ReleaseAddViewController.h
//  shipping
//
//  Created by ios on 2018. 4. 12..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"

@protocol ReleaseAddDelegate
- (void)reloadViewController;
@end

@interface ReleaseAddViewController : BaseViewController <ListPopupViewDelegate> {
    NSArray                         *warehouseListData;
    NSArray                         *groupListData;
    NSArray                         *modelListData;
    NSArray                         *detailListData;
    NSArray                         *productListData;
    NSMutableArray                  *addedArray;
    ReleaseProductData              *productData;
}

@property (weak) id<ReleaseAddDelegate> delegate;
@property (weak, nonatomic) ReleaseListData *receiveData;
@property (weak, nonatomic) IBOutlet UIButton *warehouseBtn;
@property (weak, nonatomic) IBOutlet UIButton *productBtn;
@property (weak, nonatomic) IBOutlet UIButton *groupBtn;
@property (weak, nonatomic) IBOutlet UIButton *modelBtn;
@property (weak, nonatomic) IBOutlet UIButton *detailBtn;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet CTTextField *quantityTF;
@property (weak, nonatomic) IBOutlet UIButton *releaseAddBtn;
@property (weak, nonatomic) IBOutlet UITableView *listTbV;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *groupHeightLC;
@end
