//
//  ReleaseAddViewController.m
//  shipping
//
//  Created by ios on 2018. 4. 12..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ReleaseAddViewController.h"
#import "ReleaseAddTableViewCell.h"

static NSString *kReleaseAddCellIdentifier = @"ListCell";

@interface ReleaseAddViewController ()

@end

@implementation ReleaseAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestWarehouseList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:STORYBOARD_LIST_POPUP_PRODUCT_SEGUE]) {
        ListPopupViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.datas = sender;
        vc.isSearch = YES;
        vc.popupType = kListPopupTypeProduct;
    }
}

- (void)initValue {
    addedArray = [NSMutableArray array];
}

- (void)initLayout {
    [super initLayoutForTitle:TITLE_RELEASE_ADD_ACTIONBAR];
    
    [_quantityTF setAccessoryView];
    [_warehouseBtn applyRoundBorder:4.0];
    [_productBtn applyRoundBorder:4.0];
    [_groupBtn applyRoundBorder:4.0];
    [_detailBtn applyRoundBorder:4.0];
    [_modelBtn applyRoundBorder:4.0];
    [_releaseAddBtn applyRoundBorder:4.0];
    
    UINib *cellNib = [UINib nibWithNibName:@"ReleaseAddTableViewCell" bundle:nil];
    [_listTbV registerNib:cellNib forCellReuseIdentifier:kReleaseAddCellIdentifier];
    
    _listTbV.tableFooterView = [[UIView alloc] init];
    
    [self changeBtnTitle:_productBtn index:0];
}

- (void)checkReceiveData {
    if(_receiveData != nil) {
        _warehouseBtn.enabled = NO;
        [_warehouseBtn setTitle:_receiveData.fromSlNm forState:UIControlStateNormal];
    } else {
        [self changeBtnTitle:_warehouseBtn index:0];
    }
}

- (void)checkAddedData {
    _warehouseBtn.enabled = addedArray.count == 0;
}

- (void)changeBtnTitle:(UIButton *)btn index:(NSInteger)index {
    NSString *title = nil;
    if([btn isEqual:_warehouseBtn]) {
        ReleaseWarehouseData *data = warehouseListData[index];
        title = data.slNm;
    } else if([btn isEqual:_productBtn]) {
        title = [CommonObject ReleaseProductNameArray][index];
        _groupHeightLC.active = [title isEqualToString:@"사은품"] == YES;
    }
    [btn setTitle:title forState:UIControlStateNormal];
}

- (void)requestWarehouseList {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"1000" forKey:@"groupCd"];
    [ApiManager requestToPost:WAREHOUSE_LIST_URL parameters:parameters success:^(id responseObject) {
        warehouseListData = [ReleaseWarehouseData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        [self checkReceiveData];
        [self requestGroupList:@"1" isSelect:NO];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 그룹, 모델 리스트 요청

 @param depth 그룹 구분
 @param isSelect 사용자 선택 여부
 */
- (void)requestGroupList:(NSString *)depth isSelect:(BOOL)isSelect {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NSString *code = nil;
    if([depth isEqualToString:@"1"]) {
        code = [CommonObject codeFromReleaseDataType:kReleaseDataTypeProduct name:_productBtn.currentTitle];
    } else if([depth isEqualToString:@"2"]) {
        code = [self codeFromData:groupListData name:_groupBtn.currentTitle];
    } else if([depth isEqualToString:@"3"]) {
        code = [self codeFromData:modelListData name:_modelBtn.currentTitle];
    }
    [parameters setValue:code forKey:@"itemGroup"];
    [parameters setValue:depth forKey:@"itemDepth"];
    [ApiManager requestToPost:RELEASE_GROUP_LIST_URL parameters:parameters success:^(id responseObject) {
        if([depth isEqualToString:@"1"]) {
            if(groupListData != nil) {
                groupListData = nil;
            }
            groupListData = [ReleaseGroupData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
            ReleaseGroupData *data = groupListData.firstObject;
            [_groupBtn setTitle:data.itemGroupNm forState:UIControlStateNormal];
            if(isSelect == YES) {
                NSMutableArray *names = [NSMutableArray array];
                for(ReleaseGroupData *data in groupListData) {
                    [names addObject: data.itemGroupNm];
                }
                [CommonUtil showActionSheet:names target:self selected:^(NSInteger index) {
                    [_groupBtn setTitle:names[index] forState:UIControlStateNormal];
                    [self requestGroupList:@"2" isSelect:NO];
                }];
            } else {
                [self requestGroupList:@"3" isSelect:NO];
            }
        } else if([depth isEqualToString:@"2"]) {
            if(modelListData != nil) {
                modelListData = nil;
            }
            modelListData = [ReleaseGroupData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
            if(isSelect == YES) {
                NSMutableArray *names = [NSMutableArray array];
                for(ReleaseGroupData *data in modelListData) {
                    [names addObject: data.itemGroupNm];
                }
                [CommonUtil showActionSheet:names target:self selected:^(NSInteger index) {
                    [_modelBtn setTitle:names[index] forState:UIControlStateNormal];
                }];
            }
        } else if([depth isEqualToString:@"3"]) {
            if(detailListData != nil) {
                detailListData = nil;
            }
            detailListData = [ReleaseGroupData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
            if(isSelect == YES) {
                NSMutableArray *names = [NSMutableArray array];
                for(ReleaseGroupData *data in detailListData) {
                    [names addObject: data.itemGroupNm];
                }
                [CommonUtil showActionSheet:names target:self selected:^(NSInteger index) {
                    [_detailBtn setTitle:names[index] forState:UIControlStateNormal];
                }];
            }
        }
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (NSString *)codeFromData:(NSArray *)datas name:(NSString *)name {
    for(JSONModel *data in datas) {
        if([data isKindOfClass:[ReleaseGroupData class]]) {
            ReleaseGroupData *groupData = (ReleaseGroupData *)data;
            if([groupData.itemGroupNm isEqualToString:name]) {
                return groupData.itemGroupCd;
            }
        }
    }
    return @"";
}

- (NSString *)warehouseCode:(BOOL)isWarehouse {
    for(ReleaseWarehouseData *data in warehouseListData) {
        if([data.slNm isEqualToString:_warehouseBtn.currentTitle]) {
            if(isWarehouse == YES) {
                return data.slCd;
            } else {
                return data.plantCd;
            }
        }
    }
    return @"";
}

- (BOOL)isGiftType {
    if([_productBtn.currentTitle isEqualToString:@"사은품"]) { // 사은품이 선택된 경우
        return YES;
    }
    return NO;
}

/**
 제품 리스트 (사은품의 경우에만)
 */
- (void)requestProductList {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:[self warehouseCode:YES] forKey:@"slCd"];

    [ApiManager requestToPost:GIFT_STOCK_LIST_URL parameters:parameters success:^(id responseObject) {
        if(productListData != nil) {
            productListData = nil;
        }
        productListData = [ReleaseProductData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        if(productListData.count > 0) {
            [self openViewController:STORYBOARD_LIST_POPUP_PRODUCT_SEGUE sender:productListData];
        } else {
            [CommonUtil showToastMessage:kDescStockNone];
        }
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 제품 재고 리스트
 */
- (void)requestStockList {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NSString *productCode = [self codeFromData:detailListData name:_detailBtn.currentTitle];
    [parameters setValue:productCode forKey:@"itemGroup"];
    [parameters setValue:[self warehouseCode:YES] forKey:@"slCd"];
    
    [ApiManager requestToPost:RELEASE_STOCK_LIST_URL parameters:parameters success:^(id responseObject) {
        if(productListData != nil) {
            productListData = nil;
        }
        productListData = [ReleaseProductData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        if(productListData.count > 0) {
            [self openViewController:STORYBOARD_LIST_POPUP_PRODUCT_SEGUE sender:productListData];
        } else {
            [CommonUtil showToastMessage:kDescStockNone];
        }
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)requestRegister {
    if(addedArray.count == 0) {
        [CommonUtil showToastMessage:kDescProductAddNeed];
        return;
    }
    NSMutableDictionary *items = [NSMutableDictionary dictionary];
    [items setValue:[self warehouseCode:YES] forKey:@"fromSlCd"];
    [items setValue:[self warehouseCode:NO] forKey:@"plantCd"];
    [items setValue:_receiveData == nil ? @"" : _receiveData.issueNo forKey:@"issueNo"];
    
    NSMutableArray* datas = [NSMutableArray array];
    for(ReleaseProductData* data in addedArray) {
        NSMutableDictionary *item = [NSMutableDictionary dictionary];
        [item setValue:data.itemCd forKey:@"item"];
        [item setValue:data.inputQty forKey:@"qty"];
        [item setValue:data.giftYn forKey:@"giftYn"];
        [item setValue:data.invYn forKey:@"invYn"];
        [datas addObject:item];
    }
    [items setValue:datas forKey:@"itemList"];
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:items options:kNilOptions error:nil];
    NSString* jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:jsonString forKey:@"jsonData"];

    [ApiManager requestToPost:RELEASE_REGISTER_URL parameters:parameters success:^(id responseObject) {
        ResultModel *resultData = [[ResultModel alloc] initWithDictionary:responseObject error:nil];
        if([resultData.resultCode isEqualToString:CONNECT_SUCCESS_CODE]) {
            [_delegate reloadViewController];
            [self closeViewController];
        }
        [CommonUtil showToastMessage:resultData.resultMsg];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)removeAllData {
    [self removeModelData];
    [self removeDetailData];
    [self removeProductData];
}

- (void)removeModelData {
    if(modelListData != nil) {
        modelListData = nil;
    }
    
    [_modelBtn setTitle:@"" forState:UIControlStateNormal];
    [self removeDetailData];
    [self removeProductData];
}

- (void)removeDetailData {
    if(detailListData != nil) {
        detailListData = nil;
    }
    
    [_detailBtn setTitle:@"" forState:UIControlStateNormal];
}

- (void)removeProductData {
    if(productData != nil) {
        productData = nil;
    }
    
    _nameTF.text = @"";
    _quantityTF.text = @"";
}

#pragma mark - ActionBarDelegate
- (IBAction)pressedWarehouse:(UIButton *)sender {
    NSMutableArray *names = [NSMutableArray array];
    for(ReleaseWarehouseData *data in warehouseListData) {
        [names addObject: data.slNm];
    }
    [CommonUtil showActionSheet:names target:self selected:^(NSInteger index) {
        [self changeBtnTitle:sender index:index];
    }];
}

- (IBAction)pressedProduct:(UIButton *)sender {
    NSArray *product = [CommonObject ReleaseProductNameArray];
    [CommonUtil showActionSheet:product target:self selected:^(NSInteger index) {
        [self removeAllData];
        [self changeBtnTitle:sender index:index];
        [self requestGroupList:@"1" isSelect:NO];
    }];
}

- (IBAction)pressedGroup:(UIButton *)sender {
    if(groupListData.count > 0) {
        NSMutableArray *names = [NSMutableArray array];
        for(ReleaseGroupData *data in groupListData) {
            [names addObject: data.itemGroupNm];
        }
        [CommonUtil showActionSheet:names target:self selected:^(NSInteger index) {
            [_groupBtn setTitle:names[index] forState:UIControlStateNormal];
            [self removeModelData];
            [self requestGroupList:@"3" isSelect:NO];
        }];
    } else {
        [self requestGroupList:@"2" isSelect:YES];
    }
}

- (IBAction)pressedModel:(UIButton *)sender {
    if([NSString isEmptyString:_groupBtn.currentTitle] == NO) {
        if(modelListData.count > 0) {
            NSMutableArray *names = [NSMutableArray array];
            for(ReleaseGroupData *data in modelListData) {
                [names addObject: data.itemGroupNm];
            }
            [CommonUtil showActionSheet:names target:self selected:^(NSInteger index) {
                [_modelBtn setTitle:names[index] forState:UIControlStateNormal];
                [self removeDetailData];
                [self removeProductData];
            }];
        } else {
            [self requestGroupList:@"2" isSelect:YES];
        }
    } else {
        [CommonUtil showToastMessage:@"중분류을 선택해주세요"];
    }
}

- (IBAction)pressedDetail:(UIButton *)sender {
    if([NSString isEmptyString:_modelBtn.currentTitle] == NO) {
        if(detailListData.count > 0) {
            NSMutableArray *names = [NSMutableArray array];
            for(ReleaseGroupData *data in detailListData) {
                [names addObject: data.itemGroupNm];
            }
            [CommonUtil showActionSheet:names target:self selected:^(NSInteger index) {
                [_detailBtn setTitle:names[index] forState:UIControlStateNormal];
                [self removeProductData];
            }];
        } else {
            [self requestGroupList:@"3" isSelect:YES];
        }
    } else {
        [CommonUtil showToastMessage:@"소분류을 선택해주세요"];
    }
}

- (IBAction)pressedProductNm:(UIButton *)sender {
    if([self isGiftType]) {
        [self requestProductList];
    } else {
        if([NSString isEmptyString:_detailBtn.currentTitle] == NO) {
            [self requestStockList];
        } else {
            [CommonUtil showToastMessage:@"세분류을 선택해주세요"];
        }
    }
}

- (IBAction)pressedReleaseAdd:(id)sender {
    [CommonUtil hideKeyboard];
    
    NSString *quantity = _quantityTF.text;
    BOOL isQuantity = [NSString isEmptyString:quantity];
    BOOL isContain = NO;
    for(ProductValidationData *data in addedArray) {
        if([data.itemCd isEqualToString:productData.itemCd]) {
            isContain = YES;
            break;
        }
    }
    
    if(isQuantity || isContain) {
        if(isQuantity) { // 수량 입력 필요
            [CommonUtil showToastMessage:kDescQuantityNeed];
        } else if(isContain) { // 동일 제품 존재
            [CommonUtil showToastMessage:kDescAlreadyAdded];
        }
        return;
    }
    
    // 재고 수량 체크
    if([productData.qty integerValue] < [quantity integerValue]) {
        [CommonUtil showToastMessage:kDescStockEnoughNeed];
        return;
    }
    
    productData.inputQty = quantity;
    NSString *code = [CommonObject codeFromReleaseDataType:kReleaseDataTypeProduct name:_productBtn.currentTitle];
    if([code isEqualToString:@"Z10000"] == NO) { // 제품의 경우는 'N'
        productData.giftYn = @"N";
    }
    
    productData.invYn = @"N"; // 여유재고판단 무조건 ('N')
    [addedArray addObject:productData];
    [_listTbV reloadData];
    [self removeAllData];
    [self checkAddedData];
}

- (IBAction)pressedComplete:(UIButton *)sender {
    [self requestRegister];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    _listTbV.hidden = addedArray == nil;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return addedArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ReleaseAddTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kReleaseAddCellIdentifier];
    cell.deleteHandler = ^{
        [addedArray removeObjectAtIndex:indexPath.row];
        [_listTbV reloadData];
        [self checkAddedData];
    };
    [cell setCellFromAddData:addedArray[indexPath.row]];
    return cell;
}

#pragma mark - ListPopupViewDelegate
- (void)selectedItem:(JSONModel *)data type:(ListPopupType)type {
    productData = (ReleaseProductData *)data;
    _nameTF.text = [NSString stringWithFormat:@"%@", productData.itemNm];
}

@end
