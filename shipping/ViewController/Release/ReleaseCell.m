//
//  ReleaseCell.m
//  shipping
//
//  Created by ios on 2018. 4. 12..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ReleaseCell.h"

static NSString *kValueCellIdentifier = @"ValueCell";

@implementation ReleaseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initLayout];
}

- (void)initLayout {
    [_contentV applyRoundBorder:4.0 color:UIColorFromRGB(0xB5B5B5) width:0.4];
    
    _valueTbV.delegate = self;
    _valueTbV.dataSource = self;
    
    UINib *cellNib = [UINib nibWithNibName:@"ReleaseAddTableViewCell" bundle:nil];
    [_valueTbV registerNib:cellNib forCellReuseIdentifier:kValueCellIdentifier];
    
    _valueTbV.estimatedSectionHeaderHeight = UITableViewAutomaticDimension;
    _valueTbV.estimatedSectionFooterHeight = UITableViewAutomaticDimension;
    _valueTbV.estimatedRowHeight = UITableViewAutomaticDimension;
}

- (void)setCellFromData:(NSArray *)datas {
    values = datas;
    [_valueTbV reloadData];
    
    _valueHeightLC.constant = 10000;
    [_valueTbV layoutIfNeeded];
    _valueHeightLC.constant = _valueTbV.contentSize.height;
    
    ReleaseListData *firstData = datas.firstObject;
    _dateLb.text = firstData.reqDt;
    _titleLb.text = firstData.issueNo;
    _stateLb.text = firstData.movTypeNm;
    _locationLb.text = firstData.fromSlNm;
    _countLb.text = [NSString stringWithFormat:@"%zd", datas.count];
    
    _deleteBtn.hidden = [firstData.erpFlag isEqualToString:@"Y"];
    _addBtn.hidden = _deleteBtn.isHidden;
}

- (IBAction)pressedDelete:(UIButton *)sender {
    _deleteHandler();
}

- (IBAction)pressedAdd:(UIButton *)sender {
    _addHandler();
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return values.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ReleaseAddTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kValueCellIdentifier forIndexPath:indexPath];
    cell.deleteHandler = ^{
        _subDeleteHandler(indexPath.row);
    };
    ReleaseListData *data = values[indexPath.row];
    [cell setCellFromReleaseData:data];
    return cell;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _dateLb.text = nil;
    _titleLb.text = nil;
    _stateLb.text = nil;
    _countLb.text = nil;
    values = nil;
}

@end
