//
//  ReleaseViewController.h
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"
#import "ReleaseCell.h"
#import "ReleaseAddViewController.h"

@interface ReleaseViewController : BaseViewController <ReleaseAddDelegate> {
    ReleaseCell                             *infinitiveCell;
    NSMutableArray                          *listArray;
}

@property (weak, nonatomic) IBOutlet UICollectionView *listCV;
@property (weak, nonatomic) IBOutlet UIView *noDataV;
@end
