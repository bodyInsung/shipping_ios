//
//  ReleaseAddTableViewCell.h
//  shipping
//
//  Created by ios on 2018. 4. 13..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReleaseAddTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet UILabel *valueLb;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@property (strong, nonatomic) void(^deleteHandler)(void);

- (void)setCellFromAddData:(ReleaseProductData *)data;
- (void)setCellFromReleaseData:(ReleaseListData *)data;

@end
