//
//  ReleaseAddTableViewCell.m
//  shipping
//
//  Created by ios on 2018. 4. 13..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ReleaseAddTableViewCell.h"

@implementation ReleaseAddTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setCellFromAddData:(ReleaseProductData *)data {
    _titleLb.text = [NSString stringWithFormat:@"%@ %@", data.itemNm, data.itemCd];
    _valueLb.text = data.inputQty;
}

- (void)setCellFromReleaseData:(ReleaseListData *)data {
    NSMutableString *value = [NSMutableString string];
    [value appendFormat:@"제품명 : %@\n상태 : %@", data.itemNm, data.procStatNm];
    
    NSString *code = nil;
    if([data.giftYn isEqualToString:@"Y"]) {
        code = data.itemCd;
    } else {
        code = data.serialCd;
    }
    if([NSString isEmptyString:code] == NO) {
        [value appendFormat:@"\n코드 : %@", code];
    }
    
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:value];
    [attributed addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:[value rangeOfString:code]];
    _titleLb.attributedText = attributed;
    
    _deleteBtn.hidden = [data.erpFlag isEqualToString:@"Y"];
}

- (IBAction)pressedDelete:(UIButton *)sender {
    _deleteHandler();
}

- (void)prepareForReuse {
    [super prepareForReuse];
    _titleLb.text = nil;
    _valueLb.text = nil;
}

@end
