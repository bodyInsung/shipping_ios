//
//  ReleaseCell.h
//  shipping
//
//  Created by ios on 2018. 4. 12..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReleaseAddTableViewCell.h"

@interface ReleaseCell : UICollectionViewCell <UITableViewDataSource, UITableViewDelegate> {
    
    NSArray                 *values;
}

@property (weak, nonatomic) IBOutlet UIView *contentV;
@property (weak, nonatomic) IBOutlet UILabel *dateLb;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet UILabel *stateLb;
@property (weak, nonatomic) IBOutlet UILabel *locationLb;
@property (weak, nonatomic) IBOutlet UILabel *countLb;
@property (weak, nonatomic) IBOutlet UITableView *valueTbV;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *valueHeightLC;

@property (strong, nonatomic) void(^deleteHandler)(void);
@property (strong, nonatomic) void(^addHandler)(void);
@property (strong, nonatomic) void(^subDeleteHandler)(NSInteger);

- (void)setCellFromData:(NSArray *)datas;
@end
