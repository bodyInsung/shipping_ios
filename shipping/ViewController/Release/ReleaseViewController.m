//
//  ReleaseViewController.m
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ReleaseViewController.h"
#import "ReleaseListData.h"

static NSString *kReleaseCellIdentifier = @"ReleaseCell";

@interface ReleaseViewController ()

@end

@implementation ReleaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self requestList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:STORYBOARD_RELEASE_ADD_SEGUE]) {
        ReleaseAddViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.receiveData = sender;
    }
}

- (void)reloadViewController {
    [self requestList];
}

- (void)initLayout {
    [super initLayoutForTitle:TITLE_RELEASE_ACTIONBAR];
    [self showAddButton];
    [self setBaseSV:_listCV];
    
    UINib *cellNib = [UINib nibWithNibName:@"ReleaseCell" bundle:nil];
    [_listCV registerNib:cellNib forCellWithReuseIdentifier:kReleaseCellIdentifier];
    _listCV.allowsSelection = NO;
    
    // 셀의 높이를 구하기 위해 셀 오브젝트를 가지고 있는다
    infinitiveCell = [[cellNib instantiateWithOwner:nil options:nil] firstObject];
    
    [self addPullToRefresh];
}

- (void)addPullToRefresh {
    UIRefreshControl *pullToRC = [[UIRefreshControl alloc] init];
    [pullToRC addTarget:self action:@selector(refreshRequestData:) forControlEvents:UIControlEventValueChanged];
    [_listCV addSubview:pullToRC];
    [_listCV sendSubviewToBack:pullToRC];
}

- (void)requestList {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    NSDate *date = [CommonUtil preOneMonthDateFromDate:[NSDate date]];
    [parameters setValue:[CommonUtil stringFromDate:date format:@"yyyy-MM-dd"] forKey:@"fromDate"];
    
    [ApiManager requestToPost:RELEASE_LIST_URL parameters:parameters success:^(id responseObject) {
        [self restoreResultData:responseObject];
        [_listCV reloadData];
        [LoadingView hideLoadingViewToDelay];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)restoreResultData:(id)datas {
    if(listArray != nil) {
        [listArray removeAllObjects];
        listArray = nil;
    }
    listArray = [NSMutableArray array];
    NSMutableArray *responseList = [NSMutableArray arrayWithArray:datas[API_RESPONSE_KEY]];

    while(responseList.count > 0) {
        NSMutableArray *filterArray = [NSMutableArray arrayWithArray:responseList];
        [filterArray sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"yyyy-MM-dd"];
            NSDate *date1 = [df dateFromString:obj1[@"REQ_DT"]];
            NSDate *date2 = [df dateFromString:obj2[@"REQ_DT"]];
            return [date2 compare:date1];
        }];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ISSUE_NO = %@", filterArray.firstObject[@"ISSUE_NO"]];
        [filterArray filterUsingPredicate:predicate];
        NSMutableArray *shippingList = [ReleaseListData arrayOfModelsFromDictionaries:filterArray error:nil];
        [responseList removeObjectsInArray:filterArray];
        [listArray addObject:shippingList];
    }
}

- (void)requestRemove:(NSString *)no {
    [self requestRemove:no ifNo:nil];
}

- (void)requestRemove:(NSString *)no ifNo:(NSString *)ifNo {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:no forKey:@"issueNo"];
    if([NSString isEmptyString:ifNo] == NO) {
        [parameters setValue:ifNo forKey:@"ifNo"];
    }
    [ApiManager requestToPost:RELEASE_DELETE_URL parameters:parameters success:^(id responseObject) {
        ResultModel *resultData = [[ResultModel alloc] initWithDictionary:responseObject error:nil];
        [CommonUtil showToastMessage:resultData.resultMsg];
        if([resultData.resultCode isEqualToString:CONNECT_SUCCESS_CODE]) {
            [self requestList];
        } else {
            [LoadingView hideLoadingView];
        }
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

#pragma mark - Action Event
- (void)refreshRequestData:(UIRefreshControl *)sender {
    [self requestList];
    [sender endRefreshing];
}

- (IBAction)pressedRetry:(id)sender {
    [self requestList];
}

#pragma mark - UICollectionViewDataSource & UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    collectionView.hidden = !(listArray.count > 0);
    _noDataV.hidden = !collectionView.isHidden;
    return listArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ReleaseCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kReleaseCellIdentifier forIndexPath:indexPath];
    NSArray *datas = listArray[indexPath.row];
    ReleaseListData *data = datas.firstObject;
    cell.deleteHandler = ^{
        [CommonUtil showAlertTitle:kTitleDelete msg:kDescDeleteQuestion type:kAlertTypeTwo target:self cAction:^{
            [self requestRemove:data.issueNo];
        }];
    };
    cell.addHandler = ^{
        [self openViewController:STORYBOARD_RELEASE_ADD_SEGUE sender:data];
    };
    
    cell.subDeleteHandler = ^(NSInteger index) {
        [CommonUtil showAlertTitle:kTitleDelete msg:kDescDeleteQuestion type:kAlertTypeTwo target:self cAction:^{
            ReleaseListData *data = datas[index];
            [self requestRemove:data.issueNo ifNo:data.ifNo];
        }];
    };
    
    [cell setCellFromData:datas];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    // 적용된 컨텐츠뷰의 사이즈 높이를 리턴
    [infinitiveCell setCellFromData:listArray[indexPath.row]];
    CGFloat height = [infinitiveCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    UICollectionViewFlowLayout* flow = (UICollectionViewFlowLayout *)collectionView.collectionViewLayout;
    return CGSizeMake(collectionView.bounds.size.width - flow.sectionInset.left - flow.sectionInset.left, height);
}

#pragma mark - ActionBarDelegate
- (void)pressedAdd {
    [self openViewController:STORYBOARD_RELEASE_ADD_SEGUE sender:nil];
}
@end
