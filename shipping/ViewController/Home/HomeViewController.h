//
//  HomeViewController.h
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"
#import "FillChartView.h"

@interface HomeViewController : BaseViewController <FillChartDelegate> {
    FillChartView *remainChart;
    FillChartView *completionChart;
    FillChartView *montlyChart;
}

@property (weak, nonatomic) IBOutlet UILabel *salaryLb;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *salaryHeightLC;
@property (weak, nonatomic) IBOutlet UIScrollView *contentSV;
@property (weak, nonatomic) IBOutlet UIView *remainShipV;
@property (weak, nonatomic) IBOutlet UIView *completeShipV;
@property (weak, nonatomic) IBOutlet UIView *montlyShipV;
@end
