//
//  HomeViewController.m
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "HomeViewController.h"
#import "ShippingMainModel.h"

@interface HomeViewController ()
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self reloadViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)reloadViewController {
    [self requestHome];
//    [self requestSalary];
}

- (void)initLayout {
    [super initLayoutForLogo];
    [self showPromotionButton];
    
    remainChart = [[FillChartView alloc] init];
    remainChart.delegate = self;
    [_remainShipV addSubview:remainChart];
    [remainChart applyAutoLayoutFromSuperview:_remainShipV];
    remainChart.chartColor = UIColorFromRGB(0x48A6C6);
    
    completionChart = [[FillChartView alloc] init];
    completionChart.delegate = self;
    [_completeShipV addSubview:completionChart];
    [completionChart applyAutoLayoutFromSuperview:_completeShipV];
    completionChart.chartColor = UIColorFromRGB(0x1D5886);
    
    montlyChart = [[FillChartView alloc] init];
    montlyChart.delegate = self;
    [_montlyShipV addSubview:montlyChart];
    [montlyChart applyAutoLayoutFromSuperview:_montlyShipV];
    montlyChart.chartColor = UIColorFromRGB(0x0D2E4E);
    
    [self addPullToRefresh];
}

/**
 당겨서 새로고침 추가
 */
- (void)addPullToRefresh {
    UIRefreshControl *pullToRC = [[UIRefreshControl alloc] init];
    [pullToRC addTarget:self action:@selector(refreshRequestData:) forControlEvents:UIControlEventValueChanged];
    [_contentSV addSubview:pullToRC];
    [_contentSV sendSubviewToBack:pullToRC];
}

/**
 홈 데이터 요청
 */
- (void)requestHome {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:[CommonUtil todayFromDateFormat:DATE_YEAR_MONTH_DAY] forKey:@"date"];
    [parameters setValue:kProductTypeMassage forKey:@"s_producttype"];
    
    [ApiManager requestToPost:HOME_URL parameters:parameters success:^(id responseObject) {
        ShippingMainModel *shippingMain = [[ShippingMainModel alloc] initWithDictionary:responseObject[API_RESPONSE_KEY] error:nil];
        
        [remainChart reloadFromDataForArray:shippingMain.remainList chartType:kChartTypeRemain];
        [completionChart reloadFromDataForArray:shippingMain.compList chartType:kChartTypeCompletion];
        [montlyChart reloadFromDataForObject:shippingMain.monthCnt chartType:kChartTypeMontly];
        [LoadingView hideLoadingViewToDelay];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

/**
 수당 데이터 요청 (사용안함)
 */
- (void)requestSalary {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    UserResultData *userInfo = [UserManager loadUserData:USER_DATA_KEY];
    [parameters setValue:userInfo.adminId forKey:@"employeeId"];
    NSDate *startDate = [CommonUtil firstDateFromFromDate:[NSDate date]];
    [parameters setValue:[CommonUtil stringFromDate:startDate format:DATE_HYPHEN_YEAR_MONTH_DAY] forKey:@"dateStart"];
    [parameters setValue:[CommonUtil todayFromDateFormat:DATE_HYPHEN_YEAR_MONTH_DAY] forKey:@"dateEnd"];
    
    [ApiManager requestToPost:SALARY_MONTHLY_LIST_URL parameters:parameters isLoading:NO success:^(id responseObject) {
        SalaryResultData *data = [(NSArray<SalaryResultData> *)[SalaryResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil] firstObject];
        NSString *salary = [NSString addCommaFromString:data.grandTotal];
        _salaryLb.text = [NSString stringWithFormat:@"%@원", salary];
        _salaryHeightLC.active = NO;
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        _salaryLb.text = @"";
        _salaryHeightLC.active = YES;
        [LoadingView hideLoadingView];
    }];
}

#pragma mark - Action Event
- (void)refreshRequestData:(UIRefreshControl *)sender {
    [self requestHome];
    [sender endRefreshing];
}

- (void)pressedPromotion {
    [self openViewController:STORYBOARD_PROMOTION_SEGUE sender:self];
}

#pragma mark - FillChartDelegate
- (void)pressedChart:(ChartType)type {
    if(type == kChartTypeRemain) { // 남은 배송건
        self.tabBarController.selectedIndex = 1;
    } else if(type == kChartTypeCompletion || type == kChartTypeMontly) { // 배송 완료건, 당월 완료건 
        self.tabBarController.selectedIndex = 3;
    }
}

@end
