//
//  FillChartView.m
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "FillChartView.h"
#import "ProductData.h"
#import "MonthCountData.h"

@implementation FillChartView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init {
    self = [super init];
    if(self) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] firstObject];
    }
    return self;
}

- (void)setChartColor:(UIColor *)chartColor {
    _valueLb.textColor = chartColor;
    _chartColor = chartColor;
}

- (void)reloadFromDataForArray:(NSArray *)datas chartType:(ChartType)type {
    chartType = type;

    if(type == kChartTypeRemain || type == kChartTypeCompletion) {
        if(type == kChartTypeRemain) {
            _titleLb.text = @"남은 배송건";
        } else if(type == kChartTypeCompletion) {
            _titleLb.text = @"배송 완료건";
        }
        _valueLb.text = [NSString stringWithFormat:@"%zd", datas.count];
        [self addChartTextForArray:datas];
    }
    
    [self drawChart];
}

- (void)reloadFromDataForObject:(NSObject *)object chartType:(ChartType)type {
    chartType = type;
    
    if(type == kChartTypeMontly) {
        _titleLb.text = @"당월 완료건";
        
        if([object isMemberOfClass:[MonthCountData class]]) {
            MonthCountData *datas = (MonthCountData *)object;
            NSInteger dataCount = 0;
            for(NSString *count in datas.toDictionary.allValues) {
                dataCount += count.integerValue;
            }
            // 해당 4개는 갯수에 포함되지 않음(전동침대, 전동기타, 고급프레임, 고급프레임기타)
            dataCount -= datas.moterizedBed.integerValue + datas.moterizedBedEtc.integerValue + datas.royalFrame.integerValue + datas.royalFrameEtc.integerValue;
            
            _valueLb.text = [NSString stringWithFormat:@"%zd", dataCount];
            [self addChartTextForMontly:datas];
        }
    }
    
    [self drawChart];
}

- (void)drawChart {
    if(fillLayer != nil) {
        [fillLayer removeFromSuperlayer];
        fillLayer = nil;
    }
    
    [_chartV layoutIfNeeded];
    
    fillLayer = [CAShapeLayer layer];
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointZero];
    
    CGPoint endPoint = CGPointMake(_chartV.frame.size.width, 0);
    CGPoint controlPoint1 = CGPointMake(_chartV.frame.size.width/4, -8);
    CGPoint controlPoint2 = CGPointMake(_chartV.frame.size.width/4*3, 8);
    [bezierPath addCurveToPoint:endPoint controlPoint1:controlPoint1 controlPoint2:controlPoint2];
    [bezierPath addLineToPoint:CGPointMake(_chartV.frame.size.width, _chartV.frame.size.height)];
    [bezierPath addLineToPoint:CGPointMake(0, _chartV.frame.size.height)];

    fillLayer.fillColor = _chartColor.CGColor;
    fillLayer.path = bezierPath.CGPath;
    [_chartV.layer insertSublayer:fillLayer atIndex:0];
}

- (void)addChartTextForArray:(NSArray *)datas {
    [_chartV removeAllSubviews];
    
    UIView *preView = nil;
    for (ProductData *data in datas) {
        UILabel *timeLb = [[UILabel alloc] init];
        timeLb.text = data.promiseTime;
        timeLb.font = [UIFont boldSystemFontOfSize:10.0];
        timeLb.textColor = UIColorFromRGB(0xFFFFFF);
        timeLb.backgroundColor = [UIColor clearColor];
        [_chartV addSubview:timeLb];
        
        [timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(_chartV).offset(4);
            if(preView != nil) {
                make.top.equalTo(preView.mas_bottom);
            } else {
                make.top.equalTo(_chartV).offset(6);
            }
            if([data isEqual:datas.lastObject]) {
                make.bottom.equalTo(_chartV).offset(-4);
            }
        }];
        
        UILabel *nameLb = [[UILabel alloc] init];
        nameLb.text = data.productName;
        nameLb.font = [UIFont systemFontOfSize:10.0];
        nameLb.textColor = UIColorFromRGB(0xFFFFFF);
        nameLb.numberOfLines = 0;
        [_chartV addSubview:nameLb];
        
        [nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(timeLb.mas_trailing).offset(4);
            make.height.equalTo(timeLb);
            make.centerY.equalTo(timeLb);
            make.trailing.equalTo(_chartV).offset(-4);
        }];
        [nameLb setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
        preView = timeLb;
    }
}

- (void)addChartTextForMontly:(MonthCountData *)datas {
    [_chartV removeAllSubviews];
    
    UIView *preView = nil;
    for (NSString *title in [CommonObject ShippingMonthArray]) {
        UILabel *titleLb = [[UILabel alloc] init];
        titleLb.text = title;
        titleLb.font = [UIFont boldSystemFontOfSize:10.0];
        titleLb.textColor = UIColorFromRGB(0xFFFFFF);
        titleLb.backgroundColor = [UIColor clearColor];
        [_chartV addSubview:titleLb];
        
        [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(_chartV).offset(4);
            if(preView != nil) {
                make.top.equalTo(preView.mas_bottom);
            } else {
                make.top.equalTo(_chartV).offset(142);
            }
            if([title isEqual:[CommonObject ShippingMonthArray].lastObject]) {
                make.bottom.equalTo(_chartV).offset(-4);
            }
        }];
        
        UILabel *nameLb = [[UILabel alloc] init];
        NSInteger count = 0;
        if([title isEqualToString:kTitleMoterizedBed]) {
            count = datas.moterizedBed.integerValue;
        } else if([title isEqualToString:kTitleMoterizedBedEtc]) {
            count = datas.moterizedBedEtc.integerValue;
        } else if([title isEqualToString:kTitleRoyalFrame]) {
            count = datas.royalFrame.integerValue;
        } else if([title isEqualToString:kTitleRoyalFrameEtc]) {
            count = datas.royalFrameEtc.integerValue;
        } else if([title isEqualToString:kTitleDelivery]) {
            count = datas.deliveryOfMonth.integerValue;
        } else if([title isEqualToString:kTitleChange]) {
            count = datas.changeOfMonth.integerValue;
        } else if([title isEqualToString:kTitleCancel]) {
            count = datas.cancelOfMonth.integerValue;
        } else if([title isEqualToString:kTitleRelocationOfMonth1]) {
            count = datas.relocationOfMonth1.integerValue;
        } else if([title isEqualToString:kTitleRelocationOfMonth0]) {
            count = datas.relocationOfMonth0.integerValue;
        } else if([title isEqualToString:kTitleReinstall]) {
            count = datas.reinstallOfMonth.integerValue;
        } else if([title isEqualToString:kTitleAsOfMonth]) {
            count = datas.asOfMonth.integerValue;
        } else if([title isEqualToString:kTitleFaultyOfMonth]) {
            count = datas.faultyOfMonth.integerValue;
        } else if([title isEqualToString:kTitleEtcOfMonth]) {
            count = datas.etcOfMonth.integerValue;
        }
        nameLb.text = [NSString stringWithFormat:kFormatCase, count];
        nameLb.font = [UIFont systemFontOfSize:10.0];
        nameLb.textColor = UIColorFromRGB(0xFFFFFF);
        nameLb.backgroundColor = [UIColor clearColor];
        nameLb.numberOfLines = 0;
        [_chartV addSubview:nameLb];
        
        [nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(titleLb.mas_trailing).offset(4);
            make.height.equalTo(titleLb);
            make.centerY.equalTo(titleLb);
            make.trailing.equalTo(_chartV).offset(-4);
        }];
        preView = titleLb;
    }
}

#pragma mark - Action Event
- (IBAction)tapGestureFromBack:(id)sender {
    [_delegate pressedChart:chartType];
}

@end
