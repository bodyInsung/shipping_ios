//
//  FillChartView.h
//  shipping
//
//  Created by ios on 2018. 3. 20..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kChartTypeRemain,           // 남은 배송건
    kChartTypeCompletion,       // 배송 완료건
    kChartTypeMontly,           // 당월 완료건
} ChartType;

@protocol FillChartDelegate
- (void)pressedChart:(ChartType)type;
@end

@interface FillChartView : UIView {
    CAShapeLayer        *fillLayer;
    ChartType           chartType;
}

@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet UILabel *valueLb;
@property (weak, nonatomic) IBOutlet UIView *chartV;
@property (weak, nonatomic) UIColor *chartColor;
@property (weak) id<FillChartDelegate> delegate;

- (void)reloadFromDataForArray:(NSArray *)datas chartType:(ChartType)type;
- (void)reloadFromDataForObject:(NSObject *)object chartType:(ChartType)type;
@end
