//
//  NoticeDetailViewController.m
//  shipping
//
//  Created by ios on 2018. 4. 13..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "NoticeDetailViewController.h"

@interface NoticeDetailViewController ()

@end

@implementation NoticeDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)reloadViewController {
    [self closeViewController];
}

- (void)initLayout {
    [super initLayoutForTitle:TITLE_NOTICE_DETAIL_ACTIONBAR];
    
    _contentLb.text = _data.noticeContents;
    [_contentIV setImageWithUrlForDynamic:_data.pictureOne];
}

- (void)closeViewController {
    [super closeViewController];
    [_delegate reloadViewController];
}

#pragma mark - Action Event
- (IBAction)tapGestureImage:(UITapGestureRecognizer *)sender {
    if(sender.state == UIGestureRecognizerStateEnded) {
        NSString *urlString = [_data.pictureOne stringByReplacingOccurrencesOfString:@"/svc" withString:@""];
        [CommonUtil openLinkWeb:[NSString encodeUrl:urlString]];
    }
}
@end
