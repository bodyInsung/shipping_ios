//
//  NoticeDetailViewController.h
//  shipping
//
//  Created by ios on 2018. 4. 13..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"
#import "NoticeResultData.h"

@protocol NoticeDetailViewDelegate
- (void)reloadViewController;
@end

@interface NoticeDetailViewController : BaseViewController

@property (weak) id<NoticeDetailViewDelegate> delegate;
@property (strong, nonatomic) NoticeResultData *data;
@property (weak, nonatomic) IBOutlet UILabel *contentLb;
@property (weak, nonatomic) IBOutlet UIImageView *contentIV;
@end
