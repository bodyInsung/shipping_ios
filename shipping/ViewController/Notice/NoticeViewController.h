//
//  NoticeViewController.h
//  shipping
//
//  Created by ios on 2018. 3. 28..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"
#import "NoticeDetailViewController.h"

@interface NoticeViewController : BaseViewController <NoticeDetailViewDelegate> {
    
    NSArray                 *noticeData;
}

@property (weak, nonatomic) IBOutlet UITableView *listTbV;
@property (weak, nonatomic) IBOutlet UIView *noDataV;
@end
