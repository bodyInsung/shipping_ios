//
//  NoticeTableViewCell.m
//  shipping
//
//  Created by ios on 2018. 4. 13..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "NoticeTableViewCell.h"

@implementation NoticeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellFromData:(NoticeResultData *)data {
    _titleLb.text = data.noticeTitle;
    _dateLb.text = data.rgDt;
    _iconIV.hidden = ![data.noticeNew boolValue];
}
@end
