//
//  NoticeViewController.m
//  shipping
//
//  Created by ios on 2018. 3. 28..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "NoticeViewController.h"
#import "NoticeTableViewCell.h"
#import "NoticeDetailViewController.h"

static NSString *kNoticeCellIdentifier = @"NoticeCell";

@interface NoticeViewController ()

@end

@implementation NoticeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:STORYBOARD_NOTICE_DETAIL_SEGUE]) {
        NoticeDetailViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.data = sender;
    }
}

- (void)reloadViewController {
    [self requestList];
}

- (void)initLayout {
    [super initLayoutForTitle:TITLE_NOTICE_ACTIONBAR];
    
    UINib *cellNib = [UINib nibWithNibName:@"NoticeTableViewCell" bundle:nil];
    [_listTbV registerNib:cellNib forCellReuseIdentifier:kNoticeCellIdentifier];
    
    [self addPullToRefresh];
}

/**
 당겨서 새로고침 추가
 */
- (void)addPullToRefresh {
    UIRefreshControl *pullToRC = [[UIRefreshControl alloc] init];
    [pullToRC addTarget:self action:@selector(refreshRequestData:) forControlEvents:UIControlEventValueChanged];
    [_listTbV addSubview:pullToRC];
    [_listTbV sendSubviewToBack:pullToRC];
}

/**
 공지사항 데이터 요청
 */
- (void)requestList {
    [ApiManager requestToPost:NOTICE_URL parameters:nil success:^(id responseObject) {
        if(noticeData != nil) {
            noticeData = nil;
        }
        noticeData = [NoticeResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY][@"list"] error:nil];
        [_listTbV reloadData];
        [LoadingView hideLoadingViewToDelay];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)requestNoticeLog:(NoticeResultData *)data {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"SP" forKey:@"sess_group_cd"];
    [parameters setValue:data.noticeSeq forKey:@"notice_seq"];
    [ApiManager requestToPost:NOTICE_LOG_URL parameters:parameters success:^(id responseObject) {
        [self openViewController:STORYBOARD_NOTICE_DETAIL_SEGUE sender:data];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [CommonUtil showToastMessage:errorMsg];
        [LoadingView hideLoadingView];
    }];
}

#pragma mark - Action Event
- (void)refreshRequestData:(UIRefreshControl *)sender {
    [self requestList];
    [sender endRefreshing];
}

- (IBAction)pressedRetry:(id)sender {
    [self requestList];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    tableView.hidden = !([noticeData count] > 0);
    _noDataV.hidden = !tableView.isHidden;
    return [noticeData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NoticeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNoticeCellIdentifier];
    [cell setCellFromData:noticeData[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NoticeResultData *data = noticeData[indexPath.row];
    [self requestNoticeLog:data];
}
@end
