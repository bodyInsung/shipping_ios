//
//  EditMapViewController.h
//  shipping
//
//  Created by insung on 2018. 10. 19..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"

@interface EditMapViewController : BaseViewController<GMSMapViewDelegate> {

    NSArray                     *resultData;
    NSMutableArray              *selectedData;
    GMSCoordinateBounds         *mapBounds;
}

@property (weak, nonatomic) IBOutlet UICollectionView *listCV;
@property (weak, nonatomic) IBOutlet GMSMapView *mapV;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *listHeightLC;

@end
