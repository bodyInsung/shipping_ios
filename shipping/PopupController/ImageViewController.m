//
//  ImageViewController.m
//  shipping
//
//  Created by ios on 2018. 4. 10..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController ()

@end

@implementation ImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initLayout {
    [self landscapeViewController];
    
    [_contentV applyRoundBorder:4.0 color:UIColorFromRGB(0xB5B5B5) width:0.4];
    [_confirmBtn applyRoundBorder:4.0];
    
    [_contentIV setImageWithBaseUrl:_imageUrl];
}

- (IBAction)pressedConfirm:(id)sender {
    [self closeViewController];
}
@end
