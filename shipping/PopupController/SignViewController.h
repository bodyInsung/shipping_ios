//
//  SignViewController.h
//  shipping
//
//  Created by ios on 2018. 4. 4..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BaseViewController.h"
#import "DrawView.h"

@protocol SignViewControllerDelegate
- (void)receiveImageData:(UIImage *)image;
@end

@interface SignViewController : BasePopupViewController {
    CALayer                 *drawLayer;
}

@property (weak) id<SignViewControllerDelegate> delegate;
@property (strong, nonatomic) NSString *custName;
@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet DrawView *drawV;
@property (weak, nonatomic) IBOutlet UIButton *resetBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@end
