//
//  ListPopupTableViewCell.h
//  shipping
//
//  Created by insung on 2018. 7. 10..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListPopupTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet UILabel *detailLb;
@end
