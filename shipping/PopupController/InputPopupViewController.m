//
//  InputPopupViewController.m
//  shipping
//
//  Created by insung on 17/06/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "InputPopupViewController.h"

@interface InputPopupViewController ()

@end

@implementation InputPopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initLayout {
    [_contentV applyRoundBorder:4.0];
    [_inputTV applyRoundBorder:4.0 color:UIColorFromRGB(0xB5B5B5) width:0.4];
    [_confirmBtn applyRoundBorder:4.0];
    
    [_inputTV setAccessoryView];
}

- (void)requestInput {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [ApiManager requestToPost:SEARCH_CODE_URL parameters:parameters success:^(id responseObject) {
        [LoadingView hideLoadingViewToDelay];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

#pragma mark - Action Event
- (IBAction)pressedClose:(id)sender {
    [self closeViewController];
}

- (IBAction)pressedConfirm:(id)sender {
    [_delegate didInputData:_inputTV.text];
    [self closeViewController];
//    [self requestInput];
}

@end
