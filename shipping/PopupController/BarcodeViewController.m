//
//  BarcodeViewController.m
//  shipping
//
//  Created by ios on 2018. 4. 11..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BarcodeViewController.h"

@interface BarcodeViewController ()

@end

@implementation BarcodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.overlayV.scanFrame = self.scanV.frame;
    self.scanner.scanRect = self.overlayV.scanFrame;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initLayout {
    [_cancelBtn applyRoundBorder:4.0];
    
    self.scanner = [[MTBBarcodeScanner alloc] initWithPreviewView:_overlayV];
    self.scanner.allowTapToFocus = NO;

    [self startScanning];
}

- (void)startScanning {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
            __block BOOL isScanning = NO;
            if(success) {
                NSError *error;
                [self.scanner startScanningWithResultBlock:^(NSArray *codes) {
                    if(isScanning == NO) {
                        isScanning = YES;
                        AVMetadataMachineReadableCodeObject *code = [codes firstObject];
                        [self.scanner captureStillImage:^(UIImage *image, NSError *error) {
                            if(error != nil) {
                                [CommonUtil showToastMessage:@"스캔되지 않았습니다."];
                            } else {
                                if(_isUseImage == YES) {
                                    [_delegate scanBarcode:code.stringValue image:image];
                                } else {
                                    [_delegate scanBarcode:code.stringValue];
                                }
                                [self removeCapture];
                                [self closeViewController];
                            }
                        }];
                    }
                } error:&error];
            } else {
                [CommonUtil showToastMessage:@"스캔되지 않았습니다."];
            }
        }];
    });
}

- (void)removeCapture {
    [self.scanner stopScanning];
    self.scanner = nil;
}

#pragma mark - Action Event
- (IBAction)pressedCancel:(id)sender {
    [self removeCapture];
    [self closeViewController];
}

@end
