//
//  BarcodeOverlayView.m
//  shipping
//
//  Created by ios on 2018. 4. 11..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BarcodeOverlayView.h"

@implementation BarcodeOverlayView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initLayout];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];

    // 바코드 프레임 오버레이
    UIBezierPath *overlayPath = [UIBezierPath bezierPathWithRect:self.bounds];
    UIBezierPath *transparentPath = [UIBezierPath bezierPathWithRect:_scanFrame];
    [overlayPath appendPath:transparentPath];
    [overlayPath setUsesEvenOddFillRule:YES];
    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.path = overlayPath.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.fillColor = UIColorFromRGBA(0x000000, 0.3).CGColor;
    [self.layer addSublayer:fillLayer];

    // 바코드 센터 라인
    UIBezierPath *centerLinePath = [UIBezierPath bezierPath];
    [centerLinePath moveToPoint:CGPointMake(0, _scanFrame.size.height/2)];
    [centerLinePath addLineToPoint:CGPointMake(_scanFrame.size.width, _scanFrame.size.height/2)];
    centerLineLayer = [CAShapeLayer layer];
    centerLineLayer.path = centerLinePath.CGPath;
    centerLineLayer.frame = _scanFrame;
    centerLineLayer.lineWidth = 1.0;
    centerLineLayer.strokeColor = UIColorFromRGB(0xFF0000).CGColor;
    centerLineLayer.fillColor = UIColorFromRGBA(0x000000, 0.0).CGColor;
    [self.layer addSublayer:centerLineLayer];
}

- (void)initLayout {
    // 센터 라인 깜박 반복
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(blinkCenterLine:) userInfo:nil repeats:YES];
}

/**
 센터 라인 깜박 거림

 @param sender sender
 */
- (void)blinkCenterLine:(NSTimer *)sender {
    centerLineLayer.hidden = !centerLineLayer.hidden;
}

@end
