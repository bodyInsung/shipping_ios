//
//  InputPopupViewController.h
//  shipping
//
//  Created by insung on 17/06/2019.
//  Copyright © 2019 bodyfiend. All rights reserved.
//

#import "BasePopupViewController.h"

@protocol InputPopupViewDelegate
- (void)didInputData:(NSString *)string;
@end

@interface InputPopupViewController : BasePopupViewController

@property (weak) id<InputPopupViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *contentV;
@property (weak, nonatomic) IBOutlet CTTextView *inputTV;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@end
