//
//  ListPopupTableViewCell.m
//  shipping
//
//  Created by insung on 2018. 7. 10..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ListPopupTableViewCell.h"

@implementation ListPopupTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    _titleLb.text = nil;
    _detailLb.text = nil;
    
}

@end
