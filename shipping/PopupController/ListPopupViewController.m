//
//  ListPopupViewController.m
//  shipping
//
//  Created by ios on 2018. 4. 2..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ListPopupViewController.h"
#import "ListPopupTableViewCell.h"

static NSString *kListCellIdentifier = @"ListCell";

@interface ListPopupViewController ()

@end

@implementation ListPopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initValue {
    if(_popupType == kListPopupTypeDeliveryman) {
        UserResultData *emptyData = [[UserResultData alloc] init];
        emptyData.adminNm = @"미지정";
        [_datas insertObject:emptyData atIndex:0];
    }
    
    _searchHeightLC.active = !_isSearch;
    listDatas = [NSMutableArray arrayWithArray:_datas];
}

- (void)initLayout {
    if(_popupType == kListPopupTypeDeliveryman) {
        _titleLb.text = kTitleSubDeleveryManPopup;
        _searchTF.placeholder = kDescSubDeliveryManNeed;
    } else if(_popupType == kListPopupTypeCar) {
        _titleLb.text = kTitleCarPopup;
        _searchTF.placeholder = kDescCarSelectionNeed;
    } else if(_popupType == kListPopupTypeProduct ||
              _popupType == kListPopupTypeStock) {
        _titleLb.text = kTitleProductPopup;
        _searchTF.placeholder = kDescProductNameNeed;
    } else if(_popupType == kListPopupTypeWarehouse) {
        _titleLb.text = kTitleWarehouse;
        _searchTF.placeholder = kDescWarehouseNameNeed;
    } else if(_popupType == kListPopupTypeWarehousingDeliveryman) {
        _titleLb.text = kTitleStockDeliveryman;
        _searchTF.placeholder = kDescStockDeliverymanNameNeed;
    }
    
    [_searchTF setAccessoryView];
    [_contentV applyRoundBorder:4.0];
    UINib *cellNib = [UINib nibWithNibName:@"ListPopupTableViewCell" bundle:nil];
    [_listTbV registerNib:cellNib forCellReuseIdentifier:kListCellIdentifier];
}

#pragma mark - Action Event
- (IBAction)pressedClose:(UIButton *)sender {
    [self closeViewController];
}

- (IBAction)changedSearch:(UITextField *)sender {
    if(listDatas != nil) {
        [listDatas removeAllObjects];
        listDatas = nil;
    }
    if(sender.text.length == 0) {
        listDatas = [NSMutableArray arrayWithArray:_datas];
    } else {
        NSPredicate *predicate = nil;
        if(_popupType == kListPopupTypeDeliveryman) {
            predicate = [NSPredicate predicateWithFormat:@"adminNm CONTAINS %@", sender.text];
        } else if(_popupType == kListPopupTypeCar) {
            predicate = [NSPredicate predicateWithFormat:@"carNumber CONTAINS %@", sender.text];
        } else if(_popupType == kListPopupTypeProduct ||
                  _popupType == kListPopupTypeStock) {
            predicate = [NSPredicate predicateWithFormat:@"itemNm CONTAINS %@", sender.text];
        } else if(_popupType == kListPopupTypeWarehouse ||
                  _popupType == kListPopupTypeWarehousingDeliveryman) {
            predicate = [NSPredicate predicateWithFormat:@"slNm CONTAINS %@", sender.text];
        }
        listDatas = [NSMutableArray arrayWithArray:[_datas filteredArrayUsingPredicate:predicate]];
    }
    [_listTbV reloadData];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listDatas != nil ? listDatas.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {    
    ListPopupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kListCellIdentifier];
    // 셀 높이를 잡아주기 위함
    [cell.contentView layoutIfNeeded];
    if(_popupType == kListPopupTypeDeliveryman) {
        UserResultData *data = listDatas[indexPath.row];
        cell.titleLb.text = data.adminNm;
    } else if(_popupType == kListPopupTypeCar) {
        CarResultData *data = listDatas[indexPath.row];
        NSMutableString *carNo = [NSMutableString stringWithString:data.carNumber];
        if(![NSString isEmptyString:data.adminInfo]) {
            [carNo appendFormat:@" (%@)", data.adminInfo];
        }
        cell.titleLb.text = carNo;
    } else if(_popupType == kListPopupTypeProduct) {
        ReleaseProductData *data = listDatas[indexPath.row];
        NSMutableString *title = [NSMutableString stringWithFormat:@"제품명 : %@\n", data.itemNm];
        NSString *rentalNm = data.rentalNm;
        if([NSString isEmptyString:rentalNm] == NO) {
            [title appendFormat:@"렌탈명 : %@\n", rentalNm];
        }
        NSString *code = data.itemCd;
        if([NSString isEmptyString:code] == NO) {
            [title appendFormat:@"코드 : %@", code];
        }
        cell.titleLb.text = title;
        if(data.qty != nil) {
            cell.detailLb.text = [NSString stringWithFormat:@"수량 : %@", data.qty];
        }
    } else if(_popupType == kListPopupTypeWarehouse ||
              _popupType == kListPopupTypeWarehousingDeliveryman) {
        ReleaseWarehouseData *data = listDatas[indexPath.row];
        cell.titleLb.text = data.slNm;
    } else if(_popupType == kListPopupTypeStock) {
        ProductValidationData *data = listDatas[indexPath.row];
        NSString *code = nil;
        if([data.giftYn isEqualToString:@"Y"]) {
            code = data.itemCd;
        } else {
            code = [data.serialCd isEqualToString:@"*"] ? @"확인 불필요" : data.serialCd;
        }
        NSMutableString *title = [NSMutableString stringWithFormat:@"제품명 : %@", data.itemNm];
        if([NSString isEmptyString:code] == NO) {
            [title appendFormat:@"\n코드 : %@", code];
        }
        
        NSInteger good = [data.goodAmount integerValue];
        NSInteger bad = [data.badAmount integerValue];
        NSMutableString *count = [NSMutableString string];
        if([data.giftYn isEqualToString:@"S"] &&
           [data.lotFlag isEqualToString:@"N"]) {
            if(good > 0) {
                [count appendFormat:@"양품 : %@", data.goodAmount];
            }
            if(bad > 0) {
                if(good > 0) {
                    [count appendString:@"\n"];
                }
                [count appendFormat:@"불량품 : %@", data.badAmount];
            }
        } else {
            if(good > 0) {
                [count appendString:@"양품"];
            } else if(bad > 0) {
                [count appendString:@"불량품"];
            }
        }
        cell.detailLb.text = count;
        cell.titleLb.text = title;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [_delegate selectedItem:listDatas[indexPath.row] type:_popupType];
    [self closeViewController];
}

@end
