//
//  SignViewController.m
//  shipping
//
//  Created by ios on 2018. 4. 4..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "SignViewController.h"

@interface SignViewController ()

@end

@implementation SignViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initValue {
}

- (void)initLayout {
    [self landscapeViewController];
    
    [_drawV applyRoundBorder:4.0 color:UIColorFromRGB(0xB5B5B5) width:0.4];
    [_resetBtn applyRoundBorder:4.0];
    [_cancelBtn applyRoundBorder:4.0];
    [_confirmBtn applyRoundBorder:4.0];
    
    _nameLb.text = [NSString stringWithFormat:@"(%@ 고객님)", _custName];
}

#pragma mark - Action Event
- (IBAction)pressedReset:(id)sender {
    [_drawV.drawPath removeAllPoints];
    [_drawV setNeedsDisplay];
}

- (IBAction)pressedCancel:(id)sender {
    [self closeViewController];
}

- (IBAction)pressedConfirm:(id)sender {
    if([_drawV.drawPath isEmpty]) {
        [CommonUtil showToastMessage:kDescSignNeed];
    } else {
        [_delegate receiveImageData:[_drawV imageCaptureView]];
        [self closeViewController];
    }
}

@end
