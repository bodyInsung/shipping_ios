//
//  SignPopupViewController.h
//  shipping
//
//  Created by ios on 2018. 4. 4..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "SignViewController.h"

@protocol SignPopupViewDelegate
- (void)completeSign:(UIImage *)image data:(CodeResultData *)data memo:(NSString *)memo;
@end

@interface SignPopupViewController : BasePopupViewController <SignViewControllerDelegate> {
    NSMutableArray                  *selectedDatas;
    NSMutableArray                  *relationDatas;
    NSInteger                       indexOfRelation;
}

@property (weak) id<SignPopupViewDelegate> delegate;
@property (strong, nonatomic) NSString *custName;
@property (strong, nonatomic) NSMutableArray *datas;
@property (weak, nonatomic) IBOutlet UIView *contentV;
@property (weak, nonatomic) IBOutlet UIScrollView *contentSV;
@property (weak, nonatomic) IBOutlet UITableView *listTV;
@property (weak, nonatomic) IBOutlet UIButton *relationBtn;
@property (weak, nonatomic) IBOutlet CTTextField *relationTF;
@property (weak, nonatomic) IBOutlet CTTextField *memoTF;
@property (weak, nonatomic) IBOutlet UIImageView *signIV;
@property (weak, nonatomic) IBOutlet UIButton *signBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIView *signContentV;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *signHeightLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *signBottomLC;
@end
