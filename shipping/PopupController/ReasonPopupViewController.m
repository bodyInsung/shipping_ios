//
//  ReasonPopupViewController.m
//  shipping
//
//  Created by ios on 2018. 4. 19..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "ReasonPopupViewController.h"

@interface ReasonPopupViewController ()

@end

@implementation ReasonPopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestCode:PROMISE_IMPOSSIBLE_CODE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initLayout {
    [_contentV applyRoundBorder:4.0];
    [_confirmBtn applyRoundBorder:4.0];
    
    [_inputTF setAccessoryView];
}

- (void)requestCode:(NSString *)code {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:code forKey:@"code"];
    [ApiManager requestToPost:SEARCH_CODE_URL parameters:parameters success:^(id responseObject) {
        resultData = (NSArray<CodeResultData> *)[CodeResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        [self restoreResultData];
        [LoadingView hideLoadingViewToDelay];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)restoreResultData {
    restoreData = [CommonObject codeNamesFromData:resultData];
    [_inputBtn setTitle:restoreData[0] forState:UIControlStateNormal];
    _inputTF.text = [NSString stringWithFormat:@"[%@] ", restoreData[0]];
}

- (void)requestInput {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:_shippingSeq forKey:@"m_shippingseq"];
    NSString *inputCode = [CommonObject codeFromData:resultData codeName:_inputBtn.currentTitle];
    [parameters setValue:inputCode forKey:@"m_promisefailcd"];
    [parameters setValue:_inputTF.text forKey:@"m_promisefailps"];
    [parameters setValue:@"N" forKey:@"m_promise"];
    [ApiManager requestToPost:SHIPPING_MODIFY_URL parameters:parameters success:^(id responseObject) {
        [_delegate reloadViewController:kDescModifySuccess];
        [self closeViewController];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

#pragma mark - Action Event
- (IBAction)pressedClose:(id)sender {
    [self closeViewController];
}

- (IBAction)pressedInput:(UIButton *)sender {
    [CommonUtil showActionSheet:restoreData target:self selected:^(NSInteger index) {
        [sender setTitle:restoreData[index] forState:UIControlStateNormal];
        _inputTF.text = [NSString stringWithFormat:@"[%@]", restoreData[index]];
    }];
}

- (IBAction)pressedConfirm:(id)sender {
    [self requestInput];
}
@end
