//
//  ReasonPopupViewController.h
//  shipping
//
//  Created by ios on 2018. 4. 19..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BasePopupViewController.h"

@protocol ReasonPopupViewDelegate
- (void)reloadViewController:(NSString *)msg;
@end

@interface ReasonPopupViewController : BasePopupViewController {
    NSArray<CodeResultData>     *resultData;
    NSArray                     *restoreData;
}

@property (weak) id<ReasonPopupViewDelegate> delegate;
@property (strong, nonatomic) NSString *shippingSeq;
@property (weak, nonatomic) IBOutlet UIView *contentV;
@property (weak, nonatomic) IBOutlet UIButton *inputBtn;
@property (weak, nonatomic) IBOutlet CTTextField *inputTF;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@end
