//
//  ImageViewController.h
//  shipping
//
//  Created by ios on 2018. 4. 10..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BasePopupViewController.h"

@interface ImageViewController : BasePopupViewController

@property (strong, nonatomic) NSString *imageUrl;
@property (weak, nonatomic) IBOutlet UIView *contentV;
@property (weak, nonatomic) IBOutlet UIImageView *contentIV;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@end
