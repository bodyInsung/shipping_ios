//
//  DashLineView.m
//  shipping
//
//  Created by ios on 2018. 4. 5..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "DashLineView.h"

@implementation DashLineView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    UIBezierPath *linePath = [UIBezierPath bezierPath];
    if(self.bounds.size.height < self.bounds.size.width) {
        [linePath moveToPoint:CGPointMake(0, self.bounds.size.height/2)];
        [linePath addLineToPoint:CGPointMake(self.bounds.size.width, self.bounds.size.height/2)];
    } else if(self.bounds.size.height > self.bounds.size.width) {
        [linePath moveToPoint:CGPointMake(self.bounds.size.width/2, 0)];
        [linePath addLineToPoint:CGPointMake(self.bounds.size.width/2, self.bounds.size.height)];
    }
    
    CGFloat dashPattern[] = {5, 2};
    [linePath setLineDash:dashPattern count:2 phase:0];
    linePath.lineWidth = 0.4;
    [UIColorFromRGB(0xB5B5B5) setStroke];
    [linePath stroke];
}
@end
