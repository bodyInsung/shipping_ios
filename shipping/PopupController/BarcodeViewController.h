//
//  BarcodeViewController.h
//  shipping
//
//  Created by ios on 2018. 4. 11..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "BasePopupViewController.h"
#import "BarcodeOverlayView.h"

@protocol BarcodeViewDelegate
@optional
- (void)scanBarcode:(NSString *)code;
- (void)scanBarcode:(NSString *)code image:(UIImage *)image;
@end
@interface BarcodeViewController : BasePopupViewController

@property (weak) id<BarcodeViewDelegate> delegate;
@property (strong, nonatomic) MTBBarcodeScanner *scanner;
@property (weak, nonatomic) IBOutlet BarcodeOverlayView *overlayV;
@property (weak, nonatomic) IBOutlet UIView *scanV;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (assign) BOOL isUseImage;

@end
