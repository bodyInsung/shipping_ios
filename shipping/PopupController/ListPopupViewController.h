//
//  ListPopupViewController.h
//  shipping
//
//  Created by ios on 2018. 4. 2..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

typedef enum {
    kListPopupTypeDeliveryman,
    kListPopupTypeCar,
    kListPopupTypeProduct,
    kListPopupTypeWarehouse,
    kListPopupTypeWarehousingDeliveryman,
    kListPopupTypeStock
} ListPopupType;

@protocol ListPopupViewDelegate
- (void)selectedItem:(JSONModel *)data type:(ListPopupType)type;
@end

@interface ListPopupViewController : BasePopupViewController {
    NSMutableArray          *listDatas;
}

@property (weak) id<ListPopupViewDelegate> delegate;
@property (assign) ListPopupType popupType;
@property (assign) BOOL isSearch;
@property (strong, nonatomic) NSMutableArray *datas;
@property (weak, nonatomic) IBOutlet UIView *contentV;
@property (weak, nonatomic) IBOutlet UILabel *titleLb;
@property (weak, nonatomic) IBOutlet UIView *searchV;
@property (weak, nonatomic) IBOutlet CTTextField *searchTF;
@property (weak, nonatomic) IBOutlet UITableView *listTbV;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *searchHeightLC;
@end
