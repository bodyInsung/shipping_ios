//
//  SignPopupViewController.m
//  shipping
//
//  Created by ios on 2018. 4. 4..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import "SignPopupViewController.h"
#import "CheckButtonTableViewCell.h"

static NSString *kListCellIdentifier = @"ListCell";

@interface SignPopupViewController ()

@end

@implementation SignPopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestRelation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:STORYBOARD_SIGN_SEGUE]) {
        SignViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.custName = _custName;
    }
}

- (void)initValue {
    CodeResultData *data = [[CodeResultData alloc] init];
    data.desc = kDescUseProduct;
    [_datas addObject:data];
    
    selectedDatas = [NSMutableArray array];
    [self setAllSelectedDatas:NO];
}

- (void)initLayout {
    [self setBaseSV:_contentSV];
    [self setBaseV:_contentV];
    
    [_contentV applyRoundBorder:4.0];
    [_signBtn applyRoundBorder:4.0];
    [_confirmBtn applyRoundBorder:4.0];
    [_memoTF setAccessoryView];
    [_relationTF setAccessoryView];
    [_signContentV applyRoundBorder:4.0 color:UIColorFromRGB(0xB5B5B5) width:0.4];
    
    UINib *cellNib = [UINib nibWithNibName:@"CheckButtonTableViewCell" bundle:nil];
    [_listTV registerNib:cellNib forCellReuseIdentifier:kListCellIdentifier];
    
    _signBottomLC.active = NO;
    _signHeightLC.constant = 0;
}

- (void)requestRelation {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:RELATION_CODE forKey:@"code"];
    [ApiManager requestToPost:SEARCH_CODE_URL parameters:parameters success:^(id responseObject) {
        relationDatas = [CodeResultData arrayOfModelsFromDictionaries:responseObject[API_RESPONSE_KEY] error:nil];
        [self addEtcData];
        [self setRelationType:0];
        [_listTV reloadData];
        [LoadingView hideLoadingView];
    } failure:^(NSString *errorMsg) {
        NSLog(@"error : %@", errorMsg);
        [LoadingView hideLoadingView];
    }];
}

- (void)addEtcData {
    CodeResultData *data = [[CodeResultData alloc] init];
    data.detCdNm = @"기타";
    data.detCd = @"99";
    [relationDatas addObject:data];
}

- (void)setSelectedDatas:(NSInteger)index {
    NSString* selectedData = selectedDatas[index];
    if(index == selectedDatas.count-1) {
        [self setAllSelectedDatas:![selectedData boolValue]];
    } else {
        [selectedDatas replaceObjectAtIndex:index withObject:[selectedData boolValue] ? @"N" : @"Y"];
    }
    [_listTV reloadData];
}

- (void)setAllSelectedDatas:(BOOL)isSelected {
    [selectedDatas removeAllObjects];
    for(int i = 0; i < _datas.count+1; i++) {
        [selectedDatas addObject:isSelected ? @"Y" : @"N"];
    }
}

- (NSInteger)unselectedIndex {
    for(int i = 0; i < selectedDatas.count; i++) {
        if(![selectedDatas[i] boolValue]) {
            return i;
        }
    }
    return NSNotFound;
}

- (void)setRelationType:(NSInteger)index {
    indexOfRelation = index;
    CodeResultData *data = relationDatas[index];
    _relationTF.hidden = index < relationDatas.count-1;
    [_relationBtn setTitle:data.detCdNm forState:UIControlStateNormal];
}

#pragma mark - Action Event
- (IBAction)pressedClose:(UIButton *)sender {
    [self closeViewController];
}

- (IBAction)pressedRelation:(UIButton *)sender {
    NSArray *restoreDatas = [CommonObject codeNamesFromData:relationDatas];
    [CommonUtil showActionSheet:restoreDatas target:self selected:^(NSInteger index) {
        [self setRelationType:index];
    }];
}

- (IBAction)pressedSign:(id)sender {
    [self openViewController:STORYBOARD_SIGN_SEGUE sender:self];
}

- (IBAction)pressedConfirm:(id)sender {
    NSInteger unselectedIndex = [self unselectedIndex];
    if(unselectedIndex < _datas.count) {
        CodeResultData *codeData = _datas[unselectedIndex];
        NSString *decription = [NSString stringWithFormat:@"%@ 항목을 확인 해주세요", codeData.desc];
        [CommonUtil showToastMessage:decription];
    } else if(_signIV.image == nil) {
        [CommonUtil showToastMessage:kDescSignNeed];
    } else {
        if(indexOfRelation == relationDatas.count - 1) {
            CodeResultData *data = relationDatas.lastObject;
            data.detCdNm = _relationTF.text;
        }
        [_delegate completeSign:_signIV.image data:relationDatas[indexOfRelation] memo:_memoTF.text];
        [self closeViewController];
    }
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _datas != nil ? selectedDatas.count : 0 ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CheckButtonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kListCellIdentifier];
    NSString* isSelected = selectedDatas[indexPath.row];
    if(indexPath.row < _datas.count) {
        [cell setCellFromData:_datas[indexPath.row] isSelected:[isSelected boolValue]];
    } else {
        [cell setLastCell:[isSelected boolValue]];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self setSelectedDatas:indexPath.row];
}

#pragma mark - SignViewControllerDelegate
- (void)receiveImageData:(UIImage *)image {
    CGFloat ratio = image.size.height / image.size.width;
    _signHeightLC.constant = _signIV.bounds.size.width * ratio;
    _signBottomLC.active = YES;
    _signIV.image = image;
}
@end
