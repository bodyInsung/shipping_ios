//
//  BarcodeOverlayView.h
//  shipping
//
//  Created by ios on 2018. 4. 11..
//  Copyright © 2018년 bodyfiend. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BarcodeOverlayView : UIView {
    CAShapeLayer            *centerLineLayer;
}

@property (assign) CGRect scanFrame;
@end
